MagickD Contributing Guide
==========================

Thank you for your interest in contributing to MagickD!

The goal for MagickD is to provide both low-level bindings
(`graphicsmagick_c`) and a high-level interface (`magickd`) to the 
[GraphicsMagick] library.  Currently, `graphicsmagick_c` is a complete
binding to GraphicsMagick version 1.3.0, meanwhile `magickd` is still
in the early stages of development.

[GraphicsMagick]: http://www.graphicsmagick.org


These are the main links that you'll need for contributing on MagickD:

* [Repository](https://codeberg.org/supercell/magickd)
* [Issue Tracker](https://codeberg.org/supercell/magickd/issues)



Where to help
-------------

> *I can't fly this ship alone.*

If you're computer runs macOS or Windows, then making sure MagickD works
on your OS would be of immense help.  Otherwise, simply using the
library and reporting any bugs that you encounter is also incredibly
helpful.

Whist I've been trying to improve the documentation, you could also help
by fleshing out areas that might still be a bit terse.



How to help
-----------

### Issue Reporting

> *Make sure you search existing issues so you don't create duplicates!
> If you do create a duplicate issue, I **will** send you a bill for
> [1 Million Dollars] in the mail.*

If you want to report a bug, first search [the issue tracker] for
existing issues that describe your issues.

Can't find an existing issue? Then please create one! The ideal format
currently is the following (copy, paste, and edit!)

```md
**Description**

`graphicsmagick_c` examples doesn't compile on Windows.

The bash scripts don't work on Windows due to $reason.

I tried doing X, Y, Z but they didn't work either.

**Version Information**

* Operating System: Windows 10
* GraphicsMagick: 1.3.38
* MagickD: 0.0.0 ;)

**Stack Trace**

    object.Exception@onlineapp.d(5): Any one reading this?
    ----------------
    ./onlineapp.d:5 void onlineapp.throwException(immutable(char)[]) [0x55d8f8943def]
    ./onlineapp.d:10 void onlineapp.func() [0x55d8f8943e0b]
    ./onlineapp.d:14 _Dmain [0x55d8f8943e18]
```

If you're including a stack trace or snippets of code, please use the
Markdown code block syntax. While I ask that you provide as much
information as you can, don't worry too much much as I'll likely ask for
more details anyway.

[the issue tracker]: https://codeberg.org/supercell/magickd/issues
[1 Million Dollars]: https://www.youtube.com/watch?v=cKKHSAE1gIs

### Environment Setup

> *After you've started your playlist of Amazarashi; procured some snacks
> and water...*

Make sure you've installed a [D Compiler] and [dub] (note that DMD comes
with dub).  You should also install GraphicsMagick to check your
changes: http://www.graphicsmagick.org/README.html.

From there, you should fork the repository on Codeberg, *clone your
fork*, and get to work!

[D Compiler]: https://dlang.org/download.html
[dub]: https://dub.pm/

### Code Style

> *"Did you know you need to wait 24 hours before reporting a missing
> person?" That's bullshit. You needn't. Please pay attention.*

This project follows the [D Style] conventions, with a couple of
differences:

* Braces are [K&R style].
* Indentation is 3 spaces.

You should be able to run [dfmt] to fix any styling issues.

[dfmt]: https://github.com/dlang-community/dfmt
[D Style]: https://dlang.org/dstyle.html
[K&R style]: https://en.wikipedia.org/wiki/Indentation_style#K&R_style


### Testing your changes

> *beep...boop...beep... \*computer explodes\**

Unfortunately there are no unit tests as of yet, so I'm relying on you,
the committer, to try your changes through the provided examples &mdash;
creating a new example if need be.

When building the examples, make sure you specify which Quantum Depth
your GraphicsMagick installation uses (Q8, Q16, or Q32).  You can find
this out by running `gm version | head -n1`.

### Sending pull requests

> *"clone your fork"? What about my spoon?*

All development happens on the [Codeberg repository], this includes
issue tracking.

For the moment, the only real rules for sending a pull request is that
you're targeting the `master` branch (this is where all development
happens), and that your not creating excessive commits.

If your changes are made in relation to an issue, please reference this
in extended commit message.  For example:

```
[examples] Migrate convert.d to new API

See Also: https://codeberg.org/supercell/issues/6
```


[Codeberg repository]: https://codeberg.org/supercell/magickd

Parting Words
-------------

> *It's the nature of things that matters, not it's form.*

If there are minor issues with your code (e.g. style, placement,
comments, etc.), these will be fixed by myself.  I'd rather you solve
the issue or implement your feature than worry too much about how to
name a variable.
