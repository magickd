import std.file : exists, remove;
import std.format : format;

import magickd;

unittest
{
   // Test reading an image
   initializeMagick(null);

   MagickWand wand = MagickWand.create();
   wand.readImage("unittest/dman.png");
   assert(wand.width == 160, "dman.png width not read as 160px");
   assert(wand.height == 310, "dman.png height not read as 310px");
}

unittest
{
   // Test pinging an image
   initializeMagick(null);

   MagickWand wand = MagickWand.create();
   wand.pingImage("unittest/dman.png");
   assert(wand.width == 160, "dman.png (ping) width not read as 160px");
   assert(wand.height == 310, "dman.png (ping) height not read as 310px");
   assert(wand.currentFilename == "unittest/dman.png",
      "dman.png currentFilename is not unittest/dman.png");
   assert(wand.format == "PNG", "dman.png (ping) format is not PNG");
}

unittest
{
   // Test resizing and writing an image
   enum outputFilename = "unittest/" ~ __FUNCTION__ ~ ".png";

   initializeMagick(null);

   MagickWand wand = MagickWand.create();
   wand.readImage("unittest/dman.png");

   size_t newWidth = wand.width / 2;
   size_t newHeight = wand.height / 2;

   wand.resizeImage(newWidth, newHeight, FilterType.lanczos, 0);
   assert(wand.width == newWidth,
      format!"%d does not equal %d"(wand.width, newWidth));
   assert(wand.height == newHeight,
      format!"%d does not equal %d"(wand.height, newHeight));

   wand.writeImage(outputFilename);
   assert(exists(outputFilename), "Failed to write resized image.");
   remove(outputFilename);
}

unittest
{
   // Convert form one format to another.

   // Deliberately omit the extension.
   enum outputFilename = "unittest/" ~ __FUNCTION__;

   initializeMagick(null);

   MagickWand wand = MagickWand.create();
   wand.readImage("unittest/dman.png");
   wand.format = "JPEG";
   wand.writeImage(outputFilename);

   MagickWand jpgWand = MagickWand.create();
   jpgWand.readImage(outputFilename);
   assert(jpgWand.format == "JPEG");

   if (exists(outputFilename)) {
      remove(outputFilename);
   }
}
