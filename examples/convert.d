/*
  File:           examples/convert.d
  Contains:       An example program which will 'convert' an input to an output.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
import std.path : extension;
import std.stdio;

import magickd;

int main(string[] args) {
   if (args.length != 3) {
      stderr.writefln("Usage: %s <infile> <outfile>", args[0]);
      return 1;
   }

   initializeMagick(null);

   MagickWand wand = MagickWand.create();
   wand.readImage(args[1]);
   if (null !is args[2].extension) {
      wand.setImageFormat(args[2].extension[1..$]);
   }

   wand.blurImage(6, 3);

   wand.writeImage(args[2]);
   writefln("Equivalent to:\n   gm convert %s -blur 6x3 %s", args[1], args[2]);

   return 0;
}
