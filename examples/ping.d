/*
  File:           examples/ping.d
  Contains:       An example program which will 'ping' each input argument using magickd.
  Copyright:      (C) 2021, 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
import std.stdio;

import magickd;

/*
 * This example will read files provided via "args"
 * and print each of their WIDTHxHEIGHT to stdout.
 *
 * In the future it should print out all information.
 */

int main(string[] args)
{

   if (args.length < 2) {
      stderr.writefln("usage: %s in_file...", args[0]);
		return 1;
	}

   initializeMagick(null);

	foreach(arg; args[1 .. $]) {
		writeln("-------");

      // NOTE: MagickWand is a struct. To properly instantiate, call
      // MagickWand.create() or MagickWand.createFromWand()
      MagickWand wand = MagickWand.create();
      wand.pingImage(arg);

      // The filename of a the current image in the image sequence.
      // I.E. Since we're reading only one image, that's the input.
      writeln("Current filename : ", wand.currentFilename);
      writeln("          Format : ", wand.format);
      if (wand.format == "GIF") {
         writefln(" 1st Frame Delay : %d hundredths of a second", wand.delay);
      }
      writeln("          Height : ", wand.height);
      writeln("           Width : ", wand.width);
		writeln("-------");
	}

	return 0;
}
