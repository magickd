#!/bin/bash
# -------------------------------------------------------------------------------------------------
#
# Permission to use, copy, modify, and/or distribute this file for any purpose
# with or without fee is hereby granted.  This file is offered as-is, without
# any warranty.
#
# This file compiles all the examples in this directory using the specified
# compiler, either using the shared GraphicsMagick library, or the static one.
#
# usage: build.sh    [--gdc] [--ldc] [--dmd] [--debug] [--shared]
#        build.sh    --clean
#
# options:
#     --gdc          Compile all examples using GDC (https://wiki.dlang.org/GDC)
#     --ldc          Compile all examples using LDC (https://wiki.dlang.org/LDC)
#     --dmd          Compile all examples using DMD (https://wiki.dlang.org/DMD)
#
#     --debug        Compile with graphicsmagick_c debug messages.
#     --shared       Compile using the shared GraphicsMagick library.
#
#     --clean        Clean any built executables
# -------------------------------------------------------------------------------------------------
set -e

FALSE=0
TRUE=1

which gm > /dev/null || {
    printf "error: could not find 'gm' executable\n" >&2
    printf "       is graphicsmagick installed?\n"
    exit 1
}

QDEPTH="$(gm version | head -n1 | cut -d' ' -f4)"

# This doesn't always work correctly. For example, Devuan Chimaera (4)
# has a snapshot version 1.4, which is not an available version.
GM_RAW_VERSION="$(gm version | head -n1 | cut -d' ' -f2)"
GM_EDT_VERSION="$(gm version | head -n1 | cut -d' ' -f2 | sed 's/\.//g')"

GMAGICK_SOURCES="$(find ../graphicsmagick_c/src -type f -regex '.*\.d')"
MD_SOURCES="$(find ../source -maxdepth 2 -type f -regex '.*\.d')"

case "$QDEPTH" in
    Q8 | Q16 | Q32) ;; # no-op
    *) printf "error: unsupported Quantum Depth '%s'.\n" "$QDEPTH" >&2 ; exit 1 ;;
esac

GDC=$FALSE
LDC=$FALSE
DMD=$FALSE
SHARED=$FALSE
DEBUG=$FALSE
OS_TYPE="$(uname -s)"

for arg in "$@"
do
    case "$arg" in
        --gdc) GDC=$TRUE ;;
        --ldc) LDC=$TRUE ;;
        --dmd) DMD=$TRUE ;;
        --shared) SHARED=$TRUE ;;
        --debug) DEBUG=$TRUE ;;
        --clean) set -x; rm -f *_gdc *_ldc *_dmd *.o ; exit 0 ;;
        *) printf "notice: unsupported option '$arg'...\n" >&2 ;;
    esac
done


if [ $GDC -eq $FALSE ] && [ $LDC -eq $FALSE ] && [ $DMD -eq $FALSE ]
then
    printf "error: no compiler selected...\n" >&2
    printf "usage: %s [--gdc] [--ldc] [--dmd] [--debug] [--shared]\n" $0
    printf "       %s --clean\n" $0
    exit 1
fi

printf "Compiling magickd version %s with %s...\n" "$GM_RAW_VERSION" "$QDEPTH"

if [ $GDC -eq $TRUE ]
then
    printf "...for GDC..."
    DFLAGS="-g -I../source -fversion=$QDEPTH -fversion=GMagick_${GM_EDT_VERSION}"

    if [ $DEBUG  -eq $TRUE ]; then DFLAGS="$DFLAGS -fdebug=graphicsmagick_c -fdebug=magickd"; fi

    if [ $SHARED -eq $TRUE ]
    then
    	DFLAGS="$DFLAGS -fversion=GMagick_Dynamic"
    else
    	DFLAGS="$DFLAGS -fversion=GMagick_Static"
    fi

    for dfile in *.d
    do
        fname="${dfile%.*}"
        printf "$fname..."
        gdc $DFLAGS -o "${fname}_gdc" "$dfile" $GMAGICK_SOURCES $MD_SOURCES \
        	-lGraphicsMagick -lGraphicsMagickWand -ldl
    done
    printf "\n"
fi

if [ $LDC -eq $TRUE ]
then

    if [ "Darwin" = "$OS_TYPE" ]
    then
        LDFLAGS="$LDFLAGS -L-L/opt/local/lib"
    fi

    printf "...for LDC..."
    DFLAGS="$DFLAGS --d-version=$QDEPTH --d-version=GMagick_${GM_EDT_VERSION}"

    if [ $DEBUG  -eq $TRUE ]; then DFLAGS="$DFLAGS -g -d-debug=graphicsmagick_c -d-debug=magickd"; fi
    if [ $SHARED -eq $TRUE ]
    then
        DFLAGS="$DFLAGS --d-version=GMagick_Dynamic"
    else
        DFLAGS="$DFLAGS --d-version=GMagick_Static"
    fi

    for dfile in *.d
    do
        fname="${dfile%.*}"
        printf "$fname..."
        ldc2 $DFLAGS -of="${fname}_ldc" "$dfile" $GMAGICK_SOURCES $MD_SOURCES \
            $LDFLAGS -L-lGraphicsMagick -L-lGraphicsMagickWand -L-ldl
    done
    printf "\n"
fi

if [ $DMD -eq $TRUE ]
then

    if [ "Darwin" = "$OS_TYPE" ]
    then
        LDFLAGS="$LDFLAGS -L-L/opt/local/lib"
    fi

    printf "...for DMD..."
    DFLAGS="-version=$QDEPTH -version=GMagick_${GM_EDT_VERSION}"

    if [ $DEBUG  -eq $TRUE ]; then DFLAGS="$DFLAGS -g -debug -debug=graphicsmagick_c"; fi
    if [ $SHARED -eq $TRUE ]
    then
        DFLAGS="$DFLAGS -version=GMagick_Dynamic"
    else
        DFLAGS="$DFLAGS -version=GMagick_Static"
    fi

    for dfile in *.d
    do
        fname="${dfile%.*}"
        printf "$fname..."
        dmd $DFLAGS -of="${fname}_dmd" "$dfile" $GMAGICK_SOURCES $MD_SOURCES \
            $LDFLAGS -L-lGraphicsMagick -L-lGraphicsMagickWand -L-ldl
    done
    printf "\n"
fi
