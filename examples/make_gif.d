/*
  File:           examples/make_gif.d
  Contains:       An example program which takes input images and produces a GIF.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
import std.stdio;

import magickd;

int main(string[] args)
{
   initializeMagick(null);

   // At least: make_gif input1 input2 output
   if (args.length < 4) {
      writefln("usage: %s <image1> <image2> [...<imageN>] <outImage>", args[0]);
      return 1;
   }

   MagickWand wand = MagickWand.create();

   foreach(arg; args[1..$-1]) {
      wand.readImage(arg);

      // EDIT:
      //    This is the section that controls each frame of the animation.
      //    Make any necessary changes here.
      wand.delay = 12; // hundredths of a second (centisecond)
      wand.imageDispose = DisposeType.background;
      wand.imageIterations = 0;

      writefln("Read %s...", wand.currentFilename);
   }

   wand.filename = args[$ - 1];
   writeln("Saving file to ", wand.filename, "...");
   wand.writeImages(wand.filename, true);

   writeln("Equivalent to:");
   write("   gm convert -loop 0 -delay 12 -dispose Background \\\n");
   foreach(arg; args[1..$-1]) {
      writef("     %s \\\n", arg);
   }
   writef("     %s\n", args[$-1]);
   return 0;
}
