/*
  File:           examples/make_gif.d
  Contains:       An example program which takes an input GIF and breaks it
                  in to separate images.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module app;

import std.file : exists, mkdir;
import std.format : format;
import std.path : baseName, buildPath, dirSeparator, stripExtension;
import std.stdio : stderr, writefln;

import magickd;

void extract(ref MagickWand wand, string dirname)
{
   string frameFilename = format!"%s_%05d.gif"(dirname, wand.getImageIndex());
   writefln("%s:", frameFilename);
   if (wand.imageDispose < DisposeType.max && wand.imageDispose >= DisposeType.min) {
      writefln("  Dispose: %s", wand.imageDispose);
   } else {
      writefln("  Dispose:");
   }
   writefln("  Delay: %s hundredths of a second", wand.delay);
   wand.writeImage(buildPath(dirname, frameFilename));
}

void extractForward(ref MagickWand wand, string dirname)
{
   stderr.writefln("(NOTE: Images being written forwards).");

   // Extract first image so we don't break the loop early.
   extract(wand, dirname);
   while (wand.hasNextImage()) {
      wand.nextImage();
      extract(wand, dirname);
   }
}

void extractReverse(ref MagickWand wand, string dirname)
{
   stderr.writefln("(NOTE: Images being written in reverse).");

   // Extract first image so we don't break the loop early.
   extract(wand, dirname);
   while (wand.hasPreviousImage()) {
      wand.previousImage();
      extract(wand, dirname);
   }
}

int main(string[] args)
{
   if (args.length != 2) {
      stderr.writefln("usage: %s <input.gif>", args[0]);
      return 1;
   }

   initializeMagick(null);

   MagickWand gif = MagickWand.create();
   gif.readImage(args[1]);

   string filename = baseName(args[1]).stripExtension();
   if (!exists(filename)) {
      mkdir(filename);
   }

   if (gif.format != "GIF") {
      stderr.writefln("ERR: Input image format not GIF: %s", gif.format);
      return 1;
   }

   writefln("Extracting %d images to .%s%s%s", gif.getNumberImages(),
      dirSeparator, filename, dirSeparator);

   if (gif.hasPreviousImage()) {
      extractReverse(gif, filename);
   } else if (gif.hasNextImage()) {
      extractForward(gif, filename);
   } else {
      extract(gif, filename);
   }

   return 0;
}
