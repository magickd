
Archive Notice
==============

As of 2025-02-25, MagickD is no longer being maintained.


MagickD
=======

The `magickd` package provides a safe higher-level wrapper for the
`GraphicsMagick` library.  For more information about `GraphicsMagick`, see the
[official website].

[official website]: http://www.graphicsmagick.org


Dependencies
------------

In order to use the `magickd` package, you must have `libGraphicsMagick` library
installed where it can be found by `pkg-config`.

You will also need a D compiler that supports D [2.076.0].

[2.076.0]: https://dlang.org/changelog/2.076.0.html


Configuration
--------------

There are multiple ways you can configure the `magickd` package, but first, add
it as a dub dependency:

For `dub.sdl`:
```sdl
dependency "magickd" repository="git+https://codeberg.org/supercell/magickd" \
   version="6672d8200e2f1ead1dc4c9169d89a37a827433ac"
```

For `dub.json`:
```json
"dependencies": {
   "magickd": {
      "repository": "git+https://codeberg.org/supercell/magickd",
         "version": "6672d8200e2f1ead1dc4c9169d89a37a827433ac"
   }
}
```

With that done, you're good to go!

**NOTE:** ~~At some point I'll try get this package on http://dub.pm, for now
though, just use the latest git hash.~~ This package won't be on The D package
registry so long as they only support GitHub/GitLab/Bitbucket. In the mean time,
use the latest git hash.

### Dynamic or Static Bindings

By default, `magickd` will build the "dynamic" version, which will load the
GraphicsMagick libraries at runtime and bind the C symbol names to D symbols.
This process happens automatically when you import the `magickd` package.
(**NOTE**: If you're not using [dub], then you'll need to specify the
`GMagick_Dynamic` version.)

Alternatively, you can build `magickd` to use a "static" binding, which requires
linking against the GraphicsMagick library when compiling. (**NOTE**: If you're
not using [dub], then you'll need to specify the `GMagick_Static` version.)

[dub]: https://dub.pm

#### macOS - MacPorts - Dynamic Bindings

A quick heads up if you've installed GraphicsMagick via [MacPorts], you will
need to make sure that the `LD_LIBRARY_PATH` environment variable will include
the directory which holds `GraphicsMagick.dylib` and `GraphicsMagickWand.dylib`.
By default, the directory is `/opt/local/lib`.  For example:

```sh
$ cd examples/
$ ./dmd.sh --shared
$ LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/local/lib" \
  ./dmd/ping_dmd /path/to/my/picture.png
```

[MacPorts]: https://www.macports.org/

License
-------

`magickd` is licensed under the Expat license, you should have received a copy
in a file named `LICENSE`.
If not, see <https://codeberg.org/supercell/magickd/src/branch/master/LICENSE>.
