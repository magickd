/*
  File:           magickd/drawing_wand.d
  Contains:       An OOP-Style wrapper around the C DrawingWand structure.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.drawing_wand;

import graphicsmagick_c.wand.drawing_wand;
import graphicsmagick_c.wand.drawing_wand : cDrawingWand = DrawingWand;

import magickd.pixel_wand : PixelWand;

struct DrawingWand {
package(magickd):

   cDrawingWand* ptr;

   this(cDrawingWand* dwand)
   {
      ptr = dwand;
   }

   public ~this()
   {
      if (null !is ptr) {
         DestroyDrawingWand(ptr);
      }
   }

   public static DrawingWand create()
   {
      return DrawingWand(NewDrawingWand());
   }

   ///
   /// Set the fill color to be used when drawing filled objects.
   ///
   /// Params:
   ///   fill = The fill wand.
   ///
   @property public void fillColor(const ref PixelWand fill)
   in {
      assert(null !is this.ptr, "Attempting to use a DrawingWand that hasn't been created.");
      assert(null !is fill.ptr, "Attempting to use a PixelWand that hasn't beenc created.");
   }
   do {
      MagickDrawSetFillColor(this.ptr, fill.ptr);
   }

   ///
   /// Set the font size to use when annotating with text.
   ///
   /// Params:
   ///   pointSize = Text Point Size
   ///
   @property public void fontSize(double pointSize)
   in {
      assert(null !is this.ptr, "Attempting to use a DrawingWand that hasn't been created.");
   }
   do {
      MagickDrawSetFontSize(this.ptr, pointSize);
   }
}
