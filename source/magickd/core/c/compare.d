/**
 * GraphicsMagick Image Comparison Methods.
 *
 * Authors: GraphicsMagick Group
 * Copyright: &copy; 2008 GraphicsMagick Group
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 */
deprecated("Use graphicsmagick_c.magick.compare instead (remove @ 0.9)")
module magickd.core.c.compare;

@nogc @system extern (C) nothrow:

/++
 + Pixel error metrics.
 +/
alias MetricType = int;
/// Ditto
enum : int
{
  UndefinedMetric,
  MeanAbsoluteErrorMetric,
  MeanSquaredErrorMetric,
  PeakAbsoluteErrorMetric,
  PeakSignalToNoiseRatioMetric,
  RootMeanSquaredErrorMetric
}
