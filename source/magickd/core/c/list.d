/**
 * Manage image lists.
 *
 * Authors: ImageMagick Studio, GraphicsMagick Group
 * Copyright: &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2018 GraphicsMagick Group
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.list instead (remove @ 0.9)")
module magickd.core.c.list;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
    import core.stdc.config : c_ulong;
    import magickd.core.c.image;
}

@nogc @system nothrow:

/++
 + AppendImageToList() appends an image to the end of the list.
 +
 + Params:
 +   images = The image list.
 +   image = The image.
 +/
version (SHARED)
{
    __gshared MDAppendImageToList AppendImageToList;
    extern (C) alias MDAppendImageToList = void function(Image**, Image*);
}
else
{
    extern (C) void AppendImageToList(Image** images, Image* image);
}

/++
 + DestroyImageList() destroys an image list.
 +
 + There is no effect if the image pointer is null.
 +
 + Params:
 +   image = The image sequence.
 +/
version (SHARED)
{
    __gshared MDDestroyImageList DestroyImageList;
    extern (C) alias MDDestroyImageList = void function(Image* image);
}
else
{
    extern (C) void DestroyImageList(Image* image);
}

/++
 + GetImageListLength() returns the length of the list (the number of images
 + in the list).
 +
 + Params:
 +   images = The image list.
 +/
version (SHARED)
{
    __gshared MDGetImageListLength GetImageListLength;
    extern (C) alias MDGetImageListLength = c_ulong function(const(Image)* images);
}
else
{
    extern (C) c_ulong GetImageListLength(const Image* images);
}

/++
 + NewImageList() creates an empty image list.
 +
 + Returns: A newly allocated Image list.  Free it with DestroyImageList().
 +/
version (SHARED)
{
    __gshared MDNewImageList NewImageList;
    extern (C) alias MDNewImageList = Image* function();
}
else
{
    extern (C) Image* NewImageList();
}

/++
 + RemoveFirstImageFromList() removes an image from the beginning of the list.
 +
 + Params:
 +   images = The image list.
 +
 + Retruns: The removed image from the list.
 +/
version (SHARED)
{
    __gshared MDRemoveFirstImageFromList RemoveFirstImageFromList;
    extern (C) alias MDRemoveFirstImageFromList = Image* function(Image** images);
}
else
{
    extern (C) Image* RemoveFirstImageFromList(Image** images);
}

/++
 + RemoveLastImageFromList() removes the last image from the list.
 +
 + Params:
 +   images = The image list.
 +
 + Returns: The removed image from the list.
 +/
version (SHARED)
{
    __gshared MDRemoveLastImageFromList RemoveLastImageFromList;
    extern (C) alias MDRemoveLastImageFromList = Image* function(Image** image);
}
else
{
    extern (C) Image* RemoveLastImageFromList(Image** images);
}

/++
 + ReplaceImageInList() replaces an image in the list.
 +
 + Params:
 +   images = The image list.
 +   image = The image
 +/
version (SHARED)
{
    __gshared MDReplaceImageInList ReplaceImageInList;
    extern (C) alias MDReplaceImageInList = void function(Image** images, Image* image);
}
else
{
    extern (C) void ReplaceImageInList(Image** images, Image* image);
}

/++
 + ReverseImageList() reverses the image list.
 +
 + Params:
 +   images = The image list.
 +/
version (SHARED)
{
    __gshared MDReverseImageList ReverseImageList;
    extern (C) alias MDReverseImageList = void function(Image** image);
}
else
{
    extern (C) void ReverseImageList(Image** images);
}

/++
 + SpliceImageIntoList() removes `length` images from the list and replaces
 + them with the specified splice.
 +
 + Params:
 +   images = The image list.
 +   length = The length of the image list to remove.
 +   splice = Replace the removed images with this list.
 +/
version (SHARED)
{
    __gshared MDSpliceImageIntoList SpliceImageIntoList;
    extern (C) alias MDSpliceImageIntoList = void function(Image** images,
            const(c_ulong) length, Image* splice);
}
else
{
    extern (C) void SpliceImageIntoList(Image** images, const c_ulong length, Image* splice);
}

/++
 + SplitImageList() splits an image list into two lists.
 +
 + Params:
 +   images = The image list.
 +/
version (SHARED)
{
    __gshared MDSplitImageList SplitImageList;
    extern (C) alias MDSplitImageList = Image* function(Image* images);
}
else
{
    extern (C) Image* SplitImageList(Image* images);
}

/++
 + SyncNextImageInList() returns the next image in the list after the blob
 + referenced is synchronized with the current image.
 +
 + Params:
 +   images = The image list.
 +/
version (SHARED)
{
    __gshared MDSyncNextImageInList SyncNextImageInList;
    extern (C) alias MDSyncNextImageInList = Image* function(const(Image)* images);
}
else
{
    extern (C) Image* SyncNextImageInList(const(Image)* images);
}
