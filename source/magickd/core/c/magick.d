/**
 * GraphicsMagick Application Programming Interface Methods.
 *
 * Authors: E. I. du Pont de Nemours and Company, ImageMagick Studio,
 *   GraphicsMagick Group.
 * Copyright: 1991 - 1999 E. I. du Pont de Nemours and Company,
 *   &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2020 GraphicsMagick Studio
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 *
 * Macros:
 *     REF = <a href="$1.html">$1</a>
 *     TT = <tt>$1</tt>
 */
deprecated("Use graphicsmagick_c.magick.magick instead (remove @ 0.9)")
module magickd.core.c.magick;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
  import magickd.core.c.common;
  import magickd.core.c.error;
}

version(SHARED) version=MD_DOC;
version(D_Ddoc) version=MD_DOC;

version(MD_DOC) {
  import core.sys.posix.dlfcn;

  @nogc nothrow:
  /**
   * Bind a C Library symbol to a D `function` type.
   *
   * This function is exposed publically that can be used after
   * $(REF loadGraphicsMagick) if extra functions are required.
   *
   * This method is only exposed when building a $(TT SHARED)
   * version.
   *
   * ---
   * import magickd.core.c.magick;
   *
   * // "NewFunc" is just an example...
   * extern (C) @nogc nothrow __gshared void function() NewFunc;
   *
   * void* gmDynLibHandle = loadGraphicsMagick();
   * bindSymbol(gmDynLibHandle, cast(void**)&NewFunc, "NewFunc");
   * ---
   *
   * Params:
   *     handle = The dynamic library handle pointer.
   *     fn = The D `function` to bind to.
   *     name = The name of the C symbol to bind to.
   */
  @trusted void bindSymbol(void* handle, void** fn, in char* name)
  {
    void* sym = dlsym(handle, name);

    // TODO: error handling
    debug {
      import core.stdc.stdio;
      if (sym is null) {
        fprintf(stderr, "ERR %s is null\n", name);
        fprintf(stderr, "dlerror(): %s\n", dlerror());
      }
    }

    *fn = sym;
  }

  /**
   * Load the shared GraphicsMagick library.
   *
   * Returns: the shared library handle, can be used with $(REF bindSymbol).
   *
   * Bugs: Currenlty only attempts to load libGraphicsMagick.so.
   */
  @trusted void* loadGraphicsMagick()
  {
    import magickd.core.c;

    //void* handle = dlopen("libGraphicsMagick.so", RTLD_LAZY);
    void* handle = dlopen("libGraphicsMagick.so", RTLD_NOW);

    /* core.c.constitute.d */
    bindSymbol(handle, cast(void**)&PingImage, "PingImage");
    bindSymbol(handle, cast(void**)&ReadImage, "ReadImage");
    bindSymbol(handle, cast(void**)&WriteImage, "WriteImage");

    /* core.c.error.d */
    bindSymbol(handle, cast(void**)&CatchException, "CatchException");
    bindSymbol(handle, cast(void**)&DestroyExceptionInfo, "DestroyExceptionInfo");
    bindSymbol(handle, cast(void**)&GetExceptionInfo, "GetExceptionInfo");

    /* core.c.image.d */
    bindSymbol(handle, cast(void**)&AccessDefinition, "AccessDefinition");
    bindSymbol(handle, cast(void**)&AddDefinition, "AddDefinition");
    bindSymbol(handle, cast(void**)&AddDefinitions, "AddDefinitions");
    bindSymbol(handle, cast(void**)&CloneImage, "CloneImage");
    bindSymbol(handle, cast(void**)&CloneImageInfo, "CloneImageInfo");
    bindSymbol(handle, cast(void**)&DestroyImage, "DestroyImage");
    bindSymbol(handle, cast(void**)&DestroyImageInfo, "DestroyImageInfo");

    /* core.c.list.d */
    bindSymbol(handle, cast(void**)&AppendImageToList, "AppendImageToList");
    bindSymbol(handle, cast(void**)&DestroyImageList, "DestroyImageList");
    bindSymbol(handle, cast(void**)&GetImageListLength, "GetImageListLength");
    bindSymbol(handle, cast(void**)&NewImageList, "NewImageList");
    bindSymbol(handle, cast(void**)&RemoveFirstImageFromList, "RemoveFirstImageFromList");
    bindSymbol(handle, cast(void**)&RemoveLastImageFromList, "RemoveLastImageFromList");
    bindSymbol(handle, cast(void**)&ReplaceImageInList, "ReplaceImageInList");
    bindSymbol(handle, cast(void**)&ReverseImageList, "ReverseImageList");
    bindSymbol(handle, cast(void**)&SpliceImageIntoList, "SpliceImageIntoList");
    bindSymbol(handle, cast(void**)&SplitImageList, "SplitImageList");
    bindSymbol(handle, cast(void**)&SyncNextImageInList, "SyncNextImageInList");

    /* core.c.magick.d */
    bindSymbol(handle, cast(void**)&InitializeMagick, "InitializeMagick");
    bindSymbol(handle, cast(void**)&DestroyMagick, "DestroyMagick");
    bindSymbol(handle, cast(void**)&InitializeMagickEx, "InitializeMagickEx");

    /* core.c.resize.d */
    bindSymbol(handle, cast(void**)&MagnifyImage, "MagnifyImage");
    bindSymbol(handle, cast(void**)&MinifyImage, "MinifyImage");
    bindSymbol(handle, cast(void**)&ResizeImage, "ResizeImage");
    bindSymbol(handle, cast(void**)&SampleImage, "SampleImage");
    bindSymbol(handle, cast(void**)&ScaleImage, "ScaleImage");
    bindSymbol(handle, cast(void**)&ThumbnailImage, "ThumbnailImage");
    bindSymbol(handle, cast(void**)&ZoomImage, "ZoomImage");

    return handle;
  }
}

@nogc @system nothrow:

/++
 + InitializeMagick() initializes the Magick environment.
 +
 + InitializeMagick() or InitializeMagickEx() MUST be invoked by the using
 + program before making use of the other Magick functions or else the library
 + will be unusable and any usage is likely to cause a crash.
 +
 + This function should be invoked in the primary (original) thread of the
 + applications process, and before starting any OpenMP threads, as part of
 + program initialization.
 +
 + If alternate memory allocations are provided via MagickAllocFunctions()
 + then that function should be invoked before InitializeMagickEx() since the
 + memory allocation functions need to be consistent.
 +
 + Params:
 +  path = The execution path of the current Magick client (or NULL)
 +/
version (SHARED) {
  __gshared MDInitializeMagick InitializeMagick;
  extern (C) alias MDInitializeMagick = void function(const scope char* path);
} else {
  extern (C) void InitializeMagick(const scope char* path);
}

/++
 + DestroyMagick() destroys the Magick environment, releasing all allocated
 + semaphores, memory, and temporary files.
 +
 + This function should be invoked in the primary (original) thread of the
 + application's process while shutting down, and only after any threads which
 + might be using Magick functions have been terminated. Since the Magick
 + environment uses threads internally via OpenMP, it is also necessary for
 + any function calls into Magick to have already returned so that OpenMP
 + worker threads are quiesced and won't be accessing any semaphores or data
 + structures which are destroyed by this function.
 +/
version (SHARED) {
  __gshared MDDestroyMagick DestroyMagick;
  extern (C) alias MDDestroyMagick = void function();
} else {
  extern (C) void DestroyMagick();
}

/++
 + InitializeMagickEx() initializes the Magick environment, providing a bit
 + more control and visibility over initialization than the original
 + InitializeMagick().
 +
 + InitializeMagick() or InitializeMagickEx() MUST be invoked by the using
 + program before making use of the other Magick functions or else the library
 + or will be unusable and any usage is likely to cause a crash.
 +
 + This function should be invoked in the primary (original) thread of the
 + application's process, and before starting any OpenMP threads, as part of
 + program initialization.
 +
 + If alternate memory allocations are provided via MagickAllocFunctions()
 + then that function should be invoked before InitializeMagickEx() since the
 + allocation functions need to be consistent.
 +
 + Params:
 +   path = The execution path of the current Magick client (or NULL)
 +   options = Options flag tailoring initializations performed.
 +   exception = Information about initialization failure is reported here.
 +/
version (SHARED) {
  __gshared MDInitializeMagickEx InitializeMagickEx;
  extern (C) alias MDInitializeMagickEx= MagickPassFail function(const scope char* path,
                                         uint options, ExceptionInfo* exception);
} else {
   extern(C) MagickPassFail InitializeMagickEx(const scope char* path, uint options,
                                               ExceptionInfo* exception);
}
