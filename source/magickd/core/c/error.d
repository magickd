/**
 * GraphicsMagick Exception Methods.
 *
 * Authors: E. I. du Pont de Nemours and Company, ImageMagick Studio,
 *   GraphicsMagick Group.
 * Copyright: 1991 - 1999 E. I. du Pont de Nemours and Company,
 *   &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2020 GraphicsMagick Studio
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.error instead (remove @ 0.9)")
module magickd.core.c.error;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
    import core.stdc.config : c_ulong;
}

@nogc @system nothrow:

/* TODO: figure out how best to document this */

enum : int
{
    UndefinedExceptionBase = 0,
    ExceptionBase = 1,
    ResourceBase = 2,
    ResourceLimitBase = 2,
    TypeBase = 5,
    AnnotateBase = 5,
    OptionBase = 10,
    DelegateBase = 15,
    MissingDelegateBase = 20,
    CorruptImageBase = 25,
    FileOpenBase = 30,
    BlobBase = 35,
    StreamBase = 40,
    CacheBase = 45,
    CoderBase = 50,
    ModuleBase = 55,
    DrawBase = 60,
    RenderBase = 60,
    ImageBase = 65,
    WandBase = 67,
    TemporaryFileBase = 70,
    TransformBase = 75,
    XServerBase = 80,
    X11Base = 81,
    UserBase = 82,
    MonitorBase = 85,
    LocaleBase = 86,
    DeprecateBase = 87,
    RegistryBase = 90,
    ConfigureBase = 95
}
alias ExceptionBaseType = int;

enum : int
{
    UndefinedException = 0,
    EventException = 100,
    ExceptionEvent = EventException + ExceptionBase,
    ResourceEvent = EventException + ResourceBase,
    ResourceLimitEvent = EventException + ResourceLimitBase,
    TypeEvent = EventException + TypeBase,
    AnnotateEvent = EventException + AnnotateBase,
    OptionEvent = EventException + OptionBase,
    DelegateEvent = EventException + DelegateBase,
    MissingDelegateEvent = EventException + MissingDelegateBase,
    CorruptImageEvent = EventException + CorruptImageBase,
    FileOpenEvent = EventException + FileOpenBase,
    BlobEvent = EventException + BlobBase,
    StreamEvent = EventException + StreamBase,
    CacheEvent = EventException + CacheBase,
    CoderEvent = EventException + CoderBase,
    ModuleEvent = EventException + ModuleBase,
    DrawEvent = EventException + DrawBase,
    RenderEvent = EventException + RenderBase,
    ImageEvent = EventException + ImageBase,
    WandEvent = EventException + WandBase,
    TemporaryFileEvent = EventException + TemporaryFileBase,
    TransformEvent = EventException + TransformBase,
    XServerEvent = EventException + XServerBase,
    X11Event = EventException + X11Base,
    UserEvent = EventException + UserBase,
    MonitorEvent = EventException + MonitorBase,
    LocaleEvent = EventException + LocaleBase,
    DeprecateEvent = EventException + DeprecateBase,
    RegistryEvent = EventException + RegistryBase,
    ConfigureEvent = EventException + ConfigureBase,

    WarningException = 300,
    ResourceLimitWarning = 300,
    TypeWarning = 305,
    OptionWarning = 310,
    DelegateWarning = 315,
    MissingDelegateWarning = 320,
    CorruptImageWarning = 325,
    FileOpenWarning = 330,
    BlobWarning = 335,
    StreamWarning = 340,
    CacheWarning = 345,
    CoderWarning = 350,
    ModuleWarning = 355,
    DrawWarning = 360,
    ImageWarning = 365,
    XServerWarning = 380,
    MonitorWarning = 385,
    RegistryWarning = 390,
    ConfigureWarning = 395,
    ErrorException = 400,
    ResourceLimitError = 400,
    TypeError = 405,
    OptionError = 410,
    DelegateError = 415,
    MissingDelegateError = 420,
    CorruptImageError = 425,
    FileOpenError = 430,
    BlobError = 435,
    StreamError = 440,
    CacheError = 445,
    CoderError = 450,
    ModuleError = 455,
    DrawError = 460,
    ImageError = 465,
    XServerError = 480,
    MonitorError = 485,
    RegistryError = 490,
    ConfigureError = 495,
    FatalErrorException = 700,
    ResourceLimitFatalError = 700,
    TypeFatalError = 705,
    OptionFatalError = 710,
    DelegateFatalError = 715,
    MissingDelegateFatalError = 720,
    CorruptImageFatalError = 725,
    FileOpenFatalError = 730,
    BlobFatalError = 735,
    StreamFatalError = 740,
    CacheFatalError = 745,
    CoderFatalError = 750,
    ModuleFatalError = 755,
    DrawFatalError = 760,
    ImageFatalError = 765,
    XServerFatalError = 780,
    MonitorFatalError = 785,
    RegistryFatalError = 790,
    ConfigureFatalError = 795
}
alias ExceptionType = int;

/++
 + A structure that exception information is copied to
 + when used in functions.
 +/
extern(C) struct ExceptionInfo
{
    /// The severity of the exception
    ExceptionType severity;
    /// The reason the exception occured
    char* reason;
    /// The exception's description.
    char* description;
    int error_number;
    char* module_;
    char* function_;
    c_ulong line;
    c_ulong signature;
}
alias _ExceptionInfo = ExceptionInfo;

/++
 + CatchException() returns if no exceptions are found, otherwise, it reports
 + the exception as a warning, error, or fatal depending on the severity.
 +
 + Params:
 +   exception = The exception info.
 +/
version (SHARED) {
  __gshared MDCatchException CatchException;
  extern (C) alias MDCatchException = void function(ExceptionInfo* exception);
} else {
  extern(C) void CatchException(ExceptionInfo* exception);
}

/++
 + DestroyExceptionInfo() deallocates memory associated with `exception`.
 +
 + Params:
 +   exception = The exception info
 +/
version (SHARED) {
 __gshared MDDestroyExceptionInfo DestroyExceptionInfo;
 extern (C) alias MDDestroyExceptionInfo = void function(ExceptionInfo* exception);
} else {
  extern(C) void DestroyExceptionInfo(ExceptionInfo* exception);
}

/++
 + GetExceptionInfo() initializes an exception to default values.
 +
 + Params:
 +   exception = The exception info
 +/
version (SHARED) {
  __gshared MDGetExceptionInfo GetExceptionInfo;
  extern (C) alias MDGetExceptionInfo = void function(ExceptionInfo* exception);
} else {
  extern(C) void GetExceptionInfo(ExceptionInfo* exception);
}
