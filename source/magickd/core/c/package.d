/*               Copyright (C) kaerou 2021.
** Distributed under the Boost Software License, Version 1.0.
**    (See accompanying file LICENSE_1_0.txt or copy at
**          https://www.boost.org/LICENSE_1_0.txt)
*/

/**
 * GraphicsMagick Application Programming Interface declarations.
 *
 * A convenience
 * $(LINK2 https://dlang.org/spec/module.html#package-module, Package Module)
 * for the GraphicsMagick C bindings.
 *
 * Authors: kaerou
 * Copyright: &copy; 2021 kaerou
 * License: $(LINK2 http://www.boost.org/LICENSE_1_0.txt, BSL-1.0)
 */
deprecated("Use graphicsmagick_c.magick instead (remove @ 0.9)")
module magickd.core.c;

public
{
    import magickd.core.c.common;
    import magickd.core.c.compare;
    import magickd.core.c.constitute;
    import magickd.core.c.error;
    import magickd.core.c.image;
    import magickd.core.c.list;
    import magickd.core.c.magick;
    import magickd.core.c.resize;
    import magickd.core.c.timer;
    import magickd.core.c.types;
}
