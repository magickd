/**
 * Miscellaneous image methods.
 *
 * Authors: E. I. du Pont de Nemours and Company, ImageMagick Studio,
 *   GraphicsMagick Group.
 * Copyright: 1991 - 1999 E. I. du Pont de Nemours and Company,
 *   &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2019 GraphicsMagick Studio
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 *
 * Macros:
 *     REF = <a href="$1.html">$1</a>
 */
deprecated("Use graphicsmagick_c.magick.image instead (remove @ 0.9)")
module magickd.core.c.image;

// Compat because of graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
    import core.stdc.config : c_long, c_ulong;
    import core.stdc.stdio : FILE;

    import magickd.core.c.common;
    import magickd.core.c.colorspace;
    import magickd.core.c.error;
    import magickd.core.c.timer;
}

//TODO: version({Q8,Q16,Q32})
enum QuantumDepth = 16;

static if (QuantumDepth == 8)
{
    enum MaxColormapSize = 256U;
    enum MaxMap = 255U;
    enum MaxMapDepth = 8;
    enum MaxMapFloat = 255.0f;
    enum MaxMapDouble = 255.0;
    enum MaxRGB = 255U;
    enum MaxRGBFloat = 255.0f;
    enum MaxRGBDouble = 255.0;
    /* TODO: Scale functions */
    alias Quantum = ubyte;
}
else static if (QuantumDepth == 16)
{
    enum MaxColormapSize = 65_536U;
    enum MaxMap = 65_535U;
    enum MaxMapDepth = 16;
    enum MaxMapFloat = 65_535.0f;
    enum MaxMapDouble = 65_535.0;
    enum MaxRGB = 65_535U;
    enum MaxRGBFloat = 65_535.0f;
    enum MaxRGBDouble = 65_535.0;
    /* TODO: Scale functions */
    alias Quantum = ushort;
}
else static if (QuantumDepth == 32)
{
    enum MaxColormapSize = 65_536U;
    enum MaxRGB = 4_294_967_295U;
    enum MaxRGBFloat = 4_294_967_295.0f;
    enum MaxRGBDouble = 4_294_967_295.0;
    /* MaxMap defines the maximum index value for algorithms which depend on
     * lookup tables (e.g. colorspace transformations and normalizations).
     * When MaxMap is less than MaxRGB it is necessary to downscale samples to
     * fit the range of MaxMap.  The number of bits which are effectively
     * preserved depend on the size of MaxMap.  MaxMap should be a multiple of
     * 255 and no larger the MaxRGB.  Note that tables can become quite large
     * and as tables grow larger it may take more time to compute the table
     * than to process the image. */
    enum MaxMap = 65_535U;
    enum MaxMapDepth = 16;
    enum MaxMapFloat = 65_535.0f;
    enum MaxMapDouble = 65_535.0;

    static if (MaxMap == 65_535U)
    {
        /* TODO: Scale functions */
    }
    else
    {
        /* TODO: Scale functions */
    }
    alias Quantum = uint;
}
else
{
    static assert(false, "Specified value of QuantumDepth is not supported");
}

alias AlphaType = int;
enum : int
{
    UnspecifiedAlpha,
    AssociatedAlpha,
    UnassociatedAlpha
}

alias ChannelType = int;
enum : int
{
    UndefinedChannel,
    RedChannel, /* RGB Red channel */
    CyanChannel, /* CMYK Cyan channel */
    GreenChannel, /* RGB Green channel */
    MagentaChannel, /* CMYK Magenta channel */
    BlueChannel, /* RGB Blue channel */
    YellowChannel, /* CMYK Yellow channel */
    OpacityChannel, /* Opacity channel */
    BlackChannel, /* CMYK Black (K) channel */
    MatteChannel, /* Same as Opacity channel (deprecated) */
    AllChannels, /* Color channels */
    GrayChannel /* Color channels represent an intensity. */
}

/// ClassType enumeration specifies the image storage class.
alias ClassType = int;
enum : int
{
    /// Unset values
    UndefinedClass,
    /// Image is composed of pixels which represent literal color values.
    DirectClass,
    /// Image is composed of pixels which specify an index in a color palette.
    PseudoClass
}

/++
 + CompositeOperator is used to select the image composition algorithm used to
 + compose a "composite image" with an "image".
 +
 + By default, each of the "composite image" pixels are replaced by the
 + corresponding "image" tile pixel.  Specify CompositeOperator to select a
 + different algorithm.
 +
 + The image compositor requires a matte, or alpha channel in the image for
 + some operations.  This extra channel usually defines a mask which
 + represents a sort of a cookie-cutter for the image.  This is the case when
 + matte is 255 (full coverage) for pixels inside the shape, zero outside, and
 + between zero and 255 on the boundary.  For certain operations, if "image"
 + does not have a matte channel, it is initialized with `0` for any pixel
 + matching in color to pixel location `(0, 0)`, otherwise 255 (to work
 + properly borderWidth must be `0`).
 +/
alias CompositeOperator = int;
/// Ditto
enum : int
{
    /++
     + Unset value.
     +/
    UndefinedCompositeOp = 0,
    /++
     + The result is the union of the two image shapes with the "composite
     + image" obscuring the "image" in the region of overlap.
     +/
    OverCompositeOp,
    /++
     + The result is a simply "composite image" cut by the shape of "image".
     + None of the image data of "image" is included in the result.
     +/
    InCompositeOp,
    /++
     + The resulting image is "composite image" with the shape of "image" cut
     + out.
     +/
    OutCompositeOp,
    /++
     + The result is the same shape as "image", with "composite image"
     + obscuring "image" where the image shapes overlap.
     +
     + Note that this differs from `OverCompositeOp` because the portion of
     + "composite image" outside of "image"'s shape does not appear in the
     + result.
     +/
    AtopCompositeOp,
    /++
     + The result is the image data from both "composite image" and "image"
     + that is outside the overlap region.
     +
     + The overlap region will be blank.
     +/
    XorCompositeOp,
    /++
     + The result is just the sum of image data.
     +
     + Output values are cropped to 255 (no overflow).  This operation is
     + independent of the matte channels.
     +/
    PlusCompositeOp,
    /++
     + The result of `"composite image" - "image"`, with overflow cropped to
     + zero.
     +
     + The matte channel is ignored (set to 255, full coverate).
     +/
    MinusCompositeOp,
    /++
     + The result of `"composite image" + "image"`, with overflow wrapping
     + around (mod 256).
     +/
    AddCompositeOp,
    /++
     + The result of `"composite image" - "image"`, with underflow wrapping
     + around (mod 256).
     +
     + The add and subtract operators can be used to perform reversible
     + transformations.
     +/
    SubtractCompositeOp,
    /++
     + The result of `abs("composite image" - "image")`.
     +
     + This is useful for comparing two very similar images.
     +/
    DifferenceCompositeOp,
    MultiplyCompositeOp,
    /++
     + The result image shaded by "composite image".
     +/
    BumpmapCompositeOp,
    /++
     + The resulting image is "image" replaced with "composite image".
     +
     + Here the matte information is ignored.
     +/
    CopyCompositeOp,
    /++
     + The resulting image is the red layer in "image" replaced with the red
     + layer in the "composite image".
     +
     + The othe layers are copied untouched.
     +/
    CopyRedCompositeOp,
    /++
     + The resulting image is the green layer in "image" replaced with the
     + green layer in the "composite image".
     +
     + The othe layers are copied untouched.
     +/
    CopyGreenCompositeOp,
    /++
     + The resulting image is the blue layer in "image" replaced with the blue
     + layer in the "composite image".
     +
     + The othe layers are copied untouched.
     +/
    CopyBlueCompositeOp,
    /++
     + The resulting image is the matte layer in "image" replaced with the
     + matte layer in the "composite image".
     +
     + The othe layers are copied untouched.
     +/
    CopyOpacityCompositeOp,
    /++
     + Pixels in the region are set to Transparent.
     +/
    ClearCompositeOp,
    DissolveCompositeOp,
    DisplaceCompositeOp,
    /++
     + Modulate brightness in HSL space.
     +/
    ModulateCompositeOp,
    ThresholdCompositeOp,
    /++
     + Do nothing at all.
     +/
    NoCompositeOp,
    DarkenCompositeOp,
    LightenCompositeOp,
    /++
     + Copy Hue channel (from HSL colorspace)
     +/
    HueCompositeOp,
    /++
     + Copy Saturation channel (from HSL colorspace)
     +/
    SaturateCompositeOp,
    /++
     + Copy Hue and Saturation channels (from HSL colorspace)
     +/
    ColorizeCompositeOp,
    /++
     + Copy Brightness channel (from HSL colorspace)
     +/
    LuminizeCompositeOp,
    /// [Not yet implemented]
    ScreenCompositeOp,
    /// [Not yet implemented]
    OverlayCompositeOp,
    /++
     + Copy the Cyan channel.
     +/
    CopyCyanCompositeOp,
    /++
     + Copy the Magenta channel.
     +/
    CopyMagentaCompositeOp,
    /++
     + Copy the Yellow channel.
     +/
    CopyYellowCompositeOp,
    /++
     + Copy the Black channel.
     +/
    CopyBlackCompositeOp,
    DivideCompositeOp,
    HardLightCompositeOp,
    ExclusionCompositeOp,
    ColorDodgeCompositeOp,
    ColorBurnCompositeOp,
    SoftLightCompositeOp,
    LinearBurnCompositeOp,
    LinearDodgeCompositeOp,
    LinearLightCompositeOp,
    VividLightCompositeOp,
    PinLightCompositeOp,
    HardMixCompositeOp
}

/++
 + CompressionType is used to express the desired compression type when
 + encoding an image.
 +
 + Be aware that most image types only support a sub-set of the available
 + compression types.  If the compression type specified is incompatible with
 + the image, Magick will select a compression type compatible with the image
 + type, which might be no compression at all.
 +/
alias CompressionType = int;
/// Ditto
enum : int
{
    /// Unset value
    UndefinedCompression,
    /// No compression
    NoCompression,
    /++
     + BZip (Burrows-Wheeler block-sorting text compression algorithm and
     + Huffman coding) as used by bzip2 utilities.
     +/
    BZipCompression,
    /// CCITT Group 3 FAX compression
    FaxCompression,
    /// Ditto
    Group3Compression = FaxCompression,
    /// CCITT Group 4 FAX compression (used only for TIFF)
    Group4Compression,
    /// JPEG compression
    JPEGCompression,
    /// Lossless JPEG compression
    LosslessJPEGCompression,
    /// Lempel-Ziv-Welch (LZW) compression (caution: patented by Unisys)
    LZWCompression,
    /// Run-Length encoded (RLE) compression
    RLECompression,
    /// Lempel-Ziv compression (LZ77) as used in PKZIP and GNU gzip.
    ZipCompression,
    /// LZMA - Lempel-Ziv-Markov chain algorithm
    LZMACompression,
    /// JPEG 2000 - ISO/IEC std 15444-1
    JPEG2000Compression,
    /// JBIG v1 - ISO/IEC std 11544 / ITU-T rec T.82
    JBIG1Compression,
    /// JBIG v2 - ISO/IEC std 14492 / ITU-T rec T.88
    JBIG2Compression,
    /// Facebook's Zstandard/Zstd
    ZSTDCompression,
    /// Google's WebP
    WebPCompression,
}

alias DisposeType = int;
enum : int
{
    UndefinedDispose,
    NoneDispose,
    BackgroundDispose,
    PreviousDispose
}

alias EndianType = int;
enum : int
{
    UndefinedEndian,
    LSBEndian, /* "little" endian */
    MSBEndian, /* "big" endian */
    NativeEndian /* native endian */
}

alias FilterTypes = int;
enum : int
{
    UndefinedFilter,
    PointFilter,
    BoxFilter,
    TriangleFilter,
    HermiteFilter,
    HanningFilter,
    HammingFilter,
    BlackmanFilter,
    GaussianFilter,
    QuadraticFilter,
    CubicFilter,
    CatromFilter,
    MitchellFilter,
    LanczosFilter,
    BesselFilter,
    SincFilter
}

/++
 + GravityType specifies positioning of an object (e.g. text, image) within a
 + bouding region (e.g. an image).
 +
 + Gravity provides a convenient way to locate objects irrespective of the
 + size of the bounding region, in other words, you don't need to provide
 + absolute coordinates in order to position an object.  A common default for
 + gravity is NorthWestGravity.
 +/
alias GravityType = int;
/// Ditto
enum : int
{
    /// Don't use gravity.
    ForgetGravity,
    /// Position object at top-left of region.
    NorthWestGravity,
    /// Position object at top-center of region.
    NorthGravity,
    /// Position object at top-right of region.
    NorthEastGravity,
    /// Position object at left-center of region.
    WestGravity,
    /// Position object at center of region.
    CenterGravity,
    /// Position object at right-center of region.
    EastGravity,
    /// Position object at left-bottom of region.
    SouthWestGravity,
    /// Position object at bottom-center of region.
    SouthGravity,
    /// Position object at bottom-right of region.
    SouthEastGravity,
    StaticGravity
}

/++
 + ImageType indicates the type classification of the image.
 +/
alias ImageType = int;
/// Ditto
enum : int
{
    /// Unset value
    UndefinedType,
    /// Monochrome image.
    BilevelType,
    /// Grayscale image.
    GrayscaleType,
    /// Grayscale image with opacity
    GrayscaleMatteType,
    /// Indexed color (palette) image.
    PaletteType,
    /// Indexed color (palette) image with opacity.
    PaletteMatteType,
    /// Truecolor image.
    TrueColorType,
    /// Truecolor image with opacity.
    TrueColorMatteType,
    /// Cyan/Yellow/Magenta/Black (CYMK) image
    ColorSeparationType,
    /// Cyan/Yellow/Magenta/Black (CYMK) image with opacity.
    ColorSeparationMatteType,
    OptimizeType
}

/++
 + InterlaceType specifies the ordering of the red, green, and blue pixel
 + information in the image.
 +
 + Interlacing is usually used to make image information available to the user
 + faster by taking advantage of the space vs. time tradeoff.  For example,
 + interlacing allows images on the Web to be recognizable sooner and
 + satellite images to accumulate/render with image resolution increasing over
 + time.
 +
 + Use `LineInterlace` or `PlaneInterlace` to create an interlace GIF or
 + progressive JPEG image.
 +/
alias InterlaceType = int;
/// Ditto
enum : int
{
    /++
     + Unset value.
     +/
    UndefinedInterlace,
    /++
     + Don't interlace image (RGBRGBRGBRGBRGBRGB...)
     +/
    NoInterlace,
    /++
     + Use scanline interlacing (RRR...GGG...BBB...RRR...GGG...BBB)
     +/
    LineInterlace,
    /++
     + Use plane interlacing (RRRRRR...GGGGGG...BBBBBB...)
     +/
    PlaneInterlace,
    /++
     + Similar to plane interlacing except that the different planes are
     + saved to individual files (e.g. image.R, image.G, and image.B)
     +/
    PartitionInterlace
}

/++
 + NoiseType is used as an argument to select the type of noise to be added to
 + the image.
 +/
alias NoiseType = int;
/// Ditto
enum : int
{
    UniformNoise,
    GaussianNoise,
    MultiplicativeGaussianNoise,
    ImpulseNoise,
    LaplacianNoise,
    PoissonNoise,
    RandomNoise,
    UndefinedNoise
}

/++
 + OrientationType specifies the orientation of the image.
 +
 + Useful for when the image is produced via a different ordinate system, the
 + camera was turned on its side, or the page was scanned sideways.
 +/
alias OrientationType = int;
/// Ditto
enum : int
{
    /// Scanline dir: Unknown; Frame dir: Unknown
    UndefinedOrientation,
    /// Scanline dir: Left to Right; Frame dir: Top to Bottom
    TopLeftOrientation,
    /// Scanline dir: Right to Left; Frame dir: Top to Bottom
    TopRightOrientation,
    /// Scanline dir: Right to Left; Frame dir: Bottom to Top
    BottomRightOrientation,
    /// Scanline dir: Left to Right; Frame dir: Bottom to Top
    BottomLeftOrientation,
    /// Scanline dir: Top to Bottom; Frame dir: Left to Right
    LeftTopOrientation,
    /// Scanline dir: Top to Bottom; Frame dir: Right to Left
    RightTopOrientation,
    /// Scanline dir: Bottom to Top; Frame dir: Right to Left
    RightBottomOrientation,
    /// Scanline dir: Bottom to Top; Frame dir: Left to Right
    LeftBottomOrientation
}

alias PreviewType = int;
enum : int
{
    UndefinedPreview = 0,
    RotatePreview,
    ShearPreview,
    RollPreview,
    HuePreview,
    SaturationPreview,
    BrightnessPreview,
    GammaPreview,
    SpiffPreview,
    DullPreview,
    GrayscalePreview,
    QuantizePreview,
    DespecklePreview,
    ReduceNoisePreview,
    AddNoisePreview,
    SharpenPreview,
    BlurPreview,
    ThresholdPreview,
    EdgeDetectPreview,
    SpreadPreview,
    SolarizePreview,
    ShadePreview,
    RaisePreview,
    SegmentPreview,
    SwirlPreview,
    ImplodePreview,
    WavePreview,
    OilPaintPreview,
    CharcoalDrawingPreview,
    JPEGPreview
}

/++
 + Rendering intent is a concept defined by ICC Spec ICC.1:1998-09, "File
 + Format for Color Profiles".
 +
 + GraphicsMagick uses RenderingIntent in order to support ICC Color Profiles.
 +
 + From the specification:
 + "Rendering intent specifies the style of reproduction to be used during the
 + evaluation of this profile in a sequence of profiles.  It applies
 + specifically to that profile in the sequence and not to th entire sequence.
 + Typically, the user or application will set the rendering intent
 + dynamically at runtime or embedding time."
 +/
alias RenderingIntent = int;
/// Ditto
enum : int
{
    /++
     + Unset value.
     +/
    UndefinedIntent,
    /++
     + A rendering intent that specifies the saturation of the pixels in the
     + image is preserved perhaps at the expense of accuracy in hue and
     + lightness.
     +/
    SaturationIntent,
    /++
     + A rendering intent that specifies the full gamut of the image is
     + compressed or expanded to fill the gamut of the destination device.
     +
     + Gray balance is preserved but colorimetric accuracy might not be
     + preseved.
     +/
    PerceptualIntent,
    /++
     + Absolute colorimetric.
     +/
    AbsoluteIntent,
    /++
     + Relative colorimetric
     +/
    RelativeIntent
}

/++
 + By default, Magick defines resolution pixels per inch.
 +
 + ResolutionType provides a means to adjust this.
 +/
alias ResolutionType = int;
/// Ditto
enum : int
{
    /// Unset Value.
    UndefinedResolution,
    /++
     + Density specifications are specified in units of pixels per inch
     + (english units).
     +/
    PixelsPerInchResolution,
    /++
     + Density specifications are specified in units of pixels per centimeter
     + (metric units).
     +/
    PixelsPerCentimeterResolution
}


/+
 + --------------------------------------------------------------------------
 +                           Structure  Definitions
 + --------------------------------------------------------------------------
 +/

extern (C) struct AffineMatrix
{
    double sx, rx, ry, sy, tx, ty;
}

extern (C) struct PrimaryInfo
{
    double x, y, z;
}

/++
 + The ChromaticityInfo structure is used to represent chromaticity
 + (colorspace primary coordinates in xy space) values for images in Magick.
 +/
extern (C) struct ChromaticityInfo
{
    /// Chromaticity red primary point (e.g. x=0.64, y=0.33)
    PrimaryInfo red_primary;
    /// Chromaticity green primary point (e.g. x=3, y=0.6)
    PrimaryInfo green_primary;
    /// Chromaticity blue primary point (e.g. x=0.15, y=0.06)
    PrimaryInfo blue_primary;
    /// Chromaticity white point (e.g. x=0.3127, y=0.329)
    PrimaryInfo white_point;
}

extern (C) struct PixelPacket
{
    version (BigEndian)
    {
        /* RGBA */
        //        enum MAGICK_PIXELS_RGBA = 1;
        Quantum red, green, blue, opacity;
    }
    else
    {
        /* BGRA (as used by Microsoft Windows DIB) */
        //        enum MAGICK_PIXELS_BGRA = 1;
        Quantum blue, green, red, opacity;
    }
}

extern (C) struct DoublePixelPacket
{
    double red, green, blue, opacity;
}

extern (C) struct FloatPixelPacket
{
    float red, green, blue, opacity;
}
/++
 + ErrorInfo is used to record statistical difference (error) information
 + based on computed Euclidean distance in RGB space.
 +/
extern (C) struct ErrorInfo
{
    /// Average error per pixel (absolute range).
    double mean_error_per_pixel;
    /// Average error per pixel (normalized to 1.0).
    double normalized_mean_error;
    /// Maximum error encountered (normalized to 1.0).
    double normalized_maximum_error;
}

/++
 + The RectangleInfo structure is used to represent positioning information
 + in GraphicsMagick.
 +/
extern (C) struct RectangleInfo
{
    /// Rectangle width
    c_ulong width;
    /// Rectangle height
    c_ulong height;
    /// Rectangle horizontal offset
    c_long x;
    /// Rectangle vertical offset.
    c_long y;
}

/* forward decl. */
extern (C) struct _ImageExtra;
extern (C) struct _CacheInfo;
extern (C) struct _ThreadViewSet;
extern (C) struct _ImageAttribute;
extern (C) struct _Ascii85Info;
extern (C) struct _BlobInfo;
extern (C) struct _SemaphoreInfo;

/++
 + The Image structure represents a GraphicsMagick image.
 +
 + It is initially allocated by AllocateImage() and deallocated by
 + DestroyImage().  The functions ReadImage(), ReadImages(), BlobToImage(),
 + and CreateImage() return a new Image.  Use CloneImage() to copy an image.
 + An image consists of a structure containing image attributes as well as the
 + image pixels.
 +/
extern (C) struct Image
{
    /++
     + Image storage class.
     +
     + If DirectClass then the image packets contain valid RGB or CMYK colors.
     + If PseudoClass then the image has a colormap referenced by pixel's
     + index member.
     +/
    ClassType storage_class;
    /++
     + Image pixel interpretation.
     +
     + If the colorspace is RGB the pixels are red, green, blue.  If matte is
     + true, then red, green, blue, and index.  If it is CMYK, the pixels are
     + cyan, yellow, magenta, black.  Otherwise, the colorspace is ignored.
     +/
    ColorspaceType colorspace;
    /++
     + Image compression type.
     +
     + The default compression type of the specified image file.
     +/
    CompressionType compression;
    /++
     + Should the image be dithered.
     +/
    MagickBool dither;
    /++
     + If non-zero, then the index member of pixels represents the alpha
     + channel.
     +/
    MagickBool matte;
    /++
     + Image width.
     +/
    c_ulong columns;
    /++
     + Image height.
     +/
    c_ulong rows;
    /++
     + The desired number of colors.
     +
     + Used by QuantizeImage().
     +/
    uint colors;
    /++
     + Image depth.
     +
     + Number of encoding bits per sample.  Usually 8 or 16, but sometimes
     + 10 or 12.
     +/
    uint depth;
    /++
     + PseudoColor palette array.
     +/
    PixelPacket* colormap;
    /++
     + Image background color.
     +/
    PixelPacket background_color;
    /++
     + Image border color.
     +/
    PixelPacket border_color;
    /++
     + Image matte (transparent) color.
     +/
    PixelPacket matte_color;
    /++
     + Gamma level of the image.
     +
     + The same color image displayed on two different workstations may look
     + different due to differences in the display monitor.  Use gamma
     + correction to adjust for this color difference.
     +/
    double gamma;
    /++
     + Red, green, blue, and white-point chromaticity values.
     +/
    ChromaticityInfo chromaticity;
    /++
     + Orientation of the image.
     +
     + Specifies scanline orientation and starting coordinate of image.
     +/
    OrientationType orientation;
    /++
     + The type of rendering intent.
     +/
    RenderingIntent rendering_intent;
    /++
     + Units of image resolution (density).
     +/
    ResolutionType units;
    /++
     + Tile size and offset within an image montage.
     +
     + Only valid for montage images..
     +/
    char* montage;
    /++
     + Tile names from within an image montage.
     +
     + Only valid after calling MontageImages() or reading a MIFF file which
     + contains a directory.
     +/
    char* directory;
    /++
     + Preferred size of the image when encoding.
     +/
    char* geometry;
    /++
     + Number of initial bytes to skip over when reading raw image.
     +/
    c_long offset;
    /++
     + Horizontal resolution of the image.
     +/
    double x_resolution;
    /++
     + Vertical resolution of the image.
     +/
    double y_resolution;
    /++
     + Equivalent size of Postscript page.
     +/
    RectangleInfo page;
    /++
     + Describes a tile within a image.
     +
     + For example, if your image is 640x480 you may only want 320x256 with
     + an offset of +128+64.  It is used for raw formats such as RGB and CMYK
     + as well as for TIFF.
     +/
    RectangleInfo tile_info;
    /++
     + Blur factor to apply to the image when zooming.
     +/
    double blur;
    /++
     + Colors within this distance are considered equal.
     +
     + A number of algorithms search for a target color.  By default the color
     + must be exact.  Use this option to match colors that are close to the
     + target color in RGB space.
     +/
    double fuzz;
    /++
     + Filter to use when resizing image.
     +
     + The reduction filter employed has a significant effect on the time
     + required to resize an image and the resulting quality.  The default
     + filter is Lanczos which has been shown to produce high quality results
     + when reducing most images.
     +/
    FilterTypes filter;
    /++
     + The type of interlacing scheme (default NoInterlace).
     +
     + This option is used to specify the type of interlacing scheme for raw
     + image formats such as RGB or YUV.  NoInterlace means do not interlace,
     + LineInterlace uses scanline interlacing, and PlaneInterlace uses plane
     + interlacing.  PartitionInterlace is like PlaneInterlace except the
     + different planes are saved to individual files (e.g. image.R, image.G,
     + and image.B).  Use LineInterlace or PlaneInterlace to create an
     + interlaced GIF or progressive JPEG image.
     +/
    InterlaceType interlace;
    /++
     + Byte order to use when writing image.
     +/
    EndianType endian;
    /++
     + Image placement gravity.
     +/
    GravityType gravity;
    /++
     + Image placement composition (default OverCompositeOp).
     +/
    CompositeOperator compose;
    /++
     + GIF disposal method.
     +
     + This option is used to control how successive frames are rendered (how
     + the preceding frame is disposed of) when creating a GIF animation.
     +/
    DisposeType dispose;
    /++
     + Image frame scene number.
     +/
    c_ulong scene;
    /++
     + Time is 1/100ths of a second (0 to 65535) which must expire before
     + displaying the next image in an animated sequence.
     +
     + This option is useful for regulating the animation of a sequence of
     + GIF images within Netscape.
     +/
    c_ulong delay;
    /++
     + Number of iterations to loop an animation (e.g. Netscape loop
     + extension) for.
     +/
    c_ulong iterations;
    /++
     + The number of colors in the image after QuantizeImage(), or
     + QuantizeImages() if the verbose flag was set before the call.
     +
     + Calculated by GetNumberColors().
     +/
    c_ulong total_colors;
    /++
     + Animation frame number to start looping at.
     +/
    c_long start_loop;
    /++
     + Computed image comparison or quantization error.
     +/
    ErrorInfo error;
    /++
     + Support for measuring actual (user + system) and elapsed execution
     + time.
     +/
    TimerInfo timer;
    /++
     + Use specified opaque data pointer.
     +/
    void* client_data;
    /++
     + Image file name to read or write.
     +
     + A colon delimited format identifier may be prepended to the file name
     + in order to force a particular output format.  Otherwise, the file
     + extension is used.  If no format prefix or file extension is presented,
     + then the output format is determined by the 'magick' field.
     +/
    char[MaxTextExtent] filename;
    /++
     + Original file name (before transformations).
     +/
    char[MaxTextExtent] magick_filename;
    /++
     + Image encoding format (e.g. "GIF").
     +
     + The precedence when selecting the output format is:
     +   1. magick prefix to file name (e.g. "jpeg:foo").
     +   2. file name extension (e.g. "foo.jpg")
     +   3. content of this magick field.
     +/
    char[MaxTextExtent] magick;
    /++
     + Original image width (before transformations)
     +/
    c_ulong magick_columns;
    /++
     + Original image height (before transformations)
     +/
    c_ulong magick_rows;
    /++
     + Record of any error which occurred when updating image.
     +/
    ExceptionInfo exception;
    /++
     + Previous image frame in sequence.
     +/
    Image* previous;
    /++
     + Next image frame in sequence.
     +/
    Image* next;

    /*
     * Private Members
     */

    /+ Private, Embedded profiles +/
    void* profiles;
    /+ Private, True if image is known to be monochrome. +/
    uint is_monochrome;
    /+ Private, True if image is known to be grayscale. +/
    uint is_grayscale;
    /+ Private, True if image has not been modified. +/
    uint taint;
    /+
     + Allow for expansion of Image without increasing its size.  The
     + internals are defined only in image.c.  Clients outside of image.c can
     + access the internals via the provided access functions (see below).
     +/
    _ImageExtra* extra;
    /+ Private, True if pixels are undefined. +/
    MagickBool ping;
    /+ Private, Image pixel cache. +/
    _CacheInfo* cache;
    /+ Private, default cache views +/
    _ThreadViewSet* default_views;
    /+ Private, Image attribute list. +/
    _ImageAttribute* attributes;
    /+ Private, supports huffman encoding +/
    _Ascii85Info* ascii85;
    /+ Private, file I/O object. +/
    _BlobInfo* blob;
    /+ Private, Image reference count +/
    c_long reference_count;
    /+ Private, Per image lock (for reference count). +/
    _SemaphoreInfo* semaphore;
    /+ Private, True if logging is enabled. +/
    uint logging;
    /+ Private, used only by display. +/
    Image* list;
    /+ Private, Unique code to validate structure. +/
    c_ulong signature;
}

/++
 + The ImageInfo structure is used to supply option information to the functions
 + $(REF AllocateImage)(), $(REF AnimateImages)(), $(REF BlobToImage)(),
 + $(REF CloneAnnotateInfo)(), $(REF DisplayImages)(), $(REF GetAnnotateInfo)(),
 + $(REF ImageToBlob)(), $(REF PingImage)(), $(REF ReadImage)(),
 + $(REF ReadImages)(), and, $(REF WriteImage)().
 +
 + These functions update information in ImageInfo to reflect attributes of the
 + current image.
 +
 + Use $(REF CloneImageInfo)() to duplicate an existing ImageInfo structure or
 + allocate a new one.  Use $(REF DestroyImageInfo)() to deallocate mempory
 + associated with an ImageInfo structure.  Use $(REF GetImageInfo)() to
 + initialize an existing ImageInfo structure.  Use $(REF SetImageInfo)() to
 + set image type information in the ImageInfo structure based on an existing
 + image.
 +/
extern (C) struct ImageInfo
{
    /++
     + Image compresion type.
     +
     + The default is the compression type of the specified image file.
     +/
    CompressionType compression;
    /++
     + Remove file "filename" once it has been read.
     +/
    MagickBool temporary;
    /++
     + Join images into a single multi-image file.
     +/
    MagickBool adjoin;
    /++
     + Control antialiasing of rendered Postscript and Postscript or TrueType
     + fonts.  Enabled by default.
     +/
    MagickBool antialias;
    /++
     + Subimage of an image sequence.
     +/
    c_ulong subimage;
    /++
     + Number of images relative to the base image.
     +/
    c_ulong subrange;
    /++
     + Image depth (8 or 16).
     +/
    c_ulong depth;
    /++
     + Width and height of a raw image (an image which does not support width
     + and height information).
     +
     + Size may also be used to affect the image size read from a
     + multi-resolution format (e.g. Photo CD, JBIG, or JPEG).
     +/
    char* size;
    /++
     + Tile name.
     +/
    char* tile;
    /++
     + Equivalent size of Postscript page.
     +/
    char* page;
    /++
     + The type of interlacing scheme (default NoInterlace).
     +
     + This option is used to specify the type of interlacing scheme for raw
     + image formats such as RGB or YUV.  NoInterlace means to not interlace,
     + LineInterlace uses scanline interlacing, and PlaneInterlace uses plane
     + interlacing.  PartitionInterlace is like PlaneInterlace except the
     + different planes are saved to individual files (e.g. image.R, image.G,
     + and image.B).  Use LineInterlace or PlaneInterlace to create an
     + interlaced GIF or progressive JPEG image.
     +/
    InterlaceType interlace;
    /++
     + Select MSB/LSB endian output for TIFF format.
     +/
    EndianType endian;
    /++
     + Units of image resolution.
     +/
    ResolutionType units;
    /++
     + JPEG/MIFF/PNG compression level (default 75).
     +/
    c_ulong quality;
    /++
     + JPEG, MPEG, and YUV chroma downsample factor.
     +/
    char* sampling_factor;
    /++
     + X11 display to obtain fonts from, or to capture image from.
     +/
    char* sever_name;
    /++
     + Text rendering font.
     +
     + If the font is a fully qualified X server font name, the font is
     + obtained from an X server.  To use a TrueType font, precede the
     + TrueType filename with an `@`.  Otherwise, specify a Postscript font
     + name (e.g. "helvetica").
     +/
    char* font;
    /++
     + Image filename to use as background texture.
     +/
    char* texture;
    /++
     + Vertical and horizontal resolution in pixels of the image.
     +
     + This option specifies an image density when decoding a Postscript or
     + Portable Document page.  Often used with page.
     +/
    char* density;
    /++
     + Text rendering font point size.
     +/
    double pointsize;
    /++
     + Colors within this distance are considered equal.
     +
     + A number of algorithms seach for a target color.  By default the color
     + must be exact.  Use this option to match colors that are close to the
     + target color in RGB space.
     +/
    double fuzz;
    /++
     + Stroke or fill color while drawing.
     +/
    PixelPacket pen;
    /++
     + Image background color
     +/
    PixelPacket background_color;
    /++
     + Image border color.
     +/
    PixelPacket border_color;
    /++
     + Image matte (transparent) color.
     +/
    PixelPacket matte_color;
    /++
     + Apply Floyd/Steinberg error diffusion to the image.
     +
     + The basic strategy of dithering is to trade intensity resolution for
     + spacial resolution by averaging the intesities of several neighboring
     + pixels.  Images which suffer from severe contouring when reducing
     + colors can be improved with this option.  The colors or monochrome
     + option must be set for this option to take effect.
     +/
    MagickBool dither;
    /++
     + Transform the image to black and white.
     +/
    MagickBool monochrome;
    /++
     + Show progress indication.
     +/
    MagickBool progress;
    /++
     + Image pixel interpretation.
     +
     + If the colorspace is RGB the pixels are red, green, blue.  If `matte`
     + is true, then red, green, blue, and index.  If it is CMYK, the pixels
     + are cyan, yellow, magenta, black.  Otherwise the colorspace is ignored.
     +/
    ColorspaceType colorspace;
    /++
     + Desired image type (used while reading or writing).
     +/
    ImageType type;
    /++
     + X11 Window group ID
     +/
    c_long group;
    /++
     + Print detailed information about the image if True.
     +/
    uint verbose;
    /++
     + FlashPix viewing parameters.
     +/
    char* view;
    /++
     + Password used to decrypt file.
     +/
    char* authenticate;
    /++
     + User-specified data to pass to coder.
     +/
    void* client_data;
    /++
     + stdio stream to read image from or write image to.
     +
     + If set, Magick will read from or write to the stream rather than
     + opening a file.  Used by ReadImage() and WriteImage().  The stream is
     + closed when the operation completes.
     +/
    FILE* file;
    /++
     + Image encoding format (e.g. "GIF").
     +/
    char[MaxTextExtent] magick;
    /++
     + Image file name to read or write.
     +/
    char[MaxTextExtent] filename;

    /*
     * Private Members
     */

    /+ Private. used to pass image via open cache. +/
    _CacheInfo* cache;
    /+
     + Private. Map of coder specific options passed by user.
     + Use AddDefinitions, RemoveDefinitions, & AccessDefinition
     +/
    void* definitions;
    /+ Private. Image attribute list. +/
    Image* attributes;
    /+ Private, If true, read file header only. +/
    MagickBool ping;
    /+ Private, used by PreviewImage. +/
    PreviewType preview;
    /+ Private, when true do not intuit image format. +/
    MagickBool affirm;
    /+ Private, used to pass in open blob. +/
    _BlobInfo* blob;
    /+ Private, used to pass in open blob length +/
    size_t length;
    /+ Private, passes temporary filename to TranslateText. +/
    char[MaxTextExtent] unique;
    /+ Private, passes temporary filename to TranslateText. +/
    char[MaxTextExtent] zero;
    /+ Private, used to validate structure. +/
    c_ulong signature;
}


/+
 + --------------------------------------------------------------------------
 +                              Image  Functions
 + --------------------------------------------------------------------------
 +/


/++
 + AccessDefinition() searches the definitions for an entry matching the
 + specified magick and key.
 +
 + NULL is returned if no matching entry is found.
 +
 + Params:
 +   image_info = The image info.
 +   magick = Format ID. This is usually the same as the coder name.
 +   key = The key to search for.
 +/
version (SHARED)
{
    MDAccessDefinition AccessDefinition;
    extern (C) alias MDAccessDefinition = const(char)* function(
            const(ImageInfo)* image_info, const(char)* magick, const(char)* key);
}
else
{
    extern (C) const(char)* AccessDefinition(const(ImageInfo)* image_info,
            const(char)* magick, const(char)* key);
}

/++
 + AddDefinition() adds a key/value definition to the current map of
 + definitions in ImageInfo.
 +
 + Definitions may be used by coders/decoders that read and write images.
 +
 + Params:
 +   image_info = The image info.
 +   magick = Format/classification identifier.
 +   key = Subidentifier within format/classification.
 +   value = Definition value
 +   exception = Errors result in updates to this structure.
 +/
version (SHARED)
{
    __gshared MDAddDefinition AddDefinition;
    extern (C) alias MDAddDefinition = MagickPassFail function(ImageInfo* image_info,
            const(char)* magick, const(char)* key, const(char)* value, ExceptionInfo* exception);
}
else
{
    extern (C) MagickPassFail AddDefinition(ImageInfo* image_info, const(char)* magick,
            const(char)* key, const(char)* value, ExceptionInfo* exception);
}

/++
 + AddDefinitions() adds definitions from a key/value based string to the
 + current map of difinitions in ImageInfo.
 +
 + Definitions may be used by coders/decoders that read and write images.
 +
 + Params:
 +   image_info = The image info.
 +   options = List of key/value pairs to put in the definitions map.  The
 + format of th string is "key1[=[value1]],key2[=[value2]],...".  A missing
 + value argument (with or without the equal sign) inserts an empty, zero
 + length string as value for a key.
 +   exception = Error result in updates to this structure.
 +/
version (SHARED)
{
    __gshared MDAddDefinitions AddDefinitions;
    extern (C) alias MDAddDefinitions = MagickPassFail function(ImageInfo* image_info,
            const(char)* options, ExceptionInfo* exception);
}
else
{
    extern (C) MagickPassFail AddDefinitions(ImageInfo* image_info,
            const(char)* options, ExceptionInfo* exception);
}

/++
 + CloneImage() copies an image and returns the copy as a new image
 + object.
 +
 + If the specified `columns and `rows` is `0`, an exact copy of the image is
 + returned, otherwise, the pixel data is undefined and must be initialzed
 + with the SetImagePixels() and SyncImagePixels() methods.  On failure, a
 + NULL image is returned and `exception` describes the reason for failure.
 +
 + Params:
 +   image = The image
 +   columns = The number of columns in the cloned image.
 +   rows = The numbe of rows in the cloned image.
 +   orphan = With a value other than 0, the cloned image is an orphan.  An
 + orphan is a stand-alone image that is not associated with an image list.
 + In effect, the next and previous members of the cloned image is set to
 + NULL.
 +   exception = Return any errors or warnings to this structure.
 +/
version (SHARED)
{
    __gshared MDCloneImage CloneImage;
    extern (C) alias MDCloneImage = Image* function(const(Image)* image,
            const(c_ulong) columns, const(c_ulong) rows, const(uint) orphan,
            ExceptionInfo* exception);
}
else
{
    extern (C) Image* CloneImage(const Image* image, const c_ulong columns,
            const c_ulong rows, const uint orphan, ExceptionInfo* exception);
}

/++
 + CloneImageInfo() makes a copy of the given `ImageInfo` structure.
 +
 + If NULL is specified, a new image info structure is created an initialized
 + to default values.
 +
 + Params:
 +  image_info = The image info
 +/
version (SHARED)
{
    __gshared MDCloneImageInfo CloneImageInfo;
    extern (C) alias MDCloneImageInfo = ImageInfo* function(const(ImageInfo)* image_info);
}
else
{
    extern (C) ImageInfo* CloneImageInfo(const ImageInfo* image_info);
}

/++
 + DestroyImage() dereferences an image, deallocating memory associated with
 + the image if the reference count becomes zero.
 +
 + There is no effect if the image pointer is null.
 +
 + In the interest of avoiding dangling pointers or memory leaks, the image
 + previous and next pointers should be null when this function is called, and
 + no othe image should refer to it.  In the future this may be enforced.
 +
 + Params:
 +   image = The image.
 +/
version (SHARED)
{
    __gshared MDDestroyImage DestroyImage;
    extern (C) alias MDDestroyImage = void function(Image* image);
}
else
{
    extern (C) void DestroyImage(Image* image);
}

/++
 + DestroyImageInfo() deallocates memory associated with an `ImageInfo`
 + structure.
 +
 + Params:
 +   image_info = The image info.
 +/
version (SHARED)
{
    __gshared MDDestroyImageInfo DestroyImageInfo;
    extern (C) alias MDDestroyImageInfo = void function(ImageInfo* image_info);
}
else
{
    extern (C) void DestroyImageInfo(ImageInfo* image_info);
}
