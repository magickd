/**
 * Magick API common definitions support
 *
 * Authors: GraphicsMagick Group
 * Copyright: &copy; 2009 - 2016 GraphicsMagick Group
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 */
deprecated("Use graphicsmagick_c.magick.api instead (remove @ 0.9)")
module magickd.core.c.common;

@nogc @system extern (C) nothrow:

enum MaxTextExtent = 2053;

enum MagickSignature = 0xabacadabUL;

/// Represents a success or failure of an operation
alias MagickPassFail = uint;
/// The operation was a success (value 1)
enum MagickPass = 1;
/// The operation was a failure (value 0)
enum MagickFail = 0;

/// Represents a Boolean type
alias MagickBool = uint;
/// True (value 1)
enum MagickTrue = 1;
/// False (value 0)
enum MagickFalse = 0;
