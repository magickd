/**
 * GraphicsMagick Colorspace methods
 *
 * Authors: E. I. du Pont de Nemours and Company, ImageMagick Studio,
 *   GraphicsMagick Group.
 * Copyright: 1991 - 1999 E. I. du Pont de Nemours and Company,
 *   &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 GraphicsMagick Studio
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.colorspace instead (remove @ 0.9)")
module magickd.core.c.colorspace;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

/++
 + The ColorspaceType enumeration is used to specify the colorspace that
 + quantization (color reduction and mapping) is done under or to specify the
 + colorspace when encoding an output image.
 +
 + Colorspaces are ways of describing colors to fit the requirements of a
 + particular application (e.g. Telivision, offset printing, color monitors).
 + Color reduction, by default, takes place in the RGBColorspace.  Empirical
 + evidence suggests that distance in color spaces such as YUVColorspace or
 + YIQCOlorspace correspong to perceptual color differences more closely than
 + do distances in RGB space.  These color spaces may give better results when
 + color reducing an image.  Refer to quantize for more details.
 +
 + When encoding an output image, the colorspaces RGBColorspace,
 + CMYKColorspace, and GRAYColorspace may be specified.  The CMYKColorspace
 + option is only applicable when writing TIFF, JPEG, and Adobe Photoshop
 + bitmap (PSD) files.
 +/
alias ColorspaceType = int;
/// Ditto
enum : int
{
    /// Unset value
    UndefinedColorspace,
    /// Red, Green, Blue colorspace
    RGBColorspace,
    /// Similar to Luma (Y) according to ITU-R 601.
    GRAYColorspace,
    /// RGB which preserves the matte while quantizing colors.
    TransparentColorspace,
    OHTAColorspace,
    /// CIE XYZ
    XYZColorspace,
    /// Kodak PhotoCD PhotoYCC
    YCCColorspace,
    YIQColorspace,
    YPbPrColorspace,
    /// YUV colorspace as used for compute video.
    YUVColorspace,
    /// Cyan, Magenta, Yellow, Black colorspace.
    CMYKColorspace,
    /// Kodak PhotoCD sRGB
    sRGBColorspace,
    /// Hue, Saturation, Luminosity
    HSLColorspace,
    /// Hue, Whiteness, Blackness
    HWBColorspace,
    /// ITU LAB
    LABColorspace,
    /// RGB data with Cineon Log scaling, 2.048 density range.
    CineonLogRGBColorspace,
    /// Luma (Y) according to ITU-R 601.
    Rec601LumaColorspace,
    /// YCbCr according to ITU-R 601
    Rec601YCbCrColorspace,
    /// Luma (Y) according to ITU-R 709
    Rec709LumaColorspace,
    /// YCbCr according to ITU-R 709
    Rec709YCbCrColorspace
}
