/*               Copyright (C) kaerou 2021.
** Distributed under the Boost Software License, Version 1.0.
**    (See accompanying file LICENSE_1_0.txt or copy at
**          https://www.boost.org/LICENSE_1_0.txt)
*/
deprecated("Use graphicsmagick_c.magick.forward instead (remove @ 0.9)")
module magickd.core.c.types;

@nogc @system extern(C) nothrow:


/* TODO: BlobInfo */

alias Cache = void*;
