/**
 * ImageMagick Timer Methods.
 *
 * Authors: ImageMagick Studio, GraphicsMagick Group
 * Copyright: &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 GraphicsMagick Group
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.timer instead (remove @ 0.9)")
module magickd.core.c.timer;

private
{
    import core.stdc.config : c_ulong;
}

@nogc @system nothrow:

alias TimerState = int;
enum : int
{
    UndefinedTimerState,
    StoppedTimerState,
    RunningTimerState
}

extern(C) struct Timer
{
    double start,
        stop,
        total;
}

extern(C) struct TimerInfo
{
    Timer user;
    Timer elapsed;
    TimerState state;
    c_ulong signarute;
}
