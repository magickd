/**
 * ImageMagick Image Resize Methods.
 *
 * Authors: ImageMagick Studio, GraphicsMagick Group
 * Copyright: &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2012 GraphicsMagick Group
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.resize instead (remove @ 0.9)")
module magickd.core.c.resize;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
    import core.stdc.config : c_ulong;

    import magickd.core.c.error : ExceptionInfo;
    import magickd.core.c.image : FilterTypes, Image;
}

@nogc @system nothrow:

/++
 + MagnifyImage() is a convenience method that scales an image proportionally
 + to twice its size.
 +
 + Params:
 +   image = The image.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDMagnifyImage MagnifyImage;
    extern (C) alias MDMagnifyImage = Image* function(const(Image)* image, ExceptionInfo* exception);
}
else
{
    extern (C) Image* MagnifyImage(const Image* image, ExceptionInfo* exception);
}

/++
 + MinifyImage() is a convenience method that scales an image proportionally
 + to half its size.
 +
 + Params:
 +   image = The image.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDMinifyImage MinifyImage;
    extern (C) alias MDMinifyImage = Image* function(const(Image)* image, ExceptionInfo* exception);
}
else
{
    extern (C) Image* MinifyImage(const Image* image, ExceptionInfo* exception);
}

/++
 + ResizeImage() scales an image to the desired dimensions with the chosen
 + filter.
 +
 + Possible filters are:
 + Bessel, Blackman, Box, Catrom, Cubic, Gaussian, Hanning, Hermite, Lanczos,
 + Mitchell, Point, Quadratic, Sinc, Triangle.
 +
 + Most of the filters are FIR (first impulse response), however, Bessel,
 + Gaussian, and Sinc are IIR (infinite impulse response).  Bessel and Sinc
 + are windowed (brought down to zero) with the Blackman filter.
 +
 + ResizeImage() was inspired by Paul Heckbert's zoom program.
 +
 + Params:
 +   image = The image.
 +   columns = The number of columns in the scaled image.
 +   rows = The number of rows in the scaled image.
 +   filter = Image filter to use.
 +   blur = The blur factor where > 1 is blurry, < 1 is sharp.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDResizeImage ResizeImage;
    extern (C) alias MDResizeImage = Image* function(Image*, const(c_ulong),
            const(c_ulong), const(FilterTypes), const(double), ExceptionInfo*);
}
else
{
    extern (C) Image* ResizeImage(Image* image, const c_ulong columns, const c_ulong rows,
            const FilterTypes filter, const double blur, ExceptionInfo* exception);
}

/++
 + SampleImage() scales an image to the desired dimensions with pixel
 + sampling.
 +
 + Unlike other scaling methods, this method does not introduce any additional
 + color into the scaled image.  SampleImage() is extremely fast and may be
 + used where speed is most important.
 +
 + Params:
 +   image = The image.
 +   columns = The number of columns in the sampled image.
 +   rows = The number of rows in the sampled image.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDSampleImage SampleImage;
    extern (C) alias MDSampleImage = Image* function(const(Image)* image,
            const(c_ulong) columns, const(c_ulong) rows, ExceptionInfo* exception);
}
else
{
    extern (C) Image* SampleImage(const Image* image, const c_ulong columns,
            const c_ulong rows, ExceptionInfo* exception);
}

/++
 + ScaleImage() changes the size of an image to the specified dimensions.
 +
 + This method is reasonably fast, but is not currently multi-threaded and
 + does not support image filters.  The quality of the resized image is
 + sufficient for most purposes.
 +
 + Params:
 +   image = The image.
 +   columns = The number of columns in the scaled image.
 +   rows = The number of rows in the scaled image.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDScaleImage ScaleImage;
    extern (C) alias MDScaleImage = Image* function(const(Image)* image,
            const(c_ulong) columns, const(c_ulong) rows, ExceptionInfo* exception);
}
else
{
    extern (C) Image* ScaleImage(const Image* image, const c_ulong columns,
            const c_ulong rows, ExceptionInfo* exception);
}

/++
 + ThumbnailImage() changes the size of an image to the given dimensions.
 +
 + This method was designed as a low cost thumbnail generator.
 + ThumbnailImage() is typically very fast but an attempt is made to improve
 + quality by first using a simple sampling algorithm for part of the
 + reduction, and then a filtering algorithm to produce the final image.
 +
 + Params:
 +   image = The image.
 +   columns = The number of columns in the scaled image.
 +   rows = The number of rows in the scaled image.
 +   exception = Return any errors or warnings in this structure.
 +/
version (SHARED)
{
    __gshared MDThumbnailImage ThumbnailImage;
    extern (C) alias MDThumbnailImage = Image* function(const(Image)* image,
            const(c_ulong) columns, const(c_ulong) rows, ExceptionInfo* exception);
}
else
{
    extern (C) Image* ThumbnailImage(const Image* image, const c_ulong columns,
            const c_ulong rows, ExceptionInfo* exception);
}

/++
 + ZoomImage() creates a new image that is a scaled size of an existing one.
 +
 + It allocates the memory necessary for the new Image structure and returns
 + a pointer to the new image.  The Point filter gives fast pixel replication,
 + Triangle is equivalent to bi-linear interpolation, and Mitchell gives
 + slower, very high-quality results.  See Graphic Gems III for details on
 + this algorithm.
 +
 + The filter member of the Image structure specifies which filter to use.
 + Blur specifies the blur factor where > 1 is blurry, < 1 is sharp.
 +
 + Params:
 +   image = The image.
 +   columns = The number of columns in the zoom image.
 +   rows = The number of rows in the zoom image.
 +   exception = Return any errors or warnings in this structure.
 +
 + Returns: A pointer to the image after scaling.  A null image is returned if
 + there is a memory shortage.
 +/
version (SHARED)
{
    __gshared MDZoomImage ZoomImage;
    extern (C) alias MDZoomImage = Image* function(const(Image)* image, const(c_ulong) columns,
            const(c_ulong) rows, ExceptionInfo* exception);
}
else
{
    extern (C) Image* ZoomImage(const Image* image, const c_ulong columns,
            const c_ulong rows, ExceptionInfo* exception);
}
