/**
 * Read, write, import, and export images.
 *
 * Authors: ImageMagick Studio, GraphicsMagick Group
 * Copyright: &copy; 2002 ImageMagick Studio,
 *   &copy; 2003 - 2020 GraphicsMagick Group
 * License: See $(LINK2 http://www.graphicsmagick.org/Copyright.html, Copyright.html)
 */
deprecated("Use graphicsmagick_c.magick.constitute instead (remove @ 0.9)")
module magickd.core.c.constitute;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
    import magickd.core.c.common;
    import magickd.core.c.error;
    import magickd.core.c.image;
}

@nogc @system extern(C) nothrow:

version(SHARED)
{
package:
    alias MDPingImage = Image* function(const(ImageInfo*), ExceptionInfo*);
    alias MDReadImage = Image* function(const(ImageInfo*), ExceptionInfo*);
    alias MDWriteImage = MagickPassFail function(const(ImageInfo*) image_info, Image* image);

public:
    extern (D) __gshared MDPingImage PingImage;
    extern (D) __gshared MDReadImage ReadImage;
    extern (D) __gshared MDWriteImage WriteImage;
}
else
{
    /++
     + PingImage() returns all the attributes of an image or image sequence except for the pixels.
     +
     + It is much faster and consumes less memory than $(REF ReadImage)().  On failure, a
     + $(TT null) image is returned and $(PARAM exception) describes the reason for the failure.
     +
     + Params:
     +   image_info = Ping the image defined by the file or filename members of this structure.
     +   exception = Return any errors or warnings in this structure.
     +/
    Image* PingImage(const(ImageInfo*) image_info, ExceptionInfo* exception);

    /++
     + ReadImage() reads an image or image sequence from a file or file handle.
     +
     + The method returns a NULL if there is a memory shortage or if the image
     + cannot be read.
     +
     + Params:
     +   image_info = Read the image defined by the file or filename members of
     + this structure.
     +   exception = Return any errors or warnings in this structure.
     +
     + Returns:
     +   On failure, a NULL image is returned and `exception` describes the reason
     + for the failure.
     +/
    Image* ReadImage(const ImageInfo* image_info, ExceptionInfo* exception);

    /++
     + Use WriteImage() to write an image or an image sequence to a file or file
     + handle.
     +
     + If writing to a file on disk, the name is defined by the `filename` member
     + of the `image` structure.  WriteImage() returns MagickFailure if there is
     + a memory shortage or if the image cannot be written.  Check the exception
     + member of `image` to determine the cause for any failure.
     +
     + Params:
     +   image_info = The image info
     +   image = The image
     +/
    MagickPassFail WriteImage(const(ImageInfo*) image_info, Image* image);
}
