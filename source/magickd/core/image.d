/*
  File:           core/image.d
  Contains:       Image handling methods
  Copyright:      (C) 2021 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

/**
Authors: $(LINK2 https://yume-neru.neocities.org, kaerou)
Copyright: &copy; 2021 kaerou

Macros:
    DEL = <del>$1</del>
    REF = <a href="Image.$1.html">$1</a>
*/
deprecated("No 'image' module anymore. (remove @ 0.9)")
module magickd.core.image;

private
{
    import core.stdc.config : c_ulong;

    import magickd.core.c.common;
    import magickd.core.c.constitute;
    import magickd.core.c.error : ExceptionInfo, GetExceptionInfo, DestroyExceptionInfo;
    import magickd.core.c.image;

    /* rename alias so they don't clash with enum names */
    import magickd.core.c.image : cAlphaType = AlphaType,
        cChannelType = ChannelType,
        cCompositeOperator = CompositeOperator,
        cDisposeType = DisposeType,
        cFilterTypes = FilterTypes,
        cNoiseType = NoiseType;

    import magickd.core.c.image : cImage = Image;
}

enum AlphaType : int
  {
   unspecified = UnspecifiedAlpha,
   associated = AssociatedAlpha,
   unassociated = UnassociatedAlpha
  }

enum ChannelType : int
  {
   undefined = UndefinedChannel,
   /// RGB Red channel
   red = RedChannel,
   /// CMYK Cyan channel
   cyan = CyanChannel,
   /// RGB Green channel
   green = GreenChannel,
   /// CMYK Magenta channel
   magenta = MagentaChannel,
   /// RGB Blue channel
   blue = BlueChannel,
   /// CMYK Yellow channel
   yellow = YellowChannel,
   /// Opacity channel
   opacity = OpacityChannel,
   /// CMYK Black (K) channel
   black = BlackChannel,
   /// Same as Opacity channel (deprecated)
   matte = MatteChannel,
   /// Color channels
   all = AllChannels,
   /// Color channels represent an intensity
   gray = GrayChannel
  }

/**
 * The CompositeOperator is used to select the image composition algorithm
 * used to compose a "composite image" with an "image".
 *
 * By defualt, each of the "composite image" pixels are replaced by the
 * corresponding "image" tile pixel (CompositeOperator.over).  Specify a
 * CompositeOperator to select a different algorithm.
 *
 * The image compositor requires a matte, or alpha channel in the image for
 * some operations.  This extra channel usually defines a mask which
 * represents a sort of cookie-cutter for the image.  This is the case when
 * matte is 255 (full coverage) for pixels inside the shape, zero outside, and
 * between zero and 255 on the boundary.  For certain operations, if "image"
 * does not have a matte channel, it is initialized with `0` for any pixel
 * matching in color to pixel location `(0,0)`, otherwise `255` (to work
 * properly borderWidth must be `0`).
 */
enum CompositeOperator : int
  {
   /**
    * Unset value.
    */
   undefined = UndefinedCompositeOp,
   /**
    * The result is the union of the two image shapes with the
    * "composite image" obscuring the "image" in the regions of overlap.
    */
   over = OverCompositeOp,
   /**
    * The result is simply "composite image" cut by the shape of "image".
    *
    * None of the image data of "image" is included in the result.
    */
   in_ = InCompositeOp,
   /**
    * The resulting image is "composite image" with the shape of "image" cut
    * out.
    */
   out_ = OutCompositeOp,
   /**
    * The result is the same shape as "image", with "composite image"
    * obscuring "image" where the image shapes overlap.
    *
    * Note that this differs from `CompositeOperator.over` because the portion
    * of "composite image" outside of "image"'s shape does not appear in the
    * result.
    */
   atop = AtopCompositeOp,
   /**
    * The result is the image data from both "composite image" and "image"
    * that is outside the overlap region.
    *
    * The overlap region will be blank.
    */
   xor = XorCompositeOp,
   /**
    * The result is just the sum of the image data.
    *
    * Output values are cropped to 255 (no overflow).  This operation is
    * independent of the matte channels.
    */
   plus = PlusCompositeOp,
   /**
    * The result of `"composite image" - "image"`, with overflow cropped to
    * zero.
    *
    * The matte channel is ignored (set to 255, full coverage).
    */
   minus = MinusCompositeOp,
   /**
    * The result of `"composite image" + "image"`, with overflow wrapping
    * around (mod 256).
    */
   add = AddCompositeOp,
   /**
    * The result of `"composite image" - "image"`, with underflow wrapping
    * around (mod 256).
    *
    * The add and subtract operators can be used to perform reversible
    * transformations.
    */
   subtract = SubtractCompositeOp,
   /**
    * The result of `abs("composite image" - "image")`.
    *
    * This is useful for comparing two very similar images.
    */
   difference = DifferenceCompositeOp,
   multiply = MultiplyCompositeOp,
   /**
    * The result image is shaded by "composite image".
    */
   bumpmap = BumpmapCompositeOp,
   /**
    * The resulting image is "image" replaced with "composite image".
    *
    * Here the matte information is ignored.
    */
   copy = CopyCompositeOp,
   /**
    * The resulting image is the red layer in "image" replaced with the red
    * layer in the "composite image".
    *
    * The other layers are copied untouched.
    */
   copyRed = CopyRedCompositeOp,
   /**
    * The resulting image is the green layer in "image" replaced with the
    * green layer in the "composite image".
    *
    * The other layers are copied untouched.
    */
   copyGreen = CopyGreenCompositeOp,
   /**
    * The resulting image is the blue layer in "image" replaced with the
    * blue layer in the "composite image".
    *
    * The other layers are copied untouched.
    */
   copyBlue = CopyBlueCompositeOp,
   /**
    * The resulting image is the matte layer in "image" replaced with the
    * matte layer in the "composite image".
    *
    * The other layers are copied untouched.
    */
   copyOpacity = CopyOpacityCompositeOp,
   /**
    * Pixels in the region are set to transparent.
    */
   clear = ClearCompositeOp,
   dissolve = DissolveCompositeOp,
   displace = DisplaceCompositeOp,
   /**
    * Modulate brighness in HSL space.
    */
   modulate = ModulateCompositeOp,
   threshold = ThresholdCompositeOp,
   /**
    * Do nothing at all.
    */
   none = NoCompositeOp,
   darken = DarkenCompositeOp,
   lighten = LightenCompositeOp,
   /**
    * Copy Hue channel (from HSL colorspace)
    */
   hue = HueCompositeOp,
   /**
    * Copy Saturation channel (from HSL colorspace)
    */
   saturate = SaturateCompositeOp,
   /**
    * Copy Hue and Saturation channels (from HSL colorspace)
    */
   colorize = ColorizeCompositeOp,
   /**
    * Copy Brightness channel (from HSL colorspace)
    */
   luminize = LuminizeCompositeOp,
   /// [Not yet implemented]
   screen = ScreenCompositeOp,
   /// [Not yet implemented]
   overlay = OverlayCompositeOp,
   /**
    * Copy the Cyan channel.
    */
   copyCyan = CopyCyanCompositeOp,
   /**
    * Copy the Magenta channel.
    */
   copyMagenta = CopyMagentaCompositeOp,
   /**
    * Copy the Yellow channel.
    */
   copyYellow = CopyYellowCompositeOp,
   /**
    * Copy the Black channel.
    */
   copyBlack = CopyBlackCompositeOp,
   // the rest don't have documentation...
   divide = DivideCompositeOp,
   hardLight = HardLightCompositeOp,
   exclusion = ExclusionCompositeOp,
   colorDodge = ColorDodgeCompositeOp,
   colorBurn = ColorBurnCompositeOp,
   softLight = SoftLightCompositeOp,
   linearBurn = LinearBurnCompositeOp,
   linearDodge = LinearDodgeCompositeOp,
   linearLight = LinearLightCompositeOp,
   vividLight = VividLightCompositeOp,
   pinLight = PinLightCompositeOp,
   hardMix = HardMixCompositeOp
  }

enum DisposeType : int
  {
   undefined = UndefinedDispose,
   none = NoneDispose,
   background = BackgroundDispose,
   previous = PreviousDispose
  }

enum FilterTypes : int
  {
   undefined = UndefinedFilter,
   point = PointFilter,
   box = BoxFilter,
   triangle = TriangleFilter,
   hermite = HermiteFilter,
   hanning = HanningFilter,
   hamming = HammingFilter,
   blackman = BlackmanFilter,
   gaussian = GaussianFilter,
   cubic = CubicFilter,
   quadratic = QuadraticFilter,
   mitchell = MitchellFilter,
   lanczos = LanczosFilter,
   bessel = BesselFilter,
   sinc = SincFilter
  }

/**
 * NoiseType is used as an argument to select the type of noise to be added to
 * the image.
 */
enum NoiseType : int
  {
   uniform = UniformNoise,
   gaussian = GaussianNoise,
   impulse = ImpulseNoise,
   laplacian = LaplacianNoise,
   random = RandomNoise,
   undefined = UndefinedNoise
  }

/**
 * Specifies the orientation of the image.
 *
 * Useful for when the image is produced via a different ordinate system, the
 * camera was turned on its side, or the page was scanned sideways.
 */
enum Orientation : int
  {
   /// Scanline dir: Unknown; Frame dir: Unknown
   undefined = UndefinedOrientation,
   /// Scanline dir: Left to right; Frame dir: Top to bottom
   topLeft = TopLeftOrientation,
   /// Scanline dir: Right to left; Frame dir: Top to bottom
   topRight = TopRightOrientation,
   /// Scanline dir: Right to left; Frame dir: Bottom to top
   bottomRight = BottomRightOrientation,
   /// Scanline dir: Left to right; Frame dir: Bottom to top
   bottomLeft = BottomLeftOrientation,
   /// Scanline dir: Top to bottom; Frame dir: Left to right
   leftTop = LeftTopOrientation,
   /// Scanline dir: Top to bottom; Frame dir: Right to left
   rightTop = RightTopOrientation,
   /// Scanline dir: Bottom to top; Frame dir: Right to left
   rigthBottom = RightBottomOrientation,
   /// Scanline dir: Bottom to top; Frame dir: Left to right
   leftBottom = LeftBottomOrientation
  }

/**
The Image structure provides an easy to use API for interacting with the GraphicsMagick Core
library.

---
import magickd.core;

int main(string[] args)
{
    // Load the shared library if required.
    version (SHARED) loadGraphicsMagick();

    // Initialize the API.  Can pass `null` if `args` is not available.
    initializeMagick(args[0]);

    // Construct the image object.  Separating image construction from
    // the read operation ensures that a failure to read the image file
    // doesn't render the image object useless.
    Image image;

    try {
        // Determine if Warning exceptions are thrown
        // Use is optional.  Set to true to block Warning exceptions
        image.quiet(false);

        // Read a file into image object
        image.read("girl.gif");

        // Crop the image to specified size (width, height, xOffset, yOffset)
        image.crop( Geometry(100, 100, 100, 100) );

        // Write the image to a file.
    } catch (Exception e) {
        writefln("Caught exception: %s", e.msg);
        return 1;
    }
    return 0;
}
---
*/
struct Image
{
public:

    this(ref return scope Image rhs)
    {
        _imageRef = rhs._imageRef;
        _imageRef._refCount++;
    }

    ~this()
    {
        _imageRef.release();
    }

    /**
    Ping is similar to $(DEL $(REF read)) except it only reads enough of the image to determine
    the number of columns, rows, and file size.

    See_Also: $(REF columns)(), $(REF rows)(), $(DEL $(REF fileSize)()).
    */
    void ping(in string imageSpec)
    {
        ImageInfo* info = CloneImageInfo(null);
        ExceptionInfo exception;
        GetExceptionInfo(&exception);
        scope(exit) DestroyExceptionInfo(&exception);

        if (imageSpec.length < MaxTextExtent) {
            info.filename[0 .. imageSpec.length] = imageSpec;
            info.filename[imageSpec.length] = '\0';
        } else { // >=
            info.filename[0 .. MaxTextExtent] = imageSpec[0 .. MaxTextExtent];
            info.filename[MaxTextExtent-1] = '\0';
        }

        cImage* image = PingImage(info, &exception);
        // TODO: handle exception
        if (_imageRef !is null) _imageRef.release();
        _imageRef = new ImageRef(image, info);
    }

    /**
    The number of columns in the image (i.e. image width in pixels).

    Bugs: Doesn't check if the image is null.
    */
    @trusted
    c_ulong columns() const nothrow
    {
        return _imageRef._image.columns;
    }

    /**
    The file name of the image.

    Returns: A copy of the image's file name.
    */
    @trusted
    string fileName() const nothrow
    {
        import core.stdc.stdlib : free;
        import core.stdc.string : strdup;
        import std.conv : text;
        import std.string : fromStringz;

        char* chars = strdup(_imageRef._imageInfo.filename.ptr);
        scope(exit) free(chars);

        string ret = text( fromStringz(chars));
        return ret;
    }

    /**
    Set the image file name.
    */
    @trusted
    void fileName(in string fileName_) nothrow
    {
        if (fileName_.length < MaxTextExtent) {
            _imageRef._imageInfo.filename[0 .. fileName_.length] = fileName_;
            _imageRef._imageInfo.filename[fileName_.length] = '\0';
        } else {
            _imageRef._imageInfo.filename = fileName_[0 .. MaxTextExtent];
            _imageRef._imageInfo.filename[MaxTextExtent-1] = '\0';
        }
    }

    /**
    The number of rows in the image (i.e. image height in pixels)

    Bugs: Doesn't check if the image is null.
    */
    @trusted
    c_ulong rows() const nothrow
    {
        return _imageRef._image.rows;
    }

package:
    ImageRef* _imageRef;
}

private:

/**
Private structure which contains pointers to the (C) Image and ImageInfo structures.

This is a rather basic form of reference counting, uncertain how it will perform in a
concurrent environment.
*/
struct ImageRef
{
package:
    size_t _refCount = 1;

    cImage* _image = null;
    ImageInfo* _imageInfo = null;

public:

    @disable this(ref return scope ImageRef rhs);

    this(cImage* image, ImageInfo* imageInfo)
    {
        _image = image;
        _imageInfo = imageInfo;
    }

    ~this()
    {
        if (_refCount <= 0) {
            if (_image !is null) {
                DestroyImage(_image);
                _image = null;
            }
            if (_imageInfo !is null) {
                DestroyImageInfo(_imageInfo);
                _imageInfo = null;
            }
        }
    }

    /**
    Decrease the internal reference count.

    Will destroy the object if the reference count is less than or equal to zero.
    */
    @trusted
    void release()
    {
        assert(_refCount >= 0, "Error with internal ImageRef struct");
        _refCount -= 1;

        if (_refCount <= 0) this.destroy();
    }
}
