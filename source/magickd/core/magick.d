/*
  File:           core/magick.d
  Contains:       Methods for handling the GraphicsMagick library.
  Copyright:      (C) 2021 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("No 'magick' module anymore. (remove @ 0.9)")
module magickd.core.magick;

private
{
  import magickd.core.c.magick;
}

/**
 * Convert a D `string` to C "string".
 *
 * This differs from the phobos implementation by being `@nogc`.
 */
@nogc @trusted pure
private char* toStringz(string input) nothrow
{
  import core.memory : pureRealloc, pureMalloc;
  import core.stdc.string : memcpy;

  if (input is null)
    return null;

  size_t len = char.sizeof * input.length;
  char* str = cast(char*)pureMalloc(len);
  scope(failure) pureRealloc(str, 0);
  memcpy(&str[0], input.ptr, len);
  str[len] = '\0';

  return str;
}

unittest
{
  import std.string : fromStringz;

  string a = "alpha";
  auto c = a.toStringz;
  assert(c.fromStringz == a);
  a.destroy();
  assert(a is null);
  assert(c !is null);
  assert(c.fromStringz == "alpha");
}

/**
 * Destroys the Magick environment, releasing all allocated semaphores,
 * memory, and temporary files.
 *
 * This function should be invoked in the primary (original) thread of the
 * application's process while shutting down, and only after any threads
 * which might be using Magick functions have terminated.  Since Magick uses
 * threads internally via OpenMP, it is also necessary for any function calls
 * into Magick to have already returned so that OpenMP worker threads are
 * quiesced and won't be accessing any semaphores or data structures which
 * are destroyed by this function.
 */
deprecated("Memory is now automatically freed. (Remove @ 1.0)")
@nogc @trusted
public void destroyMagick() nothrow
{
  if (true == _md_cleanup_guard_.destroyed)
    return;
  _md_cleanup_guard_.destroyed = true;
  DestroyMagick();
}

/**
 * Initializes the GraphicsMagick environment.
 *
 * `initializeMagick` **must** be invoked by the using program before making
 * use of any other GraphicsMagick functions or else the library will be
 * unusable and any usage is likely to cause a crash.
 *
 * Params:
 *  path = The execution path of the current client (or `null`).
 */
@nogc @trusted
public void initializeMagick(string path) nothrow
{
  /* would GC.addRoot be better? */
  auto s = path.toStringz;

  if (path is null)
    InitializeMagick(null);
  else
    InitializeMagick(s);
}

private:

struct MagickDCleanUpGuard
{
  /* TODO: Remove this when 1.0 */
  bool destroyed = false;

  ~this()
  {
    if (false == destroyed) {
      DestroyMagick();
      destroyed = true;
    }
  }
}

/* I don't like this, though it's similar to what the Magick++ library does. */
__gshared MagickDCleanUpGuard _md_cleanup_guard_;
