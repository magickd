/*
  File:           core/error.d
  Contains:       Error handling
  Copyright:      (C) 2021 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("No 'error' module anymore. (remove @ 0.9)")
module magickd.core.error;

private
{
    import magickd.core.c.error;
    import magickd.core.c.error : cExceptionBaseType = ExceptionBaseType;
    import magickd.core.c.error : cExceptionType = ExceptionType;
}

/* the only difference would be string instead of char* */
public import magickd.core.c.error : ExceptionInfo;

enum ExceptionBaseType : int
{
    undefined = UndefinedExceptionBase,
    exception = ExceptionBase,
    resource = ResourceBase,
    resourceLimit = ResourceLimitBase,
    type = TypeBase,
    annotate = AnnotateBase,
    option = OptionBase,
    delegate_ = DelegateBase,
    missingDelegate = MissingDelegateBase,
    corruptImage = CorruptImageBase,
    fileOpen = FileOpenBase,
    blob = BlobBase,
    stream = StreamBase,
    cache = CacheBase,
    coder = CoderBase,
    module_ = ModuleBase,
    draw = DrawBase,
    render = RenderBase,
    image = ImageBase,
    wand = WandBase,
    temporaryFile = TemporaryFileBase,
    transform = TransformBase,
    XServer = XServerBase,
    X11 = X11Base,
    user = UserBase,
    monitor = MonitorBase,
    locale = LocaleBase,
    deprecate = DeprecateBase,
    registry = RegistryBase,
    configure = ConfigureBase
}

enum ExceptionType : int
{
    undefined = UndefinedException,
    event = EventException,
    exceptionEvent = ExceptionEvent,
    resourceEvent = ResourceEvent,
    resourceLimitEvent = ResourceLimitEvent,
    typeEvent = TypeEvent,
    annotateEvent = AnnotateEvent,
    optionEvent = OptionEvent,
    delegateEvent = DelegateEvent,
    missingDelegateEvent = MissingDelegateEvent,
    corruptImageEvent = CorruptImageEvent,
    fileOpenEvent = FileOpenEvent,
    blobEvent = BlobEvent,
    streamEvent = StreamEvent,
    cacheEvent = CacheEvent,
    coderEvent = CoderEvent,
    moduleEvent = ModuleEvent,
    drawEvent = DrawEvent,
    renderEvent = RenderEvent,
    imageEvent = ImageEvent,
    wandEvent = WandEvent,
    temporaryFileEvent = TemporaryFileEvent,
    transformEvent = TransformEvent,
    XServerEvent = event + ExceptionBaseType.XServer,
    X11Event = event + ExceptionBaseType.X11,
    userEvent = UserEvent,
    monitorEvent = MonitorEvent,
    localeEvent = LocaleEvent,
    deprecateEvent = DeprecateEvent,
    registryEvent = RegistryEvent,
    configureEvent = ConfigureEvent,

    warning = WarningException,
    resourceLimitWarning = ResourceLimitWarning,
    typeWarning = TypeWarning,
    optionWarning = OptionWarning,
    delegateWarning = DelegateWarning,
    missingDelegateWarning = MissingDelegateWarning,
    corruptImageWarning = CorruptImageWarning,
    fileOpenWarning = FileOpenWarning,
    blobWarning = BlobWarning,
    streamWarning = StreamWarning,
    cacheWarning = CacheWarning,
    coderWarning = CoderWarning,
    moduleWarning = ModuleWarning,
    drawWarning = DrawWarning,
    imageWarning = ImageWarning,
    XServerWarning = 380,
    monitorWarning = MonitorWarning,
    registryWarning = RegistryWarning,
    configureWarning = ConfigureWarning,

    error = ErrorException,
    resourceLimitError = ResourceLimitError,
    typeError = TypeError,
    optionError = OptionError,
    delegateError = DelegateError,
    missingDelegateError = MissingDelegateError,
    corruptImageError = CorruptImageError,
    fileOpenError = FileOpenError,
    blobError = BlobError,
    streamError = StreamError,
    cacheError = CacheError,
    coderError = CoderError,
    moduleError = ModuleError,
    drawError = DrawError,
    imageError = ImageError,
    XServerError = 480,
    monitorError = MonitorError,
    registryError = RegistryError,
    configureError = ConfigureError,

    fatal = FatalErrorException,
    resourceLimit = ResourceLimitFatalError,
    type = TypeFatalError,
    option = OptionFatalError,
    delegate_ = DelegateFatalError,
    missingDelegate = MissingDelegateFatalError,
    corruptImage = CorruptImageFatalError,
    fileOpen = FileOpenFatalError,
    blob = BlobFatalError,
    stream = StreamFatalError,
    cache = CacheFatalError,
    coder = CoderFatalError,
    module_ = ModuleFatalError,
    draw = DrawFatalError,
    image = ImageFatalError,
    XServer = XServerFatalError,
    monitor = MonitorFatalError,
    registry = RegistryFatalError,
    configure = ConfigureFatalError
}
