/*
  File:           magickd/pixel_wand.d
  Contains:       An OOP-Style wrapper around the C PixelWand structure.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.pixel_wand;

import std.string : toStringz;

import graphicsmagick_c.magick.image : PixelPacket, Quantum;

import graphicsmagick_c.wand.pixel_wand;
import graphicsmagick_c.wand.pixel_wand : cPixelWand = PixelWand;

///
/// A PixelWand provides access to individual pixel properties, such as
/// colour and opacity.
///
struct PixelWand {
package(magickd):
   cPixelWand* ptr;

   this(cPixelWand* ptr_)
   {
      ptr = ptr_;
   }

   public ~this()
   {
      if (null !is this.ptr) {
         DestroyPixelWand(this.ptr);
      }
   }

   ///
   /// Create a new PixelWand structure.
   ///
   public static PixelWand create()
   {
      return PixelWand(NewPixelWand());
   }

   ///
   /// Returns the normalized black color of the pixel wand.
   ///
   @property public double black()
   {
      return this.getBlack();
   }

   ///
   /// Returns the normalized blue color of the pixel wand.
   ///
   @property public double blue()
   {
      return this.getBlue();
   }

   ///
   /// Returns the color count associated with this pixel wand.
   ///
   @property public ulong colorCount()
   {
      return this.getColorCount();
   }

   ///
   /// Returns the normalized cyan color of the pixel wand.
   ///
   @property public double cyan()
   {
      return this.getCyan();
   }

   ///
   /// Returns the normalized green color of the pixel wand.
   ///
   @property public double green()
   {
      return this.getGreen();
   }

   ///
   /// Returns the normalized magenta color of the pixel wand.
   ///
   @property public double magenta()
   {
      return this.getMagenta();
   }

   ///
   /// Returns the normalized opacity of the pixel wand.
   ///
   @property public double opacity()
   {
      return this.getOpacity();
   }

   ///
   /// Returns the normalized red color of the pixel wand.
   ///
   @property public double red()
   {
      return this.getRed();
   }

   ///
   /// Returns the normalized yellow color of the pixel wand.
   ///
   @property public double yellow()
   {
      return this.getYellow();
   }

   ///
   /// Sets the normalized black color of the pixel wand.
   ///
   @property public void black(double black_)
   {
      this.setBlack(black_);
   }

   ///
   /// Sets the normalized blue color of the pixel wand.
   ///
   @property public void blue(double blue_)
   {
      this.setBlue(blue_);
   }

   ///
   /// Sets the normalized cyan color of the pixel wand.
   ///
   @property public void cyan(double cyan_)
   {
      this.setCyan(cyan_);
   }

   ///
   /// Sets the normalized green color of the pixel wand.
   ///
   @property public void green(double green_)
   {
      this.setGreen(green_);
   }

   ///
   /// Sets the normalized magenta color of the pixel wand.
   ///
   @property public void magenta(double magenta_)
   {
      this.setMagenta(magenta_);
   }

   ///
   /// Sets the normalized opacity of the pixel wand.
   ///
   @property public void opacity(double opacity_)
   {
      this.setOpacity(opacity_);
   }

   ///
   /// Sets the normalized red color of the pixel wand.
   ///
   @property public void red(double red_)
   {
      this.setRed(red_);
   }

   ///
   /// Sets the normalized yellow color of the pixel wand.
   ///
   @property public void yellow(double yellow_)
   {
      this.setYellow(yellow_);
   }

   ///
   /// Sets the color of the pixel wand with a string.
   ///
   /// ```d
   /// PixelWand pixel = PixelWand.create();
   /// // Supports Color Names, Hex Values, and RGB Values
   /// pixel.color = "white";
   /// pixel.color = "#FFFFFF";
   /// pixel.color = "rgb(255, 255, 255)";
   /// ```
   ///
   /// Params:
   ///   color = The pixel wand color
   ///
   /// See_Also: www.graphicsmagick.org/color.html for a full reference
   ///           of colours.
   ///
   @property public void color(string color_)
   {
      this.setColor(color_);
   }

   ///
   /// Returns the black color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumBlack()
   {
      return this.getQuantumBlack();
   }

   ///
   /// Returns the blue color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumBlue()
   {
      return this.getQuantumBlue();
   }

   ///
   /// Return the color of this wand as a packet.
   ///
   @property public PixelPacket quantumColor()
   {
      return this.getQuantumColor();
   }

   ///
   /// Returns the cyan color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumCyan()
   {
      return this.getQuantumCyan();
   }

   ///
   /// Returns the green color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumGreen()
   {
      return this.getQuantumGreen();
   }

   ///
   /// Returns the magenta color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumMagenta()
   {
      return this.getQuantumMagenta();
   }

   ///
   /// Returns the opacity of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumOpacity()
   {
      return this.getQuantumOpacity();
   }

   ///
   /// Returns the red color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumRed()
   {
      return this.getQuantumRed();
   }

   ///
   /// Returns the yellow color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public Quantum quantumYellow()
   {
      return this.getQuantumYellow();
   }

   ///
   /// Sets the black color of the pixel wand.
   ///
   /// The color must be in the range of [0..MaxRGB]
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public void quantumBlack(Quantum black_)
   {
      this.setQuantumBlack(black_);
   }

   ///
   /// Sets the blue color of the pixel wand.
   ///
   /// The color must be in the range of [0..MaxRGB]
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   @property public void quantumBlue(Quantum blue_)
   {
      this.setQuantumBlue(blue_);
   }

   ///
   /// Returns the normalized black color of the pixel wand.
   ///
   public double getBlack()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetBlack(this.ptr);
   }

   ///
   /// Returns the normalized blue color of the pixel wand.
   ///
   public double getBlue()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetBlue(this.ptr);
   }

   ///
   /// Returns the color count associated with this pixel wand.
   ///
   public ulong getColorCount()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetColorCount(this.ptr);
   }

   ///
   /// Returns the normalized cyan color of the pixel wand.
   ///
   public double getCyan()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetCyan(this.ptr);
   }

   ///
   /// Returns the normalized green color of the pixel wand.
   ///
   public double getGreen()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetGreen(this.ptr);
   }

   ///
   /// Returns the normalized magenta color of the pixel wand.
   ///
   public double getMagenta()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetMagenta(this.ptr);
   }

   ///
   /// Returns the normalized opacity of the pixel wand.
   ///
   public double getOpacity()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetOpacity(this.ptr);
   }

   ///
   /// Returns the normalized red color of the pixel wand.
   ///
   public double getRed()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetRed(this.ptr);
   }

   ///
   /// Returns the normalized yellow color of the pixel wand.
   ///
   public double getYellow()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetYellow(this.ptr);
   }

   ///
   /// Returns the black color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumBlack()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetBlackQuantum(this.ptr);
   }

   ///
   /// Returns the blue color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumBlue()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetBlueQuantum(this.ptr);
   }

   ///
   /// Return the color of this wand as a packet.
   ///
   public PixelPacket getQuantumColor()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelPacket packet;
      PixelGetQuantumColor(this.ptr, &packet);
      return packet;
   }

   ///
   /// Returns the cyan color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumCyan()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetCyanQuantum(this.ptr);
   }

   ///
   /// Returns the green color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumGreen()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetGreenQuantum(this.ptr);
   }

   ///
   /// Returns the magenta color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumMagenta()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetMagentaQuantum(this.ptr);
   }

   ///
   /// Returns the opacity of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumOpacity()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetOpacityQuantum(this.ptr);
   }

   ///
   /// Returns the red color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumRed()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetRedQuantum(this.ptr);
   }

   ///
   /// Returns the yellow color of the pixel wand.
   ///
   /// The color is in the range of [0..MaxRGB].
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public Quantum getQuantumYellow()
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      return PixelGetYellowQuantum(this.ptr);
   }

   ///
   /// Sets the normalized black color of the pixel wand.
   ///
   public void setBlack(double black_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetBlack(this.ptr, black_);
   }

   ///
   /// Sets the normalized blue color of the pixel wand.
   ///
   public void setBlue(double blue_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetBlue(this.ptr, blue_);
   }

   ///
   /// Sets the normalized cyan color of the pixel wand.
   ///
   public void setCyan(double cyan_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetCyan(this.ptr, cyan_);
   }

   ///
   /// Sets the normalized green color of the pixel wand.
   ///
   public void setGreen(double green_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetGreen(this.ptr, green_);
   }

   ///
   /// Sets the normalized magenta color of the pixel wand.
   ///
   public void setMagenta(double magenta_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetMagenta(this.ptr, magenta_);
   }

   ///
   /// Sets the normalized opacity of the pixel wand.
   ///
   public void setOpacity(double opacity_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetOpacity(this.ptr, opacity_);
   }

   ///
   /// Sets the normalized red color of the pixel wand.
   ///
   public void setRed(double red_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetRed(this.ptr, red_);
   }

   ///
   /// Sets the normalized yellow color of the pixel wand.
   ///
   public void setYellow(double yellow_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetYellow(this.ptr, yellow_);
   }

   ///
   /// Sets the color of the pixel wand with a string.
   ///
   /// ```d
   /// PixelWand pixel = PixelWand.create();
   /// // Supports Color Names, Hex Values, and RGB Values
   /// pixel.color = "white";
   /// pixel.color = "#FFFFFF";
   /// pixel.color = "rgb(255, 255, 255)";
   /// ```
   ///
   /// Params:
   ///   color = The pixel wand color
   ///
   /// See_Also: www.graphicsmagick.org/color.html for a full reference
   ///           of colours.
   ///
   @property public void setColor(string color_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetColor(this.ptr, toStringz(color_));
   }

   ///
   /// Sets the black color of the pixel wand.
   ///
   /// The color must be in the range of [0..MaxRGB]
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public void setQuantumBlack(Quantum black_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetBlackQuantum(this.ptr, black_);
   }

   ///
   /// Sets the blue color of the pixel wand.
   ///
   /// The color must be in the range of [0..MaxRGB]
   ///
   /// MaxRGB = [Q8: `255`, Q16: `65_535`, Q32: `4_294_967_295`].
   ///
   public void setQuantumBlue(Quantum blue_)
   in {
      assert(null !is this.ptr, "Attempting to use a PixelWand that hasn't been created.");
   }
   do {
      PixelSetBlueQuantum(this.ptr, blue_);
   }
}
