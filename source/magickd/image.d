/*
  File:           magickd/image.d
  Contains:       A class which wraps the GraphicsMagick Image structure.
  Copyright:      (C) 2023 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("Please use the MagickWand API provided by MagickD (remove @ 0.9)")
module magickd.image;

import graphicsmagick_c.magick.constitute;
import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image : CompressionType, NoiseType, CloneImageInfo;
import graphicsmagick_c.magick.image : cImage = Image, cImageInfo = ImageInfo;

import magickd.exception;
import magickd.image_info;

class Image {
    package(magickd) cImage *handle = null;
    package(magickd) cImageInfo *info = null;

    this() {
        this.info = CloneImageInfo(null);
    }

    this(ImageInfo info_) {
        this.read(info_);
        info = CloneImageInfo(null);
    }

    ~this() {
        import graphicsmagick_c.magick.image : DestroyImage, DestroyImageInfo;

        if (null !is handle) {
            DestroyImage(handle);
        }

        if (null !is info) {
            DestroyImageInfo(info);
        }
    }

    void read(ImageInfo info_) {
        ExceptionInfo exception;
        GetExceptionInfo(&exception);
        handle = ReadImage(info_.handle, &exception);
        if (UndefinedException != exception.severity) {
            throw new MagickAPIException(exception);
        }
    }

    void ping(ImageInfo info_) {
        ExceptionInfo exception;
        GetExceptionInfo(&exception);
        handle = PingImage(info_.handle, &exception);
        if (UndefinedException != exception.severity) {
            throw new MagickAPIException(exception);
        }
    }

    /**
     * The time in 1/100ths of a second (0 to 65535) which must expire before
     * displaying the next image in an animated sequence.
     *
     * This option is useful for regulating the animation of a sequence of
     * GIF images within Netscape.
     *
     * Returns: The delay between frames.
     */
    @property ulong animationDelay() const {
        return handle.delay;
    }

    @property void animationDelay(ulong delay) {
        import core.stdc.config : c_ulong;

        // c_ulong could be a uint.

        if (delay > c_ulong.max) {
            handle.delay = c_ulong.max;
        } else {
            handle.delay = delay;
        }
    }

    @property CompressionType compressionType() const {
        return handle.compression;
    }

    @property void compressionType(CompressionType compressType_) {
        handle.compression = compressType_;
    }

    @property ulong columns() {
        if (null is handle)
            return ulong.min;

        return handle.columns;
    }

    @property ulong rows() {
        if (null is handle)
            return ulong.min;

        return handle.rows;
    }

    @property string filename() {
        import core.stdc.string : strlen;
        import std.algorithm.mutation : copy;

        size_t len = strlen(handle.filename.ptr);
        string ret;
        ret.reserve(len);
        ret.length = len;


        copy(handle.filename[0..len], cast(char[])ret);
        return ret;
    }

    @property void filename(string value) {
        import std.algorithm.mutation : copy;

        import graphicsmagick_c.magick.api : MaxTextExtent;

        string nullTerm = value ~ '\0';
        size_t length = (nullTerm.length >= MaxTextExtent) ? (MaxTextExtent - 1) : nullTerm.length;

        copy(nullTerm[0..length], handle.filename[0..length]);
    }

    @property string magickFilename() {
        import core.stdc.string : strlen;
        import std.algorithm.mutation : copy;

        size_t len = strlen(handle.magick_filename.ptr);
        string ret;
        ret.reserve(len);
        ret.length = len;

        copy(handle.magick_filename[0..len], cast(char[])ret);
        return ret;
    }

    /**
     * Add noise to the image with the specified noise type.
     *
     * Params:
     *   noiseType = The type of noise to use.
     */
    public void addNoise(NoiseType noiseType) {
        import graphicsmagick_c.magick.effect : AddNoiseImage;
        import graphicsmagick_c.magick.image : DestroyImage;

        ExceptionInfo exception;
        GetExceptionInfo(&exception);

        cImage* orig = this.handle;
        cImage* repl = AddNoiseImage(orig, noiseType, &exception);

        if (UndefinedException != exception.severity) {
            throw new MagickAPIException(exception);
        }
        this.handle = repl;
        DestroyImage(orig);
    }

    /**
     * Blur an image with the specified blur factor.
     *
     * Params:
     *   radius = The radius of the Gaussian, in pixels, not counting the center pixel.
     *   stigma = The standard deviation of the Laplacian, in pixels.
     */
    public void blur(double radius = 0.0, double stigma = 1.0) {
        import graphicsmagick_c.magick.effect : BlurImage;
        import graphicsmagick_c.magick.image : DestroyImage;

        ExceptionInfo exception;
        GetExceptionInfo(&exception);

        cImage* orig = this.handle;
        cImage* repl = BlurImage(orig, radius, stigma, &exception);

        if (UndefinedException != exception.severity) {
            throw new MagickAPIException(exception);
        }
        this.handle = repl;
        DestroyImage(orig);
    }

    public void write(string filename) {
        import graphicsmagick_c.magick.image : CloneImageInfo, DestroyImageInfo;
        import graphicsmagick_c.magick.constitute : WriteImage;

        this.filename = filename;

        WriteImage(info, handle);
    }
}
