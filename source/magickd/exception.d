/*
  File:           magickd/exception.d
  Contains:       Exception handling within the magickd package.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.exception;

import std.string : fromStringz;

import graphicsmagick_c.magick.error : ExceptionInfo;

///
/// Base exception class encompassing all MagickD exceptions.
///
/// Author: Mio
/// See_Also: MagickAPIException
///
class MagickException : Exception
{
package(magickd):

   ///
   /// Construct an exception
   ///
   public this(string msg)
   {
      super(msg);
   }
}

///
/// Encapsulates a GraphicsMagick ExceptionInfo structure.
///
/// Used whenever a MagickWand/PixelWand/DrawingWand throw an
/// exception in the C API.
///
/// Author: Mio
///
class MagickAPIException : MagickException
{
package(magickd):

   ///
   /// Descibes the exception that has occurred.
   ///
   private string m_description;

   ///
   /// Reason for the exception being thrown.
   ///
   private string m_reason;

   ///
   /// Severity of the exception
   ///
   private int m_severity;

   ///
   /// Construct an API exception
   ///
   /// Params:
   ///   exception = The ExceptionInfo structure from GraphicsMagick.
   ///
   this(ref ExceptionInfo exception)
   {
      m_severity = exception.severity;
      m_reason = fromStringz(exception.reason).dup;
      m_description = fromStringz(exception.description).dup;
      super(m_reason);
   }

   ///
   /// Descibes the exception that has occurred.
   ///
   @property public string description() const
   {
      return m_description;
   }

   ///
   /// Reason for the exception being thrown.
   ///
   @property public string reason() const
   {
      return m_reason;
   }

   ///
   /// Severity of the exception
   ///
   @property public int severity() const
   {
      return m_severity;
   }

   ///
   /// Descibes the exception that has occurred.
   ///
   public string getDescription() const
   {
      return m_description;
   }

   ///
   /// Reason for the exception being thrown.
   ///
   public string getReason() const
   {
      return m_reason;
   }

   ///
   /// Severity of the exception
   ///
   public int getSeverity() const
   {
      return m_severity;
   }
}
