/*
  File:           magickd/initialisation.d
  Contains:       Functions related to initialsing the GraphicsMagick C library.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.initialisation;

import std.string : toStringz;

import graphicsmagick_c.magick : InitializeMagick, DestroyMagick;

debug(magickd)
{
   import std.experimental.logger;
}

///
/// Initialize the GraphicsMagick environment.
///
/// initializeMagick **must** be invoked before making use of the magickd
/// API, otherwise the library will be unusable and any usage will likely
/// result in a crash at runtime.
///
/// Params:
///   path = The execution path of the current GraphicsMagick client.
///          (Can be `null`)
///
void initializeMagick(in string path = null)
{
   debug(magickd) infof("path = ", path is null ? "(null)" : path);
   if (null !is path) {
      InitializeMagick(toStringz(path));
   } else {
      InitializeMagick(null);
   }
}

///
/// Destroys the GraphicsMagick environment.
///
/// Calling this function will release all memory, temporary files, and
/// allocated semaphores.
///
/// Deprecated: MagickD will automatically destroy the GraphicsMagick
///             environment.  If you still want to manually destroy the
///             environment yourself, you can call
///             `graphicsmagick_c.magickd.DestroyMagick`.  This function
///             will be removed with MagickD version 0.9.
///
deprecated("MagickD automatically destroys the GraphicsMagick environment (remove @ 0.9)")
void destroyMagick()
{
   DestroyMagick();
}

/// An enumeration for the potential return values of `loadGraphicsMagick`.
enum MDLoadStatus
{
   /// Loading was successful.
   success,
   /// Some symbols could not be found in the libraries.
   badLibrary,
   /// We couldn't find all of the GraphicsMagick libraries.
   noLibrary,
}

package(magickd):

///
/// Load the GraphicsMagick/GraphicsMagickWand libraries.
///
/// Returns:
///    A value of `MDLoadStatus`, depending on how successful the loading of the
///    library was.
///
///    If loading was successful, then `MDLoadStatus.success` is returned.
///
///    If the libraries (GraphicsMagick and GraphicsMagickWand) could not be
///    found on the system, then `MDLoadStatus.noLibrary` is returned.
///
///    `MDLoadStatus.badLibrary` is returned when the libraries *could* be found,
///    but one or more of the symbols could not be found.
///
///    **NOTE**: When using the static version of magickd, this will always
///    return `MDLoadStatus.success`.
///
MDLoadStatus loadGraphicsMagick()
{
   import graphicsmagick_c.magick.magick : InitializeMagick;
   debug(magickd) trace("Loading GraphicsMagick");

   version (GMagick_Dynamic) {
      import graphicsmagick_c.config : _loadGraphicsMagick = loadGraphicsMagick,
         loadGraphicsMagickWand;

      void* lib;

      bool success = _loadGraphicsMagick(lib);
      if (null is lib) {
         return MDLoadStatus.noLibrary;
      }

      success &= loadGraphicsMagickWand(lib);
      if (null is lib) {
         return MDLoadStatus.noLibrary;
      }

      return success ? MDLoadStatus.success : MDLoadStatus.badLibrary;
   } else {

      return MDLoadStatus.success;
   }
}

/// Unload the GraphcisMagick library.
///
/// This is equivalent to calling `DestroyMagick`.
void unloadGraphicsMagick()
{
   import graphicsmagick_c.magick.magick : DestroyMagick;
   debug(magickd) trace("Unloading GraphicsMagick");

   DestroyMagick();
}
