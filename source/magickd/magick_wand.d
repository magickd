/*
  File:           magickd/magick_wand.d
  Contains:       An OOP-Style wrapper around the C MagickWand structure.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.magick_wand;

import core.stdc.config : c_ulong;
import core.stdc.stdlib : free;

import std.string : fromStringz, toStringz;

import graphicsmagick_c.magick.api : MagickTrue;
import graphicsmagick_c.magick.error : ExceptionType;
import graphicsmagick_c.wand.magick_wand;
import graphicsmagick_c.wand.magick_wand : cMagickWand = MagickWand;

import magickd.drawing_wand;
import magickd.enums;

debug (magickd) {
   import std.experimental.logger;
}

/// MagickWand is a D-Style (OOP) wrapper to the C MagickWand structure.
///
/// Creating a `MagickWand` will construct a GraphicsMagick MagickWand.
struct MagickWand {
package(magickd):

   cMagickWand* ptr = null;

   ///
   /// Create a new MagickWand structure.
   ///
   public static MagickWand create()
   {
      cMagickWand* wand = NewMagickWand();
      // TODO: Exception if null is wand.
      return MagickWand(wand);
   }

   ///
   /// Create a new MagickWand structure using a valid C MagickWand*.
   ///
   public static MagickWand createFromWand(cMagickWand* wand)
   in {
      assert(null !is wand, "Attempting to create a MagickWand from null pointer.");
   }
   do {
      return MagickWand(wand);
   }

   this(cMagickWand* ptr)
   {
      this.ptr = ptr;
   }

   public ~this()
   {
      if (null !is ptr) {
         debug (magickd)
            trace("Destroying MagickWand");
         DestroyMagickWand(ptr);
      }
   }

   ///
   /// Returns the image disposal method for the current image.
   ///
   @property public DisposeType imageDispose()
   {
      return this.getImageDispose();
   }

   ///
   /// Sets the image disposal method.
   ///
   /// Params:
   ///   dispose = The image disposal method.
   ///
   @property public void imageDispose(DisposeType dispose)
   {
      this.setImageDispose(dispose);
   }

   ///
   /// Set the image iterations.
   ///
   /// Params:
   ///   iterations = The number of iterations.
   ///
   @property public void imageIterations(ulong iterations)
   {
      this.setImageIterations(iterations);
   }

   ///
   /// Select an invividual threshold for each pixel based on the range of
   /// intensity values in its local neighbourhood.  This allows for
   /// thresholding of an image whose global intensity histogram doesn't
   /// contain distictive peaks.
   ///
   /// Params:
   ///    width  = The width of the local neighbourhood.
   ///    height = The height of the local neighbourhood.
   ///    offset = The mean offset.
   ///
   public void adaptiveThresholdImage(in ulong width, in ulong height, in ulong offset)
   in {
      assert(null !is ptr, "Trying to use a MagickWand that hasn't been created.");
   }
   do {
      MagickAdaptiveThresholdImage(this.ptr, width, height, offset);
   }

   ///
   /// Add all the images from *wand* at the current image location.
   ///
   public void addImage(const scope ref MagickWand wand)
   in {
      assert(null !is ptr, "Trying to add a to a MagickWand that hasn't been created.");
      assert(null !is wand.ptr, "Trying to add a MagickWand that hasn't been created.");
   }
   do {
      MagickAddImage(ptr, wand.ptr);
      // TODO: Throw exception if MagickAddImage returns False.
   }

   ///
   /// Annotates an image with text.
   ///
   /// Params:
   ///    draw  = The drawing wand which specifies colors, fonts, etc.
   ///    x     = The X ordinate to left of text.
   ///    y     = The Y ordinate to text baseline.
   ///    angle = Rotate text relative to this angle.
   ///    text  = The text to draw.
   ///
   public void annotateImage(const ref DrawingWand draw, double x, double y,
         double angle, string text)
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
      assert(null !is draw.ptr, "Attempting to use a DrawingWand that hasn't been created.");
   }
   do {
      MagickAnnotateImage(this.ptr, draw.ptr, x, y, angle, toStringz(text));
   }

   ///
   /// Blur the current image.
   ///
   /// GraphicsMagick will convolve the image with a Gaussian operator of the
   /// given *radius* and standard deviation (*sigma*).  For reasonable results,
   /// the *radius* should be larger than *sigma* . Use a *radius* of `0` and
   /// `blurImage()` selects a suitable radius for you.
   ///
   /// Params:
   ///   radius = The radius of the Gaussian, in pixels, not counting the center
   ///            pixel.
   ///   sigma = The standard deviation of the Gaussian, in pixels.
   ///
   public void blurImage(double radius, double sigma = 1.0)
   in {
      assert(null !is ptr, "Trying to use a MagickWand that hasn't been created.");
   }
   do {
      MagickBlurImage(this.ptr, radius, sigma);
   }

   ///
   /// Composites a set of images while respecting any page offsets and
   /// disposal methods.
   ///
   /// GIF, MIFF, and MNG animation sequences typically start with an image
   /// background and each subsequent image varies in size and offset.
   ///
   /// Returns: A new sequence where each image in the sequence is the same
   ///          size as the first and composited with the next image in the
   ///          sequence.
   ///
   public MagickWand coalesceImages()
   in {
      assert(null !is ptr, "Trying to use a MagickWand that hasn't been created.");
   }
   do {
      cMagickWand* _ptr = MagickCoalesceImages(this.ptr);
      // TODO: Handle exceptions
      return MagickWand(_ptr);
   }

   ///
   /// Retrieve the severity, reason, and description of any error that occurs
   /// when using other methods in this API.
   ///
   /// Params:
   ///   severity = The severity of the error is returned here.
   ///
   /// Returns:
   ///   A string containing the reason and description of the error.
   ///
   public string getException(out ExceptionType severity) nothrow
   in {
      assert(null !is ptr, "Trying to retrieve exception from MagickWand that hasn't been created.");
   }
   do {
      char* cstring;
      string reason;

      cstring = MagickGetException(ptr, &severity);

      reason = fromStringz(cstring).dup;
      free(cstring);
      return reason;
   }

   ///
   /// Returns the image disposal method for the current image.
   ///
   public DisposeType getImageDispose() nothrow
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      return cast(DisposeType)MagickGetImageDispose(this.ptr);
   }

   ///
   /// Returns the index of the current image.
   ///
   public long getImageIndex() nothrow
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      return MagickGetImageIndex(this.ptr);
   }

   ///
   /// Returns the number of images associated with this MagickWand.
   ///
   public ulong getNumberImages() nothrow
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      return MagickGetNumberImages(this.ptr);
   }

   ///
   /// Retrieve the size associated with this MagickWand.
   ///
   /// Params:
   ///   columns = The width in pixels.
   ///   rows = The height in pixels.
   ///
   public void getSize(out size_t columns, out size_t rows) nothrow
   in {
      assert(null !is this.ptr, "Trying to get size from a MagickWand that hasn't been created.");
   }
   do {
      size_t r, c;
      MagickGetSize(this.ptr, &c, &r);
      columns = c;
      rows = r;
   }

   ///
   /// Returns `true` if the wand has more images when traversing the list
   /// in a forward direction.
   ///
   public bool hasNextImage() nothrow
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      return MagickHasNextImage(this.ptr) == MagickTrue ? true : false;
   }

   ///
   /// Returns `true` if the wand has more images when traversing the list
   /// in the reverse direction.
   ///
   public bool hasPreviousImage() nothrow
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      return MagickHasPreviousImage(this.ptr) == MagickTrue ? true : false;
   }

   ///
   /// Associate the next image in the image list with a MagickWand.
   ///
   public void nextImage()
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickNextImage(this.ptr);
   }

   ///
   /// Similar to readImage, except the only valid information returned is
   /// the image width, height, size, and format.
   ///
   /// pingImage is designed to efficiently obtain this information from a
   /// file, without reading the entire image sequence in to memory.
   ///
   public void pingImage(string filename)
   in {
      assert(null !is this.ptr, "Trying to ping from a MagickWand that hasn't been created.");
   }
   do {
      MagickPingImage(this.ptr, toStringz(filename));
      // TODO: Throw exception if MagickPingImage returns false.
   }

   ///
   /// Selects the previous image associated with a magick wand.
   ///
   public void previousImage()
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickPreviousImage(this.ptr);
   }

   ///
   /// Read an image or an image sequence
   ///
   public void readImage(string filename)
   in {
      assert(null !is this.ptr, "Trying to read to a MagickWand that hasn't been created.");
   }
   do {
      MagickReadImage(this.ptr, toStringz(filename));
      // TODO: Throw exception if MagickReadImage returns false.
   }

   ///
   /// Scale an image to the desired dimensions with the desired filter.
   ///
   /// Most filters are FIR (finite impulse response), however, Bessel, Gaussian,
   /// and Sinc are IIR (infinite impulse response). Bessel and Sinc are windowed
   /// (brought down to zero) with the Blackman filter.
   ///
   /// Params:
   ///  columns = The number of columns in the scaled image (i.e. new width)
   ///  rows = The number of rows in the scaled image (i.e. height)
   ///  filterType = Image filter to use
   ///  blur = The blur factor where >1 is blurry, and <1 is sharp.
   ///
   public void resizeImage(size_t columns, size_t rows, FilterType filterType, double blur)
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickResizeImage(this.ptr, columns, rows, filterType, blur);
   }

   ///
   /// Sets the image disposal method.
   ///
   /// Params:
   ///   dispose = The image disposal method.
   ///
   public void setImageDispose(DisposeType dispose)
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickSetImageDispose(this.ptr, dispose);
   }

   ///
   /// Sets the format of a particular image in a sequence.
   ///
   /// The format is designated by a magick string (e.g. "GIF").
   ///
   /// Params:
   ///   format = The image format.
   ///
   public void setImageFormat(string format_)
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickSetImageFormat(this.ptr, toStringz(format_));
   }

   ///
   /// Set the image iterations.
   ///
   /// Params:
   ///   iterations = The number of iterations.
   ///
   public void setImageIterations(ulong iterations)
   in {
      assert(null !is this.ptr, "Attempting to use a MagickWand that hasn't been created.");
   }
   do {
      MagickSetImageIterations(this.ptr, iterations);
   }

   ///
   /// Writes an image to the provided *filename*.
   ///
   /// Params:
   ///   filename = The image filename to write to.
   ///
   public void writeImage(string filename)
   in {
      assert(null !is this.ptr, "Trying to write a MagickWand that hasn't been created.");
   }
   do {
      MagickWriteImage(this.ptr, toStringz(filename));
      // TODO: Throw exception if MagickWriteImage returns False.
   }

   ///
   /// Write an image or image sequence.
   ///
   /// If the wand represents an image sequence, then it is written starting
   /// at the first frame in the sequence.
   ///
   /// Params:
   ///    filename = The image filename to write to
   ///    adjoin = Join the images in to a single multi-image file.
   ///
   public void writeImages(string filename, bool adjoin)
   in {
      assert(null !is this.ptr, "Trying to write a MagickWand that hasn't been created.");
   }
   do {
      MagickWriteImages(this.ptr, toStringz(filename), adjoin);
      // TODO: Throw exception if MagickWriteImages returns False.
   }

   ///
   /// Retrieve the severity of any error that occurs when using other
   /// methods in this API.
   ///
   @property public ExceptionType exceptionType()
   {
      ExceptionType ret;
      getException(ret);
      return ret;
   }

   @property public size_t width()
   in {
      assert(null !is this.ptr, "Trying to get width from a MagickWand that hasn't been created.");
   }
   do {
      return MagickGetImageWidth(this.ptr);
   }

   @property public size_t height()
   in {
      assert(null !is this.ptr, "Trying to get height from a MagickWand that hasn't been created.");
   }
   do {
      return MagickGetImageHeight(this.ptr);
   }

   ///
   /// Get the filename associated with an image sequence.
   ///
   @property public string filename()
   in {
      assert(null !is this.ptr,
            "Trying to get filename from a MagickWand that hasn't been created.");
   }
   do {
      char* cstring = MagickGetFilename(this.ptr);
      string filename_ = fromStringz(cstring).dup;
      free(cstring);
      return filename_;
   }

   ///
   /// Set the filename before you read or write the image.
   ///
   /// Params:
   ///   filename_ = The image filename
   ///
   @property public void filename(string filename_)
   in {
      assert(null !is this.ptr, "Trying to set filename for a MagickWand that hasn't been created.");
   }
   do {
      MagickSetFilename(this.ptr, toStringz(filename_));
   }

   @property public string currentFilename()
   in {
      assert(null !is this.ptr,
            "Trying to get image filename for a MagickWand that hasn't been created.");
   }
   do {
      char* cstring = MagickGetImageFilename(this.ptr);
      string filename_ = fromStringz(cstring).dup;
      free(cstring);
      return filename_;
   }

   @property public ulong delay()
   in {
      assert(null !is this.ptr,
            "Trying to get image delay from a MagickWand that hasn't been created.");
   }
   do {
      return MagickGetImageDelay(this.ptr);
   }

   @property public void delay(ulong delay_)
   in {
      assert(null !is this.ptr,
            "Trying to set image delay from a MagickWand that hasn't been created.");
   }
   do {
      MagickSetImageDelay(this.ptr, delay_);
   }

   @property public string format()
   in {
      assert(null !is this.ptr, "Trying to get format of a MagickWand that hasn't been created.");
   }
   do {
      char* cstring = MagickGetImageFormat(this.ptr);
      string format_ = fromStringz(cstring).dup;
      free(cstring);
      return format_;
   }

   @property public void format(string format_)
   {
      setImageFormat(format_);
   }
}
