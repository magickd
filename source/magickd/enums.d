/*
  File:           magickd/enums.d
  Contains:       D-Style enum wrapper for GraphicsMagick enums.
  Copyright:      (C) 2023 Mio

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module magickd.enums;

import graphicsmagick_c.magick.image;

enum FilterType {
   undefined = UndefinedFilter,
   point = PointFilter,
   box = BoxFilter,
   triangle = TriangleFilter,
   hermite = HermiteFilter,
   hanning = HanningFilter,
   hamming = HammingFilter,
   blackman = BlackmanFilter,
   gaussian = GaussianFilter,
   quadratic = QuadraticFilter,
   cubic = CubicFilter,
   catrom = CatromFilter,
   mitchell = MitchellFilter,
   lanczos = LanczosFilter,
   bessel = BesselFilter,
   sinc = SincFilter
}

enum DisposeType {
   undefined = UndefinedDispose,
   none = NoneDispose,
   background = BackgroundDispose,
   previous = PreviousDispose
}
