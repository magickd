/**
 * Wand pixel access/update interfaces.
 *
 * Authors: ImageMagick Studio, GraphicsMagick Group
 * Copyright: &copy; 2003 ImageMagick Studio, &copy; 2003 - 2009 GraphicsMagick Group
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 */
deprecated("Use graphicsmagick_c.wand.pixel_wand instead (remove @ 0.9)")
module magickd.wand.c.pixel_wand;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

@nogc @system extern(C) nothrow:

/// PixelWand struct, defined externally.
extern struct PixelWand;

version (SHARED)
{
  package:
    alias MDDestroyPixelWand = void function(PixelWand*);
    alias MDNewPixelWand = PixelWand* function();
    alias MDPixelSetColor = uint function(PixelWand* wand, const scope char* color);

  public:
    extern(D) __gshared MDDestroyPixelWand DestroyPixelWand;
    extern(D) __gshared MDNewPixelWand NewPixelWand;
    extern(D) __gshared MDPixelSetColor PixelSetColor;
}
else
{
    /++
     + DestroyPixelWand() deallocates resources associated with a PixelWand.
     +
     + Params:
     +   wand = The pixel wand.
     +/
    void DestroyPixelWand(PixelWand* wand);

    /++
     + NewPixelWand() returns a new pixel wand.
     +/
    PixelWand* NewPixelWand();

    /++
     + PixelSetColor() sets the color of the pixel wand with a string.
     +
     + Example:
     + ---
     + import std.string : toStringz;
     +
     + auto wand = NewPixelWand();
     + PixelSetColor(wand, "blue".toStringz);
     + PixelSetColor(wand, "#0000ff".toStringz);
     + PixelSetColor(wand, "rgb(0,0,255)".toStringz);
     + ---
     +/
    uint PixelSetColor(PixelWand* wand, const scope char* color);
}
