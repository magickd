/*               Copyright (C) kaerou 2021.
** Distributed under the Boost Software License, Version 1.0.
**    (See accompanying file LICENSE_1_0.txt or copy at
**          https://www.boost.org/LICENSE_1_0.txt)
*/

/**
 * The GraphicsMagick Wand C library provides a mid-level abstract C
 * language programming interface for GraphicsMagick.
 *
 * It is originally based on the Wand API provided in ImageMagick as of
 * August 2003.  After August 2003, ImageMagick changed its license to one
 * unusable by GraphicsMagick so this version of the Wand library is not
 * completely in sync with the current ImageMagick version.
 *
 * This API is divided into a number of categories.  While reading this
 * documentation, please reference the
 * $(LINK2 http://www.graphicsmagick.org/api/types.html, types)
 * documentation as required:
 *
 * $(UL
 *   $(LI $(LINK2 /magickd/wand/c/drawing_wand.html, DrawingWand):
 *     Wand vector drawing interfaces.
 *    )
 *   $(LI $(LINK2 /magickd/wand/c/magick_wand.html, MagickWand):
 *     Wand image processing interfaces.
 *    )
 *   $(LI $(LINK2 /magickd/wand/c/pixel_wand.html, PixelWand):
 *     Wand pixel access/update interfaces.
 *    )
 * )
 *
 * The following is a simple example program which (assuming the program
 * name is $(I rotate)) is executed similar to $(I rotate infile outfile).
 * It reads from file $(I infile), rotates the image 30 degrees using a
 * black background, and writes the result to file $(I outfile):
 *
 * ---
 * // Copyright (C) 2002-2021 GraphicsMagick Group
 * // See http://www.graphicsmagick.org/Copyright.html for full details.
 * import std.stdio;
 * import std.string;
 *
 * import magickd.wand.c;
 *
 * int main(string[] args)
 * {
 *     MagickWand* magick_wand;
 *     MagickPassFail status = MagickPass;
 *     string infile, outfile;
 *
 *     version(SHARED)
 *     {
 *         // Support for shared library loading
 *         loadGraphicsMagick();
 *         loadGraphicsMagickWand();
 *     }
 *
 *     if (args.length != 3)
 *     {
 *         stderr.writefln("usage: %s infile outfile", args[0]);
 *         return 0;
 *     }
 *
 *     infile = args[1];
 *     outfile = args[2];
 *
 *     // Initialize GraphicsMagick API
 *     InitializeMagick(args[0].ptr);
 *
 *     // Allocate Wand handle
 *     magick_wand = NewMagickWand();
 *
 *     // Read input image file
 *     if (status == MagickPass)
 *     {
 *         status = MagickReadImage(magick_wand, infile.ptr);
 *     }
 *
 *     // Rotate image clockwise 30 degrees with black background
 *     if (status == MagickPass)
 *     {
 *         PixelWand* background;
 *         background = NewPixelWand();
 *         PixelSetColor(background, "#000000");
 *         status = MagickRotateImage(magick_wand, background, 30);
 *         DestroyPixelWand(background);
 *     }
 *
 *     // Write output file
 *     if (status == MagickPass)
 *     {
 *         status = MagickWriteImage(magick_wand, outfile);
 *     }
 *
 *     // Diagnose any error
 *     if (status != MagickPass)
 *     {
 *         char* description;
 *         ExceptionType severity;
 *
 *         description = MagickGetException(magick_wand, &severity);
 *         stderr.writefln("%s (severity %d)", description.fromStringz,
 *                         severity);
 *     }
 *
 *     // Release Wand handle
 *     DestroyMagickWand(magick_wand);
 *
 *     // Destroy GraphicsMagick API
 *     DestroyMagick();
 *
 *     return (status == MagickPass ? 0 : 1);
 * }
 * ---
 *
 * To compile on Unix, the command looks something like this:
 *
 * $(PRE_C sh, gcc -lGraphicsMagick -lGraphicsMagickWand rotate.d -o rotate)
 *
 * A note on shared library loading: $(TT SHARED) is used in the example
 * because that is what $(TT magickd) uses internally, you can of course use
 * another identifier &mdash; so long as $(TT magickd) is built with
 * $(TT SHARED).
 *
 * Authors: GraphicsMagick Group
 * Copyright: &copy; GraphicsMagick Group 2002 - 2021
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 *
 * Macros:
 *     PRE_C = <pre class="code"><code class="lang-$1">$2</code></pre>
 *     TT = <tt>$1</tt>
 */
deprecated("Use graphicsmagick_c.wand instead (remove @ 0.9)")
module magickd.wand.c;

public
{
  import magickd.wand.c.drawing_wand;
  import magickd.wand.c.magick_wand;
  import magickd.wand.c.pixel_wand;
}
