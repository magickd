/**
 * Wand image processing interfaces.
 *
 * Authors: GraphicsMagick Group
 * Copyright: &copy; 2003 - 2019 GraphicsMagick Group
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 */
deprecated("Use graphicsmagick_c.wand.magick_wand (remove @ 0.9)")
module magickd.wand.c.magick_wand;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

private
{
  import core.stdc.config : c_long, c_ulong;
  import core.stdc.stdio : FILE;

  import magickd.core.c.compare : MetricType;
  import magickd.core.c.error : ExceptionInfo, ExceptionType;
  import magickd.core.c.image;
  import magickd.wand.c.drawing_wand : DrawingWand;
  import magickd.wand.c.pixel_wand : PixelWand;
}

version(SHARED) version=MD_DOC;
version(D_Ddoc) version=MD_DOC;

version(MD_DOC) {
    @nogc nothrow:

    public import magickd.core.c.magick : bindSymbol, loadGraphicsMagick;

    /**
    Loads the shared GraphicsMagickWand library.

    ---
    // Load GraphicsMagick (Core) first
    void* gmHandle = loadGraphicsMagick();
    assert(gmHandle !is null, "Failed to load GraphicsMagick Library");

    // Load GraphicsMagickWand
    void* gmwHandle = loadGraphicsMagickWand();
    assert(gmwHandle !is null, "Failed to load GraphicsMagickWand Library");
    ---

    Returns: the handle for the shared library on successfull loading,
    null otherwise to allow for error handling.
    */
    @trusted void* loadGraphicsMagickWand()
    {
        import core.sys.posix.dlfcn : RTLD_NOW, dlopen;
        import magickd.wand.c;

        void* handle = dlopen("libGraphicsMagickWand.so", RTLD_NOW);
        if (handle is null)
            return null;

        /* wand.c.drawing_wand.d */
        bindSymbol(handle, cast(void**)&CloneDrawingWand, "CloneDrawingWand");
        bindSymbol(handle, cast(void**)&DestroyDrawingWand, "DestroyDrawingWand");
        bindSymbol(handle, cast(void**)&NewDrawingWand, "NewDrawingWand");

        /* wand.c.magick_wand.d */
        bindSymbol(handle, cast(void**)&CloneMagickWand, "CloneMagickWand");
        bindSymbol(handle, cast(void**)&DestroyMagickWand, "DestroyMagickWand");
        bindSymbol(handle, cast(void**)&MagickAdaptiveThresholdImage,
                "MagickAdaptiveThresholdImage");
        bindSymbol(handle, cast(void**)&MagickAddImage, "MagickAddImage");
        bindSymbol(handle, cast(void**)&MagickAddNoiseImage, "MagickAddNoseImage");
        bindSymbol(handle, cast(void**)&MagickAffineTransformImage, "MagickAffineTransformImage");
        bindSymbol(handle, cast(void**)&MagickAnnotateImage, "MagickAnnotateImage");
        bindSymbol(handle, cast(void**)&MagickAnimateImages, "MagickAnimateImages");
        bindSymbol(handle, cast(void**)&MagickAppendImages, "MagickAppendImages");
        bindSymbol(handle, cast(void**)&MagickAutoOrientImage, "MagickAutoOrientImage");
        bindSymbol(handle, cast(void**)&MagickAverageImages, "MagickAverageImages");
        bindSymbol(handle, cast(void**)&MagickBlackThresholdImage, "MagickBlackThresholdImage");
        bindSymbol(handle, cast(void**)&MagickBlurImage, "MagickBlurImage");
        bindSymbol(handle, cast(void**)&MagickBorderImage, "MagickBorderImage");
        bindSymbol(handle, cast(void**)&MagickCdlImage, "MagickCdlImage");
        bindSymbol(handle, cast(void**)&MagickCharcoalImage, "MagickCharcoalImage");
        bindSymbol(handle, cast(void**)&MagickChopImage, "MagickChopImage");
        bindSymbol(handle, cast(void**)&MagickClearException, "MagickClearException");
        bindSymbol(handle, cast(void**)&MagickClipImage, "MagickClipImage");
        bindSymbol(handle, cast(void**)&MagickClipPathImage, "MagickClipPathImage");
        bindSymbol(handle, cast(void**)&MagickCoalesceImages, "MagickCoalesceImages");
        bindSymbol(handle, cast(void**)&MagickColorFloodfillImage, "MagickColorFloodfillImage");
        bindSymbol(handle, cast(void**)&MagickColorizeImage, "MagickColorizeImage");
        bindSymbol(handle, cast(void**)&MagickCommentImage, "MagickCommentImage");
        bindSymbol(handle, cast(void**)&MagickCompareImageChannels, "MagickCompareImageChannels");
        bindSymbol(handle, cast(void**)&MagickCompareImages, "MagickCompareImages");
        bindSymbol(handle, cast(void**)&MagickCompositeImage, "MagickCompositeImage");
        bindSymbol(handle, cast(void**)&MagickContrastImage, "MagickContrastImage");
        bindSymbol(handle, cast(void**)&MagickConvolveImage, "MagickConvolveImage");
        bindSymbol(handle, cast(void**)&MagickCropImage, "MagickCropImage");
        bindSymbol(handle, cast(void**)&MagickCycleColormapImage, "MagickCycleColormapImage");
        bindSymbol(handle, cast(void**)&MagickGetException, "MagickGetException");
        bindSymbol(handle, cast(void**)&MagickGetImageHeight, "MagickGetImageHeight");
        bindSymbol(handle, cast(void**)&MagickResetIterator, "MagickResetIterator");
        bindSymbol(handle, cast(void**)&MagickReadImage, "MagickReadImage");
        bindSymbol(handle, cast(void**)&MagickResizeImage, "MagickResizeImage");
        bindSymbol(handle, cast(void**)&MagickRotateImage, "MagickRotateImage");
        bindSymbol(handle, cast(void**)&MagickSetFormat, "MagickSetFormat");
        bindSymbol(handle, cast(void**)&MagickSetImageDelay, "MagickSetImageDelay");
        bindSymbol(handle, cast(void**)&MagickSetImageDispose, "MagickSetImageDispose");
        bindSymbol(handle, cast(void**)&MagickSetImageFormat, "MagickSetImageFormat");
        bindSymbol(handle, cast(void**)&MagickSetImageOption, "MagickSetImageOption");
        bindSymbol(handle, cast(void**)&MagickSetSize, "MagickSetSize");
        bindSymbol(handle, cast(void**)&MagickWriteImage, "MagickWriteImage");
        bindSymbol(handle, cast(void**)&MagickWriteImagesFile, "MagickWriteImagesFile");
        bindSymbol(handle, cast(void**)&MagickWriteImageBlob, "MagickWriteImageBlob");
        bindSymbol(handle, cast(void**)&MagickWriteImageFile, "MagickWriteImageFile");
        bindSymbol(handle, cast(void**)&MagickWriteImages, "MagickWriteImages");
        bindSymbol(handle, cast(void**)&NewMagickWand, "NewMagickWand");


        /* wand.c.pixel_wand.d */
        bindSymbol(handle, cast(void**)&DestroyPixelWand, "DestroyPixelWand");
        bindSymbol(handle, cast(void**)&NewPixelWand, "NewPixelWand");
        bindSymbol(handle, cast(void**)&PixelSetColor, "PixelSetColor");

        return handle;
    }
}

@nogc @system extern(C) nothrow:

/// MagickWand struct, defined externally.
extern struct MagickWand;

version(SHARED)
{
  package:
    alias MDCloneMagickWand = MagickWand* function(const(MagickWand*));
    alias MDDestroyMagickWand = void function(MagickWand*);
    alias MDMagickAdaptiveThresholdImage = uint function(MagickWand*, const(c_ulong),
            const(c_ulong), const(c_long));
    alias MDMagickAddImage = uint function(MagickWand*, const(MagickWand*));
    alias MDMagickAddNoiseImage = uint function(MagickWand*, const(NoiseType));
    alias MDMagickAffineTransformImage = uint function(MagickWand*, const(DrawingWand*));
    alias MDMagickAnnotateImage = uint function(MagickWand*, const(DrawingWand*), const(double),
            const(double), const(double), const(char*));
    alias MDMagickAnimateImages = uint function(MagickWand*, const(char*));
    alias MDMagickAppendImages = MagickWand* function(MagickWand*, const(uint));
    alias MDMagickAutoOrientImage = uint function(MagickWand*, const(OrientationType),
            ExceptionInfo*);
    alias MDMagickAverageImages = MagickWand* function(MagickWand*);
    alias MDMagickBlackThresholdImage = uint function(MagickWand*, const(PixelWand*));
    alias MDMagickBlurImage = uint function(MagickWand*, const(double), const(double));
    alias MDMagickBorderImage = uint function(MagickWand*, const(PixelWand*), const(c_ulong),
            const(c_ulong));
    alias MDMagickCdlImage = uint function(MagickWand*, const(char*));
    alias MDMagickCharcoalImage = uint function(MagickWand*, const(double), const(double));
    alias MDMagickChopImage = uint function(MagickWand*, const(c_ulong), const(c_ulong),
            const(c_long), const(c_long));
    alias MDMagickClearException = void function(MagickWand*);
    alias MDMagickClipImage = uint function(MagickWand*);
    alias MDMagickClipPathImage = uint function(MagickWand*, const(char*), const(uint));
    alias MDMagickCoalesceImages = MagickWand* function(MagickWand*);
    alias MDMagickColorFloodfillImage = uint function(MagickWand*, const(PixelWand*),
            const(double), const(PixelWand*), const(c_long), const(c_long));
    alias MDMagickColorizeImage = uint function(MagickWand*, const(PixelWand*), const(PixelWand*));
    alias MDMagickCommentImage = uint function(MagickWand*, const(char*));
    alias MDMagickCompareImageChannels = MagickWand* function(MagickWand*, const(MagickWand*),
            const(ChannelType), const(MetricType), double*);
    alias MDMagickCompareImages = MagickWand* function(MagickWand*, const(MagickWand*),
            const(MetricType), double*);
    alias MDMagickCompositeImage = uint function(MagickWand*, const(MagickWand*),
            const(CompositeOperator), const(c_long), const(c_long));
    alias MDMagickContrastImage = uint function(MagickWand*, const(uint));
    alias MDMagickConvolveImage = uint function(MagickWand*, const(c_ulong), const(double*));
    alias MDMagickCropImage = uint function(MagickWand*, const(c_ulong), const(c_ulong),
            const(c_long), const(c_long));
    alias MDMagickCycleColormapImage = uint function(MagickWand*, const(c_long));
    alias MDMagickGetException = char* function(MagickWand*, ExceptionType*);
    alias MDMagickGetImageHeight = c_ulong function(MagickWand*);
    alias MDMagickResetIterator = void function(MagickWand*);
    alias MDMagickReadImage = uint function(MagickWand*, const(char*));
    alias MDMagickResizeImage = uint function(MagickWand*, const(c_ulong), const(c_ulong),
            const(FilterTypes), const(double));
    alias MDMagickRotateImage = uint function(MagickWand*, const(PixelWand*), const(double));
    alias MDMagickSetFormat = uint function(MagickWand*, const(char*));
    alias MDMagickSetImageDelay = uint function(MagickWand*, const(c_ulong));
    alias MDMagickSetImageDispose = uint function(MagickWand*, const(DisposeType));
    alias MDMagickSetImageFormat = uint function(MagickWand*, const(char*));
    alias MDMagickSetImageOption = uint function(MagickWand*, const(char*), const(char*),
            const(char*));
    alias MDMagickSetSize = uint function(MagickWand*, const(c_ulong), const(c_ulong));
    alias MDMagickWriteImage = uint function(MagickWand*, const(char*));
    alias MDMagickWriteImagesFile = uint function(MagickWand*, FILE*, const(uint));
    alias MDMagickWriteImageBlob = ubyte* function(MagickWand*, size_t*);
    alias MDMagickWriteImageFile = uint function(MagickWand*, FILE*);
    alias MDMagickWriteImages = uint function(MagickWand*, const(char*), uint);
    alias MDNewMagickWand = MagickWand* function();

  public:
    extern (D) __gshared MDCloneMagickWand CloneMagickWand;
    extern (D) __gshared MDDestroyMagickWand DestroyMagickWand;
    extern (D) __gshared MDMagickAdaptiveThresholdImage MagickAdaptiveThresholdImage;
    extern (D) __gshared MDMagickAddImage MagickAddImage;
    extern (D) __gshared MDMagickAddNoiseImage MagickAddNoiseImage;
    extern (D) __gshared MDMagickAffineTransformImage MagickAffineTransformImage;
    extern (D) __gshared MDMagickAnnotateImage MagickAnnotateImage;
    extern (D) __gshared MDMagickAnimateImages MagickAnimateImages;
    extern (D) __gshared MDMagickAppendImages MagickAppendImages;
    extern (D) __gshared MDMagickAutoOrientImage MagickAutoOrientImage;
    extern (D) __gshared MDMagickAverageImages MagickAverageImages;
    extern (D) __gshared MDMagickBlackThresholdImage MagickBlackThresholdImage;
    extern (D) __gshared MDMagickBlurImage MagickBlurImage;
    extern (D) __gshared MDMagickBorderImage MagickBorderImage;
    extern (D) __gshared MDMagickCdlImage MagickCdlImage;
    extern (D) __gshared MDMagickCharcoalImage MagickCharcoalImage;
    extern (D) __gshared MDMagickChopImage MagickChopImage;
    extern (D) __gshared MDMagickClearException MagickClearException;
    extern (D) __gshared MDMagickClipImage MagickClipImage;
    extern (D) __gshared MDMagickClipPathImage MagickClipPathImage;
    extern (D) __gshared MDMagickCoalesceImages MagickCoalesceImages;
    extern (D) __gshared MDMagickColorFloodfillImage MagickColorFloodfillImage;
    extern (D) __gshared MDMagickColorizeImage MagickColorizeImage;
    extern (D) __gshared MDMagickCommentImage MagickCommentImage;
    extern (D) __gshared MDMagickCompareImageChannels MagickCompareImageChannels;
    extern (D) __gshared MDMagickCompareImages MagickCompareImages;
    extern (D) __gshared MDMagickCompositeImage MagickCompositeImage;
    extern (D) __gshared MDMagickContrastImage MagickContrastImage;
    extern (D) __gshared MDMagickConvolveImage MagickConvolveImage;
    extern (D) __gshared MDMagickCropImage MagickCropImage;
    extern (D) __gshared MDMagickCycleColormapImage MagickCycleColormapImage;
    extern (D) __gshared MDMagickGetException MagickGetException;
    extern (D) __gshared MDMagickGetImageHeight MagickGetImageHeight;
    extern (D) __gshared MDMagickResetIterator MagickResetIterator;
    extern (D) __gshared MDMagickReadImage MagickReadImage;
    extern (D) __gshared MDMagickResizeImage MagickResizeImage;
    extern (D) __gshared MDMagickRotateImage MagickRotateImage;
    extern (D) __gshared MDMagickSetFormat MagickSetFormat;
    extern (D) __gshared MDMagickSetImageDelay MagickSetImageDelay;
    extern (D) __gshared MDMagickSetImageDispose MagickSetImageDispose;
    extern (D) __gshared MDMagickSetImageFormat MagickSetImageFormat;
    extern (D) __gshared MDMagickSetImageOption MagickSetImageOption;
    extern (D) __gshared MDMagickSetSize MagickSetSize;
    extern (D) __gshared MDMagickWriteImage MagickWriteImage;
    extern (D) __gshared MDMagickWriteImagesFile MagickWriteImagesFile;
    extern (D) __gshared MDMagickWriteImageBlob MagickWriteImageBlob;
    extern (D) __gshared MDMagickWriteImageFile MagickWriteImageFile;
    extern (D) __gshared MDMagickWriteImages MagickWriteImages;
    extern (D) __gshared MDNewMagickWand NewMagickWand;
}
else
{
    /++
     + CloneMagickWand() makes an exact copy of the specified wand.
     +
     + Params:
     +   wand = The magick wand to clone.
     +/
    MagickWand* CloneMagickWand(const(MagickWand*) wand);

    /++
     + DestroyMagickWand() deallocates memory associated with an MagickWand.
     +
     + Params:
     +   wand = The magick wand.
     +/
    void DestroyMagickWand(MagickWand* wand);

    /++
     + MagickAdaptiveThresholdImage() selects an individual threshold for each
     + pixel based on the range of intensity values in its local neighbourhood.
     +
     + This allows for thresholding of an image whose global intensity histogram
     + doesn't contain distinctive peaks.
     +
     + Params:
     +   wand = The magick wand.
     +   width = The width of the local neighbourhood.
     +   height = The height of the local neighbourhood.
     +   offset = The mean offset.
     +/
    uint MagickAdaptiveThresholdImage(MagickWand* wand, const(c_ulong) width,
            const(c_ulong) height, const(c_long) offset);

    /++
     + MagickAddImage() adds a clone of the images from the second wand and
     + inserts them into the first wand.
     +
     + Params:
     +   wand = The magick wand.
     +   add_wand = A wand that contains the image list to be added.
     +/
    uint MagickAddImage(MagickWand* wand, const MagickWand* add_wand);

    /++
     + MagickAddNoiseImage() adds random noise to the image.
     +
     + Params:
     +   wand = The magick wand.
     +   noise_type = The type of noise: Uniform, Gaussian, Multiplicative,
     +                Impulse, Laplacian, Poisson, or Random.
     +/
    uint MagickAddNoiseImage(MagickWand* wand, const NoiseType noise_type);

    /++
     + MagickAffineTransformImage() transforms an image as dictated by the affine
     + matrix of the drawing wand.
     +
     + Params:
     +   wand = The magick wand.
     +   drawing_wand = The draw wand.
     +/
    uint MagickAffineTransformImage(MagickWand* wand,
            const DrawingWand* drawing_wand);

    /++
     + MagickAnnotateImage() annotates an image with text.
     +
     + Params:
     +   wand = The magick wand.
     +   drawing_wand = The draw wand.
     +   x = X ordinate to left of text.
     +   y = Y ordinate to text baseline.
     +   angle = Rotate text relative to this angle.
     +   text = Text to draw.
     +/
    uint MagickAnnotateImage(MagickWand* wand, const DrawingWand* drawing_wand,
            const double x, const double y, const double angle, const char* text);

    /++
     + MagickAnimateImages() animates an image or image sequence.
     +
     + Params:
     +   wand = The magick wand.
     +   server_name = The X server name.
     +/
    uint MagickAnimateImages(MagickWand* wand, const char* server_name);

    /++
     + MagickAppendImages() appends a set of images.
     +
     + Params:
     +   wand = The magick wand containing the image/images to append.
     +   stack = By default, images are stacked left-to-right. Set to True to
     + stack them top-to-bottom.
     +
     + Returns:
     +   A clone of `wand` containing the appended images.
     +/
    MagickWand* MagickAppendImages(MagickWand* wand, const(uint) stack);

    /++
     + MagickAutoOrientImage() adjusts the current image so that its orientation
     + is suitable for viewing (i.e. top-left orientation).
     +
     + Note that after successful auto-orientation the internal orientation will
     + be set to TopLeftOrientation.  However, this internal value is only written
     + to TIFF files.  For JPEG files, there is currently no support for resetting
     + the EXIF orientation tag to TopLeft so the JPEG should be stripped or EXIF
     + profile removed if preset to prevent saved auto-oriented images from being
     + incorrectly rotated a second time by follow-on viewers that understand the
     + EXIT orientation tag.
     +
     + Params:
     +  wand = The magick wand.
     +  current_orientation = Current image orientation.
     +  exception = Any errors or issues are returned on this structure.
     +
     + Returns: True on success, False otherwise.
     +/
    uint MagickAutoOrientImage(MagickWand* wand,
            const(OrientationType) current_orientation, ExceptionInfo* exception);

    /++
     + MagickAverageImages() averages a set of images.
     +
     + Params:
     +   wand = The magick wand.
     +
     + Returns:
     +   A clone of `wand` containing the averaged image.
     +/
    MagickWand* MagickAverageImages(MagickWand* wand);

    /++
     + MagickBlackThresholdImage() is like MagickThresholdImage() but forces all
     + pixels below the threshold into black while leaving all pixels above the
     + threshold unchanged.
     +
     + Params:
     +   wand = The magick wand.
     +   threshold = The pixel wand.
     +/
    uint MagickBlackThresholdImage(MagickWand* wand, const(PixelWand*) threshold);

    /++
     + MagickBlurImage() blurs an image.
     +
     + We convolve the image with a Gaussian operator of the given radius and
     + standard deviation (sigma).  For reasonable results, the radius should be
     + larger than sigma.  Use a radius of `0` and BlurImage() selects a suitable
     + radius for you/
     +
     + Params:
     +   wand = The magick wand.
     +   radius = The radius of the Gaussian, in pixels, not counting the center
     + pixel.
     +   sigma = The standard deviation of the Gaussian, in pixels.
     +/
    uint MagickBlurImage(MagickWand* wand, const(double) radius,
            const(double) sigma);

    /++
     + MagickBorderImage() surrounds the image with a border of the color defined
     + by the bordercolor pixel wand.
     +
     + Params:
     +   wand = The magick wand.
     +   bordercolor = The border color pixel wand.
     +   width = The border width.
     +   height = The border height.
     +/
    uint MagickBorderImage(MagickWand* wand, const(PixelWand*) bordercolor,
            const(c_ulong) width, const(c_ulong) height);

    /++
     + The MagickCdlImage() method applies ("bakes in") the ASC-CDL, which is a
     + format for the exchange of basic primary color grading information between
     + equipment and software from different manugacturers.
     +
     + The format defines the math for three functions: slope, offset, and power.
     + Each function uses a number for the red, green, and blue color channels for
     + a total of nine numbers comprising a single color decision.  A tenth number
     + for chrominance (saturation) has been proposed but is not yet standardized.
     +
     + The `cdl` argument string is comma delimited and is in the form (but
     + without invervening spaces or line breaks):
     +
     + redslope, redoffset, redpower : greenslope, greenoffset, greenpower :
     + blueslope, blueoffset, bluepower : saturation
     +
     + With the unity (no change) specification being:
     +
     + "1.0,0.0,1.0:1.0,0.0,1.0:1.0,0.0,1.0:0.0"
     +
     + Params:
     +   wand = The image want
     +   cdl = Define the coefficients for the slope offset and power in the red,
     + green, and blue channels, plus saturation.
     +
     + See_Also: http://wikipedia.org/wiki/ASC_CDL
     +/
    uint MagickCdlImage(MagickWand* wand, const char* cdl);

    /++
     + MagickCharcoalImage() simulates a charcoal drawing.
     +
     + Params:
     +   wand = The magick wand.
     +   radius = The radius of the Gaussian, in pixels, not counting the center
     + pixel.
     +   sigma = The standard deviation of the Gaussian, in pixels.
     +/
    uint MagickCharcoalImage(MagickWand* wand, const(double) radius,
            const(double) sigma);

    /++
     + MagickChopImage() removes a region of an image and collapses the image to
     + occupy the removed portion.
     +
     + Params:
     +    wand = The magick wand.
     +    width = The region width.
     +    height = The region height.
     +    x = The region x offset.
     +    y = The region y offset.
     +/
    uint MagickChopImage(MagickWand* wand, const(c_ulong) width,
            const(c_ulong) height, const(c_long) x, const(c_long) y);

    /++
     + MagickClearException() clears the last wand exception.
     +
     + Params:
     +   wand = The magick wand.
     +/
    void MagickClearException(MagickWand* wand);

    /++
     + MagickClipImage() clips along the first path from the 8BIM profile, if
     + present.
     +
     + Params:
     +   wand = The magick wand.
     +/
    uint MagickClipImage(MagickWand* wand);

    /++
     + MagickClipPathImage() clips along the named paths from the 8BIM profile, if
     + present.
     +
     + Later operations take effect inside the path.  `pathname` may be a number
     + if  preceded with '#', to work on numbered paths, e.g. "#1" to use the
     + first path.
     +
     + Params:
     +   wand = The magick wand.
     +   pathname = name of clipping path resource.  If name is preceded by '#',
     + use clipping path numbered by name.
     +   inside = If non-zero, later operations take effect inside the clipping
     + path.  Otherwise, later options take effect outside.
     +/
    uint MagickClipPathImage(MagickWand* wand, const char* pathname,
            const(uint) inside);

    /++
     + MagickCoalesceImages() composites a set of images while respecting any page
     + offsets and disposal methods.
     +
     + GIF, MIFF, and MNG animation sequences typically start with an image
     + background and each subsequent image varies in size and offset.
     + MagickCoalesceImages() returns a new sequence where each image in the
     + sequence is the same size as the first, and composited with the next image
     + in the sequence.
     +
     + Params:
     +   wand = The magick wand.
     +/
    MagickWand* MagickCoalesceImages(MagickWand* wand);

    /++
     + MagickColorFloodfillImage() changes the color value of any pixel that
     + matches target and is an immediate neighbour.
     +
     + If the method FillToBorderMethod() is specified, the color value is changed
     + for any neighbour pixel that does not match the bordercolor member of
     + image.
     +
     + Params:
     +   wand = The magick wand.
     +   fill = The floodfill color pixel wand.
     +   fuzz = By default, target must match a particular pixel color exactly.
     + However, in many cases two colors may differ by a small amount.  The fuzz
     + member of image defines how much tolerance is acceptable to consider two
     + colors as the same.  For example, set fuzz to 10 and the color red at
     + intensities of 100 and 102 respectively are now interpreted as the same
     + color for the purposes for the floodfill.
     +   bordercolor = The border color pixel wand.
     +   x = The starting X location of the operation.
     +   y = The starting Y location of the operation.
     +/
    uint MagickColorFloodfillImage(MagickWand* wand, const PixelWand* fill,
            const double fuzz, const PixelWand* bordercolor, const c_long x,
            const c_long y);

    /++
     + MagickColorizeImage() blends the fill color with each pixel in the image.
     +
     + Params:
     +   wand = The magick wand.
     +   colorize = The colorize pixel wand.
     +   opacity = The opacity pixel wand.
     +/
    uint MagickColorizeImage(MagickWand* wand, const PixelWand* colorize,
            const PixelWand* opacity);

    /++
     + MagickCommentImage() adds a comment to your image.
     +
     + Params:
     +   wand = The magick wand.
     +   comment = The image comment
     +/
    uint MagickCommentImage(MagickWand* wand, const char* comment);

    /++
     + MagickCompareImageChannels() compares one or more image channels and
     + returns specified distortion metric.
     +
     + Params:
     +   wand = The magick wand.
     +   reference = The reference wand.
     +   channel = The channel
     +   metric = The metric
     +   distortion = The computed distortion between images.
     +/
    MagickWand* MagickCompareImageChannels(MagickWand* wand,
            const MagickWand* reference, const ChannelType channel,
            const MetricType metric, double* distortion);

    /++
     + MagickCompareImage() comparse one or more images and returns the specified
     + distortion metric.
     +
     + Params:
     +   wand = The magick wand.
     +   reference = The reference wand.
     +   metric = The metric.
     +   distortion = The computed distortion between the images.
     +/
    MagickWand* MagickCompareImages(MagickWand* wand, const MagickWand* reference,
            const MetricType metric, double* distortion);

    /++
     + MagickCompositeImage() composites one image onto another at the specified
     + offset.
     +
     + Params:
     +   wand = The magick wand
     +   composite_wand = The composite wand
     +   compose = This operator affects how the composite is applied to the
     + image. The default is OverCompositeOp.
     +   x = The column offset of the composited image.
     +   y = The row offset of the composited image.
     +/
    uint MagickCompositeImage(MagickWand* wand, const MagickWand* composite_wand,
            const CompositeOperator compose, const c_long x, const c_long y);

    /++
     + MagickContrastImage() enhances the intensity differences between the
     + lighter and darker elements of the image.
     +
     + Set `sharpen` to a value other than `0` to increase the image contrast,
     + otherwise, contrast is reduced.
     +
     + Params:
     +   wand = The magick wand.
     +   sharpen = Increase or decrease image contrast.
     +/
    uint MagickContrastImage(MagickWand* wand, const(uint) sharpen);

    /++
     + MagickConvolveImage() applies a custom convolution kernel to the image.
     +
     + Params:
     +   wand = The magick wand.
     +   order = The number of columns and rows in the filter kernel.
     +   kernel = An array of doubles representing the convolution kernel.
     +/
    uint MagickConvolveImage(MagickWand* wand, const(c_ulong) order,
            const(double*) kernel);

    /++
     + MagickCropImage() extracts a region of the image.
     +
     + Params:
     +   wand = The magick wand.
     +   width = The region width.
     +   height = The region height.
     +   x = The region x offset.
     +   y = The region y offset.
     +/
    uint MagickCropImage(MagickWand* wand, const(c_ulong) width,
            const(c_ulong) height, const(c_long) x, const(c_long) y);

    /++
     + MagickCycleColormapImage() displaces an image's colormap by a given number
     + of positions.
     +
     + If you cycle the colormap a number of times you can produce a psychodelic
     + effect.
     +
     + Params:
     +   wand = The magick wand.
     +   displace = The amount to displace the colormap by.
     +/
    uint MagickCycleColormapImage(MagickWand* wand, const(c_long) displace);

    /++
     + MagickGetException() returns the severity, reason, and description of any
     + error that occurs when using othe methods in this API.
     +
     + Params:
     +   wand = The magick wand.
     +   severity = The severity of the error is returned here.
     +/
    char* MagickGetException(MagickWand* wand, ExceptionType* severity);

    /++
     + MagickGetImageHeight() return the image height.
     +
     + Params:
     +   wand = The magick wand.
     +/
    c_ulong MagickGetImageHeight(MagickWand* wand);

    /++
     + MagickResetIterator() resets the wand iterator.
     +
     + Use it in conjunction with MagickNextImage() to iterator over all the
     + images in a wand container.
     +
     + Params:
     +   wand = The magick wand.
     +/
    void MagickResetIterator(MagickWand* wand);

    /++
     + MagickReadImage() reads an image or image sequence.
     +
     + Params:
     +   wand = The magick wand.
     +   filename = The image filename.
     +/
    uint MagickReadImage(MagickWand* wand, const char* filename);

    /++
     + MagicResizeImage() scales an image to the desired dimensions with one of
     + these filters:
     +
     + Bessel, Blackman, Box, Catrom, Cubic, Gaussian, Hanning, Hemite, Lanczos,
     + Mitchell, Point, Quandratic, Sinc, Triangle.
     +
     + Most of the filters are FIR (finite impulse response), however, Bessel,
     + Gaussian, and Sinc are IIR (infinite impulse response). Bessel and Sinc are
     + windowed (brought down to zero) with the Blackman filter.
     +
     + Params:
     +   wand = The magick wand.
     +   columns = The number of columns in the scaled image.
     +   rows = The number of rows in the scaled image.
     +   filter = Image filter to use.
     +   blur = The blur factor, where >1 is blurry, <1 is sharp.
     +/
    uint MagickResizeImage(MagickWand* wand, const c_ulong columns,
            const c_ulong rows, const FilterTypes filter, const double blur);

    /++
     + MagickRotateImage() rotates an image the specified number of degrees.
     +
     + Empty triangles left over from rotating the image are filled with the
     + background color.
     +
     + Params:
     +   wand = The magick wand.
     +   background = The background pixel wand.
     +   degrees = The numbe of degrees to rotate the image.
     +/
    uint MagickRotateImage(MagickWand* wand, const PixelWand* background,
            const double degrees);

    /++
     + MagickSetFormat() sets the file or blob format (e.g. "BMP") to be used
     + when a file or blob is read.
     +
     + Usually this is not necessary because the format is auto-detected based on
     + the file header (or file extension), but some formats do not use a unique
     + header or selection may be ambigious. Use MagickSetImageFormat() to set the
     + format to be used when a file or blob is to be written.
     +
     + Params:
     +   wand = The magick wand.
     +   format = The file or blob format.
     +/
    uint MagickSetFormat(MagickWand* wand, const char* format);

    /++
     + MagickSetImageDelay() sets the image delay.
     +
     + Params:
     +   wand = The magick wand.
     +   delay = The image delay in 1/100th of a second.
     +/
    uint MagickSetImageDelay(MagickWand* wand, const c_ulong delay);

    /++
     + MagickSetImageDispose() sets the image disposal method.
     +
     + Params:
     +   wand = The magick wand.
     +   dispose = The image disposal type.
     +/
    uint MagickSetImageDispose(MagickWand* wand, const DisposeType dispose);

    /++
     + MagickSetImageFormat() sets the format of a particular image in a sequence.
     +
     + The format is designated by a magick string (e.g. "GIF").
     +
     + Params:
     +   wand = The magick wand.
     +   format = The image format.
     +/
    uint MagickSetImageFormat(MagickWand* wand, const char* format);

    /++
     + MagickSetImageOption() associates one or more options with a particular
     + image format.
     +
     + Example: MagickSetImageOption(wand, "jpeg", "preserve-settings", "true").
     +
     + Params:
     +   wand = The magick wand
     +   format = The image format.
     +   key = The key.
     +   value = The value.
     +/
    uint MagickSetImageOption(MagickWand* wand, const char* format, const char* key,
            const char* value);

    /++
     + MagickSetSize() sets the size of the magick wand.
     +
     + Set it before you read a raw image format such as RGB, GRAY, or CMYK.
     +
     + Params:
     +   wand = The magick wand.
     +   columns = The width in pixels.
     +   rows = The height in pixels.
     +/
    uint MagickSetSize(MagickWand* wand, const c_ulong columns, const c_ulong rows);

    /++
     + MagickWriteImage() writes an image.
     +
     + Params:
     +   wand = The magick wand.
     +   filename = The image filename.
     +/
    uint MagickWriteImage(MagickWand* wand, const char* filename);

    /++
     + MagickWriteImagesFile() writes an image or image sequence to a stdio FILE
     + handle.
     +
     + This may be used to append an encoded image to an already existing appended
     + image sequence if the file seek position is at the end of an existing file.
     +
     + Params:
     +   wand = The magick wand.
     +   file = The open (and positioned) file handle.
     +   adjoin = Join images into a single multi-image file.
     +/
    uint MagickWriteImagesFile(MagickWand* wand, FILE* file, const uint adjoin);

    /++
     + MagickWriteImageBlob() implements direct to memory image formats.
     +
     + It returns the image as a blob (a formatted "file" in memory) and its
     + length, starting from the current position in the image sequence. Use
     + MagickSetImageFormat() to set the format to write to the blob (GIF, JPEG,
     + PNG, etc.).
     +
     + Use MagickResetIterator() on the wand if it is desired to write a sequence
     + from the beginning and the iterator is not currently at the beginning.
     +
     + Params:
     +   wand = The magick wand.
     +   length = The length of the blob
     +/
    ubyte* MagickWriteImageBlob(MagickWand* wand, size_t* length);

    /++
     + MagickWriteImageFile() writes an image to an open file descriptor.
     +
     + Params:
     +   wand = The magick wand.
     +   file = The file descriptor.
     +/
    uint MagickWriteImageFile(MagickWand* wand, FILE* file);

    /++
     + MagickWriteImages() writes an image or image sequence.
     +
     + If the wand represents an image sequence, then it is written starting at
     + the first frame in the sequence.
     +
     + Params:
     +   wand = The magick wand.
     +   filename = The image filename.
     +   adjoin = Join images into a single multi-image file.
     +/
    uint MagickWriteImages(MagickWand* wand, const char* filename, uint adjoin);

    /++
     + NewMagickWand() returns a wand required for all other methods in the API.
     +/
    MagickWand* NewMagickWand();
}
