/**
 * Wand vector drawing interfaces.
 *
 * Authors: GraphicsMagick Group
 * Copyright: &copy; 2003 - 2009 GraphicsMagick Group
 * License: $(LINK2 http://www.graphicsmagick.org/Copyright.html, MIT)
 */
deprecated("Use graphicsmagick_c.wand.drawing_wand instead (remove @ 0.9)")
module magickd.wand.c.drawing_wand;

// Compat for graphicsmagick_c
version (GMagick_Static) {}
else { version = SHARED; }

@nogc @system extern(C) nothrow:

/// DrawingWand struct, defined externally.
extern struct DrawingWand;

version (SHARED)
{
  package:
    alias MDCloneDrawingWand = DrawingWand* function(const(DrawingWand*));
    alias MDDestroyDrawingWand = void function(DrawingWand*);
    alias MDNewDrawingWand = DrawingWand* function();

  public:
    extern (D) __gshared MDCloneDrawingWand CloneDrawingWand;
    extern (D) __gshared MDDestroyDrawingWand DestroyDrawingWand;
    extern (D) __gshared MDNewDrawingWand NewDrawingWand;
}
else
{
    /++
     + CloneDrawingWand() returns a new drawing wand which is a full (deep) copy
     + of an existing drawing wand.
     +
     + Params:
     +   drawing_wand = The drawing wand to copy.
     +/
    DrawingWand* CloneDrawingWand(const(DrawingWand*) drawing_wand);

    /++
     + DestroyDrawingWand() frees all resources associated with the drawing wand.
     +
     + Once the drawing wand has been freed, it should not be used any further
     + unless it is re-allocated.
     +
     + Params:
     +   drawing_wand = The drawing wand to destroy.
     +/
    void DestroyDrawingWand(DrawingWand* drawing_wand);

    /++
     + NewDrawingWand() returns a drawing wand required for all other methods in the
     + API.
     +/
    DrawingWand* NewDrawingWand();
}
