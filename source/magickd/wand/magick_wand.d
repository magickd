/*
  File:           wand/magick_wand.d
  Contains:       MagickWand class
  Copyright:      (C) 2021 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("Use magickd instead. (remove @ 0.9)")
module magickd.wand.magick_wand;

private
{
  import core.stdc.config : c_long, c_ulong;
  import std.stdio : File;
  import std.string : toStringz;

  import magickd.core.c.common : MagickPassFail, MagickPass, MagickFail;
  import magickd.core.c.error : cExceptionType = ExceptionType,
    CatchException;
  import magickd.wand.c.magick_wand;
  import magickd.wand.c.magick_wand : cMagickWand = MagickWand;

  import magickd.core.compare;
  import magickd.core.image;
  import magickd.core.error;
  import magickd.wand.pixel_wand;
}

public class
MagickWand
{
  private cMagickWand* _handle;

  /**
   * Create a new MagickWand for editing an image.
   */
  this()
  {
    _handle = NewMagickWand();
  }

  /* used for .clone */
  @safe
  package this(cMagickWand* handle)
  {
    _handle = handle;
  }

  ~this()
  {
    DestroyMagickWand(_handle);
  }

  /**
   * Make an exact copy of the wand.
   */
  @trusted
  MagickWand clone()
  {
    cMagickWand* clone_ = CloneMagickWand(_handle);
    return new MagickWand(clone_);
  }

  /**
   * Select an individual threshold for each pixel based on the range of
   * intensity values in its local neighbourhood.
   *
   * This allows thresholding of an image whose global intensity histogram
   * doesn't contain distinctive peaks.
   */
  @nogc @trusted
  uint adaptiveThreshold(c_ulong width, c_ulong height, c_long offset)
  {
    return MagickAdaptiveThresholdImage(_handle, width, height, offset);
  }

  /**
   * Add a clone of the images from the second wand and inserts them into
   * this wand.
   *
   * Params:
   *   addWand = A wand that contains the image list to be added.
   */
  @trusted
  uint addImage(MagickWand addWand)
  {
    return MagickAddImage(_handle, addWand.ptr);
  }

  /**
   * Adds random noise to the image.
   *
   * Params:
   *   noiseType = The type of noise.
   */
  @nogc @trusted
  uint addNoise(NoiseType noiseType)
  {
    return MagickAddNoiseImage(_handle, noiseType);
  }

  /*
  @nogc @trusted
  uint affineTransform(DrawingWand drawingWand)
  {
    return MagickAffineTransformImage(_handle, drawingWand.ptr);
  }
  */

  /+
  /* @nogc */ @trusted
  uint annotateImage(DrawingWand drawingWand, const(double) x,
		     const(double) y, const(double) angle, string text)
  {
    import std.string : toStringz;

    return MagickAnnotateImage(_handle, drawingWand.ptr, x, y, angle,
			       text.toStringz);
  }
  +/

  /**
   * Animates an image or image sequence.
   *
   * Params:
   *   serverName = The X server name.
   */
  /* @nogc */ @trusted
  uint animageImages(string serverName)
  {
    return MagickAnimateImages(_handle, serverName.toStringz);
  }

  /**
   * Append the images stored in this wand.
   *
   * Params:
   *   stack = Stack images top-to-bottom (true), or left-to-right (false).
   */
  @nogc @trusted
  void appendImages(const(bool) stack)
  {
    _handle = MagickAppendImages(_handle, cast(uint)stack);
  }

  /**
   * Adjust the current image so that it's orientation is suitable for viewing
   * (i.e. top-left orientation).
   *
   * Note that after successful auto-orientation, the internal orientation
   * of the image will be set to `Orientation.topLeft`.  However, this
   * internal value is only written to TIFF files.  For JPEG files, there is
   * currently no support for resetting the EXIF orientation tag to TopLeft,
   * so the JPEG should be stripped or EXIF profile removed, if present, to
   * prevent saved auto-oriented images from being incorrectly rotated a
   * second time by follow-on viewers that understand the EXIF orientation
   * tag.
   *
   * Params:
   *   currentOrientation = The current image orientation.
   *
   * See_Also:
   *   stripImage()
   */
  @trusted
  uint autoOrientImage(const(Orientation) currentOrientation)
  {
    import std.conv : to;
    import std.string : fromStringz;

    ExceptionInfo exception;
    MagickPassFail res = MagickPass;

    res = MagickAutoOrientImage(_handle, currentOrientation, &exception);

    if (MagickPass == res)
      return res;

    CatchException(&exception);
    throw new Exception(to!(string)(exception.reason.fromStringz));
  }

  /**
   * Average a set of images.
   *
   * Internally this calls the AverageImages() function, which returns a
   * single image with each corresponding pixel component of each image
   * averaged.
   *
   * See_Also:
   *   magickd.core.average.averageImages
   */
  @nogc @trusted
  void averageImages()
  {
    _handle = MagickAverageImages(_handle);
  }

  /**
   * Similar to thresholdImage() but forces all pixels below the threshold
   * into black while leaving pixels above the threshold unchanged.
   *
   * Params:
   *   threshold = The pixel wand with the threshold set.
   */
  @trusted
  blackThresholdImage(PixelWand* threshold)
  {
    return MagickBlackThresholdImage(_handle, threshold.ptr);
  }

  /**
   * Blurs the current image.
   *
   * We convolve the image with a Gaussian operator of the given radius and
   * standard deviation (sigma).  For reasonable results, the radius should be
   * larger than sigma.  Use `0` for radius and a suitable radius is selected
   * for you.
   *
   * Params:
   *   radius = The radius of the Gaussian, in pixels, not counting the center
   * pixel.
   *   sigma = The standard deviation of the Gaussian, in pixels.
   */
  @nogc @trusted
  uint blurImage(const(double) radius, const(double) sigma)
  {
    return MagickBlurImage(_handle, radius, sigma);
  }

  /**
   * Surrounds the current image with a border of the color defined by the
   * borderColor PixelWand.
   *
   * Params:
   *   borderColor = The border color PixelWand.
   *   width = The width of the border.
   *   height = The height of the border.
   */
  @trusted
  uint borderImage(PixelWand borderColor, const(uint) width,
		   const(uint) height)
  {
    return MagickBorderImage(_handle, borderColor.ptr, width, height);
  }

  /**
   * Apply the ASC-CDL, which is a format for the exchange of basic primary
   * color grading information between equipment and software from different
   * manufacturers.
   *
   * The format defines the math for three functions: slope, offset, and
   * power.  Each function uses a number for the red, green, and blue colour
   * channels for a total of nine number comprising a single colour decision.
   * A tenth number for chrominance (saturation) has been proposed, but is
   * not yet standardized.
   *
   * The `cdl` argument string is comma delimited, and is in the form (but
   * without line breaks):
   *
   * redslope,redoffset,redpower:greenslope,greenoffset:greenpower:blueslope,
   * blueoffset,bluepower:saturation
   *
   * With the unity (no change) specification being:
   *
   * "1.0,0.0,1.0:1.0,0.0,1.0:1.0,0.0,1.0:0.0"
   *
   * Params:
   *   cdl = Define the coefficients for slope, offset, and power in the red,
   * green, and blue channels, plus saturation.
   *
   * See_Also:
   *    https://wikipedia.org/wiki/ASC_CDL
   */
  @trusted
  uint cdlImage(string cdl)
  {
    return MagickCdlImage(_handle, cdl.toStringz);
  }

  /**
   * Simulate a charcoal drawing for the image.
   *
   * Params:
   *   radius = The radius of the Gaussian, in pixels, excluding the center
   * pixel.
   *   sigma = The standard deviation of the Gaussian, in pixels.
   */
  @nogc @trusted
  uint charcoalImage(const(double) radius, const(double) sigma)
  {
    return MagickCharcoalImage(_handle, radius, sigma);
  }

  /**
   * Remove a region from the image and collapse the image to occupy the
   * removed portion.
   *
   * Params:
   *   width = The region's width.
   *   height = The region's height.
   *   x = The region's x offset.
   *   y = The region's y offset.
   */
  @nogc @trusted
  uint chopImage(const(c_ulong) width, const(c_ulong) height, const(c_long) x,
		 const(c_long) y)
  {
    return MagickChopImage(_handle, width, height, x, y);
  }

  /**
   * Clears the last wand exception.
   *
   * See_Also: getException()
   */
  @nogc @trusted
  void clearException()
  {
    MagickClearException(_handle);
  }

  /**
   * Clip the image along the first path from the 8BIM profile, if present.
   */
  @nogc @trusted
  uint clipImage()
  {
    return MagickClipImage(_handle);
  }

  /**
   * Clip along the named paths from the 8BIM profile, if present.
   *
   * Later operations to the image take effect inside the path.  `pathname`
   * may be a number if it's preceded with a `#` to work on numbered paths,
   * e.g. "#1" to use the first path.
   *
   * Params:
   *    pathname = Name of clipping resource.
   *    inside = Should later operations effect inside the clipping path?
   */
  @trusted
  uint clipPathImage(string pathname, bool inside)
  {
    return MagickClipPathImage(_handle, pathname.toStringz, cast(uint)inside);
  }

  /**
   * Composite a set of images while respecting any page offsets and disposal
   * methods.
   *
   * GIF, MIFF, and MNG animation sequences typically start with an image
   * background and each subsequent image varies in size and offset.
   *
   * Returns:
   *    A new sequence where each image in the sequence is the same size as
   * the first and composited with the next image in the sequence.
   */
  @trusted
  MagickWand coalesceImages()
  {
    cMagickWand* newWand;

    newWand = MagickCoalesceImages(_handle);

    return new MagickWand(newWand);
  }

  /**
   * Change the color value of any pixel that matches the target and is an
   * immediate neighbour.
   *
   * If the bordercolor is null, the FillToBorderMethod is used, otherwise,
   * the FloodfillMethod is used.
   *
   * Params:
   *   fill = The floodfill color PixelWand.
   *   fuzz = How much tolerance is acceptable to consider two colors the
   * same.
   *   bordercolor = The bordercolor PixelWand (can be null).
   *   x = The starting X location of the operation.
   *   y = The starting Y location of the operation.
   */
  @trusted
  uint colorFloodfillImage(PixelWand fill, const(double) fuzz,
			   PixelWand bordercolor, const(c_long) x,
			   const(c_long) y)
  {
    auto bc = (bordercolor is null) ? null : bordercolor.ptr;

    return MagickColorFloodfillImage(_handle,
				     (fill is null) ? null : fill.ptr,
				     fuzz, bc, x, y);
  }

  /**
   * Blend the fill color with each pixel in the image.
   *
   * Params:
   *   colorize = The colorize PixelWand.
   *   opacity = The opacity PixelWand.
   */
  @trusted
  uint colorizeImage(PixelWand colorize, PixelWand opacity)
  {
    /* fun fact: opacity isn't actually used. */
    return MagickColorizeImage(_handle,
			       (colorize is null) ? null : colorize.ptr,
			       (opacity is null) ? null : opacity.ptr);
  }

  /**
   * Add a comment to your image.
   *
   * Params:
   *   comment = The image comment.
   */
  @trusted
  uint commentImage(string comment)
  {
    return MagickCommentImage(_handle, comment.toStringz);
  }

  /**
   * Compare this image's channel to one or more channels.
   *
   * Params:
   *    reference = The reference wand.
   *    channel = The channel.
   *    metric = The metric.
   *    distortion = The computed distortion between the images.
   *
   * Returns:
   *   null if there was an error, a clone of the image with the annotated
   * difference otherwise.
   */
  @trusted
  MagickWand compareImageChannels(MagickWand reference,
				  const(ChannelType) channel,
				  const(Metric) metric, out double distortion)
  {
    double dist;
    cMagickWand* returnWand;

    returnWand = MagickCompareImageChannels(_handle, reference.ptr, channel,
					    metric, &dist);

    distortion = dist;
    if (returnWand is null)
      return null;

    return new MagickWand(returnWand);
  }

  /**
   * Compare this image to one or more.
   *
   * Params:
   *   reference = The reference wand.
   *   metric = The metric
   *   distortion = The computed distortion between the images.
   *
   * Returns: null if there was an error, or a clone of the image with the
   * annotated difference.
   */
  @trusted
  MagickWand compareImages(MagickWand reference, const(Metric) metric,
			   out double distortion)
  {
    double dist;
    cMagickWand* returnWand;
    auto rw = (reference is null) ? null : reference.ptr;

    returnWand = MagickCompareImages(_handle, rw, metric, &dist);
    distortion = dist;

    if (returnWand is null)
      return null;

    return new MagickWand(returnWand);
  }

  /**
   * Composite one image onto another image at the specified offset.
   *
   * Params:
   *   compositeWand = The composite wand.
   *   compose = The way in which the composite is applied.
   *   x = The column offset of the composited image.
   *   y = The row offset of the composited image.
   */
  @trusted
  uint compositeImage(MagickWand compositeWand,
		      const(CompositeOperator) compose, const(c_long) x,
		      const(c_long) y)
  {
    auto cw = (compositeWand is null) ? null : compositeWand.ptr;

    return MagickCompositeImage(_handle, cw, compose, x, y);
  }

  /**
   * Enchance the intensity differences between the lighter and darker
   * elements of the image.
   *
   * Setting `sharpen` to a value other than `0` will increase the image
   * contrast, otherwise the contrast is reduced.
   *
   * Params:
   *   sharpen = Amount to increase or decrease the image contrast.
   */
  @nogc @trusted
  uint contrastImage(const(uint) sharpen)
  {
    return MagickContrastImage(_handle, sharpen);
  }

  /**
   * Apply a custom convolution kernel to the image.
   *
   * Params:
   *    order = The number of columns and rows in the filter kernel.
   *    kernel = An array of doubles representing the convolution kernel.
   */
  @nogc @trusted
  uint convolveImage(const(c_ulong) order, double[] kernel)
  {
    return MagickConvolveImage(_handle, order, kernel.ptr);
  }

  /**
   * Extract a region of the image.
   *
   * Params:
   *   width = The region width.
   *   height = The region height.
   *   x = The region's X offset.
   *   y = The region's Y offset.
   */
  @nogc @trusted
  uint cropImage(const(c_ulong) width, const(c_ulong) height,
		 const(c_long) x, const(c_long) y)
  {
    return MagickCropImage(_handle, width, height, x, y);
  }

  /**
   * Displace the current image's colormap by a given number of positions.
   *
   * If you cycle the colormap a number of times, you can produce a
   * psychodelic effect.
   *
   * Params:
   *   displace = The amount to displace the colormap by.
   */
  @nogc @trusted
  uint cycleColormapImage(const(c_long) displace)
  {
    return MagickCycleColormapImage(_handle, displace);
  }

  @trusted
  string getException(out ExceptionType severity)
  {
    import std.conv : to;
    import std.string : fromStringz;

    char* description;
    cExceptionType sev;

    description = MagickGetException(_handle, &sev);
    severity = cast(ExceptionType)sev;

    return to!(string)(description.fromStringz);
  }

  /* @nogc */ @trusted
  uint readImage(string filename)
  {
    return MagickReadImage(_handle, filename.toStringz);
  }

  deprecated("Use .readImage() instead (remove @ 1.0)")
  @trusted uint read(string filename)
  {
    return readImage(filename);
  }

  /* @nogc */ @trusted
  uint rotateImage(PixelWand background, const(double) degrees)
  {
    return MagickRotateImage(_handle, background.ptr, degrees);
  }

  /**
   * Sets the image delay.
   *
   * Params:
   *   delay = The image delay in 1/100th of a second.
   */
  @nogc @trusted
  uint setDelay(c_ulong delay)
  {
    return MagickSetImageDelay(_handle, delay);
  }

  /**
   * Sets the image disposal method.
   *
   * Params:
   *   dispose = The image disposal type.
   */
  @nogc @trusted
  uint setImageDispose(DisposeType dispose)
  {
    return MagickSetImageDispose(_handle, dispose);
  }

  /**
   * Associate one or more options with a particular image format.
   *
   * ---
   * auto wand = new MagickWand();
   * wand.readImage("image.jpeg");
   * wand.setOption("jpeg", "preserver-settings", "true");
   * ---
   *
   * Params:
   *   format = The image format
   *   key = The option key.
   *   value = The new value for the option.
   */
  @trusted
  uint setOption(string format, string key, string value)
  {
    return MagickSetImageOption(_handle, format.toStringz, key.toStringz,
				value.toStringz);
  }

  /**
   * Sets the file or blob format (e.g. "BMP") to be used when a file or blob
   * is read.
   *
   * Usually this is not necessary because GraphicsMagick is able to
   * auto-detect the format based on the file header (or the file extension),
   * but some formats do not use a unique header or the selection may be
   * ambigious.
   *
   * Params:
   *   format = The file or blob format.
   *
   * See_Also:
   *   setWriteFormat() to set the format to be used when a file or blob is to
   * be written.
   */
  @trusted
  uint setReadFormat(string format)
  {
    return MagickSetFormat(_handle, format.toStringz);
  }

  /**
   * Write an image to the provided filename.
   *
   * Params:
   *   filename = The image filename.
   */
  /* @nogc */ @trusted
  uint writeImage(string filename)
  {
    import std.string : toStringz;

    return MagickWriteImage(_handle, filename.toStringz);
  }

  /// Ditto
  deprecated("Use .writeImage instead (removed @ 1.0)")
  @trusted write(string filename)
  {
    return writeImage(filename);
  }

  /* @nogc */ @trusted
  uint writeImages(ref File file, bool adjoin)
  {
    return MagickWriteImagesFile(_handle, file.getFP(), cast(uint)adjoin);
  }

  /* @nogc */ @trusted
  uint writeImages(string filename, bool adjoin)
  {
    return MagickWriteImages(_handle, filename.toStringz, cast(uint)adjoin);
  }

  package
  cMagickWand* ptr()
  {
    return _handle;
  }
}
