/*
  File:           wand/pixel_wand.d
  Contains:       PixelWand class
  Copyright:      (C) 2021 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("Use magickd instead. (remove @ 0.9)")
module magickd.wand.pixel_wand;

private
{
  import magickd.wand.c.pixel_wand;
  import magickd.wand.c.pixel_wand : cPixelWand = PixelWand;
}

public class
PixelWand
{
  private cPixelWand* _handle;

  this()
  {
    _handle = NewPixelWand();
  }

  ~this()
  {
    DestroyPixelWand(_handle);
  }

  cPixelWand* ptr()
  {
    return _handle;
  }

  @trusted
  setColor(string color)
  {
    import std.string : toStringz;

    return PixelSetColor(_handle, color.toStringz);
  }
}
