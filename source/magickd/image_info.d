/*
  File:           magickd/image_info.d
  Contains:       A class which wraps the GraphicsMagick ImageInfo structure.
  Copyright:      (C) 2023 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
deprecated("Please use the MagickWand API provided by MagickD (remove @ 0.9)")
module magickd.image_info;

import graphicsmagick_c.magick.image;
import graphicsmagick_c.magick.image : cImageInfo = ImageInfo;

class ImageInfo {
    package(magickd) cImageInfo* handle = null;

    private void initialize() {
        handle = CloneImageInfo(null);
    }

    /**
     * Constructor.
     */
    this() {
        this.initialize();
    }

    /**
     * Constructor.
     *
     * Params:
     *   fileName = Initial value of the filename attribute.
     */
    this(string fileName) {
        initialize();
        this.fileName = fileName;
    }

    /**
     * Destructor.
     */
    ~this() {
        if (null !is handle)
            DestroyImageInfo(handle);
    }

    @property int adjoin() {
        return handle.adjoin;
    }

    @property void adjoin(int value) {
        handle.adjoin = value;
    }

    @property bool affirm() {
        return (handle.affirm == 1);
    }

    @property void affirm(bool value) {
        handle.affirm = value ? 1 : 0;
    }

    @property string fileName() {
        import core.stdc.string : strlen;
        import std.algorithm.mutation : copy;

        string ret;
        size_t len = strlen(&(handle.filename[0]));
        ret.reserve(len);
        ret.length = len;
        copy(handle.filename[0..len], cast(char[])ret);

        return ret;

    }

    @property void fileName(string value) {
        import std.algorithm.mutation : copy;

        import graphicsmagick_c.magick.api : MaxTextExtent;

        string nullTerm = value ~ '\0';
        size_t length = (nullTerm.length >= MaxTextExtent) ? (MaxTextExtent - 1) : nullTerm.length;

        copy(nullTerm[0..length], handle.filename[0..length]);
    }
}
