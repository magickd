graphicsmagcick_c
=================

GraphicsMagick binding for D.

Dependencies
------------

In order to use these bindings, GraphicsMagick must be installed.

For installation steps, please read <http://www.graphicsmagick.org/README.html>.

These bindings aren't yet (2023-01-12) complete.  The intention is that they'll
have a release which matches each of the GraphicsMagick releases (e.g. 1.3 up
to 1.3.39 as of writing).

Configuration
-------------

To use this package via dub, you'll have to manually add the Git repository.
For this to work, you'll need dub v1.23.0 or above (comes with DMD 2.094.0
and above).


For `dub.sdl` files, this will look something like:

```sdl
dependency "magickd:graphicsmagick_c" repository="git+https://repo.or.cz/magickd.git" \
    version="d331323932b3833aaff5cab0762c7099e3b36555"
```

And for `dub.json` files:

```json
"dependencies": {
    "magickd:graphicsmagick_c": {
        "repository": "git+https://repo.or.cz/magickd.git",
        "version": "d331323932b3833aaff5cab0762c7099e3b36555"
    }
}
```

You then need to specify whether to use static or dynamic bindings.
In you `dub.sdl` file:

```sdl
# For dynamic bindings
versions "GMagick_Dynamic"
# For static bindings
versions "GMagick_Static"
```

In `dub.json`:

```json
{
    // For static bindings
    "versions": ["GMagick_Static"],
    // For dynamic bindings
    "versions": ["GMagick_Dynamic"],
}
```


The final step: specify which Quantum Depth to use (Q8, Q16, or Q32).
You can find this out by running `gm version` in a shell.
It should be in the first lines.

To specify the version in dub.sdl (replace Q16):

```sdl
subConfiguration "magickd:graphicsmagick_c" "Q16"
```

In dub.json (again, replace Q16):

```json
"subConfigurations": {
    "magickd:graphicsmagick_c": "Q16"
}
```

### macOS - MacPorts - Dynamic Bindings

A quick heads up if you've installed GraphicsMagick via [MacPorts], you will
need to make sure that the `LD_LIBRARY_PATH` environment variable will include
the directory which holds `GraphicsMagick.dylib` and `GraphicsMagickWand.dylib`.
By default, the directory is `/opt/local/lib`.  For example:

```sh
$ cd examples/
$ ./dmd.sh --shared
$ LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/opt/local/lib" \
  ./dmd/ping_dmd /path/to/my/picture.png
```

### Logging

By default, graphicsmagick_c does **not** print anything to the stderr file.
You can change this by specifying the `graphicsmagick_c` debug identifier. In
DUB this can be achieved with the `debugVersions` build setting.

The only error messages are those returned by `dlerror` when building the shared
version.

Examples
--------

See the `examples/` directory.

[MacPorts]: https://www.macports.org/

License
-------

`graphicsmagick_c` shares the same license as GraphicsMagick where applicable.
See the `Copyright.txt` file, or <http://www.graphicsmagick.org/Copyright.html>.
