/*
  File:           graphicsmagick_c/examples/ping.d
  Contains:       An example program which will 'ping' each input argument
  Copyright:      (C) 2022, 2023 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/

import core.stdc.stdio;
import core.stdc.string;

import graphicsmagick_c.config;
import graphicsmagick_c.magick;

extern(C) int main(int argc, char** argv)
{
    Image* image;

    ImageInfo* imageInfo;

    ExceptionInfo exception;

    version (GMagick_Static) {}
	else {
		void* libgm;

		bool success = loadGraphicsMagick(libgm);
		if (success) {
			puts("Successfully loaded GraphicsMagick!\n");
		} else {
			puts("Loaded GraphicsMagick with some errors!\n");
		}
	}

    if (argc < 2) {
        fprintf(stderr, "Usage: %s <image1> [<imageN>...]\n", argv[0]);
        return 1;
    }

    InitializeMagick(null);
    GetExceptionInfo(&exception);
    imageInfo = CloneImageInfo(null);

    for (int i = 1; i < argc; i++) {

        /* Ping the image, skipping if failure to read. */

        strcpy(imageInfo.filename.ptr, argv[i]);
        image = PingImage(imageInfo, &exception);
        if (UndefinedException != exception.severity) {
            CatchException(&exception);
            fprintf(stderr, "ERROR(ReadImage): %s\n  %s\n",
                exception.reason, exception.description);
            continue;
        }

        puts("-------");
        printf("File   : %s\n", image.filename.ptr);
        printf("Magick : %s\n", image.magick.ptr);
        printf("Columns: %lu\n", image.columns);
        printf("Rows   : %lu\n", image.rows);
        puts("-------");

        DestroyImage(image);
    }

    DestroyImageInfo(imageInfo);
    DestroyExceptionInfo(&exception);

    DestroyMagick();

    return 0;
}
