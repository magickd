/* Copyright (C) GraphicsMagick Group 2002 - 2022 */
import core.stdc.stdio;
import core.stdc.string;

import graphicsmagick_c.config;
import graphicsmagick_c.magick;

extern (C) int main(int argc, char** argv)
{
    Image* image = null;

    char[MaxTextExtent] infile;
    char[MaxTextExtent] outfile;

    int arg = 1;
    int exit_status = 0;

    ImageInfo* imageInfo;

    ExceptionInfo exception;

    version(GMagick_Static) {}
	else {
		void* libgm;

		bool success = loadGraphicsMagick(libgm);
		if (success) {
			puts("Successfully loaded GraphicsMagick!\n");
		} else {
			puts("Loaded GraphicsMagick with some errors!\n");
		}
	}

    InitializeMagick(null);
    imageInfo = CloneImageInfo(null);
    GetExceptionInfo(&exception);

    if (argc != 3) {
        fprintf(stderr, "Usage: %s infile outfile\n", argv[0]);
        fflush(stderr);
        exit_status = 0;
        goto program_exit;
    }

    // array.ptr and &array[0] are the same thing
    strncpy(infile.ptr, argv[arg], MaxTextExtent-1);
    arg++;
    strncpy(&outfile[0], argv[arg], MaxTextExtent-1);

    strcpy(imageInfo.filename.ptr, infile.ptr);
    image = ReadImage(imageInfo, &exception);
    if (image is null) {
        CatchException(&exception);
        exit_status = 1;
        goto program_exit;
    }

    strcpy(image.filename.ptr, outfile.ptr);
    if (!WriteImage(imageInfo, image)) {
        CatchException(&exception);
        exit_status = 1;
        goto program_exit;
    }

program_exit:

    if (image !is null)
        DestroyImage(image);

    if (imageInfo !is null)
        DestroyImageInfo(imageInfo);

    DestroyMagick();

    return exit_status;
}
