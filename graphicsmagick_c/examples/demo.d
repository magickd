/* Copyright (C) GraphicsMagick Group 2002 - 2022 */
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

import graphicsmagick_c.config;
import graphicsmagick_c.magick;

extern (C) int main(int argc, char** argv)
{
    ExceptionInfo exception;

    Image* image;
    Image* images;
    Image* resize_image;
    Image* thumbnails;

    ImageInfo* image_info;

    int i;

    version (GMagick_Static) {}
	else {
		void* libgm;

		bool success = loadGraphicsMagick(libgm);
		if (success) {
			puts("Successfully loaded GraphicsMagick!\n");
		} else {
			puts("Loaded GraphicsMagick with some errors!\n");
		}
	}

    /*
     * Initialize the image info structure and read the list of files
     * provided by the user as an image sequence.
     */
    InitializeMagick(*argv);
    GetExceptionInfo(&exception);
    image_info = CloneImageInfo(null);
    images = NewImageList();

    for (i = 1; i < argc - 1; i++) {
        strcpy(image_info.filename.ptr, argv[i]);
        printf("Reading %s...\n", image_info.filename.ptr);
        image = ReadImage(image_info, &exception);
        if (exception.severity != UndefinedException)
            CatchException(&exception);
        if (image !is null)
            AppendImageToList(&images, image);
    }

    if (images is null) {
        printf("Failed to read any images!\n");
        exit(1);
    }

    /*
     * Create a thumbnail sequence
     */
    thumbnails = NewImageList();
    while ((image = RemoveFirstImageFromList(&images)) !is null) {
        resize_image = ResizeImage(image, 106, 80, LanczosFilter, 1.0, &exception);
        DestroyImage(image);
        if (resize_image is null) {
            CatchException(&exception);
            continue;
        }
        AppendImageToList(&thumbnails, resize_image);
    }

    /*
     * Write the thumbnail image sequence to file.
     */
    if (thumbnails !is null) {
        strcpy(thumbnails.filename.ptr, argv[argc - 1]);
        image_info.adjoin = MagickTrue;
        printf("Writing %s ... %lu frames\n", thumbnails.filename.ptr,
            GetImageListLength(thumbnails));
        WriteImage(image_info, thumbnails);
    }

    /*
     * Release resources
     */
    DestroyImageList(thumbnails);
    DestroyImageInfo(image_info);
    DestroyExceptionInfo(&exception);
    DestroyMagick();
    return 0;
}
