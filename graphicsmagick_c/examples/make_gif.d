/*
  File:           graphicsmagick_c/examples/make_gif.d
  Contains:       An example program to make a GIF from multiple input images.
  Copyright:      (C) 2022, 2023 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

import graphicsmagick_c.config;
import graphicsmagick_c.magick;

extern (C) int main(int argc, char **argv)
{

    version (GMagick_Static) {}
	else {
		void* libgm;

		bool success = loadGraphicsMagick(libgm);
		if (success) {
			puts("Successfully loaded GraphicsMagick!\n");
		} else {
			puts("Loaded GraphicsMagick with some errors!\n");
		}
	}

    if (argc < 4) {
        fprintf(stderr, "Usage: %s <image1> <image2> [<imageN>...] <output.gif>\n", argv[0]);
        return EXIT_FAILURE;
    }

    // Current input image
    Image* image;
    // The in-memory list of images for GIF
    Image* images;

    ImageInfo* imageInfo;

    ExceptionInfo exception;

    InitializeMagick(null);

    GetExceptionInfo(&exception);
    imageInfo = CloneImageInfo(null);
    images = NewImageList();

    for (int i = 1; i < argc - 1; i++) {
        strcpy(imageInfo.filename.ptr, argv[i]);
        
        printf("Reading image %s...\n", argv[i]);

        // No need to destroy this image, since it is destoryed when the
        // `images` are destroyed.
        image = ReadImage(imageInfo, &exception);

        if (UndefinedException != exception.severity) {
            CatchException(&exception);
            fprintf(stderr, "ERROR(ReadImage): %s\n  %s\n", exception.reason, exception.description);
        }

        if (null !is image) {
            image.delay = 200;               // delay is in ms
            image.dispose = PreviousDispose; // completely dispose of previous image
            image.iterations = 0;            // no limit on the number of loops
            AppendImageToList(&images, image);
        }
    }

    if (null !is images) {
        printf("Writing %s...\n", argv[argc - 1]);
        WriteImages(imageInfo, images, argv[argc - 1], &exception);
        if (UndefinedException != exception.severity) {
            CatchException(&exception);
            fprintf(stderr, "ERROR(WriteImages): %s\n  %s\n", exception.reason, exception.description);
        }
        DestroyImageList(images);
    }

    if (null !is imageInfo)
        DestroyImageInfo(imageInfo);
    
    DestroyExceptionInfo(&exception);
    DestroyMagick();

    return 0;
}
