// Copyright (C) GraphicsMagick Group 2002 - 2022
import core.stdc.stdio;

import graphicsmagick_c.config;
import graphicsmagick_c.wand.wand_api;

extern (C) int main(int argc, char** argv)
{
    MagickWand* magick_wand;
    MagickPassFail status = MagickPass;

    char* infile, outfile;

    if (argc != 3) {
        fprintf(stderr, "Usage: %s infile outfile\n", argv[0]);
        return 1;
    }

    infile = argv[1];
    outfile = argv[2];

    // Load dynamic bindings if required
    version (GMagick_Static) {}
	else {
		void* libgm;
        void* libgmwand;

		bool success = loadGraphicsMagick(libgm);
		if (success) {
			puts("Successfully loaded GraphicsMagick!\n");
		} else {
			puts("Loaded GraphicsMagick with some errors!\n");
		}

        success = loadGraphicsMagickWand(libgmwand);
        if (success) {
            puts("Successfully loaded GraphicsMagickWand!\n");
        } else {
            puts("Loaded GraphicsMagikcWand with some errors!\n");
        }
	}

    // Initialize GraphicsMagick API
    InitializeMagick(*argv);

    // Allocate Wand handle
    magick_wand = NewMagickWand();

    // Read input image file
    if (status == MagickPass) {
        status = MagickReadImage(magick_wand, infile);
    }

    // Rotate the image clockwise 30 degrees with a black background
    if (status == MagickPass) {
        PixelWand* background;
        background = NewPixelWand();
        PixelSetColor(background, "#000000");
        status = MagickRotateImage(magick_wand, background, 30);
        DestroyPixelWand(background);
    }

    // Write output file
    if (status == MagickPass) {
        status = MagickWriteImage(magick_wand, outfile);
    }

    // Diagnose any error
    if (status != MagickPass) {
        char* description;
        ExceptionType severity;

        description = MagickGetException(magick_wand, &severity);
        fprintf(stderr, "%.1024s (severity %d)\n", description, severity);
    }

    // Release Wand handle
    DestroyMagickWand(magick_wand);

    // Destroy GraphicsMagick API
    DestroyMagick();

    return (status == MagickPass ? 0 : 1);
}
