/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Shear Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.shear;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : AffineMatrix, Image;

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    Image* AffineTransformImage(const scope Image*, const scope AffineMatrix*, ExceptionInfo*);
    Image* RotateImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* ShearImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mAffineTransformImage = Image* function(const scope Image*, const scope AffineMatrix*, ExceptionInfo*);
        alias mRotateImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mShearImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);
    }

    __gshared
    {
        mAffineTransformImage AffineTransformImage;
        mRotateImage RotateImage;
        mShearImage ShearImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadShearH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            AffineTransformImage = cast(mAffineTransformImage)dlsym(dlib, "AffineTransformImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.shear:\n  %s\n", errmsg);
                }
                success = false;
            }
            RotateImage = cast(mRotateImage)dlsym(dlib, "RotateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.shear:\n  %s\n", errmsg);
                }
                success = false;
            }
            ShearImage = cast(mShearImage)dlsym(dlib, "ShearImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.shear:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
