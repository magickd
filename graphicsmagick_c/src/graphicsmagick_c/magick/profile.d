/*
  Copyright (C) 2004, 2005 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Methods For Manipulating Embedded Image Profiles.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.profile;

import graphicsmagick_c.magick.api : MagickBool, MagickPassFail;
import graphicsmagick_c.magick.image : Image;

/*
  Generic iterator for traversing profiles.
*/
alias ImageProfileIterator = void*;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    const(ubyte)* GetImageProfile(const scope Image* image, const scope char* name, size_t* length);

    MagickPassFail DeleteImageProfile(Image* image, const scope char* name);

    MagickPassFail ProfileImage(Image* image, const scope char* name, ubyte* profile,
            const scope size_t length, MagickBool clone);

    ImageProfileIterator* AllocateImageProfileIterator(const scope Image* image);

    MagickPassFail NextImageProfile(ImageProfileIterator profile_iterator, const(char)** name,
            const(ubyte)** profile, size_t* length);

    void DeallocateImageProfileIterator(ImageProfileIterator profile_iterator);

} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetImageProfile = const(ubyte)* function(const scope Image* image, const scope char* name, size_t* length);

        alias mDeleteImageProfile = MagickPassFail function(Image* image, const scope char* name);

        alias mProfileImage = MagickPassFail function(Image* image, const scope char* name, ubyte* profile,
                const scope size_t length, MagickBool clone);

        alias mAllocateImageProfileIterator = ImageProfileIterator* function(const scope Image* image);

        alias mNextImageProfile = MagickPassFail function(ImageProfileIterator profile_iterator, const(char)** name,
                const(ubyte)** profile, size_t* length);

        alias mDeallocateImageProfileIterator = void function(ImageProfileIterator profile_iterator);
    }

    __gshared
    {
        mGetImageProfile GetImageProfile;

        mDeleteImageProfile DeleteImageProfile;

        mProfileImage ProfileImage;

        mAllocateImageProfileIterator AllocateImageProfileIterator;

        mNextImageProfile NextImageProfile;

        mDeallocateImageProfileIterator DeallocateImageProfileIterator;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadProfileH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetImageProfile = cast(mGetImageProfile)dlsym(dlib, "GetImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            DeleteImageProfile = cast(mDeleteImageProfile)dlsym(dlib, "DeleteImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            ProfileImage = cast(mProfileImage)dlsym(dlib, "ProfileImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            AllocateImageProfileIterator = cast(mAllocateImageProfileIterator)dlsym(dlib, "AllocateImageProfileIterator");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            NextImageProfile = cast(mNextImageProfile)dlsym(dlib, "NextImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            DeallocateImageProfileIterator = cast(mDeallocateImageProfileIterator)dlsym(dlib, "DeallocateImageProfileIterator");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.profile:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
