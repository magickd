/*
  Copyright (C) 2003, 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Log methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.deprecate;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.constitute : QuantumType;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.forward : ViewInfo;
import graphicsmagick_c.magick.image : Image, PixelPacket;

version(GMagick_Static)
{
    @system @nogc nothrow extern(C):

    uint PopImagePixels(const scope Image*, const scope QuantumType, ubyte*);
    uint PushImagePixels(Image*, const scope QuantumType, const scope ubyte*);

    void* AcquireMemory(const scope size_t);
    void* CloneMemory(void*, const scope void*, const scope size_t);

    void LiberateMemory(void**);
    void ReacquireMemory(void**, const scope size_t);

    PixelPacket* AcquireCacheView(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns,
        const scope c_ulong rows, ExceptionInfo* exception);
    PixelPacket* GetCacheView(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows);
    PixelPacket* SetCacheView(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows);

    MagickPassFail SyncCacheView(ViewInfo* view);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mPopImagePixels = uint function(const scope Image*, const scope QuantumType, ubyte*);
        alias mPushImagePixels = uint function(Image*, const scope QuantumType, const scope ubyte*);

        alias mAcquireMemory = void* function(const scope size_t);
        alias mCloneMemory = void* function(void*, const scope void*, const scope size_t);

        alias mLiberateMemory = void function(void**);
        alias mReacquireMemory = void function(void**, const scope size_t);

        alias mAcquireCacheView = PixelPacket* function(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows, ExceptionInfo* exception);
        alias mGetCacheView = PixelPacket* function(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows);
        alias mSetCacheView = PixelPacket* function(ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows);

        alias mSyncCacheView = MagickPassFail function(ViewInfo* view);
    }

    __gshared
    {
        mPopImagePixels PopImagePixels;
        mPushImagePixels PushImagePixels;

        mAcquireMemory AcquireMemory;
        mCloneMemory CloneMemory;

        mLiberateMemory LiberateMemory;
        mReacquireMemory ReacquireMemory;

        mAcquireCacheView AcquireCacheView;
        mGetCacheView GetCacheView;
        mSetCacheView SetCacheView;

        mSyncCacheView SyncCacheView;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadDeprecateH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            PopImagePixels = cast(mPopImagePixels)dlsym(dlib, "PopImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }
            PushImagePixels = cast(mPushImagePixels)dlsym(dlib, "PushImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireMemory = cast(mAcquireMemory)dlsym(dlib, "AcquireMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }
            CloneMemory = cast(mCloneMemory)dlsym(dlib, "CloneMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }

            LiberateMemory = cast(mLiberateMemory)dlsym(dlib, "LiberateMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReacquireMemory = cast(mReacquireMemory)dlsym(dlib, "ReacquireMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireCacheView = cast(mAcquireCacheView)dlsym(dlib, "AcquireCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetCacheView = cast(mGetCacheView)dlsym(dlib, "GetCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetCacheView = cast(mSetCacheView)dlsym(dlib, "SetCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }

            SyncCacheView = cast(mSyncCacheView)dlsym(dlib, "SyncCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.deprecate:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
