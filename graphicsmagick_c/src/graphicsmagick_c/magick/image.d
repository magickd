/*
  Copyright (C) 2003 - 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Image Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.image;

import graphicsmagick_c.magick.api : MagickBool, MagickFalse,
    MagickPassFail, MaxTextExtent;
import graphicsmagick_c.magick.colorspace : ColorspaceType;
import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.forward : _Ascii85InfoPtr_, _BlobInfoPtr_,
    _CacheInfoPtr_, _ImageAttributePtr_, _SemaphoreInfoPtr_, _ThreadViewSetPtr_;
import graphicsmagick_c.magick.timer : TimerInfo;

import core.stdc.config;
import core.stdc.stdio : FILE;

version (Q8) {
    enum QuantumDepth = 8;
    alias Quantum = ubyte;
} else version (Q16) {
    enum QuantumDepth = 16;
    alias Quantum = ushort;
} else version (Q32) {
    enum QuantumDepth = 32;
    alias Quantum = uint;
} else {
    enum QuantumDepth = -1;
}

/*
  Maximum unsigned RGB value which fits in the specified bits
*/
pragma(inline, true)
c_ulong MaxValueGivenBits(T)(T bits)
{
    return cast(c_ulong)(0x01UL << (bits - 1)) + ((0x01UL << (bits - 1)) - 1);
}

static if (QuantumDepth == 8) {
    enum MaxColormapSize = 256U;
    enum MaxMap = 255U;
    enum MaxMapFloat = 255.0f;
    enum MaxMapDouble = 255.0;
    enum MaxRGB =  255U;
    enum MaxRGBFloat = 255.0f;
    enum MaxRGBDouble = 255.0;
    pragma(inline, true) ubyte ScaleCharToMap(uint value)          { return cast(ubyte)(value); }
    pragma(inline, true) Quantum ScaleCharToQuantum(uint value)    { return cast(Quantum)(value); }
    pragma(inline, true) Quantum ScaleLongToQuantum(c_ulong value) { return cast(Quantum)(value/16_843_009UL); }
    pragma(inline, true) uint ScaleMapToChar(ubyte value)          { return cast(uint)(value); }
    pragma(inline, true) Quantum ScaleMapToQuantum(ubyte value)    { return cast(Quantum)(value); }
    pragma(inline, true) c_ulong ScaleQuantum(Quantum quantum)       { return cast(c_ulong)(quantum); }
    pragma(inline, true) ubyte ScaleQuantumToChar(Quantum quantum)   { return cast(ubyte)(quantum); }
    pragma(inline, true) c_ulong ScaleQuantumToLong(Quantum quantum) { return cast(c_ulong)(16_843_009UL*quantum); }
    pragma(inline, true) ubyte ScaleQuantumToMap(Quantum quantum)    { return cast(ubyte)(quantum); }
    pragma(inline, true) ushort ScaleQuantumToShort(Quantum quantum) { return cast(ushort)(257U*quantum); }
    pragma(inline, true) Quantum ScaleShortToQuantum(ushort value) { return cast(Quantum)(value/257U); }
    pragma(inline, true) c_ulong ScaleToQuantum(T)(T value)        { return cast(c_ulong)(value); }
    pragma(inline, true) ubyte ScaleQuantumToIndex(Quantum value)    { return cast(ubyte)(value); }
} else static if (QuantumDepth == 16) {
    enum MaxColormapSize =  65_536U;
    enum MaxMap = 65_535U;
    enum MaxMapFloat = 65_535.0f;
    enum MaxMapDouble = 65_535.0;
    enum MaxRGB = 65_535;
    enum MaxRGBFloat = 65_535.0f;
    enum MaxRGBDouble = 65_535.0;
    pragma(inline, true) ushort ScaleCharToMap(uint value)         { return cast(ushort)(257U*(value)); }
    pragma(inline, true) Quantum ScaleCharToQuantum(uint value)    { return cast(Quantum)(value); }
    pragma(inline, true) Quantum ScaleLongToQuantum(c_ulong value) { return cast(Quantum)(value/65_537UL); }
    pragma(inline, true) uint ScaleMapToChar(ushort value)         { return cast(uint)(value/257U); }
    pragma(inline, true) Quantum ScaleMapToQuantum(ushort value)   { return cast(Quantum)(value); }
    pragma(inline, true) c_ulong ScaleQuantum(Quantum quantum)       { return cast(c_ulong)(quantum/257UL); }
    pragma(inline, true) ubyte ScaleQuantumToChar(Quantum quantum)   { return cast(ubyte)(quantum/257U); }
    pragma(inline, true) c_ulong ScaleQuantumToLong(Quantum quantum) { return cast(c_ulong)(65_537UL*quantum); }
    pragma(inline, true) ushort ScaleQuantumToMap(Quantum quantum)   { return cast(ushort)(quantum); }
    pragma(inline, true) ushort ScaleQuantumToShort(Quantum quantum) { return cast(ushort)(quantum); }
    pragma(inline, true) Quantum ScaleShortToQuantum(ushort value) { return cast(Quantum)(value); }
    pragma(inline, true) c_ulong ScaleToQuantum(T)(T value)        { return cast(c_ulong)(257UL*value); }
    pragma(inline, true) ushort ScaleQuantumToIndex(Quantum value)   { return cast(ushort)(value); }
} else static if (QuantumDepth == 32) {
    enum MaxColormapSize = 65_536U;
    enum MaxRGB = 4_294_967_295U;
    enum MaxRGBFloat = 294_967_295.0f;
    enum MaxRGBDouble = 294_967_295.0;
    pragma(inline, true) Quantum ScaleCharToQuantum(ubyte value)   { return cast(Quantum)(16_843_009U*value); }
    pragma(inline, true) Quantum ScaleLongToQuantum(c_ulong value) { return cast(Quantum)(value); }
    pragma(inline, true) c_ulong ScaleQuantum(Quantum quantum)       { return cast(c_ulong)(quantum/16_843_009UL); }
    pragma(inline, true) ubyte ScaleQuantumToChar(Quantum quantum)   { return cast(ubyte)(quantum/16_843_009U); }
    pragma(inline, true) c_ulong ScaleQuantumToLong(Quantum quantum) { return cast(c_ulong)(quantum); }
    pragma(inline, true) ushort ScaleQuantumToShort(Quantum quantum) { return cast(ushort)(quantum/65_537U); }
    pragma(inline, true) Quantum ScaleShortToQuantum(ushort value) { return cast(Quantum) (65_537U*value); }
    pragma(inline, true) c_ulong ScaleToQuantum(T)(T value)        { return cast(c_ulong) (16_843_009UL*value); }
    pragma(inline, true) ushort ScaleQuantumToIndex(Quantum value)   { return cast(ushort) (value/65_537U); }

    /*
      MaxMap defines the maximum index value for algorithms which depend
      on lookup tables (e.g. colorspace transformations and
      normalization). When MaxMap is less than MaxRGB it is necessary to
      downscale samples to fit the range of MaxMap. The number of bits
      which are effectively preserved depends on the size of MaxMap.
      MaxMap should be a multiple of 255 and no larger than MaxRGB.  Note
      that tables can become quite large and as the tables grow larger it
      may take more time to compute the table than to process the image.
    */
    enum MaxMap = 65_535U;
    enum MaxMapFloat = 65_535.0f;
    enum MaxMapDouble = 65_535.0;
    static if (MaxMap == 65_535U) {
      pragma(inline, true) ushort ScaleCharToMap(uint value)       { return cast(ushort) (257U*value); }
      pragma(inline, true) uint ScaleMapToChar(ushort value)       { return cast(uint) (value/257U); }
      pragma(inline, true) Quantum ScaleMapToQuantum(ushort value) { return cast(Quantum) (65_537U*value); }
      pragma(inline, true) ushort ScaleQuantumToMap(Quantum quantum) { return cast(ushort) (quantum/65_537U); }
    } else {
      pragma(inline, true) ushort ScaleCharToMap(uint value)       { return cast(ushort) ((MaxMap/255U)*value); }
      pragma(inline, true) uint ScaleMapToChar(ushort value)       { return cast(uint) (value/(MaxMap/255U)); }
      pragma(inline, true) Quantum ScaleMapToQuantum(ushort value) { return cast(Quantum) ((MaxRGB/MaxMap)*value); }
      pragma(inline, true) ushort ScaleQuantumToMap(Quantum quantum) { return cast(ushort) (quantum/(MaxRGB/MaxMap)); }
    }
} else {
    static assert(0, "Missing/Invalid QuantumDepth version (supported: Q8, Q16, or Q32).");
}

enum OpaqueOpacity = 0UL;
enum TransparentOpacity = MaxRGB;

pragma(inline, true)
Quantum RoundDoubleToQuantum(double value)
{
    return cast(Quantum)(value < 0.0 ? 0U : (value > MaxRGBDouble) ? MaxRGB : value + 0.5);
}

pragma(inline, true)
Quantum RoundFloatToQuantum(float value)
{
    return cast(Quantum)(value < 0.0f ? 0U : (value > MaxRGBFloat) ? MaxRGB : value + 0.5f);
}

pragma(inline, true)
int ConstrainToRange(int min, int max, int value)
{
    return (value < min ? min : (value > max) ? max : value);
}

pragma(inline, true)
int ConstrainToQuantum(int value)
{
    return ConstrainToRange(0, MaxRGB, value);
}

pragma(inline, true) /* MD: A and B can be the same */
Quantum ScaleAnyToQuantum(A, B)(A x, B max_value)
{
    return cast(Quantum)((cast(double)MaxRGBDouble*x)/max_value+0.5);
}

pragma(inline, true)
const(char[]) MagickBoolToString(MagickBool value)
{
    return value != MagickFalse ? "True" : "False";
}

/*
  Return MagickTrue if channel is enabled in channels.  Allows using
  code to adapt if ChannelType enumeration is changed to bit masks.
*/
pragma(inline, true)
bool MagickChannelEnabled(ChannelType channels, ChannelType channel)
{
    return (channels == AllChannels) || (channels == channel);
}

/*
  Deprecated defines.
*/
alias RunlengthEncodedCompression = RLECompression;
pragma(inline, true) Quantum RoundSignedToQuantum(double value) { return RoundDoubleToQuantum(value); }
pragma(inline, true) Quantum RoundToQuantum(double value) { return RoundDoubleToQuantum(value); }

/*
  Enum declarations.
*/

alias AlphaType = int;
enum : AlphaType
{
    UnspecifiedAlpha,
    AssociatedAlpha,
    UnassociatedAlpha
}

alias ChannelType = int;
enum : ChannelType
{
    UndefinedChannel,
    RedChannel,     /* RGB Red channel */
    CyanChannel,    /* CMYK Cyan channel */
    GreenChannel,   /* RGB Green channel */
    MagentaChannel, /* CMYK Magenta channel */
    BlueChannel,    /* RGB Blue channel */
    YellowChannel,  /* CMYK Yellow channel */
    OpacityChannel, /* Opacity channel */
    BlackChannel,   /* CMYK Black (K) channel */
    MatteChannel,   /* Same as Opacity channel (deprecated) */
    AllChannels,    /* Color channels */
    GrayChannel     /* Color channels represent an intensity. */
}

alias ClassType = int;
enum : ClassType
{
    UndefinedClass,
    DirectClass,
    PseudoClass
}

alias ComplianceType = int;
enum : ComplianceType
{
    UndefinedCompliance = 0x0000,
    NoCompliance = 0x0000,
    SVGCompliance = 0x0001,
    X11Compliance = 0x0002,
    XPMCompliance = 0x0004,
    AllCompliance = 0xffff
}

alias CompositeOperator = int;
enum : CompositeOperator
{
    UndefinedCompositeOp = 0,
    OverCompositeOp,
    InCompositeOp,
    OutCompositeOp,
    AtopCompositeOp,
    XorCompositeOp,
    PlusCompositeOp,
    MinusCompositeOp,
    AddCompositeOp,
    SubtractCompositeOp,
    DifferenceCompositeOp,
    MultiplyCompositeOp,
    BumpmapCompositeOp,
    CopyCompositeOp,
    CopyRedCompositeOp,
    CopyGreenCompositeOp,
    CopyBlueCompositeOp,
    CopyOpacityCompositeOp,
    ClearCompositeOp,
    DissolveCompositeOp,
    DisplaceCompositeOp,
    ModulateCompositeOp,
    ThresholdCompositeOp,
    NoCompositeOp,
    DarkenCompositeOp,
    LightenCompositeOp,
    HueCompositeOp,
    SaturateCompositeOp,
    ColorizeCompositeOp,
    LuminizeCompositeOp,
    ScreenCompositeOp, /* Not yet implemented */
    OverlayCompositeOp,  /* Not yet implemented */
    CopyCyanCompositeOp,
    CopyMagentaCompositeOp,
    CopyYellowCompositeOp,
    CopyBlackCompositeOp,
    DivideCompositeOp
}

alias CompressionType = int;
enum : CompressionType
{
    UndefinedCompression,
    NoCompression,
    BZipCompression,
    FaxCompression,
    Group4Compression,
    JPEGCompression,
    LosslessJPEGCompression,
    LZWCompression,
    RLECompression,
    ZipCompression
}

alias DisposeType = int;
enum : DisposeType
{
    UndefinedDispose,
    NoneDispose,
    BackgroundDispose,
    PreviousDispose
}

alias EndianType = int;
enum : EndianType
{
    UndefinedEndian,
    LSBEndian,            /* "little" endian */
    MSBEndian,            /* "big" endian */
    NativeEndian          /* native endian */
}

alias FilterTypes = int;
enum : FilterTypes
{
    UndefinedFilter,
    PointFilter,
    BoxFilter,
    TriangleFilter,
    HermiteFilter,
    HanningFilter,
    HammingFilter,
    BlackmanFilter,
    GaussianFilter,
    QuadraticFilter,
    CubicFilter,
    CatromFilter,
    MitchellFilter,
    LanczosFilter,
    BesselFilter,
    SincFilter
}

alias GeometryFlags = int;
enum : GeometryFlags
{
    NoValue = 0x0000,
    XValue = 0x0001,
    YValue = 0x0002,
    WidthValue = 0x0004,
    HeightValue = 0x0008,
    AllValues = 0x000F,
    XNegative = 0x0010,
    YNegative = 0x0020,
    PercentValue = 0x1000,
    AspectValue = 0x2000,
    LessValue = 0x4000,
    GreaterValue = 0x8000,
    AreaValue = 0x10000
}

alias GravityType = int;
enum : GravityType
{
    ForgetGravity,
    NorthWestGravity,
    NorthGravity,
    NorthEastGravity,
    WestGravity,
    CenterGravity,
    EastGravity,
    SouthWestGravity,
    SouthGravity,
    SouthEastGravity,
    StaticGravity
}

alias ImageType = int;
enum : ImageType
{
    UndefinedType,
    BilevelType,
    GrayscaleType,
    GrayscaleMatteType,
    PaletteType,
    PaletteMatteType,
    TrueColorType,
    TrueColorMatteType,
    ColorSeparationType,
    ColorSeparationMatteType,
    OptimizeType
}

alias InterlaceType = int;
enum : InterlaceType
{
    UndefinedInterlace,
    NoInterlace,
    LineInterlace,
    PlaneInterlace,
    PartitionInterlace
}

alias MontageMode = int;
enum : MontageMode
{
    UndefinedMode,
    FrameMode,
    UnframeMode,
    ConcatenateMode
}

alias NoiseType = int;
enum : NoiseType
{
    UniformNoise,
    GaussianNoise,
    MultiplicativeGaussianNoise,
    ImpulseNoise,
    LaplacianNoise,
    PoissonNoise
}

/*
  Image orientation.  Based on TIFF standard values.
*/
alias OrientationType = int;
enum : OrientationType       /* Line direction / Frame Direction */
{                            /* -------------- / --------------- */
    UndefinedOrientation,    /* Unknown        / Unknown         */
    TopLeftOrientation,      /* Left to right  / Top to bottom   */
    TopRightOrientation,     /* Right to left  / Top to bottom   */
    BottomRightOrientation,  /* Right to left  / Bottom to top   */
    BottomLeftOrientation,   /* Left to right  / Bottom to top   */
    LeftTopOrientation,      /* Top to bottom  / Left to right   */
    RightTopOrientation,     /* Top to bottom  / Right to left   */
    RightBottomOrientation,  /* Bottom to top  / Right to left   */
    LeftBottomOrientation    /* Bottom to top  / Left to right   */
}

alias PreviewType = int;
enum : PreviewType
{
    UndefinedPreview = 0,
    RotatePreview,
    ShearPreview,
    RollPreview,
    HuePreview,
    SaturationPreview,
    BrightnessPreview,
    GammaPreview,
    SpiffPreview,
    DullPreview,
    GrayscalePreview,
    QuantizePreview,
    DespecklePreview,
    ReduceNoisePreview,
    AddNoisePreview,
    SharpenPreview,
    BlurPreview,
    ThresholdPreview,
    EdgeDetectPreview,
    SpreadPreview,
    SolarizePreview,
    ShadePreview,
    RaisePreview,
    SegmentPreview,
    SwirlPreview,
    ImplodePreview,
    WavePreview,
    OilPaintPreview,
    CharcoalDrawingPreview,
    JPEGPreview
}

alias RenderingIntent = int;
enum : RenderingIntent
{
    UndefinedIntent,
    SaturationIntent,
    PerceptualIntent,
    AbsoluteIntent,
    RelativeIntent
}

alias ResolutionType = int;
enum : ResolutionType
{
    UndefinedResolution,
    PixelsPerInchResolution,
    PixelsPerCentimeterResolution
}

/*
 * Typedef declarations.
 */
struct _AffineMatrix
{
    double sx;
    double rx;
    double ry;
    double sy;
    double tx;
    double ty;
}
alias AffineMatrix = _AffineMatrix;

struct _PrimaryInfo
{
    double x;
    double y;
    double z;
}
alias PrimaryInfo = _PrimaryInfo;

struct _ChromaticityInfo
{
    PrimaryInfo red_primary;
    PrimaryInfo green_primary;
    PrimaryInfo blue_primary;
    PrimaryInfo white_point;
}
alias ChromaticityInfo = _ChromaticityInfo;

version (MAGICK_IMPLEMENTATION) {
    /*
     * Useful macros for accessing PixelPacket members in a generic way.
     */
    pragma(inline, true) Quantum GetRedSample(PixelPacket p) { return p.red; }
    pragma(inline, true) Quantum GetGreenSample(PixelPacket p) { return p.green; }
    pragma(inline, true) Quantum GetBlueSample(PixelPacket p) { return p.blue; }
    pragma(inline, true) Quantum GetOpacitySample(PixelPacket p) { return p.opacity; }

    pragma(inline, true) Quantum SetRedSample(PixelPacket q, Quantum value)
    {
        return q.red = value;
    }
    pragma(inline, true) Quantum SetGreenSample(PixelPacket q, Quantum value)
    {
        return q.green = value;
    }
    pragma(inline, true) Quantum SetBlueSample(PixelPacket q, Quantum value)
    {
        return q.blue = value;
    }
    pragma(inline, true) Quantum SetOpacitySample(PixelPacket q, Quantum value)
    {
        return q.opacity = value;
    }
} /* version (MAGICK_IMPLEMENTATION) */

struct _PixelPacket
{
    version(BigEndian) {
        enum MAGICK_PIXELS_RGBA = 1;
        Quantum red, green, blue, opacity;
    } else {
        /* BGRA (as used by microsoft windows DIB) */
        enum MAGICK_PIXELS_BGRA = 1;
        Quantum blue, green, red, opacity;
    }
}
alias PixelPacket = _PixelPacket;

struct _ColorInfo
{
    char* path;
    char* name;

    ComplianceType compliance;

    PixelPacket color;

    uint stealth;

    c_ulong signature;

    _ColorInfo* previous;
    _ColorInfo* next;
}
alias ColorInfo = _ColorInfo;

struct _DoublePixelPacket
{
    double red;
    double green;
    double blue;
    double opacity;
}
alias DoublePixelPacket = _DoublePixelPacket;

/*
 * ErrorInfo is used to record statistical difference (error)
 * information based on computed Euclidean distance in RGB space.
 */
struct _ErrorInfo
{
    double mean_error_per_pixel;     /** Average error per pixel (absolute range) */
    double normalized_mean_error;    /** Average error per pixel (normalized to 1.0) */
    double normalized_maximum_error; /** Maximum error encountered (normalized to 1.0) */
}
alias ErrorInfo = _ErrorInfo;

struct _FrameInfo
{
    c_ulong width;
    c_ulong height;

    c_long x;
    c_long y;
    c_long inner_bevel;
    c_long outer_bevel;
}
alias FrameInfo = _FrameInfo;

alias IndexPacket = Quantum;

struct _LongPixelPacket
{
    c_ulong red;
    c_ulong green;
    c_ulong blue;
    c_ulong opactiy;
}
alias LongPixelPacket = _LongPixelPacket;

struct _MontageInfo
{
    char* geometry;
    char* tile;
    char* title;
    char* frame;
    char* texture;
    char* font;

    double pointsize;

    c_ulong border_width;

    uint shadow;

    PixelPacket fill;
    PixelPacket stroke;
    PixelPacket background_color;
    PixelPacket border_color;
    PixelPacket matte_color;

    GravityType gravity;

    char[MaxTextExtent] filename;

    c_ulong signature;
}
alias MontageInfo = _MontageInfo;

struct _ProfileInfo
{
    size_t length;

    char* name;

    ubyte* info;
}
alias ProfileInfo = _ProfileInfo;

struct _RectangleInfo
{
    c_ulong width;
    c_ulong height;

    c_long x;
    c_long y;
}
alias RectangleInfo = _RectangleInfo;

struct _SegmentInfo
{
    double x1;
    double y1;
    double x2;
    double y2;
}
alias SegmentInfo = _SegmentInfo;

struct _ImageChannelStatistics
{
    /** Minimum value observed */
    double maximum;
    /** Maximum value observed */
    double minimum;
    /** Average (mean) value observed */
    double mean;
    /** Standard deviation, sqrt(variance) */
    double standard_deviation;
    /** Variance */
    double variance;
}
alias ImageChannelStatistics = _ImageChannelStatistics;

struct _ImageStatistics
{
    ImageChannelStatistics red;
    ImageChannelStatistics green;
    ImageChannelStatistics blue;
    ImageChannelStatistics opacity;
}
alias ImageStatistics = _ImageStatistics;

struct _ImageCharacteristics
{
    MagickBool cmyk;       /** CMYK(A) image */
    MagickBool grayscale;  /** Grayscale image */
    MagickBool monochrome; /** Black/white image */
    MagickBool opaque;     /** Opaque image */
    MagickBool palette;    /** Colormapped image */
}
alias ImageCharacteristics = _ImageCharacteristics;

struct _Image
{
    ClassType storage_class;          /** DirectClass (TrueColor) or PseudoClass (colormapped) */

    ColorspaceType colorspace;        /** Current image colorspace/model */

    CompressionType compression;      /** Compression algorithm to use when encoding image */

    MagickBool dither;                /** True if image is to be dithered */
    MagickBool matte;                 /** True if image has an opacity (alpha) channel */

    c_ulong columns;                  /** Number of image columns */
    c_ulong rows;                     /** Number if image rows */

    uint colors;                      /** Current number of colors in PseudoClass colormap */
    uint depth;                       /** Bits of precision to preserve in color quantum */

    PixelPacket *colormap;            /** Pseudoclass colormap array */

    PixelPacket background_color;     /** Background color */
    PixelPacket border_color;         /** Border color */
    PixelPacket matte_color;          /** Matte (transparent) color */

    double gamma;                     /** Image gamma (e.g. 0.45) */

    ChromaticityInfo chromaticity;    /** Red, green, blue, and white chromaticity values */

    OrientationType orientation;      /** Image orientation */

    RenderingIntent rendering_intent; /** Rendering intent */

    ResolutionType units;             /** Units of image resolution (density) */

    char* montage;                    /** Tile size and offset within an image montage */
    char* directory;                  /** Tile names from within an image montage */
    char* geometry;                   /** Composite/Crop options */

    c_long offset;                    /** Offset to start of image data */

    double x_resolution;              /** Horizontal resolution (also see units) */
    double y_resolution;              /** Vertical resolution (also see units) */

    RectangleInfo page;               /** Offset to apply when placing image */
    RectangleInfo tile_info;          /** Subregion tile dimensions and offset */

    double blur;                      /** Filter to use when zoomin image */
    double fuzz;                      /** Colors within this distance match target color */

    FilterTypes filter;               /** Filter to use when zooming image */

    InterlaceType interlace;          /** Interlace pattern to use when writing image */

    EndianType endian;                /** Byte order to use when writing image */

    GravityType gravity;              /** Image placement gravity */

    CompositeOperator compose;        /** Image placement composition */

    DisposeType dispose;              /** GIF disposal option */

    c_ulong scene;                    /** Animation frame scene number */
    c_ulong delay;                    /** Animation frame scene delay */
    c_ulong iterations;               /** Animation iterations */
    c_ulong total_colors;             /** Number of unique colors. See GetNumberColors() */

    c_long start_loop;                /** Animation frame number to start looping at */

    ErrorInfo error;                  /** Computed image comparison or quantization error */

    TimerInfo timer;                  /** Operation micro-timer */

    void* client_data;                /** User specified opaque data pointer */

    /**
     * Output file name.
     *
     * A colon delimited format identifier may be prepended to the file
     * name in order to force a particular output format. Otherwise the
     * file extension is used. If no format prefix or file extension is
     * present, then the output format is determined by the 'magick'
     * field.
     */
    char[MaxTextExtent] filename;

    /** Original file name (name of the input image file) */
    char[MaxTextExtent] magick_filename;

    /**
     * File format of the input file, and the default output format.
     *
     * The precedence when selecting the output format is:
     * $(OL
     *   $(LI magick prefix to file name (e.g. "jpeg:foo").)
     *   $(LI file name extension. (e.g. "foo.jpg"))
     *   $(LI content of this magick field.)
     * )
     */
    char[MaxTextExtent] magick;

    /** Orignal image width (before transformations) */
    c_ulong magick_columns;

    /** Original image height (before transformations) */
    c_ulong magick_rows;

    ExceptionInfo exception;          /** Any error associated with this image frame */

    _Image* previous;                 /** Pointer to previous frame */
    _Image* next;                     /** Pointer to next frame */

    /*
     * Only private members appear past this point.
     */

    void* profiles;                   /* Private, Embedded profiles */

    uint is_monochrome;               /* Private, True if image is known to be monochrome */
    uint is_grayscale;                /* Private, True if image is known to be grayscale */
    uint taint;                       /* Private, True if image has not been modified */

    _Image* clip_mask;                /* Private, Clipping mask to apply when updating pixels */

    MagickBool ping;                  /* Private, if true, pixels are undefined. */

    _CacheInfoPtr_ cache;             /* Private, image pixel cache */

    _ThreadViewSetPtr_ default_views; /* Private, default cache views */

    _ImageAttributePtr_ attributes;   /* Private, Image attribute list */

    _Ascii85InfoPtr_ ascii85;         /* Private, supports huffman encoding */

    _BlobInfoPtr_ blob;               /* Private, file I/O object */

    c_long reference_count;           /* Private, Image reference count */

    _SemaphoreInfoPtr_ semaphore;     /* Private, Per image lock (for reference count) */

    uint logging;                     /* Private, True if logging is enabled */

    _Image* list;                     /* Private, used only by display */

    c_ulong signature;                /* Private, Unique code to validate structures */
}
alias Image = _Image;

struct _ImageInfo
{
    CompressionType compression; /** Image compression to use while decoding */

    MagickBool temporary;        /** Remove file "filename" once it has been read */
    MagickBool adjoin;           /** If True, join multiple frames into one file */
    MagickBool antialias;        /** If True, antialias while rendering */

    c_ulong subimage;            /** Starting image scene ID to select */
    c_ulong subrange;            /** Span of image scene IDs (from stating scene) to select */
    c_ulong depth;               /** Number of quantum bits to preserve while encoding */

    char* size;                  /** Desired/known dimensions to use when decoding image */
    char* tile;                  /** Deprecated, name of image to tile of background */
    char* page;                  /** Output page size & offset */

    InterlaceType interlace;     /** Interlace scheme to use when decoding image */

    EndianType endian;           /** Select MSB/LSB endian output for TIFF format */

    ResolutionType units;        /** Units to apply when evaluating the density option */

    c_ulong quality;             /** Compression quality factor (format specific) */

    char* sampling_factor;       /** JPEG, MPEG, and YUV chroma downsample factor */
    char* server_name;           /** X11 server display specification */
    char* font;                  /** Font name to use for text annotations */
    char* texture;               /** Name of texture image to use for background fills */
    char* density;               /** Image resolution (also see units) */

    double pointsize;            /** Font pointsize */

    double fuzz;                 /** Colors within this distance are a match */

    PixelPacket pen;              /** Stroke or fill color while drawing */
    PixelPacket background_color; /** Background color */
    PixelPacket border_color;     /** Border color (color surrounding frame */
    PixelPacket matte_color;      /** Matte color (frame color) */

    MagickBool dither;            /** If true, dither image while writing */
    MagickBool monochrome;        /** If true, use monochrome format */
    MagickBool progress;          /** If true, show progress indications */

    ColorspaceType colorspace;    /** Colorspace representation of image pixels */

    ImageType type;               /** Desired image type (used while reading or writing something */

    c_long group;                 /** X11 window group ID */

    uint verbose;                 /** If non-zero, display high-level processing */

    char* view;                   /** FlashPIX view specification */
    char* authenticate;           /** Password used to decrypt file */

    void* client_data;            /** User-specified data to pass to coder */

    FILE* file;                   /** If not null, stdio FILE to read image from */

    char[MaxTextExtent] magick;   /** File format to read. Overrides file extension */
    char[MaxTextExtent] filename; /** File name to read */

    /*
     * Only private members appear past this point
     */

    _CacheInfoPtr_ cache;         /* Private. Used to pass image via open cache */

    void* definitions;            /* Private. Map of coder specific options passed by user
                                   * Use AddDefinitions, RemoveDefinitions, & AccessDefinition
                                   * to access and manipulate this data.
                                   */

    Image* attributes;            /* Private. Image attribute list */

    MagickBool ping;              /* Private, if true, read file header only */

    PreviewType preview_type;     /* Private. used by PreviewImage */

    MagickBool affirm;            /* Private, when true do not intuit image format */

    _BlobInfoPtr_ blob;           /* Private, used to pass in open blob */

    size_t length;                /* Private, used to pass in open blob length */

    char[MaxTextExtent] unique;   /* Private, passes temporary filename to TranslateText */
    char[MaxTextExtent] zero;     /* Private, passes temporary filename to TranslateText */

    c_ulong signature;            /* Private, used to validate structure */
}
alias ImageInfo = _ImageInfo;

/*
 * Image utilities methods
 */
version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    ExceptionType CatchImageException(Image*);

    Image* AllocateImage(const scope ImageInfo*);
    Image* AppendImages(const scope Image*, const scope uint, ExceptionInfo*);
    Image* AverageImages(const scope Image*, ExceptionInfo*);
    Image* CloneImage(const scope Image*, const scope c_ulong, const scope c_ulong, const scope uint, ExceptionInfo*);
    Image* GetImageClipMask(const scope Image*, ExceptionInfo*);
    Image* ReferenceImage(Image*);

    ImageInfo* CloneImageInfo(const scope ImageInfo*);

    ImageType GetImageType(const scope Image*, ExceptionInfo*);

    const(char)* AccessDefinition(const scope ImageInfo*, const scope char*, const scope char*);

    int GetImageGeometry(const scope Image*, const scope char*, const scope uint, RectangleInfo*);

    RectangleInfo GetImageBoundingBox(const scope Image*, ExceptionInfo*);

    /* Functions which return unsigned int as a True/False boolean value */
    MagickBool IsTaintImage(const scope Image*);
    MagickBool IsSubimage(const scope char*, const scope uint);

    /* Functions which return unsigned int to indicate operation pass/fail */
    MagickPassFail AddDefinitions(ImageInfo*, const scope char*, ExceptionInfo*);
    MagickPassFail AllocateImageColormap(Image*, const scope c_ulong);
    MagickPassFail AnimateImages(const scope ImageInfo*, Image*);
    MagickPassFail ClipImage(Image*);
    MagickPassFail ClipPathImage(Image*, const scope char*, const scope MagickBool);
    MagickPassFail CycleColormapImage(Image*, const scope int);
    MagickPassFail DescribeImage(Image*, FILE*, const scope uint);
    MagickPassFail DisplayImages(const scope ImageInfo*, Image*);
    MagickPassFail GetImageCharacteristics(const scope Image*, ImageCharacteristics*, const scope MagickBool,
        ExceptionInfo*);
    MagickPassFail GetImageStatistics(const scope Image*, ImageStatistics*, ExceptionInfo*);
    MagickPassFail GradientImage(Image*, const scope PixelPacket*, const scope PixelPacket*);
    MagickPassFail PlasmaImage(Image*, const scope SegmentInfo*, c_ulong, c_ulong);
    MagickPassFail RemoveDefinitions(const scope ImageInfo*, const scope char*);
    MagickPassFail ReplaceImageColormap(Image*, const scope PixelPacket*, const scope uint);
    MagickPassFail SetImage(Image*, const scope Quantum);
    MagickPassFail SetImageClipMask(Image*, const scope Image*);
    MagickPassFail SetImageDepth(Image*, const scope c_ulong);
    MagickPassFail SetImageInfo(ImageInfo*, const scope MagickBool, ExceptionInfo*);
    MagickPassFail SetImageType(Image*, const scope ImageType);
    MagickPassFail SortColormapByIntensity(Image*);
    MagickPassFail SyncImage(Image*);
    MagickPassFail TextureImage(Image*, const scope Image*);

    c_ulong GetImageDepth(const scope Image*, ExceptionInfo*);

    void AllocateNextImage(const scope ImageInfo*, Image*);
    void DestroyImage(Image*);
    void DestroyImageInfo(ImageInfo*);
    void GetImageException(Image*, ExceptionInfo*);
    void GetImageInfo(ImageInfo*);
    void ModifyImage(Image**, ExceptionInfo*);
    void SetImageOpacity(Image*, const scope uint);
} // version(GMagick_Static)
else // dynamic binding
{
    @system @nogc nothrow extern (C)
    {
        alias mCatchImageException = ExceptionType function(Image*);

        alias mAllocateImage = Image* function(const scope ImageInfo*);
        alias mAppendImages = Image* function(const scope Image*, const scope uint, ExceptionInfo*);
        alias mAverageImages = Image* function(const scope Image*, ExceptionInfo*);
        alias mCloneImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, const scope uint, ExceptionInfo*);
        alias mGetImageClipMask = Image* function(const scope Image*, ExceptionInfo*);
        alias mReferenceImage = Image* function(Image*);

        alias mCloneImageInfo = ImageInfo* function(const scope ImageInfo*);

        alias mGetImageType = ImageType function(const scope Image*, ExceptionInfo*);

        alias mAccessDefinition = const(char)* function(const scope ImageInfo*, const scope char*, const scope char*);

        alias mGetImageGeometry = int function(const scope Image*, const scope char*, const scope uint, RectangleInfo*);

        alias mGetImageBoundingBox = RectangleInfo function(const scope Image*, ExceptionInfo*);

        /* Functions which return unsigned int as a True/False boolean value */
        alias mIsTaintImage = MagickBool function(const scope Image*);
        alias mIsSubimage = MagickBool function(const scope char*, const scope uint);

        /* Functions which return unsigned int to indicate operation pass/fail */
        alias mAddDefinitions = MagickPassFail function(ImageInfo*, const scope char*, ExceptionInfo*);
        alias mAllocateImageColormap = MagickPassFail function(Image*, const scope c_ulong);
        alias mAnimateImages = MagickPassFail function(const scope ImageInfo*, Image*);
        alias mClipImage = MagickPassFail function(Image*);
        alias mClipPathImage = MagickPassFail function(Image*, const scope char*, const scope MagickBool);
        alias mCycleColormapImage = MagickPassFail function(Image*, const scope int);
        alias mDescribeImage = MagickPassFail function(Image*, FILE*, const scope uint);
        alias mDisplayImages = MagickPassFail function(const scope ImageInfo*, Image*);
        alias mGetImageCharacteristics = MagickPassFail function(const scope Image*, ImageCharacteristics*, const scope MagickBool,
            ExceptionInfo*);
        alias mGetImageStatistics = MagickPassFail function(const scope Image*, ImageStatistics*, ExceptionInfo*);
        alias mGradientImage = MagickPassFail function(Image*, const scope PixelPacket*, const scope PixelPacket*);
        alias mPlasmaImage = MagickPassFail function(Image*, const scope SegmentInfo*, c_ulong, c_ulong);
        alias mRemoveDefinitions = MagickPassFail function(const scope ImageInfo*, const scope char*);
        alias mReplaceImageColormap = MagickPassFail function(Image*, const scope PixelPacket*, const scope uint);
        alias mSetImage = MagickPassFail function(Image*, const scope Quantum);
        alias mSetImageClipMask = MagickPassFail function(Image*, const scope Image*);
        alias mSetImageDepth = MagickPassFail function(Image*, const scope c_ulong);
        alias mSetImageInfo = MagickPassFail function(ImageInfo*, const scope MagickBool, ExceptionInfo*);
        alias mSetImageType = MagickPassFail function(Image*, const scope ImageType);
        alias mSortColormapByIntensity = MagickPassFail function(Image*);
        alias mSyncImage = MagickPassFail function(Image*);
        alias mTextureImage = MagickPassFail function(Image*, const scope Image*);

        alias mGetImageDepth = c_ulong function(const scope Image*, ExceptionInfo*);

        alias mAllocateNextImage = void function(const scope ImageInfo*, Image*);
        alias mDestroyImage = void function(Image*);
        alias mDestroyImageInfo = void function(ImageInfo*);
        alias mGetImageException = void function(Image*, ExceptionInfo*);
        alias mGetImageInfo = void function(ImageInfo*);
        alias mModifyImage = void function(Image**, ExceptionInfo*);
        alias mSetImageOpacity = void function(Image*, const scope uint);
    }

    __gshared
    {
        mCatchImageException CatchImageException;

        mAllocateImage AllocateImage;
        mAppendImages AppendImages;
        mAverageImages AverageImages;
        mCloneImage CloneImage;
        mGetImageClipMask GetImageClipMask;
        mReferenceImage ReferenceImage;

        mCloneImageInfo CloneImageInfo;

        mGetImageType GetImageType;

        mAccessDefinition AccessDefinition;

        mGetImageGeometry GetImageGeometry;

        mGetImageBoundingBox GetImageBoundingBox;

        /* Functions which return unsigned int as a True/False boolean value */
        mIsTaintImage IsTaintImage;
        mIsSubimage IsSubimage;

        /* Functions which return unsigned int to indicate operation pass/fail */
        mAddDefinitions AddDefinitions;
        mAllocateImageColormap AllocateImageColormap;
        mAnimateImages AnimateImages;
        mClipImage ClipImage;
        mClipPathImage ClipPathImage;
        mCycleColormapImage CycleColormapImage;
        mDescribeImage DescribeImage;
        mDisplayImages DisplayImages;
        mGetImageCharacteristics GetImageCharacteristics;
        mGetImageStatistics GetImageStatistics;
        mGradientImage GradientImage;
        mPlasmaImage PlasmaImage;
        mRemoveDefinitions RemoveDefinitions;
        mReplaceImageColormap ReplaceImageColormap;
        mSetImage SetImage;
        mSetImageClipMask SetImageClipMask;
        mSetImageDepth SetImageDepth;
        mSetImageInfo SetImageInfo;
        mSetImageType SetImageType;
        mSortColormapByIntensity SortColormapByIntensity;
        mSyncImage SyncImage;
        mTextureImage TextureImage;

        mGetImageDepth GetImageDepth;

        mAllocateNextImage AllocateNextImage;
        mDestroyImage DestroyImage;
        mDestroyImageInfo DestroyImageInfo;
        mGetImageException GetImageException;
        mGetImageInfo GetImageInfo;
        mModifyImage ModifyImage;
        mSetImageOpacity SetImageOpacity;
    }

    package (graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadImageH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            /*
             * these were generated by a script. I would use a mixin,
             * but would that result in slower compile time?
             */
            CatchImageException = cast(mCatchImageException) dlsym(dlib, "CatchImageException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AllocateImage = cast(mAllocateImage) dlsym(dlib, "AllocateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AppendImages = cast(mAppendImages) dlsym(dlib, "AppendImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AverageImages = cast(mAverageImages) dlsym(dlib, "AverageImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneImage = cast(mCloneImage) dlsym(dlib, "CloneImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageClipMask = cast(mGetImageClipMask) dlsym(dlib, "GetImageClipMask");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            ReferenceImage = cast(mReferenceImage) dlsym(dlib, "ReferenceImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneImageInfo = cast(mCloneImageInfo) dlsym(dlib, "CloneImageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageType = cast(mGetImageType) dlsym(dlib, "GetImageType");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AccessDefinition = cast(mAccessDefinition) dlsym(dlib, "AccessDefinition");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageGeometry = cast(mGetImageGeometry) dlsym(dlib, "GetImageGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageBoundingBox = cast(mGetImageBoundingBox) dlsym(dlib, "GetImageBoundingBox");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            /* Functions which return unsigned int as a True/False boolean value */
            IsTaintImage = cast(mIsTaintImage) dlsym(dlib, "IsTaintImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            IsSubimage = cast(mIsSubimage) dlsym(dlib, "IsSubimage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            /* Functions which return unsigned int to indicate operation pass/fail */
            AddDefinitions = cast(mAddDefinitions) dlsym(dlib, "AddDefinitions");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AllocateImageColormap = cast(mAllocateImageColormap) dlsym(dlib, "AllocateImageColormap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AnimateImages = cast(mAnimateImages) dlsym(dlib, "AnimateImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            ClipImage = cast(mClipImage) dlsym(dlib, "ClipImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            ClipPathImage = cast(mClipPathImage) dlsym(dlib, "ClipPathImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            CycleColormapImage = cast(mCycleColormapImage) dlsym(dlib, "CycleColormapImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            DescribeImage = cast(mDescribeImage) dlsym(dlib, "DescribeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            DisplayImages = cast(mDisplayImages) dlsym(dlib, "DisplayImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageCharacteristics = cast(mGetImageCharacteristics) dlsym(dlib, "GetImageCharacteristics");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageStatistics = cast(mGetImageStatistics) dlsym(dlib, "GetImageStatistics");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GradientImage = cast(mGradientImage) dlsym(dlib, "GradientImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            PlasmaImage = cast(mPlasmaImage) dlsym(dlib, "PlasmaImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            RemoveDefinitions = cast(mRemoveDefinitions) dlsym(dlib, "RemoveDefinitions");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            ReplaceImageColormap = cast(mReplaceImageColormap) dlsym(dlib, "ReplaceImageColormap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImage = cast(mSetImage) dlsym(dlib, "SetImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageClipMask = cast(mSetImageClipMask) dlsym(dlib, "SetImageClipMask");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageDepth = cast(mSetImageDepth) dlsym(dlib, "SetImageDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageInfo = cast(mSetImageInfo) dlsym(dlib, "SetImageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageType = cast(mSetImageType) dlsym(dlib, "SetImageType");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SortColormapByIntensity = cast(mSortColormapByIntensity) dlsym(dlib, "SortColormapByIntensity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SyncImage = cast(mSyncImage) dlsym(dlib, "SyncImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            TextureImage = cast(mTextureImage) dlsym(dlib, "TextureImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageDepth = cast(mGetImageDepth) dlsym(dlib, "GetImageDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            AllocateNextImage = cast(mAllocateNextImage) dlsym(dlib, "AllocateNextImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyImage = cast(mDestroyImage) dlsym(dlib, "DestroyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyImageInfo = cast(mDestroyImageInfo) dlsym(dlib, "DestroyImageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageException = cast(mGetImageException) dlsym(dlib, "GetImageException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageInfo = cast(mGetImageInfo) dlsym(dlib, "GetImageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            ModifyImage = cast(mModifyImage) dlsym(dlib, "ModifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageOpacity = cast(mSetImageOpacity) dlsym(dlib, "SetImageOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.image:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
