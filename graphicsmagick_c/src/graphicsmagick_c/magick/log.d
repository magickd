/*
  Copyright (C) 2003, 2004 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Log methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.log;

import core.stdc.config;
import core.stdc.stdarg;

import graphicsmagick_c.magick.api : MagickBool;
import graphicsmagick_c.magick.error;

/*
 * Define declarations
 */
enum const(char)[] MagickLogFilename = "log.mgk";

/* Skipping: GetCurrentFunction() -- use __FUNCTION__                       */
/* Skipping: GetMagickModule()    -- use __FILE__ ~ __FUNCTION__ ~ __LINE__ */

/* NOTE: any changes to these effect PerlMagick */
alias LogEventType = int;
enum : LogEventType
{
    UndefinedEventMask     = 0x00000000,
    NoEventMask            = 0x00000000,
    ConfigureEventMask     = 0x00000001,
    AnnotateEventMask      = 0x00000002,
    RenderEventMask        = 0x00000004,
    TransformEventMask     = 0x00000008,
    LocaleEventMask        = 0x00000010,
    CoderEventMask         = 0x00000020,
    X11EventMask           = 0x00000040,
    CacheEventMask         = 0x00000080,
    BlobEventMask          = 0x00000100,
    DeprecateEventMask     = 0x00000200,
    UserEventMask          = 0x00000400,
    ResourceEventMask      = 0x00000800,
    TemporaryFileEventMask = 0x00001000,
    /* ExceptionEventMask = WarningEventMask | ErrorEventMask |  FatalErrorEventMask */
    ExceptionEventMask     = 0x00070000,
    OptionEventMask        = 0x00004000,
    InformationEventMask   = 0x00008000,
    WarningEventMask       = 0x00010000,
    ErrorEventMask         = 0x00020000,
    FatalErrorEventMask    = 0x00040000,
    AllEventMask           = 0x7FFFFFFF
}

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    MagickBool IsEventLogging();
    MagickBool LogMagickEvent(const scope ExceptionType type, const scope char* module_, const scope char* function_,
        const scope c_ulong line, const scope char* fmt, ...);
    MagickBool LogMagickEventList(const scope ExceptionType type, const scope char* module_, const scope char* function_,
        const scope c_ulong line, const scope char* fmt, va_list operands);

    c_ulong SetLogEventMask(const scope char* events);

    void DestroyLogInfo();
    void SetLogFormat(const scope char* format);
} // version(GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mIsEventLogging = MagickBool function();
        alias mLogMagickEvent = MagickBool function(const scope ExceptionType, const scope char*, const scope char*,
            const scope c_ulong, const scope char* fmt, ...);
        alias mLogMagickEventList = MagickBool function(const scope ExceptionType, const scope char*, const scope char*,
            const scope c_ulong, const scope char* fmt, va_list);

        alias mSetLogEventMask = c_ulong function(const scope char*);

        alias mDestroyLogInfo = void function();
        alias mSetLogFormat = void function(const scope char*);
    }

    __gshared
    {
        mIsEventLogging IsEventLogging;
        mLogMagickEvent LogMagickEvent;
        mLogMagickEventList LogMagickEventList;

        mSetLogEventMask SetLogEventMask;

        mDestroyLogInfo DestroyLogInfo;
        mSetLogFormat SetLogFormat;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn;
        import core.stdc.stdio;

        bool _loadLogH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            IsEventLogging = cast(mIsEventLogging)dlsym(dlib, "IsEventLogging");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }
            LogMagickEvent = cast(mLogMagickEvent)dlsym(dlib, "LogMagickEvent");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }
            LogMagickEventList = cast(mLogMagickEventList)dlsym(dlib, "LogMagickEventList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetLogEventMask = cast(mSetLogEventMask)dlsym(dlib, "SetLogEventMask");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyLogInfo = cast(mDestroyLogInfo)dlsym(dlib, "DestroyLogInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetLogFormat = cast(mSetLogFormat)dlsym(dlib, "SetLogFormat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.log:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
