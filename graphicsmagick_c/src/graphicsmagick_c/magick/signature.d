/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
 
  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.
 
  Digital signature methods.
*/
// Complete graphicsmagick_c wrapper for v1.3
module graphicsmagick_c.magick.signature;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.image : Image;

/*
    Define declarations
*/
enum SignatureSize = 64;

/*
    Typedef declarations
*/
struct _SignatureInfo
{
    c_ulong[8] digest;
    c_ulong low_order;
    c_ulong high_order;

    c_long offset;

    ubyte[SignatureSize] message;
}
alias SignatureInfo = _SignatureInfo;

/*
    Method declarations
*/
version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    uint SignatureImage(Image*);

    void FinalizeSignature(SignatureInfo*);
    void GetSignatureInfo(SignatureInfo*);
    void TransformSignature(SignatureInfo*);
    void UpdateSignature(SignatureInfo*);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mSignatureImage = uint function(Image*);

        alias mFinalizeSignature = void function(SignatureInfo*);
        alias mGetSignatureInfo = void function(SignatureInfo*);
        alias mTransformSignature = void function(SignatureInfo*);
        alias mUpdateSignature = void function(SignatureInfo*);
    }

    __gshared
    {
        mSignatureImage SignatureImage;

        mFinalizeSignature FinalizeSignature;
        mGetSignatureInfo GetSignatureInfo;
        mTransformSignature TransformSignature;
        mUpdateSignature UpdateSignature;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadSignatureH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            SignatureImage = cast(mSignatureImage)dlsym(dlib, "SignatureImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.signature:\n  %s\n", errmsg);
                }
                success = false;
            }

            FinalizeSignature = cast(mFinalizeSignature)dlsym(dlib, "FinalizeSignature");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.signature:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetSignatureInfo = cast(mGetSignatureInfo)dlsym(dlib, "GetSignatureInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.signature:\n  %s\n", errmsg);
                }
                success = false;
            }
            TransformSignature = cast(mTransformSignature)dlsym(dlib, "TransformSignature");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.signature:\n  %s\n", errmsg);
                }
                success = false;
            }
            UpdateSignature = cast(mUpdateSignature)dlsym(dlib, "UpdateSignature");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.signature:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
