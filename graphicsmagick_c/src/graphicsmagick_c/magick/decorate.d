/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Decorate Methods.
*/
// Complete graphicsmagick_c wrapper for v1.3.0
module graphicsmagick_c.magick.decorate;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : FrameInfo, Image, RectangleInfo;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* BorderImage(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
    Image* FrameImage(const scope Image*, const scope FrameInfo*, ExceptionInfo*);

    uint RaiseImage(Image*, const scope RectangleInfo*, const scope int);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mBorderImage = Image* function(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
        alias mFrameImage = Image* function(const scope Image*, const scope FrameInfo*, ExceptionInfo*);

        alias mRaiseImage = uint function(Image*, const scope RectangleInfo*, const scope int);
    }

    __gshared
    {
        mBorderImage BorderImage;
        mFrameImage FrameImage;

        mRaiseImage RaiseImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadDecorateH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            BorderImage = cast(mBorderImage)dlsym(dlib, "BorderImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.decorate:\n  %s\n", errmsg);
                }
                success = false;
            }
            FrameImage = cast(mFrameImage)dlsym(dlib, "FrameImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.decorate:\n  %s\n", errmsg);
                }
                success = false;
            }

            RaiseImage = cast(mRaiseImage)dlsym(dlib, "RaiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.decorate:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
