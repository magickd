/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Methods to Reduce the Number of Unique Colors in an Image.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.quantize;

import graphicsmagick_c.magick.colorspace;
import graphicsmagick_c.magick.image : Image;

import core.stdc.config : c_ulong;

/*
    Define declarations
*/
enum MaxTreeDepth = 8;
enum NodesInAList = 1536;

/*
    Typedef declarations
*/
struct _QuantizeInfo {
    c_ulong number_colors;

    uint tree_depth;
    uint dither;

    ColorspaceType colorspace;

    uint measure_error;

    c_ulong signature;
}
alias QuantizeInfo = _QuantizeInfo;

/*
    Quantization utilities methods.
*/
version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    QuantizeInfo* CloneQuantizeInfo(const scope QuantizeInfo*);

    uint GetImageQuantizeError(Image*);
    uint MapImage(Image*, const scope Image*, const scope uint);
    uint MapImages(Image*, const scope Image*, const scope uint);
    uint OrderedDitherImage(Image*);
    uint QuantizeImage(const scope QuantizeInfo*, Image*);
    uint QuantizeImages(const scope QuantizeInfo*, Image*);
    uint SegmentImage(Image*, const scope ColorspaceType, const scope uint, const scope double,
        const scope double);

    void CompressImageColormap(Image*);
    void DestroyQuantizeInfo(QuantizeInfo*);
    void GetQuantizeInfo(QuantizeInfo*);
    void GrayscalePseudoClassImage(Image*, uint);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mCloneQuantizeInfo = QuantizeInfo* function(const scope QuantizeInfo*);

        alias mGetImageQuantizeError = uint function(Image*);
        alias mMapImage = uint function(Image*, const scope Image*, const scope uint);
        alias mMapImages = uint function(Image*, const scope Image*, const scope uint);
        alias mOrderedDitherImage = uint function(Image*);
        alias mQuantizeImage = uint function(const scope QuantizeInfo*, Image*);
        alias mQuantizeImages = uint function(const scope QuantizeInfo*, Image*);
        alias mSegmentImage = uint function(Image*, const scope ColorspaceType, const scope uint, const scope double,
            const scope double);

        alias mCompressImageColormap = void function(Image*);
        alias mDestroyQuantizeInfo = void function(QuantizeInfo*);
        alias mGetQuantizeInfo = void function(QuantizeInfo*);
        alias mGrayscalePseudoClassImage = void function(Image*, uint);
    }

    __gshared
    {
        mCloneQuantizeInfo CloneQuantizeInfo;

        mGetImageQuantizeError GetImageQuantizeError;
        mMapImage MapImage;
        mMapImages MapImages;
        mOrderedDitherImage OrderedDiterImage;
        mQuantizeImage QuantizeImage;
        mQuantizeImages QuantizeImages;
        mSegmentImage SegmentImage;

        mCompressImageColormap CompressImageColorma;
        mDestroyQuantizeInfo DestroyQuantizeInfo;
        mGetQuantizeInfo GetQuantizeInfo;
        mGrayscalePseudoClassImage GrayscalePseudoClassImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadQuantizeH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            CloneQuantizeInfo = cast(mCloneQuantizeInfo)dlsym(dlib, "CloneQuantizeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageQuantizeError = cast(mGetImageQuantizeError)dlsym(dlib, "GetImageQuantizeError");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            MapImage = cast(mMapImage)dlsym(dlib, "MapImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            MapImages = cast(mMapImages)dlsym(dlib, "MapImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            OrderedDiterImage = cast(mOrderedDitherImage)dlsym(dlib, "OrderedDitherImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantizeImage = cast(mQuantizeImage)dlsym(dlib, "QuantizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantizeImages = cast(mQuantizeImages)dlsym(dlib, "QuantizeImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            SegmentImage = cast(mSegmentImage)dlsym(dlib, "SegmentImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }

            CompressImageColorma = cast(mCompressImageColormap)dlsym(dlib, "CompressImageColormap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            DestroyQuantizeInfo = cast(mDestroyQuantizeInfo)dlsym(dlib, "DestroyQuantizeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetQuantizeInfo = cast(mGetQuantizeInfo)dlsym(dlib, "GetQuantizeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }
            GrayscalePseudoClassImage = cast(mGrayscalePseudoClassImage)dlsym(dlib, "GrayscalePseudoClassImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.quantize:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
