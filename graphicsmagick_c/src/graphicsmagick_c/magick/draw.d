/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Drawing API.

  Usage synopsis:

  DrawContext context;
  context = DrawAllocateContext((DrawInfo*)NULL, image);
    [ any number of drawing commands ]
    DrawSetStrokeColorString(context,"black");
    DrawSetFillColorString(context,"#ff00ff");
    DrawSetStrokeWidth(context,4);
    DrawRectangle(context,72,72,144,144);
  DrawRender(context);
  DrawDestroyContext(context);

*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.draw;

import core.stdc.config;

import graphicsmagick_c.magick.image;
import graphicsmagick_c.magick.render;

// typedef struct _DrawContext *DrawContext;
alias DrawContext = void*;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    ClipPathUnits DrawGetClipUnits(DrawContext context);

    DrawInfo* DrawPeekGraphicContext(const scope DrawContext context);

    DecorationType DrawGetTextDecoration(DrawContext context);

    DrawContext DrawAllocateContext(const scope DrawInfo* draw_info, Image* image);

    FillRule DrawGetClipRule(DrawContext context);
    FillRule DrawGetFillRule(DrawContext context);

    GravityType DrawGetGravity(DrawContext context);

    LineCap DrawGetStrokeLineCap(DrawContext context);

    LineJoin DrawGetStrokeLineJoin(DrawContext context);

    PixelPacket DrawGetFillColor(DrawContext context);
    PixelPacket DrawGetStrokeColor(DrawContext context);
    PixelPacket DrawGetTextUnderColor(DrawContext context);

    StretchType DrawGetFontStretch(DrawContext context);

    StyleType DrawGetFontStyle(DrawContext context);

    char* DrawGetClipPath(DrawContext context);
    char* DrawGetFont(DrawContext context);
    char* DrawGetFontFamily(DrawContext context);
    char* DrawGetTextEncoding(DrawContext context);

    int DrawRender(const scope DrawContext context);

    uint DrawGetStrokeAntialias(DrawContext context);
    uint DrawGetTextAntialias(DrawContext context);

    c_ulong DrawGetFontWeight(DrawContext context);
    c_ulong DrawGetStrokeMiterLimit(DrawContext context);

    double  DrawGetFillOpacity(DrawContext context);
    double  DrawGetFontSize(DrawContext context);
    double* DrawGetStrokeDashArray(DrawContext context);
    double  DrawGetStrokeDashOffset(DrawContext context);
    double  DrawGetStrokeOpacity(DrawContext context);
    double  DrawGetStrokeWidth(DrawContext context);

    void DrawAffine(DrawContext context, const scope AffineMatrix* affine);
    void DrawAnnotation(DrawContext context,
        const scope double x, const scope double y,
        const scope ubyte* text);
    void DrawArc(DrawContext context,
        const scope double sx, const scope double sy,
        const scope double ex, const scope double ey,
        const scope double sd, const scope double ed);
    void DrawBezier(DrawContext context,
        const scope c_ulong num_coords, const scope PointInfo* coordinates);
    void DrawCircle(DrawContext context,
        const scope double ox, const scope double oy,
        const scope double px, const scope double py);
    void DrawColor(DrawContext context,
        const scope double x, const scope double y,
        const scope PaintMethod paintMethod);
    void DrawComment(DrawContext context, const scope char* comment);
    void DrawDestroyContext(DrawContext context);
    void DrawEllipse(DrawContext context,
        const scope double ox, const scope double oy,
        const scope double rx, const scope double ry,
        const scope double start, const scope double end);
    void DrawComposite(DrawContext context,
        const scope CompositeOperator composite_operator,
        const scope double x, const scope double y,
        const scope double width, const scope double height,
        const scope Image* image);
    void DrawLine(DrawContext context,
        const scope double sx, const scope double sy,
        const scope double ex, const scope double ey);
    void DrawMatte(DrawContext context,
        const scope double x, const scope double y,
        const scope PaintMethod paint_method);
    void DrawPathClose(DrawContext context);
    void DrawPathCurveToAbsolute(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x2, const scope double y2,
        const scope double x, const scope double y);
    void DrawPathCurveToRelative(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x2, const scope double y2,
        const scope double x, const scope double y);
    void DrawPathCurveToQuadraticBezierAbsolute(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x, const scope double y);
    void DrawPathCurveToQuadraticBezierRelative(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x, const scope double y);
    void DrawPathCurveToQuadraticBezierSmoothAbsolute(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathCurveToQuadraticBezierSmoothRelative(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathCurveToSmoothAbsolute(DrawContext context,
        const scope double x2, const scope double y2,
        const scope double x, const scope double y);
    void DrawPathCurveToSmoothRelative(DrawContext context,
        const scope double x2, const scope double y2,
        const scope double x, const scope double y);
    void DrawPathEllipticArcRelative(DrawContext context,
        const scope double rx, const scope double ry,
        const scope double x_axis_rotation,
        uint large_arc_flag,
        uint sweep_flag,
        const scope double x, const scope double y);
    void DrawPathFinish(DrawContext context);
    void DrawPathLineToAbsolute(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathLineToRelative(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathLineToHorizontalAbsolute(DrawContext context, const scope double x);
    void DrawPathLineToHorizontalRelative(DrawContext context, const scope double x);
    void DrawPathLineToVerticalAbsolute(DrawContext context, const scope double y);
    void DrawPathLineToVerticalRelative(DrawContext context, const scope double y);
    void DrawPathMoveToAbsolute(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathMoveToRelative(DrawContext context,
        const scope double x, const scope double y);
    void DrawPathStart(DrawContext context);
    void DrawPoint(DrawContext context, const scope double x, const scope double y);
    void DrawPolygon(DrawContext context,
        const scope c_ulong num_coords, const scope PointInfo* coordinates);
    void DrawPolyline(DrawContext context,
        const scope c_ulong num_coords, const scope PointInfo* coordinates);
    void DrawPopClipPath(DrawContext context);
    void DrawPopDefs(DrawContext context);
    void DrawPopGraphicContext(DrawContext context);
    void DrawPopPattern(DrawContext context);
    void DrawPushClipPath(DrawContext context, const scope char* clip_path_id);
    void DrawPushDefs(DrawContext context);
    void DrawPushGraphicContext(DrawContext context);
    void DrawPushPattern(DrawContext context,
        const scope char* pattern_id,
        const scope double x, const scope double y,
        const scope double width, const scope double height);
    void DrawRectangle(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x2, const scope double y2);
    void DrawRoundRectangle(DrawContext context,
        const scope double x1, const scope double y1,
        const scope double x2, const scope double y2,
        const scope double rx, const scope double ry);
    void DrawScale(DrawContext context, const scope double x, const scope double y);
    void DrawSetClipPath(DrawContext context, const scope char* clip_path);
    void DrawSetClipRule(DrawContext context, const scope FillRule fill_rule);
    void DrawSetClipUnits(DrawContext context, const scope ClipPathUnits clip_units);
    void DrawSetFillColor(DrawContext context, const scope PixelPacket* fill_color);
    void DrawSetFillColorString(DrawContext context, const scope char* fill_color);
    void DrawSetFillOpacity(DrawContext context, const scope double fill_opacity);
    void DrawSetFillRule(DrawContext context, const scope FillRule fill_rule);
    void DrawSetFillPatternURL(DrawContext context, const scope char* fill_url);
    void DrawSetFont(DrawContext context, const scope char* font_name);
    void DrawSetFontFamily(DrawContext context, const scope char* font_family);
    void DrawSetFontSize(DrawContext context, const scope double font_pointsize);
    void DrawSetFontStretch(DrawContext context, const scope StretchType font_stretch);
    void DrawSetFontStyle(DrawContext context, const scope StyleType font_style);
    void DrawSetFontWeight(DrawContext context, const scope c_ulong font_weight);
    void DrawSetGravity(DrawContext context, const scope GravityType gravity);
    void DrawRotate(DrawContext context, const scope double degrees);
    void DrawSkewX(DrawContext context, const scope double degrees);
    void DrawSkewY(DrawContext context, const scope double degrees);
    /*
     void DrawSetStopColor(DrawContext context, const scope PixelPacket* color,
         const scope double offset);
    */
    void DrawSetStrokeAntialias(DrawContext context, const scope uint true_false);
    void DrawSetStrokeColor(DrawContext context, const scope PixelPacket* stroke_color);
    void DrawSetStrokeColorString(DrawContext context, const scope char* stroke_color);
    void DrawSetStrokeDashArray(DrawContext context, const scope c_ulong num_elems,
        const scope double* dasharray);
    void DrawSetStrokeDashOffset(DrawContext context, const scope double dashoffset);
    void DrawSetStrokeLineCap(DrawContext context, const scope LineCap linecap);
    void DrawSetStrokeLineJoin(DrawContext context, const scope LineJoin linejoin);
    void DrawSetStrokeMiterLimit(DrawContext context, const scope c_ulong miterlimit);
    void DrawSetStrokeOpacity(DrawContext context, const scope double opacity);
    void DrawSetStrokePatternURL(DrawContext context, const scope char* stroke_url);
    void DrawSetStrokeWidth(DrawContext context, const scope double width);
    void DrawSetTextAntialias(DrawContext context, const scope uint true_false);
    void DrawSetTextDecoration(DrawContext context, const scope DecorationType decoration);
    void DrawSetTextEncoding(DrawContext context, const scope char* encoding);
    void DrawSetTextUnderColor(DrawContext context, const scope PixelPacket* color);
    void DrawSetTextUnderColorString(DrawContext context, const scope char* under_color);
    void DrawSetViewbox(DrawContext context,
        c_ulong x1, c_ulong y1,
        c_ulong x2, c_ulong y2);
    void DrawTranslate(DrawContext context, const scope double x, const scope double y);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mDrawGetClipUnits = ClipPathUnits function(DrawContext context);

        alias mDrawPeekGraphicContext = DrawInfo* function(const scope DrawContext context);

        alias mDrawGetTextDecoration = DecorationType function(DrawContext context);

        alias mDrawAllocateContext = DrawContext function(const scope DrawInfo* draw_info, Image* image);

        alias mDrawGetClipRule = FillRule function(DrawContext context);
        alias mDrawGetFillRule = FillRule function(DrawContext context);

        alias mDrawGetGravity = GravityType function(DrawContext context);

        alias mDrawGetStrokeLineCap = LineCap function(DrawContext context);

        alias mDrawGetStrokeLineJoin = LineJoin function(DrawContext context);

        alias mDrawGetFillColor = PixelPacket function(DrawContext context);
        alias mDrawGetStrokeColor = PixelPacket function(DrawContext context);
        alias mDrawGetTextUnderColor = PixelPacket function(DrawContext context);

        alias mDrawGetFontStretch = StretchType function(DrawContext context);

        alias mDrawGetFontStyle = StyleType function(DrawContext context);

        alias mDrawGetClipPath = char* function(DrawContext context);
        alias mDrawGetFont = char* function(DrawContext context);
        alias mDrawGetFontFamily = char* function(DrawContext context);
        alias mDrawGetTextEncoding = char* function(DrawContext context);

        alias mDrawRender = int function(const scope DrawContext context);

        alias mDrawGetStrokeAntialias = uint function(DrawContext context);
        alias mDrawGetTextAntialias = uint function(DrawContext context);

        alias mDrawGetFontWeight = c_ulong function(DrawContext context);
        alias mDrawGetStrokeMiterLimit = c_ulong function(DrawContext context);

        alias mDrawGetFillOpacity = double function(DrawContext context);
        alias mDrawGetFontSize = double function(DrawContext context);
        alias mDrawGetStrokeDashArray = double* function(DrawContext context);
        alias mDrawGetStrokeDashOffset = double function(DrawContext context);
        alias mDrawGetStrokeOpacity = double function(DrawContext context);
        alias mDrawGetStrokeWidth = double function(DrawContext context);

        alias mDrawAffine = void function(DrawContext context, const scope AffineMatrix* affine);
        alias mDrawAnnotation = void function(DrawContext context,
            const scope double x, const scope double y,
            const scope ubyte* text);
        alias mDrawArc = void function(DrawContext context,
            const scope double sx, const scope double sy,
            const scope double ex, const scope double ey,
            const scope double sd, const scope double ed);
        alias mDrawBezier = void function(DrawContext context,
            const scope c_ulong num_coords, const scope PointInfo* coordinates);
        alias mDrawCircle = void function(DrawContext context,
            const scope double ox, const scope double oy,
            const scope double px, const scope double py);
        alias mDrawColor = void function(DrawContext context,
            const scope double x, const scope double y,
            const scope PaintMethod paintMethod);
        alias mDrawComment = void function(DrawContext context, const scope char* comment);
        alias mDrawDestroyContext = void function(DrawContext context);
        alias mDrawEllipse = void function(DrawContext context,
            const scope double ox, const scope double oy,
            const scope double rx, const scope double ry,
            const scope double start, const scope double end);
        alias mDrawComposite = void function(DrawContext context,
            const scope CompositeOperator composite_operator,
            const scope double x, const scope double y,
            const scope double width, const scope double height,
            const scope Image* image);
        alias mDrawLine = void function(DrawContext context,
            const scope double sx, const scope double sy,
            const scope double ex, const scope double ey);
        alias mDrawMatte = void function(DrawContext context,
            const scope double x, const scope double y,
            const scope PaintMethod paint_method);
        alias mDrawPathClose = void function(DrawContext context);
        alias mDrawPathCurveToAbsolute = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x2, const scope double y2,
            const scope double x, const scope double y);
        alias mDrawPathCurveToRelative = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x2, const scope double y2,
            const scope double x, const scope double y);
        alias mDrawPathCurveToQuadraticBezierAbsolute = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x, const scope double y);
        alias mDrawPathCurveToQuadraticBezierRelative = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x, const scope double y);
        alias mDrawPathCurveToQuadraticBezierSmoothAbsolute = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathCurveToQuadraticBezierSmoothRelative = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathCurveToSmoothAbsolute = void function(DrawContext context,
            const scope double x2, const scope double y2,
            const scope double x, const scope double y);
        alias mDrawPathCurveToSmoothRelative = void function(DrawContext context,
            const scope double x2, const scope double y2,
            const scope double x, const scope double y);
        alias mDrawPathEllipticArcRelative = void function(DrawContext context,
            const scope double rx, const scope double ry,
            const scope double x_axis_rotation,
            uint large_arc_flag,
            uint sweep_flag,
            const scope double x, const scope double y);
        alias mDrawPathFinish = void function(DrawContext context);
        alias mDrawPathLineToAbsolute = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathLineToRelative = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathLineToHorizontalAbsolute = void function(DrawContext context, const scope double x);
        alias mDrawPathLineToHorizontalRelative = void function(DrawContext context, const scope double x);
        alias mDrawPathLineToVerticalAbsolute = void function(DrawContext context, const scope double y);
        alias mDrawPathLineToVerticalRelative = void function(DrawContext context, const scope double y);
        alias mDrawPathMoveToAbsolute = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathMoveToRelative = void function(DrawContext context,
            const scope double x, const scope double y);
        alias mDrawPathStart = void function(DrawContext context);
        alias mDrawPoint = void function(DrawContext context, const scope double x, const scope double y);
        alias mDrawPolygon = void function(DrawContext context,
            const scope c_ulong num_coords, const scope PointInfo* coordinates);
        alias mDrawPolyline = void function(DrawContext context,
            const scope c_ulong num_coords, const scope PointInfo* coordinates);
        alias mDrawPopClipPath = void function(DrawContext context);
        alias mDrawPopDefs = void function(DrawContext context);
        alias mDrawPopGraphicContext = void function(DrawContext context);
        alias mDrawPopPattern = void function(DrawContext context);
        alias mDrawPushClipPath = void function(DrawContext context, const scope char* clip_path_id);
        alias mDrawPushDefs = void function(DrawContext context);
        alias mDrawPushGraphicContext = void function(DrawContext context);
        alias mDrawPushPattern = void function(DrawContext context,
            const scope char* pattern_id,
            const scope double x, const scope double y,
            const scope double width, const scope double height);
        alias mDrawRectangle = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x2, const scope double y2);
        alias mDrawRoundRectangle = void function(DrawContext context,
            const scope double x1, const scope double y1,
            const scope double x2, const scope double y2,
            const scope double rx, const scope double ry);
        alias mDrawScale = void function(DrawContext context, const scope double x, const scope double y);
        alias mDrawSetClipPath = void function(DrawContext context, const scope char* clip_path);
        alias mDrawSetClipRule = void function(DrawContext context, const scope FillRule fill_rule);
        alias mDrawSetClipUnits = void function(DrawContext context, const scope ClipPathUnits clip_units);
        alias mDrawSetFillColor = void function(DrawContext context, const scope PixelPacket* fill_color);
        alias mDrawSetFillColorString = void function(DrawContext context, const scope char* fill_color);
        alias mDrawSetFillOpacity = void function(DrawContext context, const scope double fill_opacity);
        alias mDrawSetFillRule = void function(DrawContext context, const scope FillRule fill_rule);
        alias mDrawSetFillPatternURL = void function(DrawContext context, const scope char* fill_url);
        alias mDrawSetFont = void function(DrawContext context, const scope char* font_name);
        alias mDrawSetFontFamily = void function(DrawContext context, const scope char* font_family);
        alias mDrawSetFontSize = void function(DrawContext context, const scope double font_pointsize);
        alias mDrawSetFontStretch = void function(DrawContext context, const scope StretchType font_stretch);
        alias mDrawSetFontStyle = void function(DrawContext context, const scope StyleType font_style);
        alias mDrawSetFontWeight = void function(DrawContext context, const scope c_ulong font_weight);
        alias mDrawSetGravity = void function(DrawContext context, const scope GravityType gravity);
        alias mDrawRotate = void function(DrawContext context, const scope double degrees);
        alias mDrawSkewX = void function(DrawContext context, const scope double degrees);
        alias mDrawSkewY = void function(DrawContext context, const scope double degrees);
        /*
          alias mDrawSetStopColor = void function(DrawContext context, const scope PixelPacket* color,
             const scope double offset);
        */
        alias mDrawSetStrokeAntialias = void function(DrawContext context, const scope uint true_false);
        alias mDrawSetStrokeColor = void function(DrawContext context, const scope PixelPacket* stroke_color);
        alias mDrawSetStrokeColorString = void function(DrawContext context, const scope char* stroke_color);
        alias mDrawSetStrokeDashArray = void function(DrawContext context, const scope c_ulong num_elems,
            const scope double* dasharray);
        alias mDrawSetStrokeDashOffset = void function(DrawContext context, const scope double dashoffset);
        alias mDrawSetStrokeLineCap = void function(DrawContext context, const scope LineCap linecap);
        alias mDrawSetStrokeLineJoin = void function(DrawContext context, const scope LineJoin linejoin);
        alias mDrawSetStrokeMiterLimit = void function(DrawContext context, const scope c_ulong miterlimit);
        alias mDrawSetStrokeOpacity = void function(DrawContext context, const scope double opacity);
        alias mDrawSetStrokePatternURL = void function(DrawContext context, const scope char* stroke_url);
        alias mDrawSetStrokeWidth = void function(DrawContext context, const scope double width);
        alias mDrawSetTextAntialias = void function(DrawContext context, const scope uint true_false);
        alias mDrawSetTextDecoration = void function(DrawContext context, const scope DecorationType decoration);
        alias mDrawSetTextEncoding = void function(DrawContext context, const scope char* encoding);
        alias mDrawSetTextUnderColor = void function(DrawContext context, const scope PixelPacket* color);
        alias mDrawSetTextUnderColorString = void function(DrawContext context, const scope char* under_color);
        alias mDrawSetViewbox = void function(DrawContext context,
            c_ulong x1, c_ulong y1,
            c_ulong x2, c_ulong y2);
        alias mDrawTranslate = void function(DrawContext context, const scope double x, const scope double y);
    }

    __gshared
    {
        mDrawGetClipUnits DrawGetClipUnits;

        mDrawPeekGraphicContext DrawPeekGraphicContext;

        mDrawGetTextDecoration DrawGetTextDecoration;

        mDrawAllocateContext DrawAllocateContext;

        mDrawGetClipRule DrawGetClipRule;
        mDrawGetFillRule DrawGetFillRule;

        mDrawGetGravity DrawGetGravity;

        mDrawGetStrokeLineCap DrawGetStrokeLineCap;

        mDrawGetStrokeLineJoin DrawGetStrokeLineJoin;

        mDrawGetFillColor DrawGetFillColor;
        mDrawGetStrokeColor DrawGetStrokeColor;
        mDrawGetTextUnderColor DrawGetTextUnderColor;

        mDrawGetFontStretch DrawGetFontStretch;

        mDrawGetFontStyle DrawGetFontStyle;

        mDrawGetClipPath DrawGetClipPath;
        mDrawGetFont DrawGetFont;
        mDrawGetFontFamily DrawGetFontFamily;
        mDrawGetTextEncoding DrawGetTextEncoding;

        mDrawRender DrawRender;

        mDrawGetStrokeAntialias DrawGetStrokeAntialias;
        mDrawGetTextAntialias DrawGetTextAntialias;

        mDrawGetFontWeight DrawGetFontWeight;
        mDrawGetStrokeMiterLimit DrawGetStrokeMiterLimit;

        mDrawGetFillOpacity DrawGetFillOpacity;
        mDrawGetFontSize DrawGetFontSize;
        mDrawGetStrokeDashArray DrawGetStrokeDashArray;
        mDrawGetStrokeDashOffset DrawGetStrokeDashOffset;
        mDrawGetStrokeOpacity DrawGetStrokeOpacity;
        mDrawGetStrokeWidth DrawGetStrokeWidth;

        mDrawAffine DrawAffine;
        mDrawAnnotation DrawAnnotation;
        mDrawArc DrawArc;
        mDrawBezier DrawBezier;
        mDrawCircle DrawCircle;
        mDrawColor DrawColor;
        mDrawComment DrawComment;
        mDrawDestroyContext DrawDestroyContext;
        mDrawEllipse DrawEllipse;
        mDrawComposite DrawComposite;
        mDrawLine DrawLine;
        mDrawMatte DrawMatte;
        mDrawPathClose DrawPathClose;
        mDrawPathCurveToAbsolute DrawPathCurveToAbsolute;
        mDrawPathCurveToRelative DrawPathCurveToRelative;
        mDrawPathCurveToQuadraticBezierAbsolute DrawPathCurveToQuadraticBezierAbsolute;
        mDrawPathCurveToQuadraticBezierRelative DrawPathCurveToQuadraticBezierRelative;
        mDrawPathCurveToQuadraticBezierSmoothAbsolute DrawPathCurveToQuadraticBezierSmoothAbsolute;
        mDrawPathCurveToQuadraticBezierSmoothRelative DrawPathCurveToQuadraticBezierSmoothRelative;
        mDrawPathCurveToSmoothAbsolute DrawPathCurveToSmoothAbsolute;
        mDrawPathCurveToSmoothRelative DrawPathCurveToSmoothRelative;
        mDrawPathEllipticArcRelative DrawPathEllipticArcRelative;
        mDrawPathFinish DrawPathFinish;
        mDrawPathLineToAbsolute DrawPathLineToAbsolute;
        mDrawPathLineToRelative DrawPathLineToRelative;
        mDrawPathLineToHorizontalAbsolute DrawPathLineToHorizontalAbsolute;
        mDrawPathLineToHorizontalRelative DrawPathLineToHorizontalRelative;
        mDrawPathLineToVerticalAbsolute DrawPathLineToVerticalAbsolute;
        mDrawPathLineToVerticalRelative DrawPathLineToVerticalRelative;
        mDrawPathMoveToAbsolute DrawPathMoveToAbsolute;
        mDrawPathMoveToRelative DrawPathMoveToRelative;
        mDrawPathStart DrawPathStart;
        mDrawPoint DrawPoint;
        mDrawPolygon DrawPolygon;
        mDrawPolyline DrawPolyline;
        mDrawPopClipPath DrawPopClipPath;
        mDrawPopDefs DrawPopDefs;
        mDrawPopGraphicContext DrawPopGraphicContext;
        mDrawPopPattern DrawPopPattern;
        mDrawPushClipPath DrawPushClipPath;
        mDrawPushDefs DrawPushDefs;
        mDrawPushGraphicContext DrawPushGraphicContext;
        mDrawPushPattern DrawPushPattern;
        mDrawRectangle DrawRectangle;
        mDrawRoundRectangle DrawRoundRectangle;
        mDrawScale DrawScale;
        mDrawSetClipPath DrawSetClipPath;
        mDrawSetClipRule DrawSetClipRule;
        mDrawSetClipUnits DrawSetClipUnits;
        mDrawSetFillColor DrawSetFillColor;
        mDrawSetFillColorString DrawSetFillColorString;
        mDrawSetFillOpacity DrawSetFillOpacity;
        mDrawSetFillRule DrawSetFillRule;
        mDrawSetFillPatternURL DrawSetFillPatternURL;
        mDrawSetFont DrawSetFont;
        mDrawSetFontFamily DrawSetFontFamily;
        mDrawSetFontSize DrawSetFontSize;
        mDrawSetFontStretch DrawSetFontStretch;
        mDrawSetFontStyle DrawSetFontStyle;
        mDrawSetFontWeight DrawSetFontWeight;
        mDrawSetGravity DrawSetGravity;
        mDrawRotate DrawRotate;
        mDrawSkewX DrawSkewX;
        mDrawSkewY DrawSkewY;
        /*
          mDrawSetStopColor DrawSetStopColor;
        */
        mDrawSetStrokeAntialias DrawSetStrokeAntialias;
        mDrawSetStrokeColor DrawSetStrokeColor;
        mDrawSetStrokeColorString DrawSetStrokeColorString;
        mDrawSetStrokeDashArray DrawSetStrokeDashArray;
        mDrawSetStrokeDashOffset DrawSetStrokeDashOffset;
        mDrawSetStrokeLineCap DrawSetStrokeLineCap;
        mDrawSetStrokeLineJoin DrawSetStrokeLineJoin;
        mDrawSetStrokeMiterLimit DrawSetStrokeMiterLimit;
        mDrawSetStrokeOpacity DrawSetStrokeOpacity;
        mDrawSetStrokePatternURL DrawSetStrokePatternURL;
        mDrawSetStrokeWidth DrawSetStrokeWidth;
        mDrawSetTextAntialias DrawSetTextAntialias;
        mDrawSetTextDecoration DrawSetTextDecoration;
        mDrawSetTextEncoding DrawSetTextEncoding;
        mDrawSetTextUnderColor DrawSetTextUnderColor;
        mDrawSetTextUnderColorString DrawSetTextUnderColorString;
        mDrawSetViewbox DrawSetViewbox;
        mDrawTranslate DrawTranslate;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadDrawH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            DrawGetClipUnits = cast(mDrawGetClipUnits)dlsym(dlib, "DrawGetClipUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawPeekGraphicContext = cast(mDrawPeekGraphicContext)dlsym(dlib, "DrawPeekGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetTextDecoration = cast(mDrawGetTextDecoration)dlsym(dlib, "DrawGetTextDecoration");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawAllocateContext = cast(mDrawAllocateContext)dlsym(dlib, "DrawAllocateContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetClipRule = cast(mDrawGetClipRule)dlsym(dlib, "DrawGetClipRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetFillRule = cast(mDrawGetFillRule)dlsym(dlib, "DrawGetFillRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetGravity = cast(mDrawGetGravity)dlsym(dlib, "DrawGetGravity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetStrokeLineCap = cast(mDrawGetStrokeLineCap)dlsym(dlib, "DrawGetStrokeLineCap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetStrokeLineJoin = cast(mDrawGetStrokeLineJoin)dlsym(dlib, "DrawGetStrokeLineJoin");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetFillColor = cast(mDrawGetFillColor)dlsym(dlib, "DrawGetFillColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeColor = cast(mDrawGetStrokeColor)dlsym(dlib, "DrawGetStrokeColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetTextUnderColor = cast(mDrawGetTextUnderColor)dlsym(dlib, "DrawGetTextUnderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetFontStretch = cast(mDrawGetFontStretch)dlsym(dlib, "DrawGetFontStretch");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetFontStyle = cast(mDrawGetFontStyle)dlsym(dlib, "DrawGetFontStyle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetClipPath = cast(mDrawGetClipPath)dlsym(dlib, "DrawGetClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetFont = cast(mDrawGetFont)dlsym(dlib, "DrawGetFont");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetFontFamily = cast(mDrawGetFontFamily)dlsym(dlib, "DrawGetFontFamily");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetTextEncoding = cast(mDrawGetTextEncoding)dlsym(dlib, "DrawGetTextEncoding");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawRender = cast(mDrawRender)dlsym(dlib, "DrawRender");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetStrokeAntialias = cast(mDrawGetStrokeAntialias)dlsym(dlib, "DrawGetStrokeAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetTextAntialias = cast(mDrawGetTextAntialias)dlsym(dlib, "DrawGetTextAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetFontWeight = cast(mDrawGetFontWeight)dlsym(dlib, "DrawGetFontWeight");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeMiterLimit = cast(mDrawGetStrokeMiterLimit)dlsym(dlib, "DrawGetStrokeMiterLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawGetFillOpacity = cast(mDrawGetFillOpacity)dlsym(dlib, "DrawGetFillOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetFontSize = cast(mDrawGetFontSize)dlsym(dlib, "DrawGetFontSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeDashArray = cast(mDrawGetStrokeDashArray)dlsym(dlib, "DrawGetStrokeDashArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeDashOffset = cast(mDrawGetStrokeDashOffset)dlsym(dlib, "DrawGetStrokeDashOffset");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeOpacity = cast(mDrawGetStrokeOpacity)dlsym(dlib, "DrawGetStrokeOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawGetStrokeWidth = cast(mDrawGetStrokeWidth)dlsym(dlib, "DrawGetStrokeWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            DrawAffine = cast(mDrawAffine)dlsym(dlib, "DrawAffine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawAnnotation = cast(mDrawAnnotation)dlsym(dlib, "DrawAnnotation");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawArc = cast(mDrawArc)dlsym(dlib, "DrawArc");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawBezier = cast(mDrawBezier)dlsym(dlib, "DrawBezier");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawCircle = cast(mDrawCircle)dlsym(dlib, "DrawCircle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawColor = cast(mDrawColor)dlsym(dlib, "DrawColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawComment = cast(mDrawComment)dlsym(dlib, "DrawComment");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawDestroyContext = cast(mDrawDestroyContext)dlsym(dlib, "DrawDestroyContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawEllipse = cast(mDrawEllipse)dlsym(dlib, "DrawEllipse");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawComposite = cast(mDrawComposite)dlsym(dlib, "DrawComposite");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawLine = cast(mDrawLine)dlsym(dlib, "DrawLine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawMatte = cast(mDrawMatte)dlsym(dlib, "DrawMatte");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathClose = cast(mDrawPathClose)dlsym(dlib, "DrawPathClose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToAbsolute = cast(mDrawPathCurveToAbsolute)dlsym(dlib, "DrawPathCurveToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToRelative = cast(mDrawPathCurveToRelative)dlsym(dlib, "DrawPathCurveToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierAbsolute = cast(mDrawPathCurveToQuadraticBezierAbsolute)dlsym(dlib, "DrawPathCurveToQuadraticBezierAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierRelative = cast(mDrawPathCurveToQuadraticBezierRelative)dlsym(dlib, "DrawPathCurveToQuadraticBezierRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierSmoothAbsolute = cast(mDrawPathCurveToQuadraticBezierSmoothAbsolute)dlsym(dlib, "DrawPathCurveToQuadraticBezierSmoothAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierSmoothRelative = cast(mDrawPathCurveToQuadraticBezierSmoothRelative)dlsym(dlib, "DrawPathCurveToQuadraticBezierSmoothRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToSmoothAbsolute = cast(mDrawPathCurveToSmoothAbsolute)dlsym(dlib, "DrawPathCurveToSmoothAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToSmoothRelative = cast(mDrawPathCurveToSmoothRelative)dlsym(dlib, "DrawPathCurveToSmoothRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathEllipticArcRelative = cast(mDrawPathEllipticArcRelative)dlsym(dlib, "DrawPathEllipticArcRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathFinish = cast(mDrawPathFinish)dlsym(dlib, "DrawPathFinish");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToAbsolute = cast(mDrawPathLineToAbsolute)dlsym(dlib, "DrawPathLineToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToRelative = cast(mDrawPathLineToRelative)dlsym(dlib, "DrawPathLineToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToHorizontalAbsolute = cast(mDrawPathLineToHorizontalAbsolute)dlsym(dlib, "DrawPathLineToHorizontalAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToHorizontalRelative = cast(mDrawPathLineToHorizontalRelative)dlsym(dlib, "DrawPathLineToHorizontalRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToVerticalAbsolute = cast(mDrawPathLineToVerticalAbsolute)dlsym(dlib, "DrawPathLineToVerticalAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToVerticalRelative = cast(mDrawPathLineToVerticalRelative)dlsym(dlib, "DrawPathLineToVerticalRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathMoveToAbsolute = cast(mDrawPathMoveToAbsolute)dlsym(dlib, "DrawPathMoveToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathMoveToRelative = cast(mDrawPathMoveToRelative)dlsym(dlib, "DrawPathMoveToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathStart = cast(mDrawPathStart)dlsym(dlib, "DrawPathStart");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPoint = cast(mDrawPoint)dlsym(dlib, "DrawPoint");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPolygon = cast(mDrawPolygon)dlsym(dlib, "DrawPolygon");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPolyline = cast(mDrawPolyline)dlsym(dlib, "DrawPolyline");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPopClipPath = cast(mDrawPopClipPath)dlsym(dlib, "DrawPopClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPopDefs = cast(mDrawPopDefs)dlsym(dlib, "DrawPopDefs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPopGraphicContext = cast(mDrawPopGraphicContext)dlsym(dlib, "DrawPopGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPopPattern = cast(mDrawPopPattern)dlsym(dlib, "DrawPopPattern");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPushClipPath = cast(mDrawPushClipPath)dlsym(dlib, "DrawPushClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPushDefs = cast(mDrawPushDefs)dlsym(dlib, "DrawPushDefs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPushGraphicContext = cast(mDrawPushGraphicContext)dlsym(dlib, "DrawPushGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPushPattern = cast(mDrawPushPattern)dlsym(dlib, "DrawPushPattern");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawRectangle = cast(mDrawRectangle)dlsym(dlib, "DrawRectangle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawRoundRectangle = cast(mDrawRoundRectangle)dlsym(dlib, "DrawRoundRectangle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawScale = cast(mDrawScale)dlsym(dlib, "DrawScale");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetClipPath = cast(mDrawSetClipPath)dlsym(dlib, "DrawSetClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetClipRule = cast(mDrawSetClipRule)dlsym(dlib, "DrawSetClipRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetClipUnits = cast(mDrawSetClipUnits)dlsym(dlib, "DrawSetClipUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFillColor = cast(mDrawSetFillColor)dlsym(dlib, "DrawSetFillColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFillColorString = cast(mDrawSetFillColorString)dlsym(dlib, "DrawSetFillColorString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFillOpacity = cast(mDrawSetFillOpacity)dlsym(dlib, "DrawSetFillOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFillRule = cast(mDrawSetFillRule)dlsym(dlib, "DrawSetFillRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFillPatternURL = cast(mDrawSetFillPatternURL)dlsym(dlib, "DrawSetFillPatternURL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFont = cast(mDrawSetFont)dlsym(dlib, "DrawSetFont");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFontFamily = cast(mDrawSetFontFamily)dlsym(dlib, "DrawSetFontFamily");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFontSize = cast(mDrawSetFontSize)dlsym(dlib, "DrawSetFontSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFontStretch = cast(mDrawSetFontStretch)dlsym(dlib, "DrawSetFontStretch");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFontStyle = cast(mDrawSetFontStyle)dlsym(dlib, "DrawSetFontStyle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetFontWeight = cast(mDrawSetFontWeight)dlsym(dlib, "DrawSetFontWeight");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetGravity = cast(mDrawSetGravity)dlsym(dlib, "DrawSetGravity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawRotate = cast(mDrawRotate)dlsym(dlib, "DrawRotate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSkewX = cast(mDrawSkewX)dlsym(dlib, "DrawSkewX");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSkewY = cast(mDrawSkewY)dlsym(dlib, "DrawSkewY");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            /*
              DrawSetStopColor = cast(mDrawSetStopColor)dlsym(dlib, "DrawSetStopColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            */
            DrawSetStrokeAntialias = cast(mDrawSetStrokeAntialias)dlsym(dlib, "DrawSetStrokeAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeColor = cast(mDrawSetStrokeColor)dlsym(dlib, "DrawSetStrokeColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeColorString = cast(mDrawSetStrokeColorString)dlsym(dlib, "DrawSetStrokeColorString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeDashArray = cast(mDrawSetStrokeDashArray)dlsym(dlib, "DrawSetStrokeDashArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeDashOffset = cast(mDrawSetStrokeDashOffset)dlsym(dlib, "DrawSetStrokeDashOffset");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeLineCap = cast(mDrawSetStrokeLineCap)dlsym(dlib, "DrawSetStrokeLineCap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeLineJoin = cast(mDrawSetStrokeLineJoin)dlsym(dlib, "DrawSetStrokeLineJoin");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeMiterLimit = cast(mDrawSetStrokeMiterLimit)dlsym(dlib, "DrawSetStrokeMiterLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeOpacity = cast(mDrawSetStrokeOpacity)dlsym(dlib, "DrawSetStrokeOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokePatternURL = cast(mDrawSetStrokePatternURL)dlsym(dlib, "DrawSetStrokePatternURL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetStrokeWidth = cast(mDrawSetStrokeWidth)dlsym(dlib, "DrawSetStrokeWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetTextAntialias = cast(mDrawSetTextAntialias)dlsym(dlib, "DrawSetTextAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetTextDecoration = cast(mDrawSetTextDecoration)dlsym(dlib, "DrawSetTextDecoration");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetTextEncoding = cast(mDrawSetTextEncoding)dlsym(dlib, "DrawSetTextEncoding");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetTextUnderColor = cast(mDrawSetTextUnderColor)dlsym(dlib, "DrawSetTextUnderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetTextUnderColorString = cast(mDrawSetTextUnderColorString)dlsym(dlib, "DrawSetTextUnderColorString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawSetViewbox = cast(mDrawSetViewbox)dlsym(dlib, "DrawSetViewbox");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawTranslate = cast(mDrawTranslate)dlsym(dlib, "DrawTranslate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.draw:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
