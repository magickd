/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image List Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.list;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

version(GMagick_Static)
{
    @system @nogc nothrow extern(C):

    Image* CloneImageList(const scope Image*, ExceptionInfo*);
    Image* GetFirstImageInList(const scope Image*);
    Image* GetImageFromList(const scope Image*, const scope c_long);
    Image* GetLastImageInList(const scope Image*);
    Image* GetNextImageInList(const scope Image*);
    Image* GetPreviousImageInList(const scope Image*);
    Image** ImageListToArray(const scope Image*, ExceptionInfo*);
    Image* NewImageList();
    Image* RemoveLastImageFromList(Image**);
    Image* RemoveFirstImageFromList(Image**);
    Image* SplitImageList(Image*);
    Image* SyncNextImageInList(const scope Image*);

    c_long GetImageIndexInList(const scope Image*);

    c_ulong GetImageListLength(const scope Image*);

    void AppendImageToList(Image**, Image*);
    void DeleteImageFromList(Image**);
    void DestroyImageList(Image*);
    void InsertImageInList(Image**, Image*);
    void PrependImageToList(Image**, Image*);
    void ReplaceImageInList(Image** images, Image* image);
    void ReverseImageList(Image**);
    void SpliceImageIntoList(Image**, const scope c_ulong, Image*);
} // version(GMagick_Static)
else
{
    @system @nogc nothrow extern(C)
    {
        alias mCloneImageList = Image* function(const scope Image*, ExceptionInfo*);
        alias mGetFirstImageInList = Image* function(const scope Image*);
        alias mGetImageFromList = Image* function(const scope Image*, const scope c_long);
        alias mGetLastImageInList = Image* function(const scope Image*);
        alias mGetNextImageInList = Image* function(const scope Image*);
        alias mGetPreviousImageInList = Image* function(const scope Image*);
        alias mImageListToArray = Image** function(const scope Image*, ExceptionInfo*);
        alias mNewImageList = Image* function();
        alias mRemoveLastImageFromList = Image* function(Image**);
        alias mRemoveFirstImageFromList = Image* function(Image**);
        alias mSplitImageList = Image* function(Image*);
        alias mSyncNextImageInList = Image* function(Image*);

        alias mGetImageIndexInList = c_long function(const scope Image*);

        alias mGetImageListLength = c_ulong function(const scope Image*);

        alias mAppendImageToList = void function(Image**, Image*);
        alias mDeleteImageFromList = void function(Image**);
        alias mDestroyImageList = void function(Image*);
        alias mInsertImageInList = void function(Image**, Image*);
        alias mPrependImageToList = void function(Image**, Image*);
        alias mReplaceImageInList = void function(Image**, Image*);
        alias mReverseImageList = void function(Image**);
        alias mSpliceImageIntoList = void function(Image**, const scope c_ulong, Image*);
    }

    __gshared
    {
        mCloneImageList CloneImageList;
        mGetFirstImageInList GetFirstImageInList;
        mGetImageFromList GetImageFromList;
        mGetLastImageInList GetLastImageInList;
        mGetNextImageInList GetNextImageInList;
        mGetPreviousImageInList GetPreviousImageInList;
        mImageListToArray ImageListToArray;
        mNewImageList NewImageList;
        mRemoveLastImageFromList RemoveLastImageFromList;
        mRemoveFirstImageFromList RemoveFirstImageFromList;
        mSplitImageList SplitImageList;
        mSyncNextImageInList SyncNextImageInList;

        mGetImageIndexInList GetImageIndexInList;

        mGetImageListLength GetImageListLength;

        mAppendImageToList AppendImageToList;
        mDeleteImageFromList DeleteImageFromList;
        mDestroyImageList DestroyImageList;
        mInsertImageInList InsertImageInList;
        mPrependImageToList PrependImageToList;
        mReplaceImageInList ReplaceImageInList;
        mReverseImageList ReverseImageList;
        mSpliceImageIntoList SpliceImageIntoList;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadListH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            CloneImageList = cast(mCloneImageList)dlsym(dlib, "CloneImageList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetFirstImageInList = cast(mGetFirstImageInList)dlsym(dlib, "GetFirstImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetImageFromList = cast(mGetImageFromList)dlsym(dlib, "GetImageFromList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetLastImageInList = cast(mGetLastImageInList)dlsym(dlib, "GetLastImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetNextImageInList = cast(mGetNextImageInList)dlsym(dlib, "GetNextImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetPreviousImageInList = cast(mGetPreviousImageInList)dlsym(dlib, "GetPreviousImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImageListToArray = cast(mImageListToArray)dlsym(dlib, "ImageListToArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            NewImageList = cast(mNewImageList)dlsym(dlib, "NewImageList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            RemoveLastImageFromList = cast(mRemoveLastImageFromList)dlsym(dlib, "RemoveLastImageFromList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            RemoveFirstImageFromList = cast(mRemoveFirstImageFromList)dlsym(dlib, "RemoveFirstImageFromList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            SplitImageList = cast(mSplitImageList)dlsym(dlib, "SplitImageList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            SyncNextImageInList = cast(mSyncNextImageInList)dlsym(dlib, "SyncNextImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageIndexInList = cast(mGetImageIndexInList)dlsym(dlib, "GetImageIndexInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageListLength = cast(mGetImageListLength)dlsym(dlib, "GetImageListLength");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }

            AppendImageToList = cast(mAppendImageToList)dlsym(dlib, "AppendImageToList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            DeleteImageFromList = cast(mDeleteImageFromList)dlsym(dlib, "DeleteImageFromList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            DestroyImageList = cast(mDestroyImageList)dlsym(dlib, "DestroyImageList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            InsertImageInList = cast(mInsertImageInList)dlsym(dlib, "InsertImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            PrependImageToList = cast(mPrependImageToList)dlsym(dlib, "PrependImageToList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReplaceImageInList = cast(mReplaceImageInList)dlsym(dlib, "ReplaceImageInList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReverseImageList = cast(mReverseImageList)dlsym(dlib, "ReverseImageList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }
            SpliceImageIntoList = cast(mSpliceImageIntoList)dlsym(dlib, "SpliceImageIntoList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.list:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
