/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Montage Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.montage;

import graphicsmagick_c.magick.image : Image, ImageInfo, MontageInfo;
import graphicsmagick_c.magick.error : ExceptionInfo;

version(GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* MontageImages(const scope Image*, const scope MontageInfo*, ExceptionInfo*);

    MontageInfo* CloneMontageInfo(const scope ImageInfo*, const scope MontageInfo*);

    void DestroyMontageInfo(MontageInfo*);
    void GetMontageInfo(const scope ImageInfo*, MontageInfo*);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mMontageImages = Image* function(const scope Image*, const scope MontageInfo*, ExceptionInfo*);

        alias mCloneMontageInfo = MontageInfo* function(const scope ImageInfo*, const scope MontageInfo*);

        alias mDestroyMontageInfo = void function(MontageInfo*);
        alias mGetMontageInfo = void function(const scope ImageInfo*, MontageInfo*);
    }

    __gshared
    {
        mMontageImages MontageImages;

        mCloneMontageInfo CloneMontageInfo;

        mDestroyMontageInfo DestroyMontageInfo;
        mGetMontageInfo GetMontageInfo;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMontageH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            MontageImages = cast(mMontageImages)dlsym(dlib, "MontageImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.montage:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneMontageInfo = cast(mCloneMontageInfo)dlsym(dlib, "CloneMontageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.montage:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMontageInfo = cast(mDestroyMontageInfo)dlsym(dlib, "DestroyMontageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.montage:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetMontageInfo = cast(mGetMontageInfo)dlsym(dlib, "GetMontageInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.montage:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
