/*
  Copyright (C) 2003 - 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Utility Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3
module graphicsmagick_c.magick.utility;

import core.stdc.config : c_long, c_ulong;
import core.stdc.stdarg;

import graphicsmagick_c.magick.api : MagickBool, MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image, ImageInfo, RectangleInfo;
import graphicsmagick_c.magick.magick_types : magick_int64_t;

/*
    Enum declarations.
*/
enum : PathType
{
    RootPath,
    HeadPath,
    TailPath,
    BasePath,
    ExtensionPath,
    MagickPath,
    SubImagePath,
    FullPath
}
alias PathType = int;

/*
    Typedef declarations
*/

struct _TokenInfo
{
    int state;

    uint flag;

    c_long offset;

    char quote;
}
alias TokenInfo = _TokenInfo;

/*
    Utility methods
*/

alias MagickTextTranslate = size_t function(char* dst, const scope char* src, const scope size_t size);

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char* AcquireString(const scope char*);
    char* AllocateString(const scope char*);
    char* Base64Encode(const scope ubyte*, const scope size_t, size_t*);
    char* EscapeString(const scope char* , const scope char);
    char* GetPageGeometry(const scope char*);
    char** ListFiles(const scope char*, const scope char*, c_long*);
    char* SetClientName(const scope char*);
    char** StringToArgv(const scope char* , int*);
    char** StringToList(const scope char*);
    char* TranslateText(const scope ImageInfo*, Image*, const scope char*);
    char* TranslateTextEx(const scope ImageInfo*, Image*, const scope char*, MagickTextTranslate);

    const(char)* GetClientFilename();
    const(char)* SetClientFilename(const scope char*);
    const(char)* GetClientName();
    const(char)* GetClientPath();
    const(char)* SetClientPath(const scope char*);

    double StringToDouble(const scope char*, const scope double);

    int GetGeometry(const scope char*, c_long*, c_long*, c_ulong*, c_ulong*);
    int GlobExpression(const scope char*, const scope char*);
    int LocaleNCompare(const scope char*, const scope char*, const scope size_t);
    int LocaleCompare(const scope char*, const scope char*);
    int GetMagickDimension(const scope char* str, double* width, double* height, double* xoff, double* yoff);
    int GetMagickGeometry(const scope char* geometry, c_long* x, c_long* y, c_ulong* width, c_ulong* height);
    int MagickRandReentrant(uint* seed);
    int SubstituteString(char**, const scope char*, const scope char*);
    int SystemCommand(const scope uint, const scope char*);
    int Tokenizer(TokenInfo*, uint, char*, size_t, char*, char*, char*, char*,
        char*, char*, int*, char*);

    uint MagickRandNewSeed();

    ubyte* Base64Decode(const scope char*, size_t*);

    MagickPassFail CloneString(char**, const scope char*);
    MagickPassFail ConcatenateString(char**, const scope char*);
    MagickPassFail ExpandFilenames(int*, char***);
    MagickPassFail GetExecutionPath(char*);
    MagickPassFail GetExecutionPathUsingName(char*);
    MagickPassFail MagickCreateDirectoryPath(const scope char* dir, ExceptionInfo* exception);

    MagickBool IsAccessible(const scope char*);
    MagickBool IsAccessibleNoLogging(const scope char*);
    MagickBool IsAccessibleAndNotEmpty(const scope char*);
    MagickBool IsGeometry(const scope char*);
    MagickBool IsGlob(const scope char*);
    MagickBool IsWriteable(const scope char*);
    MagickBool MagickSceneFileName(char* filename, const scope char* filename_template,
        const scope char* scene_template, const scope MagickBool force, c_ulong scene);

    c_ulong MultilineCensus(const scope char*);

    void AppendImageFormat(const scope char*, char*);
    void DefineClientName(const scope char*);
    void DefineClientPathAndName(const scope char*);
    void ExpandFilename(char*);
    void FormatSize(const scope magick_int64_t size, char*);
    void GetPathComponent(const scope char*, PathType, char*);
    void GetToken(const scope char*, char**, char*);
    void LocaleLower(char*);
    void LocaleUpper(char*);
    void Strip(char*);
    void SetGeometry(const scope Image*, RectangleInfo*);

    void FormatString(char* str, const scope char* format...);
    void FormatStringList(char* str, const scope char* format, va_list operands);

    magick_int64_t MagickSizeStrToInt64(const scope char* str, const scope uint kilo);

    size_t MagickStrlCat(char* dst, const scope char* src, const scope size_t size);
    size_t MagickStrlCpy(char* dst, const scope char* src, const scope size_t size);
    size_t MagickStrlCpyTrunc(char* dst, const scope char* src, const scope size_t size);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mAcquireString = char* function(const scope char*);
        alias mAllocateString = char* function(const scope char*);
        alias mBase64Encode = char* function(const scope ubyte*, const scope size_t, size_t*);
        alias mEscapeString = char* function(const scope char* , const scope char);
        alias mGetPageGeometry = char* function(const scope char*);
        alias mListFiles = char** function(const scope char*, const scope char*, c_long*);
        alias mSetClientName = char* function(const scope char*);
        alias mStringToArgv = char** function(const scope char* , int*);
        alias mStringToList = char** function(const scope char*);
        alias mTranslateText = char* function(const scope ImageInfo*, Image*, const scope char*);
        alias mTranslateTextEx = char* function(const scope ImageInfo*, Image*, const scope char*, MagickTextTranslate);

        alias mGetClientFilename = const(char)* function();
        alias mSetClientFilename = const(char)* function(const scope char*);
        alias mGetClientName = const(char)* function();
        alias mGetClientPath = const(char)* function();
        alias mSetClientPath = const(char)* function(const scope char*);

        alias mStringToDouble = double function(const scope char*, const scope double);

        alias mGetGeometry = int function(const scope char*, c_long*, c_long*, c_ulong*, c_ulong*);
        alias mGlobExpression = int function(const scope char*, const scope char*);
        alias mLocaleNCompare = int function(const scope char*, const scope char*, const scope size_t);
        alias mLocaleCompare = int function(const scope char*, const scope char*);
        alias mGetMagickDimension = int function(const scope char* str, double* width, double* height, double* xoff,
            double* yoff);
        alias mGetMagickGeometry = int function(const scope char* geometry, c_long* x, c_long* y, c_ulong* width,
            c_ulong* height);
        alias mMagickRandReentrant = int function(uint* seed);
        alias mSubstituteString = int function(char**, const scope char*, const scope char*);
        alias mSystemCommand = int function(const scope uint, const scope char*);
        alias mTokenizer = int function(TokenInfo*, uint, char*, size_t, char*, char*, char*, char*, char*, char*,
            int*, char*);

        alias mMagickRandNewSeed = uint function();

        alias mBase64Decode = ubyte* function(const scope char*, size_t*);

        alias mCloneString = MagickPassFail function (char**, const scope char*);
        alias mConcatenateString = MagickPassFail function (char**, const scope char*);
        alias mExpandFilenames = MagickPassFail function (int*, char***);
        alias mGetExecutionPath = MagickPassFail function (char*);
        alias mGetExecutionPathUsingName = MagickPassFail function (char*);
        alias mMagickCreateDirectoryPath = MagickPassFail function (const scope char* dir, ExceptionInfo* exception);

        alias mIsAccessible = MagickBool function(const scope char*);
        alias mIsAccessibleNoLogging = MagickBool function(const scope char*);
        alias mIsAccessibleAndNotEmpty = MagickBool function(const scope char*);
        alias mIsGeometry = MagickBool function(const scope char*);
        alias mIsGlob = MagickBool function(const scope char*);
        alias mIsWriteable = MagickBool function(const scope char*);
        alias mMagickSceneFileName = MagickBool function(char* filename, const scope char* filename_template,
            const scope char* scene_template, const scope MagickBool force, c_ulong scene);

        alias mMultilineCensus = c_ulong function(const scope char*);

        alias mAppendImageFormat = void function(const scope char*, char*);
        alias mDefineClientName = void function(const scope char*);
        alias mDefineClientPathAndName = void function(const scope char*);
        alias mExpandFilename = void function(char*);
        alias mFormatSize = void function(const scope magick_int64_t size, char*);
        alias mGetPathComponent = void function(const scope char*, PathType, char*);
        alias mGetToken = void function(const scope char*, char**, char*);
        alias mLocaleLower = void function(char*);
        alias mLocaleUpper = void function(char*);
        alias mStrip = void function(char*);
        alias mSetGeometry = void function(const scope Image*, RectangleInfo*);

        alias mFormatString = void function(char* str, const scope char* format...);
        alias mFormatStringList = void function(char* str, const scope char* format, va_list operands);

        alias mMagickSizeStrToInt64 = magick_int64_t function(const scope char* str, const scope uint kilo);

        alias mMagickStrlCat = size_t function(char* dst, const scope char* src, const scope size_t size);
        alias mMagickStrlCpy = size_t function(char* dst, const scope char* src, const scope size_t size);
        alias mMagickStrlCpyTrunc = size_t function(char* dst, const scope char* src, const scope size_t size);
    }

    __gshared
    {
        mAcquireString AcquireString;
        mAllocateString AllocateString;
        mBase64Encode Base64Encode;
        mEscapeString EscapeString;
        mGetPageGeometry GetPageGeometry;
        mListFiles ListFiles;
        mSetClientName SetClientName;
        mStringToArgv StringToArgv;
        mStringToList StringToList;
        mTranslateText TranslateText;
        mTranslateTextEx TranslateTextEx;

        mGetClientFilename GetClientFilename;
        mSetClientFilename SetClientFilename;
        mGetClientName GetClientName;
        mGetClientPath GetClientPath;
        mSetClientPath SetClientPath;

        mStringToDouble StringToDouble;

        mGetGeometry GetGeometry;
        mGlobExpression GlobExpression;
        mLocaleNCompare LocaleNCompare;
        mLocaleCompare LocaleCompare;
        mGetMagickDimension GetMagickDimension;
        mGetMagickGeometry GetMagickGeometry;
        mMagickRandReentrant MagickRandReentrant;
        mSubstituteString SubstituteString;
        mSystemCommand SystemCommand;
        mTokenizer Tokenizer;

        mMagickRandNewSeed MagickRandNewSeed;

        mBase64Decode Base64Decode;

        mCloneString CloneString;
        mConcatenateString ConcatenateString;
        mExpandFilenames ExpandFilenames;
        mGetExecutionPath GetExecutionPath;
        mGetExecutionPathUsingName GetExecutionPathUsingName;
        mMagickCreateDirectoryPath MagickCreateDirectoryPath;

        mIsAccessible IsAccessible;
        mIsAccessibleNoLogging IsAccessibleNoLogging;
        mIsAccessibleAndNotEmpty IsAccessibleAndNotEmpty;
        mIsGeometry IsGeometry;
        mIsGlob IsGlob;
        mIsWriteable IsWriteable;
        mMagickSceneFileName MagickSceneFileName;

        mMultilineCensus MultilineCensus;

        mAppendImageFormat AppendImageFormat;
        mDefineClientName DefineClientName;
        mDefineClientPathAndName DefineClientPathAndName;
        mExpandFilename ExpandFilename;
        mFormatSize FormatSize;
        mGetPathComponent GetPathComponent;
        mGetToken GetToken;
        mLocaleLower LocaleLower;
        mLocaleUpper LocaleUpper;
        mStrip Strip;
        mSetGeometry SetGeometry;

        mFormatString FormatString;
        mFormatStringList FormatStringList;

        mMagickSizeStrToInt64 MagickSizeStrToInt64;

        mMagickStrlCat MagickStrlCat;
        mMagickStrlCpy MagickStrlCpy;
        mMagickStrlCpyTrunc MagickStrlCpyTrunc;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadUtilityH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            AcquireString = cast(mAcquireString)dlsym(dlib, "AcquireString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            AllocateString = cast(mAllocateString)dlsym(dlib, "AllocateString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            Base64Encode = cast(mBase64Encode)dlsym(dlib, "Base64Encode");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            EscapeString = cast(mEscapeString)dlsym(dlib, "EscapeString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetPageGeometry = cast(mGetPageGeometry)dlsym(dlib, "GetPageGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListFiles = cast(mListFiles)dlsym(dlib, "ListFiles");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetClientName = cast(mSetClientName)dlsym(dlib, "SetClientName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            StringToArgv = cast(mStringToArgv)dlsym(dlib, "StringToArgv");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            StringToList = cast(mStringToList)dlsym(dlib, "StringToList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            TranslateText = cast(mTranslateText)dlsym(dlib, "TranslateText");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            TranslateTextEx = cast(mTranslateTextEx)dlsym(dlib, "TranslateTextEx");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetClientFilename = cast(mGetClientFilename)dlsym(dlib, "GetClientFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetClientFilename = cast(mSetClientFilename)dlsym(dlib, "SetClientFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetClientName = cast(mGetClientName)dlsym(dlib, "GetClientName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetClientPath = cast(mGetClientPath)dlsym(dlib, "GetClientPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetClientPath = cast(mSetClientPath)dlsym(dlib, "SetClientPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            StringToDouble = cast(mStringToDouble)dlsym(dlib, "StringToDouble");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetGeometry = cast(mGetGeometry)dlsym(dlib, "GetGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GlobExpression = cast(mGlobExpression)dlsym(dlib, "GlobExpression");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            LocaleNCompare = cast(mLocaleNCompare)dlsym(dlib, "LocaleNCompare");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            LocaleCompare = cast(mLocaleCompare)dlsym(dlib, "LocaleCompare");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetMagickDimension = cast(mGetMagickDimension)dlsym(dlib, "GetMagickDimension");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetMagickGeometry = cast(mGetMagickGeometry)dlsym(dlib, "GetMagickGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRandReentrant = cast(mMagickRandReentrant)dlsym(dlib, "MagickRandReentrant");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SubstituteString = cast(mSubstituteString)dlsym(dlib, "SubstituteString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SystemCommand = cast(mSystemCommand)dlsym(dlib, "SystemCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            Tokenizer = cast(mTokenizer)dlsym(dlib, "Tokenizer");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickRandNewSeed = cast(mMagickRandNewSeed)dlsym(dlib, "MagickRandNewSeed");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            Base64Decode = cast(mBase64Decode)dlsym(dlib, "Base64Decode");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneString = cast(mCloneString)dlsym(dlib, "CloneString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            ConcatenateString = cast(mConcatenateString)dlsym(dlib, "ConcatenateString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExpandFilenames = cast(mExpandFilenames)dlsym(dlib, "ExpandFilenames");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetExecutionPath = cast(mGetExecutionPath)dlsym(dlib, "GetExecutionPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetExecutionPathUsingName = cast(mGetExecutionPathUsingName)dlsym(dlib, "GetExecutionPathUsingName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCreateDirectoryPath = cast(mMagickCreateDirectoryPath)dlsym(dlib, "MagickCreateDirectoryPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            IsAccessible = cast(mIsAccessible)dlsym(dlib, "IsAccessible");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsAccessibleNoLogging = cast(mIsAccessibleNoLogging)dlsym(dlib, "IsAccessibleNoLogging");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsAccessibleAndNotEmpty = cast(mIsAccessibleAndNotEmpty)dlsym(dlib, "IsAccessibleAndNotEmpty");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsGeometry = cast(mIsGeometry)dlsym(dlib, "IsGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsGlob = cast(mIsGlob)dlsym(dlib, "IsGlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsWriteable = cast(mIsWriteable)dlsym(dlib, "IsWriteable");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSceneFileName = cast(mMagickSceneFileName)dlsym(dlib, "MagickSceneFileName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            MultilineCensus = cast(mMultilineCensus)dlsym(dlib, "MultilineCensus");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            AppendImageFormat = cast(mAppendImageFormat)dlsym(dlib, "AppendImageFormat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            DefineClientName = cast(mDefineClientName)dlsym(dlib, "DefineClientName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            DefineClientPathAndName = cast(mDefineClientPathAndName)dlsym(dlib, "DefineClientPathAndName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExpandFilename = cast(mExpandFilename)dlsym(dlib, "ExpandFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            FormatSize = cast(mFormatSize)dlsym(dlib, "FormatSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetPathComponent = cast(mGetPathComponent)dlsym(dlib, "GetPathComponent");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetToken = cast(mGetToken)dlsym(dlib, "GetToken");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            LocaleLower = cast(mLocaleLower)dlsym(dlib, "LocaleLower");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            LocaleUpper = cast(mLocaleUpper)dlsym(dlib, "LocaleUpper");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            Strip = cast(mStrip)dlsym(dlib, "Strip");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetGeometry = cast(mSetGeometry)dlsym(dlib, "SetGeometry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            FormatString = cast(mFormatString)dlsym(dlib, "FormatString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            FormatStringList = cast(mFormatStringList)dlsym(dlib, "FormatStringList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickSizeStrToInt64 = cast(mMagickSizeStrToInt64)dlsym(dlib, "MagickSizeStrToInt64");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickStrlCat = cast(mMagickStrlCat)dlsym(dlib, "MagickStrlCat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickStrlCpy = cast(mMagickStrlCpy)dlsym(dlib, "MagickStrlCpy");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickStrlCpyTrunc = cast(mMagickStrlCpyTrunc)dlsym(dlib, "MagickStrlCpyTrunc");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.utility:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
