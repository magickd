/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image FX Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.fx;

import core.stdc.config : c_ulong;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image, PixelPacket;

version(GMagick_Static)
{
    @system @nogc nothrow extern(C):

    Image* CharcoalImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);
    Image* ColorizeImage(const scope Image*, const scope char*, const scope PixelPacket, ExceptionInfo*);
    Image* ConvolveImage(const scope Image*, const scope uint, const scope double*, ExceptionInfo*);
    Image* ImplodeImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* MorphImages(const scope Image*, const scope c_ulong, ExceptionInfo*);
    Image* OilPaintImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* SteganoImage(const scope Image*, const scope Image*, ExceptionInfo*);
    Image* StereoImage(const scope Image*, const scope Image*, ExceptionInfo*);
    Image* SwirlImage(const scope Image*, double, ExceptionInfo*);
    Image* WaveImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);

    uint SolarizeImage(Image*, const scope double);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mCharcoalImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);
        alias mColorizeImage = Image* function(const scope Image*, const scope char*, const scope PixelPacket, ExceptionInfo*);
        alias mConvolveImage = Image* function(const scope Image*, const scope uint, const scope double*, ExceptionInfo*);
        alias mImplodeImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mMorphImages = Image* function(const scope Image*, const scope c_ulong, ExceptionInfo*);
        alias mOilPaintImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mSteganoImage = Image* function(const scope Image*, const scope Image*, ExceptionInfo*);
        alias mStereoImage = Image* function(const scope Image*, const scope Image*, ExceptionInfo*);
        alias mSwirlImage = Image* function(const scope Image*, double, ExceptionInfo*);
        alias mWaveImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);

        alias mSolarizeImage = uint function(Image*, const scope double);
    }

    __gshared
    {
        mCharcoalImage CharcoalImage;
        mColorizeImage ColorizeImage;
        mConvolveImage ConvolveImage;
        mImplodeImage ImplodeImage;
        mMorphImages MorphImages;
        mOilPaintImage OilPaintImage;
        mSteganoImage SteganoImage;
        mStereoImage StereoImage;
        mSwirlImage SwirlImage;
        mWaveImage WaveImage;

        mSolarizeImage SolarizeImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadFxH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            CharcoalImage = cast(mCharcoalImage)dlsym(dlib, "CharcoalImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            ColorizeImage = cast(mColorizeImage)dlsym(dlib, "ColorizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            ConvolveImage = cast(mConvolveImage)dlsym(dlib, "ConvolveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImplodeImage = cast(mImplodeImage)dlsym(dlib, "ImplodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            MorphImages = cast(mMorphImages)dlsym(dlib, "MorphImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            OilPaintImage = cast(mOilPaintImage)dlsym(dlib, "OilPaintImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            SteganoImage = cast(mSteganoImage)dlsym(dlib, "SteganoImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            StereoImage = cast(mStereoImage)dlsym(dlib, "StereoImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            SwirlImage = cast(mSwirlImage)dlsym(dlib, "SwirlImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }
            WaveImage = cast(mWaveImage)dlsym(dlib, "WaveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }

            SolarizeImage = cast(mSolarizeImage)dlsym(dlib, "SolarizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.fx:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
