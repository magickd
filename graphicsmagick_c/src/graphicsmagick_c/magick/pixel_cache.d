/*
  Copyright (C) 2003 - 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Pixel Cache Methods.
*/
// Complete graphicsmagick_c wrapper for v1.3
module graphicsmagick_c.magick.pixel_cache;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.forward : _CacheInfoPtr_, ViewInfo;
import graphicsmagick_c.magick.image;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.magick_types : magick_off_t;

/*
  Enum declarations
*/
enum : VirtualPixelMethod
{
    UndefinedVirtualPixelMethod,
    ConstantVirtualPixelMethod,
    EdgeVirtualPixelMethod,
    MirrorVirtualPixelMethod,
    TileVirtualPixelMethod
}
alias VirtualPixelMethod = int;

/*
  Typedef declarations
*/
alias Cache = _CacheInfoPtr_;

/*****
 *
 * Default Interfaces
 *
 *****/

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    /*
        Read only access to a rectangular pixel region.
    */
    const(PixelPacket)* AcquireImagePixels(const scope Image* image, const scope c_long x, const scope c_long y,
        const scope c_ulong columns, const scope c_ulong rows, ExceptionInfo* exception);

    /*
        AccessImmutableIndexes() returns the read-only indexes
        associated with a rectangular pixel region already selected via
        AcquireImagePixels().
    */
    const(IndexPacket)* AccessImmutableIndexes(const scope Image* image);

    /*
        Returns one DirectClass pixel at the specified (x, y) location.
        Similar function as GetOnePixel().  Note that the value returned
        by GetIndexes() may or may not be influenced by this function.
    */
    PixelPacket AcquireOnePixel(const scope Image* image, const scope c_long x, const scope c_long y, ExceptionInfo* exception);

    /*
        GetImagePixels() and GetImagePixelsEx() obtains a pixel region for
        read/write access.
    */
    PixelPacket* GetImagePixels(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows);
    PixelPacket* GetImagePixelsEx(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows,
        ExceptionInfo* exception);

    /*
        GetImageVirtualPixelMethod() gets the "virtual pixels" method for
        the image.
    */
    VirtualPixelMethod GetImageVirtualPixelMethod(const scope Image* image);

    /*
        GetPixels() and AccessMutablePixels() returns the pixels associated
        with the last call to SetImagePixels() or GetImagePixels()
    */
    PixelPacket* GetPixels(const scope Image* image);
    PixelPacket* AccessMutablePixels(Image* image);

    /*
        GetIndexes() and AccessMutableIndexes() return the colormap
        indexes associated with the last call to SetImagePixels() or
        GetImagePixels()
    */
    IndexPacket* GetIndexes(const scope Image* image);
    IndexPacket* AccessMutableIndexes(Image* image);

    /*
        GetOnePixel() returns a single DirectClass pixel at the specified
        (x,y) location.  Similar to AcquireOnePixel().  It is preferred to
        use AcquireOnePixel() since it allows reporting to a specified
        exception structure.  Note that the value returned by GetIndexes()
        is not reliably influenced by this function.
    */
    PixelPacket GetOnePixel(Image* image, const scope c_long x, const scope c_long y);

    /*
        GetPixelCacheArea() returns the area (width * height in pixels)
        consumed by the current pixel cache.
    */
    magick_off_t GetPixelCacheArea(const scope Image* image);

    /*
        SetImagePixels() and SetImagePixelsEx() initialize a pixel region
        for write-only access.
    */
    PixelPacket* SetImagePixels(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows);
    PixelPacket* SetImagePixelsEx(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows,
        ExceptionInfo* exception);

    /*
        SetImageVirtualPixelMethod() sets the "virtual pixels" method for
        the image.
    */
    MagickPassFail SetImageVirtualPixelMethod(const scope Image* image, const scope VirtualPixelMethod method);

    /*
        SyncImagePixels() and SynImagePixelsEx() save the image pixels to
        the in-memory or disk cache.
    */
    MagickPassFail SyncImagePixels(Image* image);
    MagickPassFail SyncImagePixelsEx(Image* image, ExceptionInfo* exception);

    /*****
     *
     * Cache view interfaces
     *
     *****/

    /*
        OpenCacheView() opens a cache view
    */
    ViewInfo* OpenCacheView(Image* image);

    /*
        CloseCacheView() closes a cache view.
    */
    void CloseCacheView(ViewInfo* view);

    /*
        AccessCacheViewPixels() returns the pixels associated with the
        last request to select a view pixel regiono
        (i.e. AcquireCacheViewPixels() or GetCacheViewPixels())
    */
    PixelPacket* AccessCacheViewPixels(const scope ViewInfo* view);

    /*
        AcquireCacheViewIndexes() returns read-only indexes associated
        with a cache view.
    */
    const(IndexPacket)* AcquireCacheViewIndexes(const scope ViewInfo* view);

    /*
        AcquireCacheViewPixels() obtains a pixel region from a cache view
        for read-only access.
    */
    const(PixelPacket)* AcquireCacheViewPixels(const scope ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns,
        const scope c_ulong rows, ExceptionInfo* exception);

    /*
        AcquireOneCacheViewPixel() returns one DirectClass pixel from a
        cache view.  Note that the value returned by GetCacheViewIndexes()
        is not reliably influenced by this function.
    */
    MagickPassFail AcquireOneCacheViewPixel(const scope ViewInfo* view, PixelPacket* pixel, const scope c_long x, const scope c_long y,
        ExceptionInfo* exception);

    /*
        GetCacheViewArea() returns the area (width * height in pixels)
        currently consumed by the pixel cache view.
    */
    magick_off_t GetCacheViewArea(const scope ViewInfo* view);

    /*
        GetCacheViewImage() obtains the image used to allocate the cache view.
    */
    Image* GetCacheViewImage(const scope ViewInfo* view);

    /*
        GetCacheViewIndexes() returns the indexes associated with a cache view.
    */
    IndexPacket* GetCacheViewIndexes(const scope ViewInfo* view);

    /*
        Get CacheViewPixles() obtains a pixel region from a cache view for
        read/write access.
    */
    PixelPacket* GetCacheViewPixels(const scope ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows,
        ExceptionInfo* exception);

    /*
        Obtain the offset and size of the selected region.
    */
    RectangleInfo GetCacheViewRegion(const scope ViewInfo* view);

    /*
        SetCacheViewPixels() gets blank writeable pixels from the pixel
        cache view.
    */
    PixelPacket* SetCacheViewPixels(const scope ViewInfo* view, const scope c_long x, const scope c_long y, const scope c_ulong columns, const scope c_ulong rows,
        ExceptionInfo* exception);

    /*
        SyncCacheViewPixels() saves any changes to the pixel cache view.
    */
    MagickPassFail SyncCacheViewPixels(const scope ViewInfo* view, ExceptionInfo* exception);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mAcquireImagePixels = const(PixelPacket)* function(const scope Image* image, const scope c_long x, const scope c_long y,
            const scope c_ulong columns, const scope c_ulong rows, ExceptionInfo* exception);

        alias mAccessImmutableIndexes = const(IndexPacket)* function(const scope Image* image);

        alias mAcquireOnePixel = PixelPacket function(const scope Image* image, const scope c_long x, const scope c_long y,
            ExceptionInfo* exception);

        alias mGetImagePixels = PixelPacket* function(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows);
        alias mGetImagePixelsEx = PixelPacket* function(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows, ExceptionInfo* exception);

        alias mGetImageVirtualPixelMethod = VirtualPixelMethod function(const scope Image* image);

        alias mGetPixels = PixelPacket* function(const scope Image* image);

        alias mAccessMutablePixels = PixelPacket* function(Image* image);

        alias mGetIndexes = IndexPacket* function(const scope Image* image);

        alias mAccessMutableIndexes = IndexPacket* function(Image* image);

        alias mGetOnePixel = PixelPacket function(Image* image, const scope c_long x, const scope c_long y);

        alias mGetPixelCacheArea = magick_off_t function(const scope Image* image);

        alias mSetImagePixels = PixelPacket* function(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows);
        alias mSetImagePixelsEx = PixelPacket* function(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns,
            const scope c_ulong rows, ExceptionInfo* exception);

        alias mSetImageVirtualPixelMethod = MagickPassFail function(const scope Image* image, const scope VirtualPixelMethod method);

        alias mSyncImagePixels = MagickPassFail function(Image* image);
        alias mSyncImagePixelsEx = MagickPassFail function(Image* image, ExceptionInfo* exception);

        alias mOpenCacheView = ViewInfo* function(Image* image);

        alias mCloseCacheView = void function(ViewInfo* view);

        alias mAccessCacheViewPixels = PixelPacket* function(const scope ViewInfo* view);

        alias mAcquireCacheViewIndexes = const(IndexPacket)* function(const scope ViewInfo* view);

        alias mAcquireCacheViewPixels = const(PixelPacket)* function(const scope ViewInfo* view, const scope c_long x, const scope c_long y,
            const scope c_ulong columns, const scope c_ulong rows, ExceptionInfo* exception);

        alias mAcquireOneCacheViewPixel = MagickPassFail function(const scope ViewInfo* view, PixelPacket* pixel, const scope c_long x,
            const scope c_long y, ExceptionInfo* exception);

        alias mGetCacheViewArea = magick_off_t function(const scope ViewInfo* view);

        alias mGetCacheViewImage = Image* function(const scope ViewInfo* view);

        alias mGetCacheViewIndexes = IndexPacket* function(const scope ViewInfo* view);

        alias mGetCacheViewPixels = PixelPacket* function(const scope ViewInfo* view, const scope c_long x, const scope c_long y,
            const scope c_ulong columns, const scope c_ulong rows, ExceptionInfo* exception);

        alias mGetCacheViewRegion = RectangleInfo function(const scope ViewInfo* view);

        alias mSetCacheViewPixels = PixelPacket* function(const scope ViewInfo* view, const scope c_long x, const scope c_long y,
            const scope c_ulong columns, const scope c_ulong rows, ExceptionInfo* exception);

        alias mSyncCacheViewPixels = MagickPassFail function(const scope ViewInfo* view, ExceptionInfo* exception);
    }

    __gshared
    {
        mAcquireImagePixels AcquireImagePixels;

        mAccessImmutableIndexes AccessImmutableIndexes;

        mAcquireOnePixel AcquireOnePixel;

        mGetImagePixels GetImagePixels;
        mGetImagePixelsEx GetImagePixelsEx;

        mGetImageVirtualPixelMethod GetImageVirtualPixelMethod;

        mGetPixels GetPixels;

        mAccessMutablePixels AccessMutablePixels;

        mGetIndexes GetIndexes;

        mAccessMutableIndexes AccessMutableIndexes;

        mGetOnePixel GetOnePixel;

        mGetPixelCacheArea GetPixelCacheArea;

        mSetImagePixels SetImagePixels;
        mSetImagePixelsEx SetImagePixelsEx;

        mSetImageVirtualPixelMethod SetImageVirtualPixelMethod;

        mSyncImagePixels SyncImagePixels;
        mSyncImagePixelsEx SyncImagePixelsEx;

        mOpenCacheView OpenCacheView;

        mCloseCacheView CloseCacheView;

        mAccessCacheViewPixels AccessCacheViewPixels;

        mAcquireCacheViewIndexes AcquireCacheViewIndexes;

        mAcquireCacheViewPixels AcquireCacheViewPixels;

        mAcquireOneCacheViewPixel AcquireOneCacheViewPixel;

        mGetCacheViewArea GetCacheViewArea;

        mGetCacheViewImage GetCacheViewImage;

        mGetCacheViewIndexes GetCacheViewIndexes;

        mGetCacheViewPixels GetCacheViewPixels;

        mGetCacheViewRegion GetCacheViewRegion;

        mSetCacheViewPixels SetCacheViewPixels;

        mSyncCacheViewPixels SyncCacheViewPixels;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadPixelCacheH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            AcquireImagePixels = cast(mAcquireImagePixels)dlsym(dlib, "AcquireImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AccessImmutableIndexes = cast(mAccessImmutableIndexes)dlsym(dlib, "AccessImmutableIndexes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireOnePixel = cast(mAcquireOnePixel)dlsym(dlib, "AcquireOnePixel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImagePixels = cast(mGetImagePixels)dlsym(dlib, "GetImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetImagePixelsEx = cast(mGetImagePixelsEx)dlsym(dlib, "GetImagePixelsEx");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageVirtualPixelMethod = cast(mGetImageVirtualPixelMethod)dlsym(dlib, "GetImageVirtualPixelMethod");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetPixels = cast(mGetPixels)dlsym(dlib, "GetPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AccessMutablePixels = cast(mAccessMutablePixels)dlsym(dlib, "AccessMutablePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetIndexes = cast(mGetIndexes)dlsym(dlib, "GetIndexes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AccessMutableIndexes = cast(mAccessMutableIndexes)dlsym(dlib, "AccessMutableIndexes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetOnePixel = cast(mGetOnePixel)dlsym(dlib, "GetOnePixel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetPixelCacheArea = cast(mGetPixelCacheArea)dlsym(dlib, "GetPixelCacheArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImagePixels = cast(mSetImagePixels)dlsym(dlib, "SetImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetImagePixelsEx = cast(mSetImagePixelsEx)dlsym(dlib, "SetImagePixelsEx");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetImageVirtualPixelMethod = cast(mSetImageVirtualPixelMethod)dlsym(dlib, "SetImageVirtualPixelMethod");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            SyncImagePixels = cast(mSyncImagePixels)dlsym(dlib, "SyncImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }
            SyncImagePixelsEx = cast(mSyncImagePixelsEx)dlsym(dlib, "SyncImagePixelsEx");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            OpenCacheView = cast(mOpenCacheView)dlsym(dlib, "OpenCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloseCacheView = cast(mCloseCacheView)dlsym(dlib, "CloseCacheView");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AccessCacheViewPixels = cast(mAccessCacheViewPixels)dlsym(dlib, "AccessCacheViewPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireCacheViewIndexes = cast(mAcquireCacheViewIndexes)dlsym(dlib, "AcquireCacheViewIndexes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireCacheViewPixels = cast(mAcquireCacheViewPixels)dlsym(dlib, "AcquireCacheViewPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            AcquireOneCacheViewPixel = cast(mAcquireOneCacheViewPixel)dlsym(dlib, "AcquireOneCacheViewPixel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetCacheViewArea = cast(mGetCacheViewArea)dlsym(dlib, "GetCacheViewArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetCacheViewImage = cast(mGetCacheViewImage)dlsym(dlib, "GetCacheViewImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetCacheViewIndexes = cast(mGetCacheViewIndexes)dlsym(dlib, "GetCacheViewIndexes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetCacheViewPixels = cast(mGetCacheViewPixels)dlsym(dlib, "GetCacheViewPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetCacheViewRegion = cast(mGetCacheViewRegion)dlsym(dlib, "GetCacheViewRegion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetCacheViewPixels = cast(mSetCacheViewPixels)dlsym(dlib, "SetCacheViewPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            SyncCacheViewPixels = cast(mSyncCacheViewPixels)dlsym(dlib, "SyncCacheViewPixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_cache:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
