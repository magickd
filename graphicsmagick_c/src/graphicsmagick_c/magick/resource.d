/*
  Copyright (C) 2003, 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Resource methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.resource;

import core.stdc.stdio;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.magick_types;

/*
    Typedef declarations.
*/
alias ResourceType = int;
enum : ResourceType
{
    UndefinedResource = 0, /* Undefined value */
    DiskResource,          /* Pixel cache total disk space (Gigabytes) */
    FileResource,          /* Pixel cache number of open files (Files) */
    MapResource,           /* Pixel cache total file memory-mapping (Megabytes) */
    MemoryResource,        /* Maximum pixel cache heap memory allocations (Megabytes) */
    PixelsResource         /* Maximum number of pixels in single image (Pixels) */
}

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    MagickPassFail AcquireMagickResource(const scope ResourceType type, const scope magick_uint64_t size);
    MagickPassFail ListMagickResourceInfo(FILE* file, ExceptionInfo* exception);
    MagickPassFail SetMagickResourceLimit(const scope ResourceType type, const scope magick_int64_t limit);

    magick_int64_t GetMagickResource(const scope ResourceType type);

    void DestroyMaigckResources();
    void InitlaizeMagickResources();
    void LiberateMagickResource(const scope ResourceType type, const scope magick_uint64_t size);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mAcquireMagickResource = MagickPassFail function(const scope ResourceType, const scope magick_uint64_t);
        alias mListMagickResourceInfo = MagickPassFail function(FILE*, ExceptionInfo*);
        alias mSetMagickResourceLimit = MagickPassFail function(const scope ResourceType, const scope magick_int64_t);

        alias mGetMagickResource = magick_int64_t function(const scope ResourceType);

        alias mDestroyMagickResources = void function();
        alias mInitializeMagickResources = void function();
        alias mLiberateMagickResource = void function(const scope ResourceType, const scope magick_uint64_t);
    }

    __gshared
    {
        mAcquireMagickResource AcquireMagickResource;
        mListMagickResourceInfo ListMagickResourceInfo;
        mSetMagickResourceLimit SetMagickResourceLimit;

        mGetMagickResource GetMagickResource;

        mDestroyMagickResources DestroyMagickResources;
        mInitializeMagickResources InitializeMagickResources;
        mLiberateMagickResource LiberateMagickResource;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn;
        import core.stdc.stdio;

        bool _loadResourceH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            AcquireMagickResource = cast(mAcquireMagickResource)dlsym(dlib, "AcquireMagickResource");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListMagickResourceInfo = cast(mListMagickResourceInfo)dlsym(dlib, "ListMagickResourceInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetMagickResourceLimit = cast(mSetMagickResourceLimit)dlsym(dlib, "SetMagickResourceLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetMagickResource = cast(mGetMagickResource)dlsym(dlib, "GetMagickResource");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMagickResources = cast(mDestroyMagickResources)dlsym(dlib, "DestroyMagickResources");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }
            InitializeMagickResources = cast(mInitializeMagickResources)dlsym(dlib, "InitializeMagickResources");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }
            LiberateMagickResource = cast(mLiberateMagickResource)dlsym(dlib, "LiberateMagickResource");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resource:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
