/*
  Copyright (C) 2007 GraphicsMagick Group
 
  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Forward declarations for types used in public structures.
 
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.forward;


extern(C):
nothrow:
@nogc:
	
struct _Ascii85Info;
struct _BlobInfo;
struct _CacheInfo;
struct _Image;
struct _ImageAttribute;
struct _SemaphoreInfo;
struct _ThreadViewSet;
	
alias ImagePtr = _Image*;
alias _Ascii85InfoPtr_ = _Ascii85Info*;
alias _BlobInfoPtr_ = _BlobInfo*;
alias _CacheInfoPtr_ = _CacheInfo*;
alias _ImageAttributePtr_ = _ImageAttribute*;
alias _SemaphoreInfoPtr_ = _SemaphoreInfo*;
alias _ThreadViewSetPtr_ = _ThreadViewSet*;
alias ViewInfo = void*;