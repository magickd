/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Progress Monitor Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.monitor;

import core.stdc.stdarg;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.magick_types : magick_int64_t, magick_uint64_t;

alias MonitorHandler = @nogc nothrow extern(C) MagickPassFail function(const scope char* text, const scope magick_int64_t quantum,
    const scope magick_uint64_t span, ExceptionInfo* exception);

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    MonitorHandler SetMonitorHandler(MonitorHandler handler);

    MagickPassFail MagickMonitor(const scope char* text, const scope magick_int64_t quantum, const scope magick_uint64_t span,
        ExceptionInfo* exception);
    MagickPassFail MagickMonitorFormatted(const scope magick_int64_t quantum, const scope magick_uint64_t span,
        ExceptionInfo* exception, char* format...);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mSetMonitorHandler = MonitorHandler function(MonitorHandler handler);

        alias mMagickMonitor = MagickPassFail function(const scope char* text, const scope magick_int64_t quantum,
            const scope magick_uint64_t span, ExceptionInfo* exception);
        alias mMagickMonitorFormatted = MagickPassFail function(const scope magick_int64_t quantum, const scope magick_uint64_t span,
            ExceptionInfo* exception, char* format...);
    }

    __gshared
    {
        mSetMonitorHandler SetMonitorHandler;

        mMagickMonitor MagickMonitor;
        mMagickMonitorFormatted MagickMonitorFormatted;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMonitorH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            SetMonitorHandler = cast(mSetMonitorHandler)dlsym(dlib, "SetMonitorHandler");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.monitor:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickMonitor = cast(mMagickMonitor)dlsym(dlib, "MagickMonitor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.monitor:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMonitorFormatted = cast(mMagickMonitorFormatted)dlsym(dlib, "MagickMonitorFormatted");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.monitor:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
