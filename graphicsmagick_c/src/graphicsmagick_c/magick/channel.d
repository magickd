/*
  Copyright (C) 2004 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.channel;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

version(GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* ExportImageChannel(const scope Image* image, const scope ChannelType channel, ExceptionInfo* exception);

    uint GetImageChannelDepth(const scope Image* image, const scope ChannelType channel, ExceptionInfo* exception);

    MagickPassFail ChannelImage(Image* image, const scope ChannelType channel);
    MagickPassFail ImportImageChannel(const scope Image* src_image, Image* dst_image, const scope ChannelType channel);
    MagickPassFail ImportImageChannelsMasked(const scope Image* source_image, Image* update_image, const scope ChannelType channels);
    MagickPassFail SetImageChannelDepth(Image* image, const scope ChannelType channel, const scope uint depth);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mExportImageChannel = Image* function(const scope Image*, const scope ChannelType, ExceptionInfo*);

        alias mGetImageChannelDepth = uint function(const scope Image*, const scope ChannelType, ExceptionInfo*);

        alias mChannelImage = MagickPassFail function(Image*, const scope ChannelType);
        alias mImportImageChannel = MagickPassFail function(const scope Image*, Image*, const scope ChannelType);
        alias mImportImageChannelsMasked = MagickPassFail function(const scope Image*, Image*, const scope ChannelType);
        alias mSetImageChannelDepth = MagickPassFail function(Image*, const scope ChannelType, const scope uint);
    }

    __gshared
    {
        mExportImageChannel ExportImageChannel;

        mGetImageChannelDepth GetImageChannelDepth;

        mChannelImage ChannelImage;
        mImportImageChannel ImportImageChannel;
        mImportImageChannelsMasked ImportImageChannelsMasked;
        mSetImageChannelDepth SetImageChannelDepth;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn;
        import core.stdc.stdio;

        bool _loadChannelH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            ExportImageChannel = cast(mExportImageChannel)dlsym(dlib, "ExportImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageChannelDepth = cast(mGetImageChannelDepth)dlsym(dlib, "GetImageChannelDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }

            ChannelImage = cast(mChannelImage)dlsym(dlib, "ChannelImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportImageChannel = cast(mImportImageChannel)dlsym(dlib, "ImportImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportImageChannelsMasked = cast(mImportImageChannelsMasked)dlsym(dlib, "ImportImageChannelsMasked");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetImageChannelDepth = cast(mSetImageChannelDepth)dlsym(dlib, "SetImageChannelDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.channel:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
