/*
  Copyright (C) 2003 - 2008 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Memory Allocation Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.memory;

alias MagickMallocFunc = @nogc nothrow extern(C) void* function(size_t size);
alias MagickFreeFunc = @nogc nothrow extern(C) void function(void* ptr);
alias MagickReallocFunc = @nogc nothrow extern(C) void* function(void* ptr, size_t size);

version(GMagick_Static)
{
    @system @nogc nothrow extern (C):

    void MagickAllocFunctions(MagickFreeFunc free_func, MagickMallocFunc malloc_func, MagickReallocFunc realloc_func);
    void* MagickMalloc(const scope size_t size);
    void* MagickMallocArray(const scope size_t count, const scope size_t size);
    void* MagickCloneMemory(void* destination, const scope void* source, const scope size_t size);
    void* MagickRealloc(void* memory, const scope size_t size);
    void MagickFree(void* memory);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mMagickAllocFunctions = void function(MagickFreeFunc free_func, MagickMallocFunc malloc_func,
            MagickReallocFunc realloc_func);
        alias mMagickMalloc = void* function(const scope size_t size);
        alias mMagickMallocArray = void* function(const scope size_t count, const scope size_t size);
        alias mMagickCloneMemory = void* function(void* destination, const scope void* source, const scope size_t size);
        alias mMagickRealloc = void* function(void* memory, const scope size_t size);
        alias mMagickFree = void function(void* memory);
    }

    __gshared
    {
        mMagickAllocFunctions MagickAllocFunctions;
        mMagickMalloc MagickMalloc;
        mMagickMallocArray MagickMallocArray;
        mMagickCloneMemory MagickCloneMemory;
        mMagickRealloc MagickRealloc;
        mMagickFree MagickFree;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMemoryH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            MagickAllocFunctions = cast(mMagickAllocFunctions)dlsym(dlib, "MagickAllocFunctions");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMalloc = cast(mMagickMalloc)dlsym(dlib, "MagickMalloc");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMallocArray = cast(mMagickMallocArray)dlsym(dlib, "MagickMallocArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCloneMemory = cast(mMagickCloneMemory)dlsym(dlib, "MagickCloneMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRealloc = cast(mMagickRealloc)dlsym(dlib, "MagickRealloc");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFree = cast(mMagickFree)dlsym(dlib, "MagickFree");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.memory:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
