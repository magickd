/*
  Copyright (C) 2003, 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Magic methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.magic;

import core.stdc.stdio;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error;

/*
  Method declarations.
*/
version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    MagickPassFail GetMagickFileFormat(const scope ubyte *header, const scope size_t header_length,
        char *format, const scope size_t format_length, ExceptionInfo* exception);
    MagickPassFail InitializeMagicInfo(ExceptionInfo *exception);
    MagickPassFail ListMagicInfo(FILE *file, ExceptionInfo *exception);

    void DestroyMagicInfo();
} // version(GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetMagickFileFormat = MagickPassFail function(const scope ubyte*, const scope size_t, char*, const scope size_t,
            ExceptionInfo*);
        alias mInitializeMagicInfo = MagickPassFail function(ExceptionInfo*);
        alias mListMagicInfo = MagickPassFail function(FILE*, ExceptionInfo*);

        alias mDestroyMagicInfo = void function();
    }

    __gshared
    {
        mGetMagickFileFormat GetMagickFileFormat;
        mInitializeMagicInfo InitializeMagicInfo;
        mListMagicInfo ListMagicInfo;

        mDestroyMagicInfo DestroyMagicInfo;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMagicH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetMagickFileFormat = cast(mGetMagickFileFormat)dlsym(dlib, "GetMagickFileFormat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magic:\n  %s\n", errmsg);
                }
                success = false;
            }
            InitializeMagicInfo = cast(mInitializeMagicInfo)dlsym(dlib, "InitializeMagicInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magic:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListMagicInfo = cast(mListMagicInfo)dlsym(dlib, "ListMagicInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magic:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMagicInfo = cast(mDestroyMagicInfo)dlsym(dlib, "DestroyMagicInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magic:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
