/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Resize Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.resize;

import core.stdc.config;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

enum DefaultResizeFilter = LanczosFilter;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* MagnifyImage(const scope Image*, ExceptionInfo*);
    Image* MinifyImage(const scope Image*, ExceptionInfo*);
    Image* ResizeImage(const scope Image*, const scope c_ulong, const scope c_ulong, const scope FilterTypes, const scope double,
        ExceptionInfo*);
    Image* SampleImage(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
    Image* ScaleImage(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
    Image* ThumbnailImage(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
    Image* ZoomImage(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mMagnifyImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mMinifyImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mResizeImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, const scope FilterTypes,
            double, ExceptionInfo*);
        alias mSampleImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
        alias mScaleImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
        alias mThumbnailImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
        alias mZoomImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, ExceptionInfo*);
    }

    __gshared
    {
        mMagnifyImage MagnifyImage;
        mMinifyImage MinifyImage;
        mResizeImage ResizeImage;
        mSampleImage SampleImage;
        mScaleImage ScaleImage;
        mThumbnailImage ThumbnailImage;
        mZoomImage ZoomImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadResizeH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            MagnifyImage = cast(mMagnifyImage)dlsym(dlib, "MagnifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            MinifyImage = cast(mMinifyImage)dlsym(dlib, "MinifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            ResizeImage = cast(mResizeImage)dlsym(dlib, "ResizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            SampleImage = cast(mSampleImage)dlsym(dlib, "SampleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            ScaleImage = cast(mScaleImage)dlsym(dlib, "ScaleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            ThumbnailImage = cast(mThumbnailImage)dlsym(dlib, "ThumbnailImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }
            ZoomImage = cast(mZoomImage)dlsym(dlib, "ZoomImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.resize:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
