/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Paint Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.paint;

import core.stdc.config;

import graphicsmagick_c.magick.image;
import graphicsmagick_c.magick.render;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    uint ColorFloodfillImage(Image*, const scope DrawInfo, const scope PixelPacket, const scope c_long,
        const scope c_long, const scope PaintMethod);
    uint MatteFloodfillImage(Image*, const scope PixelPacket, const scope uint, const scope c_long,
        const scope c_long, const scope PaintMethod);

    uint OpaqueImage(Image*, const scope PixelPacket, const scope PixelPacket);
    uint TransparentImage(Image*, const scope PixelPacket, const scope uint);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mColorFloodfillImage = uint function(Image*, const scope DrawInfo, const scope PixelPacket, const scope c_long,
            const scope c_long, const scope PaintMethod);
        alias mMatteFloodfillImage = uint function(Image*, const scope PixelPacket, const scope uint, const scope c_long,
            const scope c_long, const scope PaintMethod);

        alias mOpaqueImage = uint function(Image*, const scope PixelPacket, const scope PixelPacket);
        alias mTransparentImage = uint function(Image*, const scope PixelPacket, const scope uint);
    }

    __gshared
    {
        mColorFloodfillImage ColorFloodfillImage;
        mMatteFloodfillImage MatteFloodfillImage;

        mOpaqueImage OpaqueImage;
        mTransparentImage TransparentImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn;
        import core.stdc.stdio;

        bool _loadPaintH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            ColorFloodfillImage = cast(mColorFloodfillImage)dlsym(dlib, "ColorFloodfillImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.paint:\n  %s\n", errmsg);
                }
                success = false;
            }
            MatteFloodfillImage = cast(mMatteFloodfillImage)dlsym(dlib, "MatteFloodfillImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.paint:\n  %s\n", errmsg);
                }
                success = false;
            }

            OpaqueImage = cast(mOpaqueImage)dlsym(dlib, "OpaqueImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.paint:\n  %s\n", errmsg);
                }
                success = false;
            }
            TransparentImage = cast(mTransparentImage)dlsym(dlib, "TransparentImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.paint:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
