/*
  Copyright (C) 2003, 2007 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick types typedefs
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.magick_types;

/*
  Assign ANSI C stdint.h-like typedefs based on the sizes of native types
  magick_int8_t   --                       -128 to 127
  magick_uint8_t  --                          0 to 255
  magick_int16_t  --                    -32,768 to 32,767
  magick_uint16_t --                          0 to 65,535
  magick_int32_t  --             -2,147,483,648 to 2,147,483,647
  magick_uint32_t --                          0 to 4,294,967,295
  magick_int64_t  -- -9,223,372,036,854,775,807 to 9,223,372,036,854,775,807
  magick_uint64_t --                          0 to 18,446,744,073,709,551,615
*/

version(Windows)
{
    import core.stdc.config;

    /* The following typedefs are used for WIN32 */
    alias magick_int8_t = byte;
    alias magick_uint8_t = ubyte;

    alias magick_int16_t = short;
    alias magick_uint16_t = ushort;

    alias magick_int32_t = int;
    alias magick_uint32_t = uint;

    alias magick_int64_t = long;
    alias magick_uint64_t = ulong;

    alias magick_uintmax_t = magick_uint64_t;
    alias magick_uintptr_t = c_ulong;
}
else
{
    import core.stdc.stdint;

    /* The following typedefs are subtituted when using Unixish configure */
    alias magick_int8_t = int8_t;
    alias magick_uint8_t = uint8_t;

    alias magick_int16_t = int16_t;
    alias magick_uint16_t = uint16_t;

    alias magick_int32_t = int32_t;
    alias magick_uint32_t = uint32_t;

    alias magick_int64_t = int64_t;
    alias magick_uint64_t = uint64_t;

    alias magick_uintmax_t = uintmax_t;
    alias magick_uintptr_t = uintptr_t;
}

/* 64-bit file and blob offset type */
alias magick_off_t = magick_int64_t;
