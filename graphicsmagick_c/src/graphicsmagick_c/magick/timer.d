/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Timer Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.timer;

import core.stdc.config : c_ulong;

/*
  Enum declarations.
*/
alias TimerState = int;
enum : TimerState
{
    UndefinedTimerState,
    StoppedTimerState,
    RunningTimerState
}

/*
  Typedef declarations.
*/
struct _Timer
{
    double start;
    double stop;
    double total;
}
alias Timer = _Timer;

struct _TimerInfo
{
    Timer user;
    Timer elapsed;

    TimerState state;

    c_ulong signature;
}
alias TimerInfo = _TimerInfo;

/*
    Timer methods
*/
version(GMagick_Static) {
    @system @nogc nothrow extern (C):

    double GetElapsedTime(TimerInfo*);
    double GetUserTime(TimerInfo*);
    double GetTimerResolution();

    uint ContinueTimer(TimerInfo*);

    void GetTimerInfo(TimerInfo*);
    void ResetTimer(TimerInfo*);
} // version(GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetElapsedTime = double function(TimerInfo*);
        alias mGetUserTime = double function(TimerInfo*);
        alias mGetTimerResolution = double function();

        alias mContinueTimer = uint function(TimerInfo*);

        alias mGetTimerInfo = void function(TimerInfo*);
        alias mResetTimer = void function(TimerInfo*);
    }

    __gshared
    {
        mGetElapsedTime GetElapsedTime;
        mGetUserTime GetUserTime;
        mGetTimerResolution GetTimerResolution;

        mContinueTimer ContinueTimer;

        mGetTimerInfo GetTimerInfo;
        mResetTimer ResetTimer;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadTimerH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetElapsedTime = cast(mGetElapsedTime)dlsym(dlib, "GetElapsedTime");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetUserTime = cast(mGetUserTime)dlsym(dlib, "GetUserTime");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetTimerResolution = cast(mGetTimerResolution)dlsym(dlib, "GetTimerResolution");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }

            ContinueTimer = cast(mContinueTimer)dlsym(dlib, "ContinueTimer");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetTimerInfo = cast(mGetTimerInfo)dlsym(dlib, "GetTimerInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }
            ResetTimer = cast(mResetTimer)dlsym(dlib, "ResetTimer");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.timer:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
