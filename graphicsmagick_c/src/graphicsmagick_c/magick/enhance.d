/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Enhancement Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.enhance;

import graphicsmagick_c.magick.image : ChannelType, Image;

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    uint ContrastImage(Image*, const scope uint);
    uint EqualizeImage(Image*);
    uint GammaImage(Image*, const scope char*);
    uint LevelImage(Image*, const scope char*);
    uint LevelImageChannel(Image*, const scope ChannelType, const scope double, const scope double,
        const scope double);
    uint ModulateImage(Image*, const scope char*);
    uint NegateImage(Image*, const scope uint);
    uint NormalizeImage(Image*);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mContrastImage = uint function(Image*, const scope uint);
        alias mEqualizeImage = uint function(Image*);
        alias mGammaImage = uint function(Image*, const scope char*);
        alias mLevelImage = uint function(Image*, const scope char*);
        alias mLevelImageChannel = uint function(Image*, const scope ChannelType, const scope double, const scope double, const scope double);
        alias mModulateImage = uint function(Image*, const scope char*);
        alias mNegateImage = uint function(Image*, const scope uint);
        alias mNormalizeImage = uint function(Image*);
    }

    __gshared
    {
        mContrastImage ContrastImage;
        mEqualizeImage EqualizeImage;
        mGammaImage GammaImage;
        mLevelImage LevelImage;
        mLevelImageChannel LevelImageChannel;
        mModulateImage ModulateImage;
        mNegateImage NegateImage;
        mNormalizeImage NormalizeImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadEnhanceH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            ContrastImage = cast(mContrastImage)dlsym(dlib, "ContrastImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            EqualizeImage = cast(mEqualizeImage)dlsym(dlib, "EqualizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            GammaImage = cast(mGammaImage)dlsym(dlib, "GammaImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            LevelImage = cast(mLevelImage)dlsym(dlib, "LevelImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            LevelImageChannel = cast(mLevelImageChannel)dlsym(dlib, "LevelImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            ModulateImage = cast(mModulateImage)dlsym(dlib, "ModulateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            NegateImage = cast(mNegateImage)dlsym(dlib, "NegateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }
            NormalizeImage = cast(mNormalizeImage)dlsym(dlib, "NormalizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.enhance:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
