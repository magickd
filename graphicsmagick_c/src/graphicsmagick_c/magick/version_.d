/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick version and copyright.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.version_;

import core.stdc.config : c_ulong;

version (Q8)
{
    enum MagickQuantumDepth = "Q8";
}
else version (Q16)
{
    enum MagickQuantumDepth = "Q16";
}
else version (Q32)
{
    enum MagickQuantumDepth = "Q32";
}

/*
  Define declarations.

  MagickLibVersion and MagickLibVersionNumber are defined differently
  than they are in ImageMagick. The three fields are based on library
  interface versioning.  Each field in MagickLibVersion is one byte.
  The most significant field (third byte from the right) defines the
  library major interface, which is incremented whenever the library
  ABI changes incompatibly with preceding versions. The second field
  identifies an interface in a series of upward-compatible interfaces
  with the same major interface (such as when only new functions have)
  been added. The least significant field specifies the revision across
  100% compatible interfaces.

  MagickLibVersionText provides a simple human-readable string for
  identifying the release.
*/
enum MagickPackageName = "GraphicsMagick";

enum MagickCopyright = "Copyright (C) 2002-2008 GraphicsMagick Group.\n" ~
    "Additional copyrights and licenses apply to this software.\n" ~
    "See http://www.GraphicsMagick.org/www/Copyright.html for details.";
enum MagickLibVersion = 0x030000;
enum MagickLibVersionText = "1.3";
enum int[3] MagickLibVersionNumber = [3, 0, 0];
enum MagickChangeDate = "20081109";
enum MagickReleaseDate = "2008-11-09";

enum MagickVersion = MagickPackageName ~ " " ~ MagickLibVersionText ~ " " ~
    MagickReleaseDate ~ " " ~ MagickQuantumDepth ~ " " ~ MagickWebSite;
enum MagickWebSite = "http://www." ~ MagickPackageName ~ ".org/";

/*
  Method declarations.
*/
version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    const(char)* GetMagickCopyright();
    const(char)* GetMagickVersion(c_ulong*);
    const(char)* GetMagickWebSite();
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetMagickCopyright = const(char)* function();
        alias mGetMagickVersion = const(char*) function(c_ulong*);
        alias mGetMagickWebSite = const(char*) function();
    }

    __gshared
    {
        mGetMagickCopyright GetMagickCopyright;
        mGetMagickVersion GetMagickVersion;
        mGetMagickWebSite GetMagickWebSite;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadVersionH (void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            GetMagickVersion = cast(mGetMagickVersion)dlsym (dlib, "GetMagickVersion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.version:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetMagickCopyright = cast(mGetMagickCopyright)dlsym (dlib, "GetMagickCopyright");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.version:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetMagickWebSite = cast(mGetMagickWebSite)dlsym (dlib, "GetMagickWebSite");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.version:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
