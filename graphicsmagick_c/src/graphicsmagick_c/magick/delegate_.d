/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Methods to Read/Write/Invoke Delegates.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.delegate_;

import core.stdc.config : c_ulong;
import core.stdc.stdio : FILE;

import graphicsmagick_c.magick.api : MagickBool;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image, ImageInfo;

/*
  Delegate structure definitions.
*/
struct _DelegateInfo
{
    char* path;   /** Path to delegate configuration file */
    char* decode; /** Decode from format */
    char* encode; /** Transcode to format */

    char* commands; /** Commands to execute */

    int mode; /** <0 = encoder, >0 = decoder */

    MagickBool stealth; /** Don't list this delegate */

    c_ulong signature;

    _DelegateInfo* previous;
    _DelegateInfo* next;
}
alias DelegateInfo = _DelegateInfo;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char* GetDelegateCommand(const scope ImageInfo* image_info, Image* image,
                      const scope char* decode, const scope char* encode,
                      ExceptionInfo* exception);

    const(DelegateInfo)* GetDelegateInfo(const scope char* decode, const scope char* encode,
                   ExceptionInfo* exception);
    const(DelegateInfo)* GetPostscriptDelegateInfo(const scope ImageInfo* image_info,
                   uint* antialias, ExceptionInfo* exception);

    DelegateInfo* SetDelegateInfo(DelegateInfo*);

    uint InvokePostscriptDelegate(const scope uint verbose, const scope char* command);
    uint InvokeDelegate(ImageInfo* image_info, Image* image, const scope char* decode,
                 const scope char* encode, ExceptionInfo* exception);
    uint ListDelegateInfo(FILE* file, ExceptionInfo* exception);

    void DestroyDelegateInfo();
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mGetDelegateCommand = char* function(const scope ImageInfo* image_info, Image* image, const scope char* decode,
            const scope char* encode, ExceptionInfo* exception);

        alias mGetDelegateInfo = const(DelegateInfo)* function(const scope char* decode, const scope char* encode,
            ExceptionInfo* exception);
        alias mGetPostscriptDelegateInfo = const(DelegateInfo)* function(const scope ImageInfo* image_info, uint* antialias,
            ExceptionInfo* exception);

        alias mSetDelegateInfo = DelegateInfo* function(DelegateInfo*);

        alias mInvokePostscriptDelegate = uint function(const scope uint verbose, const scope char* command);
        alias mInvokeDelegate = uint function(ImageInfo* image_info, Image* image, const scope char* decode, const scope char* encode,
            ExceptionInfo* exception);
        alias mListDelegateInfo = uint function(FILE* file, ExceptionInfo* exception);

        alias mDestroyDelegateInfo = void function();
    }

    __gshared
    {
        mGetDelegateCommand GetDelegateCommand;

        mGetDelegateInfo GetDelegateInfo;
        mGetPostscriptDelegateInfo GetPostscriptDelegateInfo;

        mSetDelegateInfo SetDelegateInfo;

        mInvokePostscriptDelegate InvokePostscriptDelegate;
        mInvokeDelegate InvokeDelegate;
        mListDelegateInfo ListDelegateInfo;

        mDestroyDelegateInfo DestroyDelegateInfo;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadDelegateH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetDelegateCommand = cast(mGetDelegateCommand)dlsym(dlib, "GetDelegateCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetDelegateInfo = cast(mGetDelegateInfo)dlsym(dlib, "GetDelegateInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetPostscriptDelegateInfo = cast(mGetPostscriptDelegateInfo)dlsym(dlib, "GetPostscriptDelegateInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetDelegateInfo = cast(mSetDelegateInfo)dlsym(dlib, "SetDelegateInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }

            InvokePostscriptDelegate = cast(mInvokePostscriptDelegate)dlsym(dlib, "InvokePostscriptDelegate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }
            InvokeDelegate = cast(mInvokeDelegate)dlsym(dlib, "InvokeDelegate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListDelegateInfo = cast(mListDelegateInfo)dlsym(dlib, "ListDelegateInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyDelegateInfo = cast(mDestroyDelegateInfo)dlsym(dlib, "DestroyDelegateInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.delegate:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
