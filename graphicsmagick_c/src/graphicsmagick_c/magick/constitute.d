/*
  Copyright (C) 2003, 2004, 2005, 2007, 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Constitute Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.constitute;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickBool, MagickPassFail;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.forward;
import graphicsmagick_c.magick.image;

/*
 * Quantum import/export types as used by ImportImagePixelArea() and
 * ExportImagePixelArea(). Values are imported or exported in network
 * byte order ("big endian") by default, but little endian may be
 * selected via the 'endian' option in ExportPixelAreaOptions and
 * ImportPixelAreaOptions.
 */
alias QuantumType = int;
enum : QuantumType
{
    /// Not specified
    UndefinedQuantum,
    /// Colormap indexes
    IndexQuantum,
    /// Grayscale values (minimum value is black)
    GrayQuantum,
    /// Colormap indexes with transparency
    IndexAlphaQuantum,
    /// Grayscale values with transparency
    GrayAlphaQuantum,
    /// Red values only (RGBA)
    RedQuantum,
    /// Cyan values only (CYMKA)
    CyanQuantum,
    /// Green values only (RGBA)
    GreenQuantum,
    /// Yellow values only (CYMKA)
    YellowQuantum,
    /// Blue values only (RGBA)
    BlueQuantum,
    /// Magenta values only (CYMKA)
    MagentaQuantum,
    /// Transparency values (RGBA or CYMKA)
    AlphaQuantum,
    /// Black values only (CYMKA)
    BlackQuantum,
    /// Red, Green, and Blue values
    RGBQuantum,
    /// Red, Green, Blue, and transparency values
    RGBAQuantum,
    /// Cyan, magenta, yellow, and black values
    CMYKQuantum,
    /// Cyan, magenta, yellow, black, and transparency values
    CMYKAQuantum,
    /// CIE Y values, based on CCIR-709 primaries
    CIEYQuantum,
    /// CIE XYZ values, based on CCIR-709 primaries
    CIEXYZQuantum
}

/* Quantum sample type for when exporting/importing a pixel area. */
alias QuantumSampleType = int;
enum : QuantumSampleType
{
    /// Not specified
    UndefinedQuantumSampleType,
    /// Unsigned integral type (1 to 32 bits)
    UnsignedQuantumSampleType,
    /// Floating point type (16, 24, 32, or 64 bits)
    FloatQuantumSampleType
}

/* Quantum size types as used by ConstituteImage() and DispatchImage() */
alias StorageType = int;
enum : StorageType
{
    /// Unsigned 8 bit 'unsigned char'
    CharPixel,
    /// Unsigned 16 bit 'unsigned short int'
    ShortPixel,
    /// Unsigned 32 bit 'unsigned int'
    IntegerPixel,
    /// Unsigned 32 or 64 bit (CPU dependent) 'unsigned long'
    LongPixel,
    /// Floating point 32-bit 'float'
    FloatPixel,
    /// Floating point 64-bit 'double'
    DoublePixel
}

/* Additional options for ExportImagePixelArea() */
struct _ExportPixelAreaOptions
{
    /// Quantum sample type
    QuantumSampleType sample_type;

    /// Minimum value (default 0.0) for linear floating point samples
    double double_minvalue;
    /// Maximum value (default 1.0) for linear floating point samples
    double double_maxvalue;

    /// Grayscale minimum value is white rather than black
    MagickBool grayscale_miniswhite;

    /// Number of pad bytes to output after pixel data
    c_ulong pad_bytes;

    /// Value to use when padding end of pixel data
    ubyte pad_value;

    /// Endian orientation for 16/32/64 bit types (default MSBEndian)
    EndianType endian;

    c_ulong signature;
}
alias ExportPixelAreaOptions = _ExportPixelAreaOptions;

/* Optional results info for ExportImagePixelArea() */
struct _ExportPixelAreaInfo
{
    /// Number of bytes which where exported.
    size_t bytes_exported;
}
alias ExportPixelAreaInfo = _ExportPixelAreaInfo;

/* Additional options for ImportImagePixelArea() */
struct _ImportPixelAreaOptions
{
    /// Quantum sample type
    QuantumSampleType sample_type;

    /// Minimum value (default 0.0) for linear floating point samples
    double double_minvalue;
    /// Maximum value (default 1.0) for linear floating point samples
    double double_maxvalue;

    /// Grayscale minimum is white rather than black
    MagickBool grayscale_miniswhite;

    /// Endian orientation for 16/32/64 bit types (default MSBEndian)
    EndianType endiant;

    c_ulong signature;
}
alias ImportPixelAreaOptions = _ImportPixelAreaOptions;

/* Optional results info for ImportImagePixelArea() */
struct _ImportPixelAreaInfo
{
    /// Number of bytes which were imported.
    size_t bytes_imported;
}
alias ImportPixelAreaInfo = _ImportPixelAreaInfo;


version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    const(char)* StorageTypeToString(const scope StorageType storage_type);
    const(char)* QuantumSampleTypeToString(const scope QuantumSampleType sample_type);
    const(char)* QuantumTypeToString(const scope QuantumType quantum_type);

    Image* ConstituteImage(const scope c_ulong width, const scope c_ulong height, const scope char* map,
        const scope StorageType type, const scope void* pixels, ExceptionInfo* exception);
    Image* ConstituteTextureImage(const scope c_ulong columns, const scope c_ulong rows,
        const scope Image* texture, ExceptionInfo* exception);
    Image* PingImage(const scope ImageInfo* image_info, ExceptionInfo* exception);
    Image* ReadImage(const scope ImageInfo* image_info, ExceptionInfo* exception);
    Image* ReadInlineImage(const scope ImageInfo* image_info, const scope char* content,
        ExceptionInfo* exception);

    MagickPassFail DispatchImage(const scope Image* image, const scope c_long x_offset, const scope c_long y_offset,
        const scope c_ulong columns, const scope c_ulong rows, const scope char* map,
        const scope StorageType type, void* pixels, ExceptionInfo* exception);
    MagickPassFail ExportImagePixelArea(const scope Image* image, const scope QuantumType quantum_type,
        const scope uint quantum_size, ubyte* destination,
        const scope ExportPixelAreaOptions* options, ExportPixelAreaInfo* export_info);
    MagickPassFail ExportViewPixelArea(const scope ViewInfo* view, const scope QuantumType quantum_type,
        const scope uint quantum_size, ubyte* destination,
        const scope ExportPixelAreaOptions* options, ExportPixelAreaInfo* export_info);

    MagickPassFail ImportImagePixelArea(Image* image, const scope QuantumType quantum_type,
        const scope uint quantum_size, const scope ubyte* source,
        const scope ImportPixelAreaOptions* options, ImportPixelAreaInfo* import_info);
    MagickPassFail ImportViewPixelArea(ViewInfo* view, const scope QuantumType quantum_type,
        const scope uint quantum_size, const scope ubyte* source,
        const scope ImportPixelAreaOptions* options, ImportPixelAreaInfo* import_info);
    MagickPassFail WriteImage(const scope ImageInfo* image_info, Image* image);
    MagickPassFail WriteImages(const scope ImageInfo* image_info, Image* image, const scope char* filename,
        ExceptionInfo* exception);

    void DestroyConstitute();
    void ExportPixelAreaOptionsInit(ExportPixelAreaOptions* options);
    void ImportPixelAreaOptionsInit(ImportPixelAreaOptions* options);

    MagickPassFail MagickFindRawImageMinMax(Image* image, EndianType endian,
        c_ulong width, c_ulong height, StorageType type,
        uint scanline_octets, void* scanline_buffer,
        double* min, double* max);

} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern(C)
    {
        alias mStorageTypeToString = const(char)* function(const scope StorageType storage_type);
        alias mQuantumSampleTypeToString = const(char)* function(const scope QuantumSampleType sample_type);
        alias mQuantumTypeToString = const(char)* function(const scope QuantumType quantum_type);

        alias mConstituteImage = Image* function(const scope c_ulong width, const scope c_ulong height, const scope char* map,
            const scope StorageType type, const scope void* pixels, ExceptionInfo* exception);
        alias mConstituteTextureImage = Image* function(const scope c_ulong columns, const scope c_ulong rows,
            const scope Image* texture, ExceptionInfo* exception);
        alias mPingImage = Image* function(const scope ImageInfo* image_info, ExceptionInfo* exception);
        alias mReadImage = Image* function(const scope ImageInfo* image_info, ExceptionInfo* exception);
        alias mReadInlineImage = Image* function(const scope ImageInfo* image_info, const scope char* content,
            ExceptionInfo* exception);

        alias mDispatchImage = MagickPassFail function(const scope Image* image, const scope c_long x_offset,
            const scope c_long y_offset, const scope c_ulong columns, const scope c_ulong rows, const scope char* map,
            const scope StorageType type, void* pixels, ExceptionInfo* exception);
        alias mExportImagePixelArea = MagickPassFail function(const scope Image* image, const scope QuantumType quantum_type,
            const scope uint quantum_size, ubyte* destination,
            const scope ExportPixelAreaOptions* options, ExportPixelAreaInfo* export_info);
        alias mExportViewPixelArea = MagickPassFail function(const scope ViewInfo* view, const scope QuantumType quantum_type,
            const scope uint quantum_size, ubyte* destination,
            const scope ExportPixelAreaOptions* options, ExportPixelAreaInfo* export_info);
        alias mImportImagePixelArea = MagickPassFail function(Image* image, const scope QuantumType quantum_type,
            const scope uint quantum_size, const scope ubyte* source,
            const scope ImportPixelAreaOptions* options, ImportPixelAreaInfo* import_info);
        alias mImportViewPixelArea = MagickPassFail function(ViewInfo* view, const scope QuantumType quantum_type,
            const scope uint quantum_size, const scope ubyte* source,
            const scope ImportPixelAreaOptions* options, ImportPixelAreaInfo* import_info);
        alias mWriteImage = MagickPassFail function(const scope ImageInfo* image_info, Image* image);
        alias mWriteImages = MagickPassFail function(const scope ImageInfo* image_info, Image* image,
            const scope char* filename, ExceptionInfo* exception);

        alias mDestroyConstitute = void function();
        alias mExportPixelAreaOptionsInit = void function(ExportPixelAreaOptions* options);
        alias mImportPixelAreaOptionsInit = void function(ImportPixelAreaOptions* options);

        alias mMagickFindRawImageMinMax = MagickPassFail function(Image* image, EndianType endian,
            c_ulong width, c_ulong height, StorageType type,
            uint scanline_octets, void* scanline_buffer,
            double* min, double* max);
    }

    __gshared
    {
        mStorageTypeToString StorageTypeToString;
        mQuantumSampleTypeToString QuantumSampleTypeToString;
        mQuantumTypeToString QuantumTypeToString;

        mConstituteImage ConstituteImage;
        mConstituteTextureImage ConstituteTextureImage;
        mPingImage PingImage;
        mReadImage ReadImage;
        mReadInlineImage ReadInlineImage;

        mDispatchImage DispatchImage;
        mExportImagePixelArea ExportImagePixelArea;
        mExportViewPixelArea ExportViewPixelArea;
        mImportImagePixelArea ImportImagePixelArea;
        mImportViewPixelArea ImportViewPixelArea;
        mWriteImage WriteImage;
        mWriteImages WriteImages;

        mDestroyConstitute DestroyConstitute;
        mExportPixelAreaOptionsInit ExportPixelAreaOptionsInit;
        mImportPixelAreaOptionsInit ImportPixelAreaOptionsInit;

        mMagickFindRawImageMinMax MagickFindRawImageMinMax;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadConstituteH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            StorageTypeToString = cast(mStorageTypeToString)dlsym(dlib, "StorageTypeToString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantumSampleTypeToString = cast(mQuantumSampleTypeToString)dlsym(dlib, "QuantumSampleTypeToString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantumTypeToString = cast(mQuantumTypeToString)dlsym(dlib, "QuantumTypeToString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }

            ConstituteImage = cast(mConstituteImage)dlsym(dlib, "ConstituteImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ConstituteTextureImage = cast(mConstituteTextureImage)dlsym(dlib, "ConstituteTextureImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            PingImage = cast(mPingImage)dlsym(dlib, "PingImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadImage = cast(mReadImage)dlsym(dlib, "ReadImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadInlineImage = cast(mReadInlineImage)dlsym(dlib, "ReadInlineImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }

            DispatchImage = cast(mDispatchImage)dlsym(dlib, "DispatchImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExportImagePixelArea = cast(mExportImagePixelArea)dlsym(dlib, "ExportImagePixelArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExportViewPixelArea = cast(mExportViewPixelArea)dlsym(dlib, "ExportViewPixelArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportImagePixelArea = cast(mImportImagePixelArea)dlsym(dlib, "ImportImagePixelArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportViewPixelArea = cast(mImportViewPixelArea)dlsym(dlib, "ImportViewPixelArea");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteImage = cast(mWriteImage)dlsym(dlib, "WriteImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteImages = cast(mWriteImages)dlsym(dlib, "WriteImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyConstitute = cast(mDestroyConstitute)dlsym(dlib, "DestroyConstitute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExportPixelAreaOptionsInit = cast(mExportPixelAreaOptionsInit)dlsym(dlib, "ExportPixelAreaOptionsInit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportPixelAreaOptionsInit = cast(mImportPixelAreaOptionsInit)dlsym(dlib, "ImportPixelAreaOptionsInit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickFindRawImageMinMax = cast(mMagickFindRawImageMinMax)dlsym(dlib, "MagickFindRawImageMinMax");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.constitute:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
