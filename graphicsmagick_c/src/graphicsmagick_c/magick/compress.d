/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Image Compression/Decompression Methods.
*/
// Complete graphicsmagick_c wrapper for versio 1.3
module graphicsmagick_c.magick.compress;

import graphicsmagick_c.magick.magick_types;
import graphicsmagick_c.magick.image;

import core.stdc.config : c_long;

/*
    Typedef declarations
*/
struct _Ascii85Info
{
    c_long offset;
    c_long line_break;

    magick_uint8_t[10] buffer;
}
alias Ascii85Info = _Ascii85Info;

/*
    TODO: Clean up the interface between BLOB write functions,
    compression functions, and encoding functions so they
    may be hooked into/stacked on top of each other. Most are
    (or can be changed to be) stream based.
*/
alias WriteByteHook = uint function(Image*, const scope magick_uint8_t, void* info);


version (GMagick_Static)
{
    @system @nogc nothrow extern (C):
    /*
        Commonly used byte writer hooks.
    */
    uint Ascii85WriteByteHook(Image* image, const scope magick_uint8_t code, void* info);
    uint BlobWriteByteHook(Image* image, const scope magick_uint8_t code, void* info);

    /*
        Compress methods
    */
    uint HuffmanDecodeImage(Image* image);
    uint HuffmanEncodeImage(const scope ImageInfo* image_info, Image* image);
    uint HuffmanEncode2Image(const scope ImageInfo* image_info, Image* image, WriteByteHook write_byte, void* info);
    uint LZWEncodeImage(Image* image, const scope size_t length, magick_uint8_t* pixels);
    uint LZWEncode2Image(Image* image, const scope size_t length, magick_uint8_t* pixels, WriteByteHook write_byte, void* info);
    uint PackbitsEncodeImage(Image* image, const scope size_t length, magick_uint8_t* pixels);
    uint PackbitsEncode2Image(Image* image, const scope size_t length, magick_uint8_t *pixels, WriteByteHook write_byte,
        void* info);

    void Ascii85Encode(Image* image, const scope magick_uint8_t code);
    void Ascii85Flush(Image* image);
    void Ascii85Initialize(Image* image);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mAscii85WriteByteHook = uint function(Image*, const scope magick_uint8_t, void*);
        alias mBlobWriteByteHook = uint function(Image*, const scope magick_uint8_t, void*);

        alias mHuffmanDecodeImage = uint function(Image*);
        alias mHuffmanEncodeImage = uint function(const scope ImageInfo*, Image*);
        alias mHuffmanEncode2Image = uint function(const scope ImageInfo*, Image*, WriteByteHook, void*);
        alias mLZWEncodeImage = uint function(Image*, const scope size_t, magick_uint8_t*);
        alias mLZWEncode2Image = uint function(Image*, const scope size_t, magick_uint8_t*, WriteByteHook, void*);
        alias mPackbitsEncodeImage = uint function(Image*, const scope size_t, magick_uint8_t*);
        alias mPackbitsEncode2Image = uint function(Image*, const scope size_t, magick_uint8_t*, WriteByteHook, void*);

        alias mAscii85Encode = void function(Image*, const scope magick_uint8_t);
        alias mAscii85Flush = void function(Image*);
        alias mAscii85Initialize = void function(Image*);
    }

    __gshared
    {
        mAscii85WriteByteHook Ascii85WriteByteHook;
        mBlobWriteByteHook BlobWriteByteHook;

        mHuffmanDecodeImage HuffmanDecodeImage;
        mHuffmanEncodeImage HuffmanEncodeImage;
        mHuffmanEncode2Image HuffmanEncode2Image;
        mLZWEncodeImage LZWEncodeImage;
        mLZWEncode2Image LZWEncode2Image;
        mPackbitsEncodeImage PackbitsEncodeImage;
        mPackbitsEncode2Image PackbitsEncode2Image;

        mAscii85Encode Ascii85Encode;
        mAscii85Flush Ascii85Flush;
        mAscii85Initialize Ascii85Initialize;
    }

    package (graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadCompressH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            Ascii85WriteByteHook = cast(mAscii85WriteByteHook)dlsym(dlib, "Ascii85WriteByteHook");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlobWriteByteHook = cast(mBlobWriteByteHook)dlsym(dlib, "BlobWriteByteHook");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }

            HuffmanDecodeImage = cast(mHuffmanDecodeImage)dlsym(dlib, "HuffmanDecodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            HuffmanEncodeImage = cast(mHuffmanEncodeImage)dlsym(dlib, "HuffmanEncodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            HuffmanEncode2Image = cast(mHuffmanEncode2Image)dlsym(dlib, "HuffmanEncode2Image");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            LZWEncodeImage = cast(mLZWEncodeImage)dlsym(dlib, "LZWEncodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            LZWEncode2Image = cast(mLZWEncode2Image)dlsym(dlib, "LZWEncode2Image");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            PackbitsEncodeImage = cast(mPackbitsEncodeImage)dlsym(dlib, "PackbitsEncodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            PackbitsEncode2Image = cast(mPackbitsEncode2Image)dlsym(dlib, "PackbitsEncode2Image");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }

            Ascii85Encode = cast(mAscii85Encode)dlsym(dlib, "Ascii85Encode");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            Ascii85Flush = cast(mAscii85Flush)dlsym(dlib, "Ascii85Flush");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }
            Ascii85Initialize = cast(mAscii85Initialize)dlsym(dlib, "Ascii85Initialize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compress:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
