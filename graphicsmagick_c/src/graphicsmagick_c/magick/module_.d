/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Modules Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.module_;

import core.stdc.config : c_ulong;
import core.stdc.stdio : FILE;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image;

/*
  Module alias list entry
  Maintains modules.mgk path, and the module name corresponding
  to each magick tag.
  Used to support module_list, which is intialized by reading modules.mgk,
*/
struct _ModuleInfo
{
    char* path;
    char* magick;
    char* name;

    uint stealth;

    c_ulong signature;

    _ModuleInfo* previous;
    _ModuleInfo* next;
}
//graphicsmagick_c: Avoid name collision with D's ModuleInfo struct.
alias GModuleInfo = _ModuleInfo;

enum : MagickModuleType
{
    MagickCoderModule,
    MagickFilterModule
}
alias MagickModuleType = int;

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    const(GModuleInfo)* GetModuleInfo(const scope char*, ExceptionInfo*);

    uint ExecuteModuleProcess(const scope char*, Image**, const scope int, char**);
    uint ExecuteStaticModuleProcess(const scope char*, Image**, const scope int, char**);
    uint ListModuleInfo(FILE*, ExceptionInfo*);
    uint OpenModule(const scope char*, ExceptionInfo*);
    uint OpenModules(ExceptionInfo*);

    void DestroyModuleInfo();
    void DestroyMagickModules();
    void InitializeMagickModules();
    void RegisterStaticModules();
    void UnregisterStaticModules();
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mGetModuleInfo = const(GModuleInfo)* function(const scope char*, ExceptionInfo*);

        alias mExecuteModuleProcess = uint function(const scope char*, Image**, const scope int, char**);
        alias mExecuteStaticModuleProcess = uint function(const scope char*, Image**, const scope int, char**);
        alias mListModuleInfo = uint function(FILE*, ExceptionInfo*);
        alias mOpenModule = uint function(const scope char*, ExceptionInfo*);
        alias mOpenModules = uint function(ExceptionInfo*);

        alias mDestroyModuleInfo = void function();
        alias mDestroyMagickModules = void function();
        alias mInitializeMagickModules = void function();
        alias mRegisterStaticModules = void function();
        alias mUnregisterStaticModules = void function();
    }

    __gshared
    {
        mGetModuleInfo GetModuleInfo;

        mExecuteModuleProcess ExecuteModuleProcess;
        mExecuteStaticModuleProcess ExecuteStaticModuleProcess;
        mListModuleInfo ListModuleInfo;
        mOpenModule OpenModule;
        mOpenModules OpenModules;

        mDestroyModuleInfo DestroyModuleInfo;
        mDestroyMagickModules DestroyMagickModules;
        mInitializeMagickModules InitializeMagickModules;
        mRegisterStaticModules RegisterStaticModules;
        mUnregisterStaticModules UnregisterStaticModules;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadModuleH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetModuleInfo = cast(mGetModuleInfo)dlsym(dlib, "GetModuleInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }

            ExecuteModuleProcess = cast(mExecuteModuleProcess)dlsym(dlib, "ExecuteModuleProcess");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            ExecuteStaticModuleProcess = cast(mExecuteStaticModuleProcess)dlsym(dlib, "ExecuteStaticModuleProcess");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListModuleInfo = cast(mListModuleInfo)dlsym(dlib, "ListModuleInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            OpenModule = cast(mOpenModule)dlsym(dlib, "OpenModule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            OpenModules = cast(mOpenModules)dlsym(dlib, "OpenModules");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyModuleInfo = cast(mDestroyModuleInfo)dlsym(dlib, "DestroyModuleInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            DestroyMagickModules = cast(mDestroyMagickModules)dlsym(dlib, "DestroyMagickModules");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            InitializeMagickModules = cast(mInitializeMagickModules)dlsym(dlib, "InitializeMagickModules");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            RegisterStaticModules = cast(mRegisterStaticModules)dlsym(dlib, "RegisterStaticModules");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }
            UnregisterStaticModules = cast(mUnregisterStaticModules)dlsym(dlib, "UnregisterStaticModules");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.module:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
