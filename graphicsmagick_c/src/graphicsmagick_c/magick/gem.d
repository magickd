/*
  Copyright (C) 2003, 2008 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Graphic Gems - Graphic Support Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.gem;

import core.stdc.config;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.forward;
import graphicsmagick_c.magick.image;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    double ExpandAffine(const scope AffineMatrix*);
    double GenerateDifferentialNoise(const scope Quantum pixel, const scope NoiseType noise_type,
        uint* seed);

    int GetOptimalKernelWidth(const scope double, const scope double);
    int GetOptimalKernelWidth1D(const scope double, const scope double);
    int GetOptimalKernelWidth2D(const scope double, const scope double);

    PixelPacket InterpolateColor(const scope Image* image, const scope double x_offset,
        const scope double y_offset, ExceptionInfo* exception);

    Quantum GenerateNoise(const scope Quantum, const scope NoiseType);

    void Contrast(const scope int, Quantum*, Quantum*, Quantum*);
    void HSLTransform(const scope double, const scope double, const scope double, Quantum*, Quantum*,
        Quantum*);
    void HWBTransform(const scope double, const scope double, const scope double, Quantum*, Quantum*,
        Quantum*);
    void Hull(const scope c_long, const scope c_long, const scope c_ulong, const scope c_ulong, Quantum*,
        Quantum*, const scope int);
    void IdentityAffine(AffineMatrix*);
    void InterpolateViewColor(const scope ViewInfo* view, PixelPacket* color,
        const scope double x_offset, const scope double y_offset, ExceptionInfo* exception);
    void Modulate(const scope double, const scope double, const scope double, Quantum*, Quantum*,
        Quantum*);
    void TransformHSL(const scope Quantum, const scope Quantum, const scope Quantum, double*, double*,
        double*);
    void TransformHWB(const scope Quantum, const scope Quantum, const scope Quantum, double*, double*,
        double*);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mExpandAffine = double function(const scope AffineMatrix*);
        alias mGenerateDifferentialNoise = double function(const scope Quantum, const scope NoiseType, uint*);

        alias mGetOptimalKernelWidth = int function(const scope double, const scope double);
        alias mGetOptimalKernelWidth1D = int function(const scope double, const scope double);
        alias mGetOptimalKernelWidth2D = int function(const scope double, const scope double);

        alias mInterpolateColor = PixelPacket function(const scope Image*, const scope double, const scope double,
            ExceptionInfo*);

        alias mGenerateNoise = Quantum function(const scope Quantum, const scope NoiseType);

        alias mContrast = void function(const scope int, Quantum*, Quantum*, Quantum*);
        alias mHSLTransform = void function(const scope double, const scope double, const scope double, Quantum*, Quantum*,
            Quantum*);
        alias mHWBTransform = void function(const scope double, const scope double, const scope double, Quantum*, Quantum*,
            Quantum*);
        alias mHull = void function(const scope c_long, const scope c_long, const scope c_ulong, const scope c_ulong, Quantum*,
            Quantum*, const scope int);
        alias mIdentityAffine = void function(AffineMatrix*);
        alias mInterpolateViewColor = void function(const scope ViewInfo*, PixelPacket*, const scope double,
            const scope double, ExceptionInfo*);
        alias mModulate = void function(const scope double, const scope double, const scope double, Quantum*, Quantum*,
            Quantum*);
        alias mTransformHSL = void function(const scope Quantum, const scope Quantum, const scope Quantum, double*, double*,
            double*);
        alias mTransformHWB = void function(const scope Quantum, const scope Quantum, const scope Quantum, double*, double*,
            double*);
    }

    __gshared
    {
        mExpandAffine ExpandAffine;
        mGenerateDifferentialNoise GenerateDifferentialNoise;

        mGetOptimalKernelWidth GetOptimalKernelWidth;
        mGetOptimalKernelWidth1D GetOptimalKernelWidth1D;
        mGetOptimalKernelWidth2D GetOptimalKernelWidth2D;

        mInterpolateColor InterpolateColor;

        mGenerateNoise GenerateNoise;

        mContrast Contrast;
        mHSLTransform HSLTransform;
        mHWBTransform HWBTransform;
        mHull Hull;
        mIdentityAffine IdentityAffine;
        mInterpolateViewColor InterpolateViewColor;
        mModulate Modulate;
        mTransformHSL TransformHSL;
        mTransformHWB TransformHWB;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlsym, dlerror;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadGemH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            ExpandAffine = cast(mExpandAffine)dlsym(dlib, "ExpandAffine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            GenerateDifferentialNoise = cast(mGenerateDifferentialNoise)dlsym(dlib, "GenerateDifferentialNoise");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetOptimalKernelWidth = cast(mGetOptimalKernelWidth)dlsym(dlib, "GetOptimalKernelWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetOptimalKernelWidth1D = cast(mGetOptimalKernelWidth1D)dlsym(dlib, "GetOptimalKernelWidth1D");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetOptimalKernelWidth2D = cast(mGetOptimalKernelWidth2D)dlsym(dlib, "GetOptimalKernelWidth2D");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }

            InterpolateColor = cast(mInterpolateColor)dlsym(dlib, "InterpolateColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }

            GenerateNoise = cast(mGenerateNoise)dlsym(dlib, "GenerateNoise");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }

            Contrast = cast(mContrast)dlsym(dlib, "Contrast");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            HSLTransform = cast(mHSLTransform)dlsym(dlib, "HSLTransform");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            HWBTransform = cast(mHWBTransform)dlsym(dlib, "HWBTransform");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            Hull = cast(mHull)dlsym(dlib, "Hull");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            IdentityAffine = cast(mIdentityAffine)dlsym(dlib, "IdentityAffine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            InterpolateViewColor = cast(mInterpolateViewColor)dlsym(dlib, "InterpolateViewColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            Modulate = cast(mModulate)dlsym(dlib, "Modulate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            TransformHSL = cast(mTransformHSL)dlsym(dlib, "TransformHSL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }
            TransformHWB = cast(mTransformHWB)dlsym(dlib, "TransformHWB");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.gem:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
