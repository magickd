/*
  Copyright (C) 2003, 2004 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Exception Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.error;

import core.stdc.config;

/*
    Enum declarations
*/
alias ExceptionBaseType = int;
enum : ExceptionBaseType
{
    UndefinedExceptionBase = 0,
    ExceptionBase = 1,
    ResourceBase = 2,
    ResourceLimitBase = 2,
    TypeBase = 5,
    AnnotateBase = 5,
    OptionBase = 10,
    DelegateBase = 15,
    MissingDelegateBase = 20,
    CorruptImageBase = 25,
    FileOpenBase = 30,
    BlobBase = 35,
    StreamBase = 40,
    CacheBase = 45,
    CoderBase = 50,
    ModuleBase = 55,
    DrawBase = 60,
    RenderBase = 60,
    ImageBase = 65,
    WandBase = 67,
    TemporaryFileBase = 70,
    TransformBase = 75,
    XServerBase = 80,
    X11Base = 81,
    UserBase = 82,
    MonitorBase = 85,
    LocaleBase = 86,
    DeprecateBase = 87,
    RegistryBase = 90,
    ConfigureBase = 95
}

alias ExceptionType = int;
enum : ExceptionType
{
    UndefinedException = 0,
    EventException = 100,
    ExceptionEvent = EventException + ExceptionBase,
    ResourceEvent = EventException + ResourceBase,
    ResourceLimitEvent = EventException + ResourceLimitBase,
    TypeEvent = EventException + TypeBase,
    AnnotateEvent = EventException + AnnotateBase,
    OptionEvent = EventException + OptionBase,
    DelegateEvent = EventException + DelegateBase,
    MissingDelegateEvent = EventException + MissingDelegateBase,
    CorruptImageEvent = EventException + CorruptImageBase,
    FileOpenEvent = EventException + FileOpenBase,
    BlobEvent = EventException + BlobBase,
    StreamEvent = EventException + StreamBase,
    CacheEvent = EventException + CacheBase,
    CoderEvent = EventException + CoderBase,
    ModuleEvent = EventException + ModuleBase,
    DrawEvent = EventException + DrawBase,
    RenderEvent = EventException + RenderBase,
    ImageEvent = EventException + ImageBase,
    WandEvent = EventException + WandBase,
    TemporaryFileEvent = EventException + TemporaryFileBase,
    TransformEvent = EventException + TransformBase,
    XServerEvent = EventException + XServerBase,
    X11Event = EventException + X11Base,
    UserEvent = EventException + UserBase,
    MonitorEvent = EventException + MonitorBase,
    LocaleEvent = EventException + LocaleBase,
    DeprecateEvent = EventException + DeprecateBase,
    RegistryEvent = EventException + RegistryBase,
    ConfigureEvent = EventException + ConfigureBase,

    WarningException = 300,
    ExceptionWarning = WarningException + ExceptionBase,
    ResourceWarning = WarningException + ResourceBase,
    ResourceLimitWarning = WarningException + ResourceLimitBase,
    TypeWarning = WarningException + TypeBase,
    AnnotateWarning = WarningException + AnnotateBase,
    OptionWarning = WarningException + OptionBase,
    DelegateWarning = WarningException + DelegateBase,
    MissingDelegateWarning = WarningException + MissingDelegateBase,
    CorruptImageWarning = WarningException + CorruptImageBase,
    FileOpenWarning = WarningException + FileOpenBase,
    BlobWarning = WarningException + BlobBase,
    StreamWarning = WarningException + StreamBase,
    CacheWarning = WarningException + CacheBase,
    CoderWarning = WarningException + CoderBase,
    ModuleWarning = WarningException + ModuleBase,
    DrawWarning = WarningException + DrawBase,
    RenderWarning = WarningException + RenderBase,
    ImageWarning = WarningException + ImageBase,
    WandWarning = WarningException + WandBase,
    TemporaryFileWarning = WarningException + TemporaryFileBase,
    TransformWarning = WarningException + TransformBase,
    XServerWarning = WarningException + XServerBase,
    X11Warning = WarningException + X11Base,
    UserWarning = WarningException + UserBase,
    MonitorWarning = WarningException + MonitorBase,
    LocaleWarning = WarningException + LocaleBase,
    DeprecateWarning = WarningException + DeprecateBase,
    RegistryWarning = WarningException + RegistryBase,
    ConfigureWarning = WarningException + ConfigureBase,

    ErrorException = 400,
    ExceptionError = ErrorException + ExceptionBase,
    ResourceError = ErrorException + ResourceBase,
    ResourceLimitError = ErrorException + ResourceLimitBase,
    TypeError = ErrorException + TypeBase,
    AnnotateError = ErrorException + AnnotateBase,
    OptionError = ErrorException + OptionBase,
    DelegateError = ErrorException + DelegateBase,
    MissingDelegateError = ErrorException + MissingDelegateBase,
    CorruptImageError = ErrorException + CorruptImageBase,
    FileOpenError = ErrorException + FileOpenBase,
    BlobError = ErrorException + BlobBase,
    StreamError = ErrorException + StreamBase,
    CacheError = ErrorException + CacheBase,
    CoderError = ErrorException + CoderBase,
    ModuleError = ErrorException + ModuleBase,
    DrawError = ErrorException + DrawBase,
    RenderError = ErrorException + RenderBase,
    ImageError = ErrorException + ImageBase,
    WandError = ErrorException + WandBase,
    TemporaryFileError = ErrorException + TemporaryFileBase,
    TransformError = ErrorException + TransformBase,
    XServerError = ErrorException + XServerBase,
    X11Error = ErrorException + X11Base,
    UserError = ErrorException + UserBase,
    MonitorError = ErrorException + MonitorBase,
    LocaleError = ErrorException + LocaleBase,
    DeprecateError = ErrorException + DeprecateBase,
    RegistryError = ErrorException + RegistryBase,
    ConfigureError = ErrorException + ConfigureBase,

    FatalErrorException = 700,
    ExceptionFatalError = FatalErrorException + ExceptionBase,
    ResourceFatalError = FatalErrorException + ResourceBase,
    ResourceLimitFatalError = FatalErrorException + ResourceLimitBase,
    TypeFatalError = FatalErrorException + TypeBase,
    AnnotateFatalError = FatalErrorException + AnnotateBase,
    OptionFatalError = FatalErrorException + OptionBase,
    DelegateFatalError = FatalErrorException + DelegateBase,
    MissingDelegateFatalError = FatalErrorException + MissingDelegateBase,
    CorruptImageFatalError = FatalErrorException + CorruptImageBase,
    FileOpenFatalError = FatalErrorException + FileOpenBase,
    BlobFatalError = FatalErrorException + BlobBase,
    StreamFatalError = FatalErrorException + StreamBase,
    CacheFatalError = FatalErrorException + CacheBase,
    CoderFatalError = FatalErrorException + CoderBase,
    ModuleFatalError = FatalErrorException + ModuleBase,
    DrawFatalError = FatalErrorException + DrawBase,
    RenderFatalError = FatalErrorException + RenderBase,
    ImageFatalError = FatalErrorException + ImageBase,
    WandFatalError = FatalErrorException + WandBase,
    TemporaryFileFatalError = FatalErrorException + TemporaryFileBase,
    TransformFatalError = FatalErrorException + TransformBase,
    XServerFatalError = FatalErrorException + XServerBase,
    X11FatalError = FatalErrorException + X11Base,
    UserFatalError = FatalErrorException + UserBase,
    MonitorFatalError = FatalErrorException + MonitorBase,
    LocaleFatalError = FatalErrorException + LocaleBase,
    DeprecateFatalError = FatalErrorException + DeprecateBase,
    RegistryFatalError = FatalErrorException + RegistryBase,
    ConfigureFatalError = FatalErrorException + ConfigureBase
}

/*
Typedef declarations.
*/

/*
ExceptionInfo is used to report exceptions to higher level routines,
and to the user.
*/
struct _ExceptionInfo
{
    /*
        Exception severity, reason, and description
    */
    ExceptionType severity;

    char* reason;
    char* description;

    /*
        Value of errno (or equivalent) when exception was thrown.
    */
    int error_number;

    /*
        Reporting source module, function (if available), and source
        module line.
    */
    pragma(mangle, "module") char* module_;
    pragma(mangle, "function") char* function_;

    c_ulong line;

    /*
        Structure sanity check
    */
    c_ulong signature;
}
alias ExceptionInfo = _ExceptionInfo;

/*
 * Exception typedef declarations.
 */
alias ErrorHandler = extern(C) nothrow void function(const scope ExceptionType, const scope char*, const scope char*);

alias FatalErrorHandler = extern(C) nothrow void function(const scope ExceptionType, const scope char*, const scope char*);

alias WarningHandler = extern(C) nothrow void function(const scope ExceptionType, const scope char*, const scope char*);


version(GMagick_Static)
{
    @system @nogc nothrow extern (C):

    /*
     * Exception declarations.
     */
    const(char)* GetLocaleExceptionMessage(const scope ExceptionType, const scope char*);
    const(char)* GetLocaleMessage(const scope char*);

    ErrorHandler SetErrorHandler(ErrorHandler);

    FatalErrorHandler SetFatalErrorHandler(FatalErrorHandler);

    void CatchException(const scope ExceptionInfo*);
    void CopyException(ExceptionInfo* copy, const scope ExceptionInfo* original);
    void DestroyExceptionInfo(ExceptionInfo*);
    void GetExceptionInfo(ExceptionInfo*);
    void MagickError(const scope ExceptionType, const scope char*, const scope char*);
    void MagickFatalError(const scope ExceptionType, const scope char*, const scope char*);
    void MagickWarning(const scope ExceptionType, const scope char*, const scope char*);
    void _MagickError(const scope ExceptionType, const scope char*, const scope char*);
    void _MagickFatalError(const scope ExceptionType, const scope char*, const scope char*);
    void _MagickWarning(const scope ExceptionType, const scope char*, const scope char*);
    void SetExceptionInfo(ExceptionInfo*, ExceptionType);
    void ThrowException(ExceptionInfo*, const scope ExceptionType, const scope char*, const scope char*);
    void ThrowLoggedException(ExceptionInfo* exception, const scope ExceptionType severity,
        const scope char* reason, const scope char* description, const scope char* module_,
        const scope char* function_, const scope c_ulong line);

    WarningHandler SetWarningHandler(WarningHandler);

} // end of version(GMagick_Static)
else // start of dynamic
{
    @system @nogc nothrow extern(C)
    {
        alias mGetLocaleExceptionMessage = const(char)* function(const scope ExceptionType, const scope char*);
        alias mGetLocaleMessage = const(char)* function(const scope char*);

        alias mSetErrorHandler = ErrorHandler function(ErrorHandler);

        alias mSetFatalErrorHandler = FatalErrorHandler function(FatalErrorHandler);

        alias mCatchException = void function(const scope ExceptionInfo*);
        alias mCopyException = void function(ExceptionInfo* copy, const scope ExceptionInfo* original);
        alias mDestroyExceptionInfo = void function(ExceptionInfo*);
        alias mGetExceptionInfo = void function(ExceptionInfo*);
        alias mMagickError = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias mMagickFatalError = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias mMagickWarning = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias m_MagickError = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias m_MagickFatalError = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias m_MagickWarning = void function(const scope ExceptionType, const scope char*, const scope char*);
        alias mSetExceptionInfo = void function(ExceptionInfo*, ExceptionType);
        alias mThrowException = void function(ExceptionInfo*, const scope ExceptionType, const scope char*, const scope char*);
        alias mThrowLoggedException = void function(ExceptionInfo* exception, const scope ExceptionType severity,
            const scope char* reason, const scope char* description, const scope char* module_,
            const scope char* function_, const scope c_ulong line);

        alias mSetWarningHandler = WarningHandler function(WarningHandler);
    }

    __gshared
    {
        mGetLocaleExceptionMessage GetLocaleExceptionMessage;
        mGetLocaleMessage GetLocaleMessage;

        mSetErrorHandler SetErrorHandler;

        mSetFatalErrorHandler SetFatalErrorHandler;

        mCatchException CatchException;
        mCopyException CopyException;
        mDestroyExceptionInfo DestroyExceptionInfo;
        mGetExceptionInfo GetExceptionInfo;
        mMagickError MagickError;
        mMagickFatalError MagickFatalError;
        mMagickWarning MagickWarning;
        m_MagickError _MagickError;
        m_MagickFatalError _MagickFatalError;
        m_MagickWarning _MagickWarning;
        mSetExceptionInfo SetExceptionInfo;
        mThrowException ThrowException;
        mThrowLoggedException ThrowLoggedException;

        mSetWarningHandler SetWarningHandler;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadErrorH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            GetLocaleExceptionMessage = cast(mGetLocaleExceptionMessage) dlsym(dlib,
                "GetLocaleExceptionMessage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetLocaleMessage = cast(mGetLocaleMessage) dlsym(dlib, "GetLocaleMessage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }


            SetErrorHandler = cast(mSetErrorHandler) dlsym(dlib, "SetErrorHandler");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }


            SetFatalErrorHandler = cast(mSetFatalErrorHandler) dlsym(dlib, "SetFatalErrorHandler");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }


            CatchException = cast(mCatchException) dlsym(dlib, "CatchException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            CopyException = cast(mCopyException) dlsym(dlib, "CopyException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyExceptionInfo = cast(mDestroyExceptionInfo) dlsym(dlib, "DestroyExceptionInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetExceptionInfo = cast(mGetExceptionInfo) dlsym(dlib, "GetExceptionInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickError = cast(mMagickError) dlsym(dlib, "MagickError");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickFatalError = cast(mMagickFatalError) dlsym(dlib, "MagickFatalError");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickWarning = cast(mMagickWarning) dlsym(dlib, "MagickWarning");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            _MagickError = cast(m_MagickError) dlsym(dlib, "_MagickError");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            _MagickFatalError = cast(m_MagickFatalError) dlsym(dlib, "_MagickFatalError");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            _MagickWarning = cast(m_MagickWarning) dlsym(dlib, "_MagickWarning");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetExceptionInfo = cast(mSetExceptionInfo) dlsym(dlib, "SetExceptionInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            ThrowException = cast(mThrowException) dlsym(dlib, "ThrowException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            ThrowLoggedException = cast(mThrowLoggedException) dlsym(dlib, "ThrowLoggedException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }


            SetWarningHandler = cast(mSetWarningHandler) dlsym(dlib, "SetWarningHandler");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.error:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
