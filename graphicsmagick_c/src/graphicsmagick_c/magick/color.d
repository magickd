/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Color Methods.
*/
// Complete graphicsmagick_c wrapper for v1.3.0
module graphicsmagick_c.magick.color;

import core.stdc.config : c_ulong;
import core.stdc.stdio : FILE;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : ColorInfo, ComplianceType, Image, PixelPacket;

/*
    PixelPacket with usage count, used to support color histograms.
*/
struct _HistogramColorPacket
{
    PixelPacket pixel;

    c_ulong count;
}
alias HistogramColorPacket = _HistogramColorPacket;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char** GetColorList(const scope char*, c_ulong*);

    ColorInfo* GetColorInfo(const scope char*, ExceptionInfo*);

    ColorInfo** GetColorInfoArray(ExceptionInfo* exception);

    HistogramColorPacket* GetColorHistogram(const scope Image*, c_ulong* colors, ExceptionInfo*);

    uint MagickConstrainColormapIndex(Image* image, uint index);
    uint FuzzyColorMatch(const scope PixelPacket*, const scope PixelPacket*, const scope double);
    uint IsGrayImage(const scope Image*, ExceptionInfo *);
    uint IsMonochromeImage(const scope Image*, ExceptionInfo *);
    uint IsOpaqueImage(const scope Image*, ExceptionInfo *);
    uint IsPaletteImage(const scope Image*, ExceptionInfo *);
    uint ListColorInfo(FILE*, ExceptionInfo *);
    uint QueryColorDatabase(const scope char*, PixelPacket*, ExceptionInfo *);
    uint QueryColorname(const scope Image*, const scope PixelPacket*, const scope ComplianceType, char*, ExceptionInfo *);

    c_ulong GetNumberColors(const scope Image*, FILE*, ExceptionInfo*);

    void DestroyColorInfo();
    void GetColorTuple(const scope PixelPacket*, const scope uint, const scope uint, const scope uint, char*);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetColorList = char** function(const scope char*, c_ulong*);

        alias mGetColorInfo = ColorInfo* function(const scope char*, ExceptionInfo*);

        alias mGetColorInfoArray = ColorInfo** function(ExceptionInfo* exception);

        alias mGetColorHistogram = HistogramColorPacket* function(const scope Image*, c_ulong* colors, ExceptionInfo*);

        alias mMagickConstrainColormapIndex = uint function(Image* image, uint index);
        alias mFuzzyColorMatch = uint function(const scope PixelPacket*, const scope PixelPacket*, const scope double);
        alias mIsGrayImage = uint function(const scope Image*, ExceptionInfo*);
        alias mIsMonochromeImage = uint function(const scope Image*, ExceptionInfo*);
        alias mIsOpaqueImage = uint function(const scope Image*, ExceptionInfo*);
        alias mIsPaletteImage = uint function(const scope Image*, ExceptionInfo*);
        alias mListColorInfo = uint function(FILE*, ExceptionInfo*);
        alias mQueryColorDatabase = uint function(const scope char*, PixelPacket*, ExceptionInfo*);
        alias mQueryColorname = uint function(const scope Image*, const scope PixelPacket*, const scope ComplianceType, char*, ExceptionInfo*);

        alias mGetNumberColors = c_ulong function(const scope Image*, FILE*, ExceptionInfo*);

        alias mDestroyColorInfo = void function();
        alias mGetColorTuple = void function(const scope PixelPacket*, const scope uint, const scope uint, const scope uint, char*);
    }

    __gshared
    {
        mGetColorList GetColorList;

        mGetColorInfo GetColorInfo;

        mGetColorInfoArray GetColorInfoArray;

        mGetColorHistogram GetColorHistogram;

        mMagickConstrainColormapIndex MagickConstrainColormapIndex;
        mFuzzyColorMatch FuzzyColorMatch;
        mIsGrayImage IsGrayImage;
        mIsMonochromeImage IsMonochromeImage;
        mIsOpaqueImage IsOpaqueImage;
        mIsPaletteImage IsPaletteImage;
        mListColorInfo ListColorInfo;
        mQueryColorDatabase QueryColorDatabase;
        mQueryColorname QueryColorname;

        mGetNumberColors GetNumberColors;

        mDestroyColorInfo DestroyColorInfo;
        mGetColorTuple GetColorTuple;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadColorH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetColorList = cast(mGetColorList)dlsym(dlib, "GetColorList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetColorInfo = cast(mGetColorInfo)dlsym(dlib, "GetColorInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetColorInfoArray = cast(mGetColorInfoArray)dlsym(dlib, "GetColorInfoArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetColorHistogram = cast(mGetColorHistogram)dlsym(dlib, "GetColorHistogram");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickConstrainColormapIndex =
                cast(mMagickConstrainColormapIndex)dlsym(dlib, "MagickConstrainColormapIndex");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            FuzzyColorMatch = cast(mFuzzyColorMatch)dlsym(dlib, "FuzzyColorMatch");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsGrayImage = cast(mIsGrayImage)dlsym(dlib, "IsGrayImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsMonochromeImage = cast(mIsMonochromeImage)dlsym(dlib, "IsMonochromeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsOpaqueImage = cast(mIsOpaqueImage)dlsym(dlib, "IsOpaqueImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            IsPaletteImage = cast(mIsPaletteImage)dlsym(dlib, "IsPaletteImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListColorInfo = cast(mListColorInfo)dlsym(dlib, "ListColorInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            QueryColorDatabase = cast(mQueryColorDatabase)dlsym(dlib, "QueryColorDatabase");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            QueryColorname = cast(mQueryColorname)dlsym(dlib, "QueryColorname");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetNumberColors = cast(mGetNumberColors)dlsym(dlib, "GetNumberColors");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyColorInfo = cast(mDestroyColorInfo)dlsym(dlib, "DestroyColorInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetColorTuple = cast(mGetColorTuple)dlsym(dlib, "GetColorTuple");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.color:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
