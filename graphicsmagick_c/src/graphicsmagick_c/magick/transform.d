/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Transform Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.transform;

import core.stdc.config : c_long;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image, RectangleInfo;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* ChopImage(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
    Image* CoalesceImages(const scope Image*, ExceptionInfo*);
    Image* CropImage(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
    Image* DeconstructImages(const scope Image*, ExceptionInfo*);
    Image* FlattenImages(const scope Image*, ExceptionInfo*);
    Image* FlipImage(const scope Image*, ExceptionInfo*);
    Image* FlopImage(const scope Image*, ExceptionInfo*);
    Image* MosaicImages(const scope Image*, ExceptionInfo*);
    Image* RollImage(const scope Image*, const scope c_long, const scope c_long, ExceptionInfo*);
    Image* ShaveImage(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);

    void TransformImage(Image**, const scope RectangleInfo*, ExceptionInfo*);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mChopImage = Image* function(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
        alias mCoalesceImages = Image* function(const scope Image*, ExceptionInfo*);
        alias mCropImage = Image* function(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);
        alias mDeconstructImages = Image* function(const scope Image*, ExceptionInfo*);
        alias mFlattenImages = Image* function(const scope Image*, ExceptionInfo*);
        alias mFlipImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mFlopImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mMosaicImages = Image* function(const scope Image*, ExceptionInfo*);
        alias mRollImage = Image* function(const scope Image*, const scope c_long, const scope c_long, ExceptionInfo*);
        alias mShaveImage = Image* function(const scope Image*, const scope RectangleInfo*, ExceptionInfo*);

        alias mTransformImage = void function(Image**, const scope RectangleInfo*, ExceptionInfo*);
    }

    __gshared
    {
        mChopImage ChopImage;
        mCoalesceImages CoalesceImages;
        mCropImage CropImage;
        mDeconstructImages DeconstructImages;
        mFlattenImages FlattenImages;
        mFlipImage FlipImage;
        mFlopImage FlopImage;
        mMosaicImages MosaicImages;
        mRollImage RollImage;
        mShaveImage ShaveImage;

        mTransformImage TransformImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadTransformH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            ChopImage = cast(mChopImage)dlsym(dlib, "ChopImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            CoalesceImages = cast(mCoalesceImages)dlsym(dlib, "CoalesceImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            CropImage = cast(mCropImage)dlsym(dlib, "CropImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            DeconstructImages = cast(mDeconstructImages)dlsym(dlib, "DeconstructImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            FlattenImages = cast(mFlattenImages)dlsym(dlib, "FlattenImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            FlipImage = cast(mFlipImage)dlsym(dlib, "FlipImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            FlopImage = cast(mFlopImage)dlsym(dlib, "FlopImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            MosaicImages = cast(mMosaicImages)dlsym(dlib, "MosaicImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            RollImage = cast(mRollImage)dlsym(dlib, "RollImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }
            ShaveImage = cast(mShaveImage)dlsym(dlib, "ShaveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }

            TransformImage = cast(mTransformImage)dlsym(dlib, "TransformImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.transform:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
