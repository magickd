/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Magick registry methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.registry;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image;

/*
    Enum declarations.
*/
enum : RegistryType
{
    UndefinedRegsitryType,
    ImageRegsitryType,
    ImageInfoRegistryType
}
alias RegistryType = int;

struct _RegistryInfo
{
    c_long id;

    RegistryType type;

    void* blob;

    size_t length;

    c_ulong signature;

    _RegistryInfo* previous;
    _RegistryInfo* next;
}
alias RegistryInfo = _RegistryInfo;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    Image* GetImageFromMagickRegistry(const scope char*, c_long* id, ExceptionInfo*);

    c_long SetMagickRegistry(const scope RegistryType, const scope void*, const scope size_t, ExceptionInfo*);

    uint DeleteMagickRegistry(const scope c_long);

    void DestroyMagickRegistry();
    void* GetMagickRegistry(const scope c_long, RegistryType*, size_t*, ExceptionInfo*);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mGetImageFromMagickRegistry = Image* function(const scope char*, c_long* id, ExceptionInfo*);

        alias mSetMagickRegistry = c_long function(const scope RegistryType, const scope void*, const scope size_t, ExceptionInfo*);

        alias mDeleteMagickRegistry = uint function(const scope c_long);

        alias mDestroyMagickRegistry = void function();
        alias mGetMagickRegistry = void* function(const scope c_long, RegistryType*, size_t*, ExceptionInfo*);
    }

    __gshared
    {
        mGetImageFromMagickRegistry GetImageFromMagickRegistry;

        mSetMagickRegistry SetMagickRegistry;

        mDeleteMagickRegistry DeleteMagickRegistry;

        mDestroyMagickRegistry DestroyMagickRegistry;
        mGetMagickRegistry GetMagickRegistry;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadRegistryH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetImageFromMagickRegistry = cast(mGetImageFromMagickRegistry)dlsym(dlib, "GetImageFromMagickRegistry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.registry:\n  %s\n", errmsg);
                }
                success = false;
            }

            SetMagickRegistry = cast(mSetMagickRegistry)dlsym(dlib, "SetMagickRegistry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.registry:\n  %s\n", errmsg);
                }
                success = false;
            }

            DeleteMagickRegistry = cast(mDeleteMagickRegistry)dlsym(dlib, "DeleteMagickRegistry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.registry:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMagickRegistry = cast(mDestroyMagickRegistry)dlsym(dlib, "DestroyMagickRegistry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.registry:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetMagickRegistry = cast(mGetMagickRegistry)dlsym(dlib, "GetMagickRegistry");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.registry:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
