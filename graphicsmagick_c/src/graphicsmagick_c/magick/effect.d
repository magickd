/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Image Effect Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.effect;

import core.stdc.config : c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : ChannelType, Image, NoiseType;

version(GMagick_Static)
{
    @system @nogc nothrow extern(C):

    Image* AdaptiveThresholdImage(const scope Image*, const scope c_ulong, const scope c_ulong, const scope double, ExceptionInfo*);
    Image* AddNoiseImage(const scope Image*, const scope NoiseType, ExceptionInfo*);
    Image* AddNoiseImageChannel(const scope Image* image, const scope ChannelType channel, const scope NoiseType noise_type,
        ExceptionInfo* exception);
    Image* BlurImage(const scope Image*, const scope double, const scope double, ExceptionInfo *);
    Image* BlurImageChannel(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma,
        ExceptionInfo* exception);
    Image* DespeckleImage(const scope Image*, ExceptionInfo*);
    Image* EdgeImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* EmbossImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);
    Image* EnhanceImage(const scope Image*, ExceptionInfo*);
    Image* GaussianBlurImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);
    Image* GaussianBlurImageChannel(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma,
        ExceptionInfo* exception);
    Image* MedianFilterImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* MotionBlurImage(const scope Image*, const scope double, const scope double, const scope double, ExceptionInfo*);
    Image* ReduceNoiseImage(const scope Image*, const scope double, ExceptionInfo*);
    Image* ShadeImage(const scope Image*, const scope uint, double, double, ExceptionInfo*);
    Image* SharpenImage(const scope Image*, const scope double, const scope double, ExceptionInfo*);
    Image* SharpenImageChannel(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma,
        ExceptionInfo* exception);
    Image* SpreadImage(const scope Image*, const scope uint, ExceptionInfo*);
    Image* UnsharpMaskImage(const scope Image*, const scope double, const scope double, const scope double, const scope double, ExceptionInfo*);
    Image* UnsharpMaskImageChannel(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma,
        const scope double amount, const scope double threshold, ExceptionInfo* exception);

    MagickPassFail BlackThresholdImage(Image* image, const scope char* thresholds);
    MagickPassFail ChannelThresholdImage(Image*, const scope char*);
    MagickPassFail RandomChannelThresholdImage(Image*, const scope char*, const scope char*, ExceptionInfo* exception);
    MagickPassFail ThresholdImage(Image*, const scope double);
    MagickPassFail WhiteThresholdImage(Image* image, const scope char* thresholds);

} // version(GMagick_Static)
else
{
    @system @nogc nothrow extern(C)
    {
        alias mAdaptiveThresholdImage = Image* function(const scope Image*, const scope c_ulong, const scope c_ulong, const scope double, ExceptionInfo*);
        alias mAddNoiseImage = Image* function(const scope Image*, const scope NoiseType, ExceptionInfo*);
        alias mAddNoiseImageChannel = Image* function(const scope Image* image, const scope ChannelType channel, const scope NoiseType noise_type, ExceptionInfo* exception);
        alias mBlurImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo *);
        alias mBlurImageChannel = Image* function(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma, ExceptionInfo* exception);
        alias mDespeckleImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mEdgeImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mEmbossImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);
        alias mEnhanceImage = Image* function(const scope Image*, ExceptionInfo*);
        alias mGaussianBlurImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);
        alias mGaussianBlurImageChannel = Image* function(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma, ExceptionInfo* exception);
        alias mMedianFilterImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mMotionBlurImage = Image* function(const scope Image*, const scope double, const scope double, const scope double, ExceptionInfo*);
        alias mReduceNoiseImage = Image* function(const scope Image*, const scope double, ExceptionInfo*);
        alias mShadeImage = Image* function(const scope Image*, const scope uint, double, double, ExceptionInfo*);
        alias mSharpenImage = Image* function(const scope Image*, const scope double, const scope double, ExceptionInfo*);
        alias mSharpenImageChannel = Image* function(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma, ExceptionInfo* exception);
        alias mSpreadImage = Image* function(const scope Image*, const scope uint, ExceptionInfo*);
        alias mUnsharpMaskImage = Image* function(const scope Image*, const scope double, const scope double, const scope double, const scope double, ExceptionInfo*);
        alias mUnsharpMaskImageChannel = Image* function(const scope Image* image, const scope ChannelType channel, const scope double radius, const scope double sigma, const scope double amount, const scope double threshold, ExceptionInfo* exception);

        alias mBlackThresholdImage = MagickPassFail function(Image* image, const scope char* thresholds);
        alias mChannelThresholdImage = MagickPassFail function(Image*, const scope char*);
        alias mRandomChannelThresholdImage = MagickPassFail function(Image*, const scope char*, const scope char*, ExceptionInfo* exception);
        alias mThresholdImage = MagickPassFail function(Image*, const scope double);
        alias mWhiteThresholdImage = MagickPassFail function(Image* image, const scope char* thresholds);
    }

    __gshared
    {
        mAdaptiveThresholdImage AdaptiveThresholdImage;
        mAddNoiseImage AddNoiseImage;
        mAddNoiseImageChannel AddNoiseImageChannel;
        mBlurImage BlurImage;
        mBlurImageChannel BlurImageChannel;
        mDespeckleImage DespeckleImage;
        mEdgeImage EdgeImage;
        mEmbossImage EmbossImage;
        mEnhanceImage EnhanceImage;
        mGaussianBlurImage GaussianBlurImage;
        mGaussianBlurImageChannel GaussianBlurImageChannel;
        mMedianFilterImage MedianFilterImage;
        mMotionBlurImage MotionBlurImage;
        mReduceNoiseImage ReduceNoiseImage;
        mShadeImage ShadeImage;
        mSharpenImage SharpenImage;
        mSharpenImageChannel SharpenImageChannel;
        mSpreadImage SpreadImage;
        mUnsharpMaskImage UnsharpMaskImage;
        mUnsharpMaskImageChannel UnsharpMaskImageChannel;

        mBlackThresholdImage BlackThresholdImage;
        mChannelThresholdImage ChannelThresholdImage;
        mRandomChannelThresholdImage RandomChannelThresholdImage;
        mThresholdImage ThresholdImage;
        mWhiteThresholdImage WhiteThresholdImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadEffectH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            AdaptiveThresholdImage = cast(mAdaptiveThresholdImage)dlsym(dlib, "AdaptiveThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            AddNoiseImage = cast(mAddNoiseImage)dlsym(dlib, "AddNoiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            AddNoiseImageChannel = cast(mAddNoiseImageChannel)dlsym(dlib, "AddNoiseImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlurImage = cast(mBlurImage)dlsym(dlib, "BlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlurImageChannel = cast(mBlurImageChannel)dlsym(dlib, "BlurImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            DespeckleImage = cast(mDespeckleImage)dlsym(dlib, "DespeckleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            EdgeImage = cast(mEdgeImage)dlsym(dlib, "EdgeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            EmbossImage = cast(mEmbossImage)dlsym(dlib, "EmbossImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            EnhanceImage = cast(mEnhanceImage)dlsym(dlib, "EnhanceImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            GaussianBlurImage = cast(mGaussianBlurImage)dlsym(dlib, "GaussianBlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            GaussianBlurImageChannel = cast(mGaussianBlurImageChannel)dlsym(dlib, "GaussianBlurImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            MedianFilterImage = cast(mMedianFilterImage)dlsym(dlib, "MedianFilterImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            MotionBlurImage = cast(mMotionBlurImage)dlsym(dlib, "MotionBlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReduceNoiseImage = cast(mReduceNoiseImage)dlsym(dlib, "ReduceNoiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            ShadeImage = cast(mShadeImage)dlsym(dlib, "ShadeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            SharpenImage = cast(mSharpenImage)dlsym(dlib, "SharpenImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            SharpenImageChannel = cast(mSharpenImageChannel)dlsym(dlib, "SharpenImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            SpreadImage = cast(mSpreadImage)dlsym(dlib, "SpreadImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            UnsharpMaskImage = cast(mUnsharpMaskImage)dlsym(dlib, "UnsharpMaskImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            UnsharpMaskImageChannel = cast(mUnsharpMaskImageChannel)dlsym(dlib, "UnsharpMaskImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }

            BlackThresholdImage = cast(mBlackThresholdImage)dlsym(dlib, "BlackThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            ChannelThresholdImage = cast(mChannelThresholdImage)dlsym(dlib, "ChannelThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            RandomChannelThresholdImage = cast(mRandomChannelThresholdImage)dlsym(dlib, "RandomChannelThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            ThresholdImage = cast(mThresholdImage)dlsym(dlib, "ThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }
            WhiteThresholdImage = cast(mWhiteThresholdImage)dlsym(dlib, "WhiteThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.effect:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
