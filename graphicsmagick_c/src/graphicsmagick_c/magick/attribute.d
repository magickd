/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Methods to Get/Set/Destroy Image Text Attributes.
*/
// Complete graphicsmagick_c wrapper for version 1.3
module graphicsmagick_c.magick.attribute;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.image;

struct _ImageAttribute
{
    char* key;
    char* value;

    size_t length;

    _ImageAttribute* previous;
    _ImageAttribute* next;
}
alias ImageAttribute = _ImageAttribute;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    const(ImageAttribute)* GetImageAttribute(const scope Image* image, const scope char* key);
    const(ImageAttribute)* GetImageClippingPathAttribute(const scope Image* image);
    const(ImageAttribute)* GetImageInfoAttribute(const scope ImageInfo* image_info, const scope Image* image, const scope char* key);

    MagickPassFail CloneImageAttributes(Image* clone_image, const scope Image* original_image);
    MagickPassFail SetImageAttribute(Image* image, const scope char* key, const scope char* value);

    void DestroyImageAttribustes(Image* image);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetImageAttribute = const(ImageAttribute)* function(const scope Image*, const scope char*);
        alias mGetImageClippingPathAttribute = const(ImageAttribute)* function(const scope Image*);
        alias mGetImageInfoAttribute = const(ImageAttribute)* function(const scope ImageInfo*, const scope Image*, const scope char*);

        alias mCloneImageAttributes = MagickPassFail function(Image*, const scope Image*);
        alias mSetImageAttribute = MagickPassFail function(Image*, const scope char*, const scope char*);
    }

    __gshared
    {
        mGetImageAttribute GetImageAttribute;
        mGetImageClippingPathAttribute GetImageClippingPathAttribute;
        mGetImageInfoAttribute GetImageInfoAttribute;

        mCloneImageAttributes CloneImageAttributes;
        mSetImageAttribute SetImageAttribute;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadAttributeH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetImageAttribute = cast(mGetImageAttribute)dlsym(dlib, "GetImageAttribute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.attribute:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetImageClippingPathAttribute = cast(mGetImageClippingPathAttribute)dlsym(dlib, "GetImageClippingPathAttribute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.attribute:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetImageInfoAttribute = cast(mGetImageInfoAttribute)dlsym(dlib, "GetImageInfoAttribute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.attribute:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneImageAttributes = cast(mCloneImageAttributes)dlsym(dlib, "CloneImageAttributes");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.attribute:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetImageAttribute = cast(mSetImageAttribute)dlsym(dlib, "SetImageAttribute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.attribute:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
