/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  ImageMagick Application Programming Interface declarations.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.magick;

import core.stdc.config : c_ulong;
import core.stdc.stdio : FILE;

import graphicsmagick_c.magick.api : MagickBool, MagickPassFail;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

//~

alias DecoderHandler = extern (C) nothrow Image* function(const scope ImageInfo*, ExceptionInfo*);

alias EncoderHandler = extern (C) nothrow uint function(const scope ImageInfo*, Image*);
alias MagickHandler = extern (C) nothrow uint function(const scope ubyte*, const scope size_t);

/*
 * Stability and usefulness of the coder.
 */
alias CoderClass = int;
enum : CoderClass
{
    UnstableCoderClass = 0,
    StableCoderClass,
    PrimaryCoderClass
}

/*
 * How the file extension should be treated (e.g. in SetImageInfo()).
 */
alias ExtensionTreatment = int;
enum : ExtensionTreatment
{
    /// Extension is a useful hint to indicate format
    HintExtensionTreatment = 0,
    /// Extension must be obeyed as format indicator
    ObeyExtensionTreatment,
    /// Extension has no associated format
    IgnoreExtensionTreatment
}

struct _MagickInfo
{
    // private, next member in list.
    _MagickInfo *next;
    // private, previous member in list.
    _MagickInfo *previous;

    /// format ID ("magick")
    const(char)* name;

    /// format description
    const(char)* description;
    /// usage note for user
    const(char)* note;
    /// support library version
    pragma(mangle, "version") const(char)* version_;
    /// name of the loadable module
    pragma(mangle, "module") const(char)* module_;

    /// function vector to decoding routine
    DecoderHandler decoder;

    /// function vector to encoding routine
    EncoderHandler encoder;

    /// function vector to format test routine
    MagickHandler magick;

    /// arbitrary user supplied data
    void* client_data;

    /// coder may read/write multiple frames (default True)
    MagickBool adjoin;
    /// coder requires that size be set (default False)
    MagickBool raw;
    /// coder should not appear in formats listing (default MagickFalse)
    MagickBool stealth;
    /// coder requires BLOB "seek" and "tell" APIs (default MagickFalse)
    ///
    /// Note that SetImageInfo() currently always copies input
    /// from a pipe, .gz, or .bz2 file, to a temporary file so
    /// that it can retrieve a bit of the file header in order to
    /// support the file header magic logic.
    MagickBool seekable_stream;
    /// coder uses BLOB APIs (default True)
    MagickBool blob_support;
    /// coder is thread safe (default True)
    MagickBool thread_support;

    /// Coder usefulness/stability level
    CoderClass coder_class;

    /// How much faith should be placed on file extension
    ExtensionTreatment extension_treatment;

    // private, structure validator
    c_ulong signature;
}
alias MagickInfo = _MagickInfo;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char* MagickToMime(const scope ubyte* magick, const scope size_t length);

    MagickBool IsMagickConflict(const scope char* magick);

    MagickPassFail ListModuleMap(FILE* file, ExceptionInfo* exception);
    MagickPassFail ListMagickInfo(FILE* file, ExceptionInfo* exception);
    MagickPassFail UnregisterMagickInfo(const scope char* name);

    void DestroyMagick();
    void InitializeMagick(const scope char* path);

    MagickInfo* GetMagickInfo(const scope char* name, ExceptionInfo* exception);
    MagickInfo** GetMagickInfoArray(ExceptionInfo* exception);

    MagickInfo* RegisterMagickInfo(MagickInfo* magick_info);
    MagickInfo* SetMagickInfo(const scope char* name);

} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mMagickToMime = char* function(const scope ubyte*, const scope size_t);

        alias mIsMagickConflict = MagickBool function(const scope char*);

        alias mListModuleMap = MagickPassFail function(FILE*, ExceptionInfo*);
        alias mListMagickInfo = MagickPassFail function(FILE*, ExceptionInfo*);
        alias mUnregisterMagickInfo = MagickPassFail function(const scope char*);

        alias mDestroyMagick = void function();
        alias mInitializeMagick = void function(const scope char*);

        alias mGetMagickInfo = MagickInfo* function(const scope char*, ExceptionInfo*);
        alias mGetMagickInfoArray = MagickInfo** function(ExceptionInfo*);

        alias mRegisterMagickInfo = MagickInfo* function(MagickInfo*);
        alias mSetMagickInfo = MagickInfo* function(const scope char*);
    }

    __gshared
    {
        mMagickToMime MagickToMime;

        mIsMagickConflict IsMagickConflict;

        mListModuleMap ListModuleMap;
        mListMagickInfo ListMagickInfo;
        mUnregisterMagickInfo UnregisterMagickInfo;

        mDestroyMagick DestroyMagick;
        mInitializeMagick InitializeMagick;

        mGetMagickInfo GetMagickInfo;
        mGetMagickInfoArray GetMagickInfoArray;

        mRegisterMagickInfo RegisterMagickInfo;
        mSetMagickInfo SetMagickInfo;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMagickH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            MagickToMime = cast(mMagickToMime)dlsym(dlib, "MagickToMime");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            IsMagickConflict = cast(mIsMagickConflict)dlsym(dlib, "IsMagickConflict");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            ListModuleMap = cast(mListModuleMap)dlsym(dlib, "ListModuleMap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListMagickInfo = cast(mListMagickInfo)dlsym(dlib, "ListMagickInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }
            UnregisterMagickInfo = cast(mUnregisterMagickInfo)dlsym(dlib, "UnregisterMagickInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMagick = cast(mDestroyMagick)dlsym(dlib, "DestroyMagick");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }
            InitializeMagick = cast(mInitializeMagick)dlsym(dlib, "InitializeMagick");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetMagickInfo = cast(mGetMagickInfo)dlsym(dlib, "GetMagickInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetMagickInfoArray = cast(mGetMagickInfoArray)dlsym(dlib, "GetMagickInfoArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            RegisterMagickInfo = cast(mRegisterMagickInfo)dlsym(dlib, "RegisterMagickInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetMagickInfo = cast(mSetMagickInfo)dlsym(dlib, "SetMagickInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.magick:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
