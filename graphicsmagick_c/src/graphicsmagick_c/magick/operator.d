/*
% Copyright (C) 2004, 2008 GraphicsMagick Group
%
% This program is covered by multiple licenses, which are described in
% Copyright.txt. You should have received a copy of Copyright.txt with this
% package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.
%
% Interfaces to support quantum operators.
% Written by Bob Friesenhahn, March 2004.
%
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.operator;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : ChannelType, Image;

enum : QuantumOperator
{
    UndefinedQuantumOp = 0,
    AddQuantumOp, /* Add value */
    AndQuantumOp, /* Bitwise AND value */
    AssignQuantumOp, /* Direct value assignment */
    DivideQuantumOp, /* Divide by value */
    LShiftQuantumOp, /* Bitwise left-shift value N bits */
    MultiplyQuantumOp, /* Multiply by value */
    OrQuantumOp, /* Bitwise OR value */
    RShiftQuantumOp, /* Bitwise right shift value */
    SubtractQuantumOp, /* Subtract value */
    ThresholdQuantumOp, /* Above threshold white, otherwise black */
    ThresholdBlackQuantumOp, /* Below threshold is black */
    ThresholdWhiteQuantumOp, /* Above threshold is white */
    XorQuantumOp, /* Bitwise XOR value */
    NoiseGaussianQuantumOp, /* Gaussian noise */
    NoiseImpulseQuantumOp, /* Impulse noise */
    NoiseLaplacianQuantumOp, /* Laplacian noise */
    NoiseMultiplicativeQuantumOp, /* Multiplicative gaussian noise */
    NoisePoissonQuantumOp, /* Poisson noise */
    NoiseUniformQuantumOp, /* Uniform noise */
    NegateQuantumOp, /* Negate channel, ignore value */
    GammaQuantumOp, /* Adjust image gamma */
    DepthQuantumOp  /* Adjust image depth */
}
alias QuantumOperator = int;

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    MagickPassFail QuantumOperatorImage(Image* image, const scope ChannelType channel, const scope QuantumOperator quantum_operator,
        const scope double rvalue, ExceptionInfo* exception);
    MagickPassFail QuantumOperatorImageMultivalue(Image* image, const scope QuantumOperator quantum_operator, const scope char* values);
    MagickPassFail QuantumOperatorRegionImage(Image* image, const scope c_long x, const scope c_long y, const scope c_ulong columns,
        const scope c_ulong rows, const scope ChannelType channel, const scope QuantumOperator quantum_operator, const scope double rvalue,
        ExceptionInfo* exception);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mQuantumOperatorImage = MagickPassFail function(Image* image, const scope ChannelType channel,
            const scope QuantumOperator quantum_operator, const scope double rvalue, ExceptionInfo* exception);
        alias mQuantumOperatorImageMultivalue = MagickPassFail function(Image* image,
            const scope QuantumOperator quantum_operator, const scope char* values);
        alias mQuantumOperatorRegionImage = MagickPassFail function(Image* image, const scope c_long x, const scope c_long y,
            const scope c_ulong columns, const scope c_ulong rows, const scope ChannelType channel, const scope QuantumOperator quantum_operator,
            const scope double rvalue, ExceptionInfo* exception);
    }

    __gshared
    {
        mQuantumOperatorImage QuantumOperatorImage;
        mQuantumOperatorImageMultivalue QuantumOperatorImageMultivalue;
        mQuantumOperatorRegionImage QuantumOperatorRegionImage;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadOperatorH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            QuantumOperatorImage = cast(mQuantumOperatorImage)dlsym(dlib, "QuantumOperatorImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.operator:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantumOperatorImageMultivalue =
                cast(mQuantumOperatorImageMultivalue)dlsym(dlib, "QuantumOperatorImageMultivalue");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.operator:\n  %s\n", errmsg);
                }
                success = false;
            }
            QuantumOperatorRegionImage = cast(mQuantumOperatorRegionImage)dlsym(dlib, "QuantumOperatorRegionImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.operator:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
