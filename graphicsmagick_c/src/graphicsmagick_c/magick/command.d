/*
  Copyright (C) 2003, 2007 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
 
  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.
 
  Image Command Methods.
*/
// Complete graphicsmagick_c wapper for version 1.3
module graphicsmagick_c.magick.command;

import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    uint AnimateImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint BenchmarkImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint CompareImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint CompositeImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint ConjureImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint ConvertImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint DisplayImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint IdentifyImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint ImportImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint MagickCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint MogrifyImage(const ImageInfo* , int, char** , Image** );
    uint MogrifyImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    uint MogrifyImages(const ImageInfo* , int, char** , Image** );
    uint MontageImageCommand(ImageInfo* image_info, int argc, char** argv,
        char** metadata, ExceptionInfo* exception);
    
    int GMCommand(int argc, char** argv);
}
else
{
    @system @nogc nothrow extern (C)
    {
        alias mAnimateImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mBenchmarkImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mCompareImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mCompositeImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mConjureImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mConvertImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mDisplayImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mIdentifyImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mImportImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mMagickCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mMogrifyImage = uint function(const ImageInfo* , int, char** , Image** );
        alias mMogrifyImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
        alias mMogrifyImages = uint function(const ImageInfo* , int, char** , Image** );
        alias mMontageImageCommand = uint function(ImageInfo* image_info, int argc, char** argv,
            char** metadata, ExceptionInfo* exception);
    
        alias mGMCommand = int function(int argc, char** argv);
    }

    __gshared
    {
        mAnimateImageCommand AnimateImageCommand;
        mBenchmarkImageCommand BenchmarkImageCommand;
        mCompareImageCommand CompareImageCommand;
        mCompositeImageCommand CompositeImageCommand;
        mConjureImageCommand ConjureImageCommand;
        mConvertImageCommand ConvertImageCommand;
        mDisplayImageCommand DisplayImageCommand;
        mIdentifyImageCommand IdentifyImageCommand;
        mImportImageCommand ImportImageCommand;
        mMagickCommand MagickCommand;
        mMogrifyImage MogrifyImage;
        mMogrifyImageCommand MogrifyImageCommand;
        mMogrifyImages MogrifyImages;
        mMontageImageCommand MontageImageCommand;
    
        mGMCommand GMCommand;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadCommandH(void* dlib)
        {
            char *errmsg;
            bool success = true;

            AnimateImageCommand = cast(mAnimateImageCommand)dlsym(dlib, "AnimateImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            BenchmarkImageCommand = cast(mBenchmarkImageCommand)dlsym(dlib, "BenchmarkImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            CompareImageCommand = cast(mCompareImageCommand)dlsym(dlib, "CompareImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            CompositeImageCommand = cast(mCompositeImageCommand)dlsym(dlib, "CompositeImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            ConjureImageCommand = cast(mConjureImageCommand)dlsym(dlib, "ConjureImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            ConvertImageCommand = cast(mConvertImageCommand)dlsym(dlib, "ConvertImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            DisplayImageCommand = cast(mDisplayImageCommand)dlsym(dlib, "DisplayImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            IdentifyImageCommand = cast(mIdentifyImageCommand)dlsym(dlib, "IdentifyImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImportImageCommand = cast(mImportImageCommand)dlsym(dlib, "ImportImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCommand = cast(mMagickCommand)dlsym(dlib, "MagickCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            MogrifyImage = cast(mMogrifyImage)dlsym(dlib, "MogrifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            MogrifyImageCommand = cast(mMogrifyImageCommand)dlsym(dlib, "MogrifyImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            MogrifyImages = cast(mMogrifyImages)dlsym(dlib, "MogrifyImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
            MontageImageCommand = cast(mMontageImageCommand)dlsym(dlib, "MontageImageCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }
        
            mGMCommand GMCommand = cast(mGMCommand)dlsym(dlib, "GMCommand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.command:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
