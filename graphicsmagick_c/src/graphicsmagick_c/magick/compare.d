/*
  Copyright (C) 2008 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Image Comparison Methods.
*/

// Complete wrapping of magick/compare.h for GraphicsMagick 1.3
module graphicsmagick_c.magick.compare;

import graphicsmagick_c.magick.api;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : ChannelType, Image, PixelPacket;

/*
    Pixel differencing algorithm.
*/
alias HighlightStyle = int;
enum : HighlightStyle
{
    UndefinedHighlightStyle,
    AssignHighlightStyle,
    ThresholdHighlightStyle,
    TintHighlightStyle,
    XorHighlightStyle,
}

struct _DifferenceImageOptions
{
    ChannelType channel;
    HighlightStyle highlight_style;
    PixelPacket highlight_color;
}
alias DifferenceImageOptions = _DifferenceImageOptions;

/*
    Pixel error metrics
*/
alias MetricType = int;
enum : MetricType
{
    UndefinedMetric,
    MeanAbsoluteErrorMetric,
    MeanSquaredErrorMetric,
    PeakAbsoluteErrorMetric,
    PeakSignalToNoiseRatioMetric,
    RootMeanSquaredErrorMetric
}

/*
    Pixel difference statistics
*/
struct _DifferenceStatistics
{
    double red;
    double green;
    double blue;
    double opacity;
    double combined;
}
alias DifferenceStatistics = _DifferenceStatistics;


version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    void InitializeDifferenceImageOptions (DifferenceImageOptions* options,
        ExceptionInfo* exception);

    Image* DifferenceImage(const scope Image* reference_image, const scope Image* compare_image,
        const scope DifferenceImageOptions* difference_options, ExceptionInfo* exception);

    void InitializeDifferenceStatistics (DifferenceStatistics* difference_statistics,
        ExceptionInfo* exception);

    MagickPassFail GetImageChannelDifference(const scope Image* reference_image,
        const scope Image* compare_image, const scope MetricType metric,
        DifferenceStatistics* statistics, ExceptionInfo* exception);
    MagickPassFail GetImageChannelDistortion(const scope Image* reference_image,
        const scope Image* compare_image, const scope ChannelType channel,
        const scope MetricType metric, double* distortion,
        ExceptionInfo* exception);
    MagickPassFail GetImageDistortion(const scope Image* reference_image,
        const scope Image* compare_image, const scope MetricType metric,
        double* distortion, ExceptionInfo* exception);

    MagickBool IsImageEqual(Image*, const scope Image*);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mInitializeDifferenceImageOptions = void function(DifferenceImageOptions*,
            ExceptionInfo*);

        alias mDifferenceImage = Image* function(const scope Image*, const scope Image*,
            const scope DifferenceImageOptions*, ExceptionInfo*);

        alias mInitializeDifferenceStatistics = void function(DifferenceStatistics*,
            ExceptionInfo*);

        alias mGetImageChannelDifference = MagickPassFail function(const scope Image*,
            const scope Image*, const scope MetricType, DifferenceStatistics*, ExceptionInfo*);
        alias mGetImageChannelDistortion = MagickPassFail function(const scope Image*,
            const scope Image*, const scope ChannelType, const scope MetricType, double*, ExceptionInfo*);
        alias mGetImageDistortion = MagickPassFail function(const scope Image*, const scope Image*,
            const scope MetricType, double*, ExceptionInfo*);

        alias mIsImageEqual = MagickBool function(Image*, const scope Image*);
    }

    __gshared
    {
        mInitializeDifferenceImageOptions InitializeDifferenceImageOptions;

        mDifferenceImage DifferenceImage;

        mInitializeDifferenceStatistics InitializeDifferenceStatistics;

        mGetImageChannelDifference GetImageChannelDifference;
        mGetImageChannelDistortion GetImageChannelDistortion;
        mGetImageDistortion GetImageDistortion;

        mIsImageEqual IsImageEqual;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadCompareH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            InitializeDifferenceImageOptions = cast(mInitializeDifferenceImageOptions) dlsym(dlib,
                "InitializeDifferenceImageOptions");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }


            DifferenceImage = cast(mDifferenceImage) dlsym(dlib, "DifferenceImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }


            InitializeDifferenceStatistics = cast(mInitializeDifferenceStatistics) dlsym(dlib,
                "InitializeDifferenceStatistics");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageChannelDifference = cast(mGetImageChannelDifference) dlsym(dlib,
                "GetImageChannelDifference");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageChannelDistortion = cast(mGetImageChannelDistortion) dlsym(dlib,
                "GetImageChannelDistortion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetImageDistortion = cast(mGetImageDistortion) dlsym(dlib, "GetImageDistortion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }

            IsImageEqual = cast(mIsImageEqual) dlsym(dlib, "IsImageEqual");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.compare:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
