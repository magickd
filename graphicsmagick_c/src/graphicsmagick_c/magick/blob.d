/*
  Copyright (C) 2003, 2004, 2007 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Image Compression/Decompression Methods.
*/
// Complete graphicsmagick_c wrapper for v1.3
module graphicsmagick_c.magick.blob;

import core.stdc.stdio : FILE;

import graphicsmagick_c.magick.api : MagickBool, MagickPassFail;
import graphicsmagick_c.magick.image : Image, ImageInfo;
import graphicsmagick_c.magick.magick_types;
import graphicsmagick_c.magick.error : ExceptionInfo;

/*
    Minimum input file size before considering for memory map.
*/
enum MinBlobExtent = 32768L;

/*
    Forward declarations.
*/
extern struct _BlobInfo;
alias BlobInfo = _BlobInfo;

/*
    Blob open modes
*/
enum : BlobMode
{
    UndefinedBlobMode,   /* Undefined */
    ReadBlobMode,        /* Open for reading (text) */ /* only locale.c */
    ReadBinaryBlobMode,  /* Open for reading (binary) */
    WriteBlobMode,       /* Open for writing (text) */ /* only mvg.c txt.c */
    WriteBinaryBlobMode  /* Open for writing (binary) */
}
alias BlobMode = int;

/*
    Memory mapping modes.
*/
enum : MapMode
{
    ReadMode,   /* Map for read-only access */
    WriteMode,  /* Map for write-only access (useless) */
    IOMode      /* Map for read/write access */
}
alias MapMode = int;

/*
 *
 * BlobInfo methods
 *
 */
version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    /*
        Makes a duplicate of the given blob info structure, or if a blob info
        is NULL, a new one.
    */
    BlobInfo* CloneBlobInfo(const scope BlobInfo* blob_info);

    /*
        Increments the reference count associated with the pixel blob,
        returning a pointer to the blob.
    */
    BlobInfo* ReferenceBlob(BlobInfo* blob);

    /*
        Deallocate memory associated with the BlobInfo structure.
    */
    void DestroyBlobInfo(BlobInfo* blob);

    /*
        If BLOB is a memory mapping then unmap it.  Reset BlobInfo structure
        to its default state.
    */
    void DetachBlob(BlobInfo* blob);

    /*
        Initialize a BlobInfo structure.
    */
    void GetBlobInfo(BlobInfo* blob);

    /*
        Attach a memory buffer to a BlobInfo structure.
    */
    void AttachBlob(BlobInfo* blob_info, const scope void* blob, const scope size_t length);

    /*
     *
     * Functions for managing a BLOB (type BlobInfo) attached to an Image.
     *
     */

    /*
        Deallocate all memory associated with an Image blob.
    */
    void DestroyBlob(Image* image);

    /*
     *
     * Formatted image I/O functions.
     *
     */

    /*
        Read an Image from a formatted in-memory "file" image ("BLOB")
    */
    Image* BlobToImage(const scope ImageInfo* image_info, const scope void* blob, const scope size_t length, ExceptionInfo* exception);

    /*
        Return an Image populated with salient information regarding a
        formatted in-memory "file" image ("BLOB") but without reading the
        image pixels.
    */
    Image* PingBlob(const scope ImageInfo* image_info, const scope void* blob, const scope size_t length, ExceptionInfo* exception);

    /*
        Writes an Image to a formatted (like a file) in-memory
        representation.
    */
    void* ImageToBlob(const scope ImageInfo* image_info, Image* image, size_t* length, ExceptionInfo* exception);

    /*
     *
     * Core File or BLOB I/O functions.
     *
     */

    /*
        Open an input or output stream for access.  May also use a stream
        provided via image_info->stream.
    */
    MagickPassFail OpenBlob(const scope ImageInfo* image_info, Image* image, const scope BlobMode mode, ExceptionInfo* exception);

    /*
        Close I/O to the file or BLOB.
    */
    void CloseBlob(Image* image);

    /*
        Read data from the file or BLOB into a buffer.
    */
    size_t ReadBlob(Image* image, const scope size_t length, void* data);

    /*
        Read data from the file or BLOB into a buffer, but support zero-copy
        if possible.
    */
    size_t ReadBlobZC(Image* image, const scope size_t length, void** data);

    /*
        Write data from a buffer to the file or BLOB.
    */
    size_t WriteBlob(Image* image, const scope size_t length, const scope void* data);

    /*
        Move the current read or write offset position in the file or BLOB.
    */
    magick_off_t SeekBlob(Image* image, const scope magick_off_t offset, const scope int whence);

    /*
        Obtain the current read or write offset position in the file or
        BLOB.
    */
    magick_off_t TellBlob(const scope Image* image);

    /*
        Test to see if EOF has been detected while reading the file or BLOB.
    */
    int EOFBlob(const scope Image* image);

    /*
        Test to see if an error has been encountered while doing I/O to the
        file or BLOB.
    */
    int GetBlobStatus(const scope Image* image);

    /*
        Obtain the current size of the file or BLOB.  Zero is returned if
        the size can not be determined.
    */
    magick_off_t GetBlobSize(const scope Image* image);


    /*
        Obtain the underlying stdio FILE* for the file (if any).
    */
    FILE* GetBlobFileHandle(const scope Image* image);

    /*
        Obtain a pointer to the base of where BLOB data is stored.  The data
        is only available if the data is stored on the heap, or is memory
        mapped.  Otherwise NULL is returned.
    */
    ubyte* GetBlobStreamData(const scope Image* image);

    /*
     *
     * Formatted File or BLOB I/O functions.
     *
     */

    /*
        Read a single byte from the file or BLOB.  Returns an EOF character
        if EOF has been detected.
    */
    int ReadBlobByte(Image* image);

    /*
        Read a 16-bit little-endian unsigned "short" value from the file
        or BLOB.
    */
    magick_uint16_t ReadBlobLSBShort(Image* image);

    /*
        Read an array of little-endian 16-bit "short" values from the file
        or BLOB.
    */
    size_t ReadBlobLSBShorts(Image* image, size_t octets, magick_uint16_t* data);

    /*
        Read a 16-bit big-endian unsigned "short" value from the file or
        BLOB.
    */
    magick_uint16_t ReadBlobMSBShort(Image* image);

    /*
        Read an array of big-endian 16-bit "short" values from the file or
        BLOB.
    */
    size_t ReadBlobMSBShorts(Image* image, size_t octets, magick_uint16_t* data);

    /*
        Read a 32-bit little-endian unsigned "long" value from the file or BLOB.
    */
    magick_uint32_t ReadBlobLSBLong(Image* image);

    /*
        Read an array of little-endian 32-bit "long" values from the file
        or BLOB.
    */
    size_t ReadBlobLSBLongs(Image* image, size_t octets, magick_uint32_t* data);

    /*
        Read a 32-bit big-endian unsigned "long" value from the file or BLOB.
    */
    magick_uint32_t ReadBlobMSBLong(Image* image);

    /*
        Read an array of big-endian 32-bit "long" values from the file or BLOB.
    */
    size_t ReadBlobMSBLongs(Image* image, size_t octets, magick_uint32_t* data);

    /*
        Read a little-endian 32-bit "float" value from the file or BLOB.
    */
    float ReadBlobLSBFloat(Image* image);

    /*
        Read an array of little-endian 32-bit "float" values from the file
        or BLOB.
    */
    size_t ReadBlobLSBFloats(Image* image, size_t octets, float* data);

    /*
        Read a big-endian 32-bit "float" value from the file or BLOB.
    */
    float ReadBlobMSBFloat(Image* image);

    /*
        Read an array of big-endian 32-bit "float" values from the file
        or BLOB.
    */
    size_t ReadBlobMSBFloats(Image* image, size_t octets, float* data);

    /*
        Read a little-endian 64-bit "double" value from the file or BLOB.
    */
    double ReadBlobLSBDouble(Image* image);

    /*
        Read an array of little-endian 64-bit "double" values from the file
        or BLOB.
    */
    size_t ReadBlobLSBDoubles(Image* image, size_t octets, double* data);

    /*
        Read a bit-endian 64-bit "double" value from the file or BLOB.
    */
    double ReadBlobMSBDouble(Image* image);

    /*
        Read an array of bit-endian 64-bit "double" values from the file
        or BLOB.
    */
    size_t ReadBlobMSBDoubles(Image* image, size_t octets, double* data);

    /*
        Read a string from the file or blob until a newline character is
        read or an end-of-file condition is encountered.
    */
    char* ReadBlobString(Image* image, char* string_);

    /*
        Write a single byte to the file or BLOB.
    */
    size_t WriteBlobByte(Image* image, const scope magick_uint8_t value);

    /*
        Write the contents of an entire disk file to the file or blob.
    */
    MagickPassFail WriteBlobFile(Image* image, const scope char* filename);

    /*
        Write a 16-bit "short" value to the  file or BLOB in little-endian order.
    */
    size_t WriteBlobLSBShort(Image* image, const scope magick_uint16_t value);

    /*
        Write a 32-bit "long" value to the file or BLOB in little-endian order.
    */
    size_t WriteBlobLSBLong(Image* image, const scope magick_uint32_t value);

    /*
        Write a 32-bit "long" value to the file or BLOB in big-endian order.
    */
    size_t WriteBlobMSBLong(Image* image, const scope magick_uint32_t value);

    /*
        Write a 16-bit "short" value to the file or BLOB in big-endian order.
    */
    size_t WriteBlobMSBShort(Image* image, const scope magick_uint16_t value);

    /*
        Write a C string to the file or BLOB, without the terminating NULL
        byte.
    */
    size_t WriteBlobString(Image* image, const scope char* string_);

    /*
     *
     * BLOB attribute access.
     *
     */

    /*
        Blob supports seek operations.  BlobSeek() and BlobTell() may safely
        be used.
    */
    MagickBool BlobIsSeekable(const scope Image* image);

    /*
        Allow file descriptor to be closed (if True).
    */
    void SetBlobClosable(Image* image, MagickBool closable);

    /*
        Blob is for a temporary file which should be deleted (if True).
    */
    void SetBlobTemporary(Image* image, MagickBool isTemporary);

    /*
        Returns MagickTrue if the file associated with the blob is a temporary
        file and should be removed when the associated image is destroyed.
    */
    MagickBool GetBlobTemporary(const scope Image* image);

    /*
     *
     * Memory mapped file support
     *
     */

    /*
        Release memory mapping for a region.
    */
    MagickPassFail UnmapBlob(void* map, const scope size_t length);

    /*
        Perform a requested memory mapping of a file descriptor.
    */
    void* MapBlob(int file, const scope MapMode mode, magick_off_t offset, size_t length);

    /*
     *
     * Buffer to File / File to Buffer functions.
     *
     */

    /*
        Writes a buffer to a named file.
    */
    MagickPassFail BlobToFile(const scope char* filename, const scope void* blob, const scope size_t length, ExceptionInfo* exception);

    /*
        Read the contents of a file into memory.
    */
    void* FileToBlob(const scope char* filename, size_t* length, ExceptionInfo *exception);

    /*
     *
     * Junk yet to be categorized.
     *
     */

    /*
        Reserve space for a specified output size.
    */
    MagickPassFail BlobReserveSize(Image* image, magick_off_t size);

    /*
        Copies data from the input stream to a file.  Useful in case it is
        necessary to perform seek operations on the input data.
    */
    MagickPassFail ImageToFile(Image *image, const scope char* filename, ExceptionInfo* exception);

    /*
        Search for a configuration file (".mgk" file) using appropriate
        rules and return as an in-memory buffer.
    */
    void* GetConfigureBlob(const scope char* filename, char* path, size_t* length, ExceptionInfo* exception);

    /*
        Converts a least-significant byte first buffer of integers to
        most-significant byte first.
    */
    void MSBOrderLong(ubyte* buffer, const scope size_t length);

    /*
        Converts a least-significant byte first buffer of integers to
        most-significant byte first.
    */
    void MSBOrderShort(ubyte* p, const scope size_t length);

} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mCloneBlobInfo = BlobInfo* function(const scope BlobInfo* blob_info);
        alias mReferenceBlob = BlobInfo* function(BlobInfo* blob);
        alias mDestroyBlobInfo = void function(BlobInfo* blob);
        alias mDetachBlob = void function(BlobInfo* blob);
        alias mGetBlobInfo = void function(BlobInfo* blob);
        alias mAttachBlob = void function(BlobInfo* blob_info, const scope void* blob, const scope size_t length);
        alias mDestroyBlob = void function(Image* image);
        alias mBlobToImage = Image* function(const scope ImageInfo* image_info, const scope void* blob, const scope size_t length,
            ExceptionInfo* exception);
        alias mPingBlob = Image* function(const scope ImageInfo* image_info, const scope void* blob, const scope size_t length,
            ExceptionInfo* exception);
        alias mImageToBlob = void* function(const scope ImageInfo* image_info, Image* image, size_t* length,
            ExceptionInfo* exception);
        alias mOpenBlob = MagickPassFail function(const scope ImageInfo* image_info, Image* image, const scope BlobMode mode,
            ExceptionInfo* exception);
        alias mCloseBlob = void function(Image* image);
        alias mReadBlob = size_t function(Image* image, const scope size_t length, void* data);
        alias mReadBlobZC = size_t function(Image* image, const scope size_t length, void** data);
        alias mWriteBlob = size_t function(Image* image, const scope size_t length, const scope void* data);
        alias mSeekBlob = magick_off_t function(Image* image, const scope magick_off_t offset, const scope int whence);
        alias mTellBlob = magick_off_t function(const scope Image* image);
        alias mEOFBlob = int function(const scope Image* image);
        alias mGetBlobStatus = int function(const scope Image* image);
        alias mGetBlobSize = magick_off_t function(const scope Image* image);
        alias mGetBlobFileHandle = FILE* function(const scope Image* image);
        alias mGetBlobStreamData = ubyte* function(const scope Image* image);
        alias mReadBlobByte = int function(Image* image);
        alias mReadBlobLSBShort = magick_uint16_t function(Image* image);
        alias mReadBlobLSBShorts = size_t function(Image* image, size_t octets, magick_uint16_t* data);
        alias mReadBlobMSBShort = magick_uint16_t function(Image* image);
        alias mReadBlobMSBShorts = size_t function(Image* image, size_t octets, magick_uint16_t* data);
        alias mReadBlobLSBLong = magick_uint32_t function(Image* image);
        alias mReadBlobLSBLongs = size_t function(Image* image, size_t octets, magick_uint32_t* data);
        alias mReadBlobMSBLong = magick_uint32_t function(Image* image);
        alias mReadBlobMSBLongs = size_t function(Image* image, size_t octets, magick_uint32_t* data);
        alias mReadBlobLSBFloat = float function(Image* image);
        alias mReadBlobLSBFloats = size_t function(Image* image, size_t octets, float* data);
        alias mReadBlobMSBFloat = float function(Image* image);
        alias mReadBlobMSBFloats = size_t function(Image* image, size_t octets, float* data);
        alias mReadBlobLSBDouble = double function(Image* image);
        alias mReadBlobLSBDoubles = size_t function(Image* image, size_t octets, double* data);
        alias mReadBlobMSBDouble = double function(Image* image);
        alias mReadBlobMSBDoubles = size_t function(Image* image, size_t octets, double* data);
        alias mReadBlobString = char* function(Image* image, char* string_);
        alias mWriteBlobByte = size_t function(Image* image, const scope magick_uint8_t value);
        alias mWriteBlobFile = MagickPassFail function(Image* image, const scope char* filename);
        alias mWriteBlobLSBShort = size_t function(Image* image, const scope magick_uint16_t value);
        alias mWriteBlobLSBLong = size_t function(Image* image, const scope magick_uint32_t value);
        alias mWriteBlobMSBLong = size_t function(Image* image, const scope magick_uint32_t value);
        alias mWriteBlobMSBShort = size_t function(Image* image, const scope magick_uint16_t value);
        alias mWriteBlobString = size_t function(Image* image, const scope char* string_);
        alias mBlobIsSeekable = MagickBool function(const scope Image* image);
        alias mSetBlobClosable = void function(Image* image, MagickBool closable);
        alias mSetBlobTemporary = void function(Image* image, MagickBool isTemporary);
        alias mGetBlobTemporary = MagickBool function(const scope Image* image);
        alias mUnmapBlob = MagickPassFail function(void* map, const scope size_t length);
        alias mMapBlob = void* function(int file, const scope MapMode mode, magick_off_t offset, size_t length);
        alias mBlobToFile = MagickPassFail function(const scope char* filename, const scope void* blob, const scope size_t length,
            ExceptionInfo* exception);
        alias mFileToBlob = void* function(const scope char* filename, size_t* length, ExceptionInfo *exception);
        alias mBlobReserveSize = MagickPassFail function(Image* image, magick_off_t size);
        alias mImageToFile = MagickPassFail function(Image *image, const scope char* filename, ExceptionInfo* exception);
        alias mGetConfigureBlob = void* function(const scope char* filename, char* path, size_t* length,
            ExceptionInfo* exception);
        alias mMSBOrderLong = void function(ubyte* buffer, const scope size_t length);
        alias mMSBOrderShort = void function(ubyte* p, const scope size_t length);
    }

    __gshared
    {
        mCloneBlobInfo CloneBlobInfo;
        mReferenceBlob ReferenceBlob;
        mDestroyBlobInfo DestroyBlobInfo;
        mDetachBlob DetachBlob;
        mGetBlobInfo GetBlobInfo;
        mAttachBlob AttachBlob;
        mDestroyBlob DestroyBlob;
        mBlobToImage BlobToImage;
        mPingBlob PingBlob;
        mImageToBlob ImageToBlob;
        mOpenBlob OpenBlob;
        mCloseBlob CloseBlob;
        mReadBlob ReadBlob;
        mReadBlobZC ReadBlobZC;
        mWriteBlob WriteBlob;
        mSeekBlob SeekBlob;
        mTellBlob TellBlob;
        mEOFBlob EOFBlob;
        mGetBlobStatus GetBlobStatus;
        mGetBlobSize GetBlobSize;
        mGetBlobFileHandle GetBlobFileHandle;
        mGetBlobStreamData GetBlobStreamData;
        mReadBlobByte ReadBlobByte;
        mReadBlobLSBShort ReadBlobLSBShort;
        mReadBlobLSBShorts ReadBlobLSBShorts;
        mReadBlobMSBShort ReadBlobMSBShort;
        mReadBlobMSBShorts ReadBlobMSBShorts;
        mReadBlobLSBLong ReadBlobLSBLong;
        mReadBlobLSBLongs ReadBlobLSBLongs;
        mReadBlobMSBLong ReadBlobMSBLong;
        mReadBlobMSBLongs ReadBlobMSBLongs;
        mReadBlobLSBFloat ReadBlobLSBFloat;
        mReadBlobLSBFloats ReadBlobLSBFloats;
        mReadBlobMSBFloat ReadBlobMSBFloat;
        mReadBlobMSBFloats ReadBlobMSBFloats;
        mReadBlobLSBDouble ReadBlobLSBDouble;
        mReadBlobLSBDoubles ReadBlobLSBDoubles;
        mReadBlobMSBDouble ReadBlobMSBDouble;
        mReadBlobMSBDoubles ReadBlobMSBDoubles;
        mReadBlobString ReadBlobString;
        mWriteBlobByte WriteBlobByte;
        mWriteBlobFile WriteBlobFile;
        mWriteBlobLSBShort WriteBlobLSBShort;
        mWriteBlobLSBLong WriteBlobLSBLong;
        mWriteBlobMSBLong WriteBlobMSBLong;
        mWriteBlobMSBShort WriteBlobMSBShort;
        mWriteBlobString WriteBlobString;
        mBlobIsSeekable BlobIsSeekable;
        mSetBlobClosable SetBlobClosable;
        mSetBlobTemporary SetBlobTemporary;
        mGetBlobTemporary GetBlobTemporary;
        mUnmapBlob UnmapBlob;
        mMapBlob MapBlob;
        mBlobToFile BlobToFile;
        mFileToBlob FileToBlob;
        mBlobReserveSize BlobReserveSize;
        mImageToFile ImageToFile;
        mGetConfigureBlob GetConfigureBlob;
        mMSBOrderLong MSBOrderLong;
        mMSBOrderShort MSBOrderShort;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadBlobH(void* dlib)
        {
            char *errmsg;
            bool success = true;

            CloneBlobInfo = cast(mCloneBlobInfo)dlsym(dlib, "CloneBlobInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReferenceBlob = cast(mReferenceBlob)dlsym(dlib, "ReferenceBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyBlobInfo = cast(mDestroyBlobInfo)dlsym(dlib, "DestroyBlobInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            DetachBlob = cast(mDetachBlob)dlsym(dlib, "DetachBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobInfo = cast(mGetBlobInfo)dlsym(dlib, "GetBlobInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            AttachBlob = cast(mAttachBlob)dlsym(dlib, "AttachBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            DestroyBlob = cast(mDestroyBlob)dlsym(dlib, "DestroyBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlobToImage = cast(mBlobToImage)dlsym(dlib, "BlobToImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            PingBlob = cast(mPingBlob)dlsym(dlib, "PingBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImageToBlob = cast(mImageToBlob)dlsym(dlib, "ImageToBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            OpenBlob = cast(mOpenBlob)dlsym(dlib, "OpenBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            CloseBlob = cast(mCloseBlob)dlsym(dlib, "CloseBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlob = cast(mReadBlob)dlsym(dlib, "ReadBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobZC = cast(mReadBlobZC)dlsym(dlib, "ReadBlobZC");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlob = cast(mWriteBlob)dlsym(dlib, "WriteBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            SeekBlob = cast(mSeekBlob)dlsym(dlib, "SeekBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            TellBlob = cast(mTellBlob)dlsym(dlib, "TellBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            EOFBlob = cast(mEOFBlob)dlsym(dlib, "EOFBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobStatus = cast(mGetBlobStatus)dlsym(dlib, "GetBlobStatus");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobSize = cast(mGetBlobSize)dlsym(dlib, "GetBlobSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobFileHandle = cast(mGetBlobFileHandle)dlsym(dlib, "GetBlobFileHandle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobStreamData = cast(mGetBlobStreamData)dlsym(dlib, "GetBlobStreamData");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobByte = cast(mReadBlobByte)dlsym(dlib, "ReadBlobByte");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBShort = cast(mReadBlobLSBShort)dlsym(dlib, "ReadBlobLSBShort");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBShorts = cast(mReadBlobLSBShorts)dlsym(dlib, "ReadBlobLSBShorts");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBShort = cast(mReadBlobMSBShort)dlsym(dlib, "ReadBlobMSBShort");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBShorts = cast(mReadBlobMSBShorts)dlsym(dlib, "ReadBlobMSBShorts");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBLong = cast(mReadBlobLSBLong)dlsym(dlib, "ReadBlobLSBLong");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBLongs = cast(mReadBlobLSBLongs)dlsym(dlib, "ReadBlobLSBLongs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBLong = cast(mReadBlobMSBLong)dlsym(dlib, "ReadBlobMSBLong");
            if (null !is (errmsg = dlerror())) {
                fprintf(stderr, "ERROR(dlsym): %s\n", errmsg);
                success = false;
            }
            ReadBlobMSBLongs = cast(mReadBlobMSBLongs)dlsym(dlib, "ReadBlobMSBLongs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBFloat = cast(mReadBlobLSBFloat)dlsym(dlib, "ReadBlobLSBFloat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBFloats = cast(mReadBlobLSBFloats)dlsym(dlib, "ReadBlobLSBFloats");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBFloat = cast(mReadBlobMSBFloat)dlsym(dlib, "ReadBlobMSBFloat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBFloats = cast(mReadBlobMSBFloats)dlsym(dlib, "ReadBlobMSBFloats");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBDouble = cast(mReadBlobLSBDouble)dlsym(dlib, "ReadBlobLSBDouble");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobLSBDoubles = cast(mReadBlobLSBDoubles)dlsym(dlib, "ReadBlobLSBDoubles");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBDouble = cast(mReadBlobMSBDouble)dlsym(dlib, "ReadBlobMSBDouble");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobMSBDoubles = cast(mReadBlobMSBDoubles)dlsym(dlib, "ReadBlobMSBDoubles");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ReadBlobString = cast(mReadBlobString)dlsym(dlib, "ReadBlobString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobByte = cast(mWriteBlobByte)dlsym(dlib, "WriteBlobByte");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobFile = cast(mWriteBlobFile)dlsym(dlib, "WriteBlobFile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobLSBShort = cast(mWriteBlobLSBShort)dlsym(dlib, "WriteBlobLSBShort");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobLSBLong = cast(mWriteBlobLSBLong)dlsym(dlib, "WriteBlobLSBLong");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobMSBLong = cast(mWriteBlobMSBLong)dlsym(dlib, "WriteBlobMSBLong");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobMSBShort = cast(mWriteBlobMSBShort)dlsym(dlib, "WriteBlobMSBShort");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            WriteBlobString = cast(mWriteBlobString)dlsym(dlib, "WriteBlobString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlobIsSeekable = cast(mBlobIsSeekable)dlsym(dlib, "BlobIsSeekable");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetBlobClosable = cast(mSetBlobClosable)dlsym(dlib, "SetBlobClosable");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            SetBlobTemporary = cast(mSetBlobTemporary)dlsym(dlib, "SetBlobTemporary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetBlobTemporary = cast(mGetBlobTemporary)dlsym(dlib, "GetBlobTemporary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            UnmapBlob = cast(mUnmapBlob)dlsym(dlib, "UnmapBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            MapBlob = cast(mMapBlob)dlsym(dlib, "MapBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlobToFile = cast(mBlobToFile)dlsym(dlib, "BlobToFile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            FileToBlob = cast(mFileToBlob)dlsym(dlib, "FileToBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            BlobReserveSize = cast(mBlobReserveSize)dlsym(dlib, "BlobReserveSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            ImageToFile = cast(mImageToFile)dlsym(dlib, "ImageToFile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetConfigureBlob = cast(mGetConfigureBlob)dlsym(dlib, "GetConfigureBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            MSBOrderLong = cast(mMSBOrderLong)dlsym(dlib, "MSBOrderLong");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }
            MSBOrderShort = cast(mMSBOrderShort)dlsym(dlib, "MSBOrderShort");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.blob:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
