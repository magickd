/*
  Copyright (C) 2003, 2004, 2007 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Application Programming Interface declarations.

*/
// WIP graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.api;

enum MaxTextExtent = 2053;
enum MagickSignature = 0xabacadabUL;

alias MagickPassFail = uint;
enum MagickPass = 1;
enum MagickFail = 0;

alias MagickBool = uint;
enum MagickTrue = 1;
enum MagickFalse = 0;

public
{
    import graphicsmagick_c.magick.magick_types;
    import graphicsmagick_c.magick.error;
    import graphicsmagick_c.magick.log;
    import graphicsmagick_c.magick.timer;
    import graphicsmagick_c.magick.image;
    import graphicsmagick_c.magick.channel;
    import graphicsmagick_c.magick.compare;
    import graphicsmagick_c.magick.list;
    import graphicsmagick_c.magick.paint;
    import graphicsmagick_c.magick.render;
    import graphicsmagick_c.magick.draw;
    import graphicsmagick_c.magick.gem;
    import graphicsmagick_c.magick.quantize;
    import graphicsmagick_c.magick.compress;
    import graphicsmagick_c.magick.attribute;
    import graphicsmagick_c.magick.command;
    import graphicsmagick_c.magick.utility;
    import graphicsmagick_c.magick.signature;
    import graphicsmagick_c.magick.blob;
    import graphicsmagick_c.magick.color;
    import graphicsmagick_c.magick.constitute;
    import graphicsmagick_c.magick.decorate;
    import graphicsmagick_c.magick.enhance;
    import graphicsmagick_c.magick.fx;
    import graphicsmagick_c.magick.magick;
    import graphicsmagick_c.magick.memory;
    import graphicsmagick_c.magick.montage;
    import graphicsmagick_c.magick.effect;
    import graphicsmagick_c.magick.resize;
    import graphicsmagick_c.magick.shear;
    import graphicsmagick_c.magick.transform;
    import graphicsmagick_c.magick.composite;
    import graphicsmagick_c.magick.registry;
    import graphicsmagick_c.magick.magic;
    import graphicsmagick_c.magick.delegate_;
    import graphicsmagick_c.magick.module_;
    import graphicsmagick_c.magick.monitor;
    import graphicsmagick_c.magick.operator;
    import graphicsmagick_c.magick.pixel_cache;
    import graphicsmagick_c.magick.pixel_iterator;
    import graphicsmagick_c.magick.profile;
    import graphicsmagick_c.magick.resource;
    import graphicsmagick_c.magick.version_;
    import graphicsmagick_c.magick.deprecate;
}
