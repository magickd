/*
  Copyright (C) 2004, 2008 GraphicsMagick Group

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Interfaces to support simple iterative pixel read/update access
  within an image or between two images.

  WARNING!  These interfaces are still subject to change. WARNING!

  Written by Bob Friesenhahn, March 2004, Updated for regions 2008.
 
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.pixel_iterator;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : Image, IndexPacket, PixelPacket;

struct _PixelIteratorOptions
{
    int max_threads;
    c_ulong signature;
}
alias PixelIteratorOptions = _PixelIteratorOptions;

/*
  Read-only access across pixel region.
*/
alias PixelIteratorMonoReadCallback = @nogc nothrow extern(C) MagickPassFail function(void* mutable_data,
    const scope void* immutable_data, const scope Image* const_image, const scope PixelPacket* pixels, const scope IndexPacket* indexes, const scope c_long npixels,
    ExceptionInfo* exception);
/*
  Read-write access across pixel region.
*/
alias PixelIteratorMonoModifyCallback = @nogc nothrow extern(C) MagickPassFail function(void* mutable_data,
    const scope void* immutable_data, Image* image, PixelPacket* pixels, IndexPacket* indexes, const scope c_long npixels,
    ExceptionInfo* exception);
/*
  Read-only access across pixel regions of two images.
*/
alias PixelIteratorDualReadCallback = @nogc nothrow extern(C) MagickPassFail function(void* mutable_data,
    const scope void* immutable_data, const scope Image* first_image, const scope PixelPacket* first_pixels, const scope IndexPacket* first_indexes,
    const scope Image* second_image, const scope PixelPacket* second_pixels, const scope IndexPacket* second_indexes, const scope c_long npixels,
    ExceptionInfo* exception);
/*
  Read-write access across pixel regions of two images. The first
  (source) image is accessed read-only while the second (update)
  image is accessed as read-write.
*/
alias PixelIteratorDualModifyCallback = @nogc nothrow extern(C) MagickPassFail function(void* mutable_data,
    const scope void* immutable_data, const scope Image* source_image, const scope PixelPacket* source_pixels, const scope IndexPacket* source_indexes,
    Image* update_image, PixelPacket* update_pixels, IndexPacket* update_indexes, const scope c_long npixels,
    ExceptionInfo* exception);
/*
  Read-write access across pixel regions of two images. The first
  (source) image is accessed read-only while the second (new)
  image is accessed for write (uninitialized pixels).
*/
alias PixelIteratorDualNewCallback = PixelIteratorDualModifyCallback;
/*
  Read-read-write access across pixel regions of three images. The
  first two images are accessed read-only while the third is
  accessed as read-write.
*/
alias PixelIteratorTripleModifyCallback = @nogc nothrow extern(C) MagickPassFail function(void* mutable_data,
    const scope void* immutable_data, const scope Image* source1_image, const scope PixelPacket* source1_pixels, const scope IndexPacket* source1_indexes,
    const scope Image* source2_image, const scope PixelPacket* source2_pixels, const scope IndexPacket* source2_indexes,
    Image* update_image, PixelPacket* update_pixels, IndexPacket* update_indexes, const scope c_long npixels,
    ExceptionInfo* exception);
/*
  Read-write access across pixel regions of two images. The first
  (source) image is accessed read-only while the second (new)
  image is accessed for write (uninitialized pixels).
*/
alias PixelIteratorTripleNewCallback = PixelIteratorTripleModifyCallback;

version(GMagick_Static)
{
    @system @nogc nothrow extern(C):

    void InitializePixelIteratorOptions(PixelIteratorOptions* options, ExceptionInfo* exception);

    MagickPassFail PixelIterateMonoRead(PixelIteratorMonoReadCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_long x,
        const scope c_long y,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* image,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateMonoModify(PixelIteratorMonoModifyCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_long x,
        const scope c_long y,
        const scope c_ulong columns,
        const scope c_ulong rows,
        Image* image,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateDualRead(PixelIteratorDualReadCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* first_image,
        const scope c_long first_x,
        const scope c_long first_y,
        const scope Image* second_image,
        const scope c_long second_x,
        const scope c_long second_y,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateDualModify(PixelIteratorDualModifyCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* source_image,
        const scope c_long source_x,
        const scope c_long source_y,
        Image* update_image,
        const scope c_long update_x,
        const scope c_long update_y,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateDualNew(PixelIteratorDualNewCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* source_image,
        const scope c_long source_x,
        const scope c_long source_y,
        Image* new_image,
        const scope c_long new_x,
        const scope c_long new_y,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateTripleModify(PixelIteratorTripleModifyCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* source1_image,
        const scope Image* source2_image,
        const scope c_long source_x,
        const scope c_long source_y,
        Image* update_image,
        const scope c_long update_x,
        const scope c_long update_y,
        ExceptionInfo* exception);

    MagickPassFail PixelIterateTripleNew(PixelIteratorTripleNewCallback call_back,
        const scope PixelIteratorOptions* options,
        const scope char* description,
        void* mutable_data,
        const scope void* immutable_data,
        const scope c_ulong columns,
        const scope c_ulong rows,
        const scope Image* source1_image,
        const scope Image* source2_image,
        const scope c_long source_x,
        const scope c_long source_y,
        Image* new_image,
        const scope c_long new_x,
        const scope c_long new_y,
        ExceptionInfo* exception);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mInitializePixelIteratorOptions = void function(PixelIteratorOptions* options, ExceptionInfo* exception);

        alias mPixelIterateMonoRead = MagickPassFail function(PixelIteratorMonoReadCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_long x,
            const scope c_long y,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* image,
            ExceptionInfo* exception);

        alias mPixelIterateMonoModify = MagickPassFail function(PixelIteratorMonoModifyCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_long x,
            const scope c_long y,
            const scope c_ulong columns,
            const scope c_ulong rows,
            Image* image,
            ExceptionInfo* exception);

        alias mPixelIterateDualRead = MagickPassFail function(PixelIteratorDualReadCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* first_image,
            const scope c_long first_x,
            const scope c_long first_y,
            const scope Image* second_image,
            const scope c_long second_x,
            const scope c_long second_y,
            ExceptionInfo* exception);

        alias mPixelIterateDualModify = MagickPassFail function(PixelIteratorDualModifyCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* source_image,
            const scope c_long source_x,
            const scope c_long source_y,
            Image* update_image,
            const scope c_long update_x,
            const scope c_long update_y,
            ExceptionInfo* exception);

        alias mPixelIterateDualNew = MagickPassFail function(PixelIteratorDualNewCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* source_image,
            const scope c_long source_x,
            const scope c_long source_y,
            Image* new_image,
            const scope c_long new_x,
            const scope c_long new_y,
            ExceptionInfo* exception);

        alias mPixelIterateTripleModify = MagickPassFail function(PixelIteratorTripleModifyCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* source1_image,
            const scope Image* source2_image,
            const scope c_long source_x,
            const scope c_long source_y,
            Image* update_image,
            const scope c_long update_x,
            const scope c_long update_y,
            ExceptionInfo* exception);

        alias mPixelIterateTripleNew = MagickPassFail function(PixelIteratorTripleNewCallback call_back,
            const scope PixelIteratorOptions* options,
            const scope char* description,
            void* mutable_data,
            const scope void* immutable_data,
            const scope c_ulong columns,
            const scope c_ulong rows,
            const scope Image* source1_image,
            const scope Image* source2_image,
            const scope c_long source_x,
            const scope c_long source_y,
            Image* new_image,
            const scope c_long new_x,
            const scope c_long new_y,
            ExceptionInfo* exception);
    }

    __gshared
    {
        mInitializePixelIteratorOptions InitializePixelIteratorOptions;

        mPixelIterateMonoRead PixelIterateMonoRead;
        mPixelIterateMonoModify PixelIterateMonoModify;
        mPixelIterateDualRead PixelIterateDualRead;
        mPixelIterateDualModify PixelIterateDualModify;
        mPixelIterateDualNew PixelIterateDualNew;
        mPixelIterateTripleModify PixelIterateTripleModify;
        mPixelIterateTripleNew PixelIterateTripleNew;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadPixelIteratorH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            InitializePixelIteratorOptions =
                cast(mInitializePixelIteratorOptions)dlsym(dlib, "InitializePixelIteratorOptions");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }

            PixelIterateMonoRead = cast(mPixelIterateMonoRead)dlsym(dlib, "PixelIterateMonoRead");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateMonoModify = cast(mPixelIterateMonoModify)dlsym(dlib, "PixelIterateMonoModify");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateDualRead = cast(mPixelIterateDualRead)dlsym(dlib, "PixelIterateDualRead");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateDualModify = cast(mPixelIterateDualModify)dlsym(dlib, "PixelIterateDualModify");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateDualNew = cast(mPixelIterateDualNew)dlsym(dlib, "PixelIterateDualNew");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateTripleModify = cast(mPixelIterateTripleModify)dlsym(dlib, "PixelIterateTripleModify");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelIterateTripleNew = cast(mPixelIterateTripleNew)dlsym(dlib, "PixelIterateTripleNew");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.pixel_iterator:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
