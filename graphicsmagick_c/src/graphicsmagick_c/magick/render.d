/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  Drawing methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.render;

import core.stdc.config;
import core.stdc.stdio;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error;
import graphicsmagick_c.magick.image;

alias AlignType = int;
enum : AlignType
{
    UndefinedAlign,
    LeftAlign,
    CenterAlign,
    RightAlign
}

alias ClipPathUnits = int;
enum : ClipPathUnits
{
    UserSpace,
    UserSpaceOnUse,
    ObjectBoundingBox
}

alias DecorationType = int;
enum : DecorationType
{
    NoDecoration,
    UnderlineDecoration,
    OverlineDecoration,
    LineThroughDecoration
}

alias FillRule = int;
enum : FillRule
{
    UndefinedRule,
    EvenOddRule,
    NonZeroRule
}

alias GradientType = int;
enum : GradientType
{
    UndefinedGradient,
    LinearGradient,
    RadialGradient
}

alias LineCap = int;
enum : LineCap
{
    UndefinedCap,
    ButtCap,
    RoundCap,
    SquareCap
}

alias LineJoin = int;
enum : LineJoin
{
    UndefinedJoin,
    MiterJoin,
    RoundJoin,
    BevelJoin
}

alias PaintMethod = int;
enum : PaintMethod
{
    PointMethod = 0,
    ReplaceMethod,
    FloodfillMethod,
    FillToBorderMethod,
    ResetMethod,
}

alias PrimitiveType = int;
enum : PrimitiveType
{
    UndefinedPrimitive = 0,
    PointPrimitive,
    LinePrimitive,
    RectanglePrimitive,
    RoundRectanglePrimitive,
    ArcPrimitive,
    EllipsePrimitive,
    CirclePrimitive,
    PolylinePrimitive,
    PolygonPrimitive,
    BezierPrimitive,
    ColorPrimitive,
    MattePrimitive,
    TextPrimitive,
    ImagePrimitive,
    PathPrimitive
}

alias ReferenceType = int;
enum : ReferenceType
{
    UndefinedReference,
    GradientReference
}

alias SpreadMethod = int;
enum : SpreadMethod
{
    UndefinedSpread,
    PadSpread,
    ReflectSpead,
    RepeatSpread
}

alias StretchType = int;
enum : StretchType
{
    NormalStretch,
    UltraCondensedStretch,
    ExtraCondensedStretch,
    CondensedStretch,
    SemiCondensedStretch,
    SemiExpandedStretch,
    ExpandedStretch,
    ExtraExpandedStretch,
    UltraExpandedStretch,
    AnyStretch
}

alias StyleType = int;
enum : StyleType
{
    NormalStyle,
    ItalicStyle,
    ObliqueStyle,
    AnyStyle
}

/*
  Typedef declarations.
*/
struct _GradientInfo
{
    GradientType type;

    PixelPacket color;

    SegmentInfo stop;

    c_ulong length;

    SpreadMethod spread;

    c_ulong signature;

    _GradientInfo* previous;
    _GradientInfo* next;
}
alias GradientInfo = _GradientInfo;

struct _ElementReference
{
    char* id;

    ReferenceType type;

    GradientInfo gradient;

    c_ulong signature;

    _ElementReference* previous;
    _ElementReference* next;
}
alias ElementReference = _ElementReference;

struct _DrawInfo
{
    char* primitive;
    char* geometry;

    AffineMatrix affine;

    GravityType gravity;

    PixelPacket fill;
    PixelPacket stroke;

    double stroke_width;

    GradientInfo gradient;

    Image* fill_pattern;
    Image* tile;
    Image* stroke_pattern;

    uint stroke_antialias;
    uint text_antialias;

    FillRule fill_rule;

    LineCap line_cap;

    LineJoin line_join;

    c_ulong miterlimit;

    double dash_offset;

    DecorationType decorate;

    CompositeOperator compose;

    char* text;
    char* font;
    char* family;

    StyleType style;

    StretchType stretch;

    c_ulong weight;

    char* encoding;

    double pointsize;

    char* density;

    pragma(mangle, "align") AlignType align_;

    PixelPacket undercolor;
    PixelPacket border_color;

    char* server_name;

    double* dash_pattern;

    char* clip_path;

    SegmentInfo bounds;

    ClipPathUnits clip_units;

    Quantum opacity;

    uint render;
    pragma(mangle, "debug") uint debug_; /* deprecated */

    ElementReference element_reference;

    c_ulong signature;
}
alias DrawInfo = _DrawInfo;

struct _ElementInfo
{
    double cx;
    double cy;
    double major;
    double minor;
    double angle;
}
alias ElementInfo = _ElementInfo;

struct _PointInfo
{
    double x;
    double y;
}
alias PointInfo = _PointInfo;

struct _PrimitiveInfo
{
    PointInfo point;

    c_ulong coordinates;

    PrimitiveType primitive;

    PaintMethod method;

    char* text;
}
alias PrimitiveInfo = _PrimitiveInfo;

struct _TypeInfo
{
    char* path;
    char* name;
    char* description;
    char* family;

    StyleType style;

    StretchType stretch;

    c_ulong weight;

    char* encoding;
    char* foundry;
    char* metrics;
    char* glyphs;

    uint stealth;

    c_ulong signature;

    _TypeInfo* previous;
    _TypeInfo* next;
}
alias TypeInfo = _TypeInfo;

struct _TypeMetric
{
    PointInfo pixels_per_em;

    double ascent;
    double descent;
    double width;
    double height;
    double max_advance;

    SegmentInfo bounds;

    double underline_position;
    double underline_thickness;
}
alias TypeMetric = _TypeMetric;

/*
  Method declarations.
*/

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char** GetTypeList(const scope char*, c_ulong*);

    const(TypeInfo)* GetTypeInfo(const scope char*, ExceptionInfo*);
    const(TypeInfo)* GetTypeInfoByFamily(const scope char*, const scope StyleType, const scope StretchType,
            const scope c_ulong, ExceptionInfo*);

    DrawInfo* CloneDrawInfo(const scope ImageInfo*, const scope DrawInfo*);

    MagickPassFail AnnotateImage(Image*, const scope DrawInfo*);
    MagickPassFail DrawAffineImage(Image*, const scope Image*, const scope AffineMatrix*);
    MagickPassFail DrawClipPath(Image*, const scope DrawInfo*, const scope char*);
    MagickPassFail DrawImage(Image*, const scope DrawInfo*);
    MagickPassFail DrawPatternPath(Image*, const scope DrawInfo*, const scope char*, Image**);
    MagickPassFail GetTypeMetrics(Image*, const scope DrawInfo*, TypeMetric*);
    MagickPassFail ListTypeInfo(FILE*, ExceptionInfo*);

    void DestroyDrawInfo(DrawInfo*);
    void DestroyTypeInfo();
    void GetDrawInfo(const scope ImageInfo*, DrawInfo*);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mGetTypeList = char** function(const scope char*, c_ulong*);

        alias mGetTypeInfo = const(TypeInfo)* function(const scope char*, ExceptionInfo*);
        alias mGetTypeInfoByFamily = const(TypeInfo)* function(const scope char*, const scope StyleType, const scope StretchType,
            const scope c_ulong, ExceptionInfo*);

        alias mCloneDrawInfo = DrawInfo* function(const scope ImageInfo*, const scope DrawInfo*);

        alias mAnnotateImage = MagickPassFail function(Image*, const scope DrawInfo*);
        alias mDrawAffineImage = MagickPassFail function(Image*, const scope Image*, const scope AffineMatrix*);
        alias mDrawClipPath = MagickPassFail function(Image*, const scope DrawInfo*, const scope char*);
        alias mDrawImage = MagickPassFail function(Image*, const scope DrawInfo*);
        alias mDrawPatternPath = MagickPassFail function(Image*, const scope DrawInfo*, const scope char*, Image**);
        alias mGetTypeMetrics = MagickPassFail function(Image*, const scope DrawInfo*, TypeMetric*);
        alias mListTypeInfo = MagickPassFail function(FILE*, ExceptionInfo*);

        alias mDestroyDrawInfo = void function(DrawInfo*);
        alias mDestroyTypeInfo = void function();
        alias mGetDrawInfo = void function(const scope ImageInfo*, DrawInfo*);
    }

    __gshared
    {
        mGetTypeList GetTypeList;

        mGetTypeInfo GetTypeInfo;
        mGetTypeInfoByFamily GetTypeInfoByFamily;

        mCloneDrawInfo CloneDrawInfo;

        mAnnotateImage AnnotateImage;
        mDrawAffineImage DrawAffineImage;
        mDrawClipPath DrawClipPath;
        mDrawImage DrawImage;
        mDrawPatternPath DrawPatternPath;
        mGetTypeMetrics GetTypeMetrics;
        mListTypeInfo ListTypeInfo;

        mDestroyDrawInfo DestroyDrawInfo;
        mDestroyTypeInfo DestroyTypeInfo;
        mGetDrawInfo GetDrawInfo;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn;
        import core.stdc.stdio;

        bool _loadRenderH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            GetTypeList = cast(mGetTypeList)dlsym(dlib, "GetTypeList");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }

            GetTypeInfo = cast(mGetTypeInfo)dlsym(dlib, "GetTypeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetTypeInfoByFamily = cast(mGetTypeInfoByFamily)dlsym(dlib, "GetTypeInfoByFamily");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneDrawInfo = cast(mCloneDrawInfo)dlsym(dlib, "CloneDrawInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }

            AnnotateImage = cast(mAnnotateImage)dlsym(dlib, "AnnotateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawAffineImage = cast(mDrawAffineImage)dlsym(dlib, "DrawAffineImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawClipPath = cast(mDrawClipPath)dlsym(dlib, "DrawClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawImage = cast(mDrawImage)dlsym(dlib, "DrawImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPatternPath = cast(mDrawPatternPath)dlsym(dlib, "DrawPatternPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetTypeMetrics = cast(mGetTypeMetrics)dlsym(dlib, "GetTypeMetrics");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            ListTypeInfo = cast(mListTypeInfo)dlsym(dlib, "ListTypeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyDrawInfo = cast(mDestroyDrawInfo)dlsym(dlib, "DestroyDrawInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            DestroyTypeInfo = cast(mDestroyTypeInfo)dlsym(dlib, "DestroyTypeInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }
            GetDrawInfo = cast(mGetDrawInfo)dlsym(dlib, "GetDrawInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.render:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
