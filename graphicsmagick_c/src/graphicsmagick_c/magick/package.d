/*
    File:           graphicsmagick_c/magick/package.d
    Contains:       Package file for graphicsmagick_c.magick
    Copyright:      (C) 2022 kaerou <stigma@disroot.org>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/
module graphicsmagick_c.magick;

public
{
    import graphicsmagick_c.magick.api;
    import graphicsmagick_c.magick.blob;
    import graphicsmagick_c.magick.channel;
    import graphicsmagick_c.magick.color;
    import graphicsmagick_c.magick.colorspace;
    import graphicsmagick_c.magick.command;
    import graphicsmagick_c.magick.compare;
    import graphicsmagick_c.magick.composite;
    import graphicsmagick_c.magick.compress;
    import graphicsmagick_c.magick.constitute;
    import graphicsmagick_c.magick.decorate;
    import graphicsmagick_c.magick.delegate_;
    import graphicsmagick_c.magick.deprecate;
    import graphicsmagick_c.magick.draw;
    import graphicsmagick_c.magick.effect;
    import graphicsmagick_c.magick.enhance;
    import graphicsmagick_c.magick.error;
    import graphicsmagick_c.magick.forward;
    import graphicsmagick_c.magick.fx;
    import graphicsmagick_c.magick.gem;
    import graphicsmagick_c.magick.image;
    import graphicsmagick_c.magick.list;
    import graphicsmagick_c.magick.log;
    import graphicsmagick_c.magick.magic;
    import graphicsmagick_c.magick.magick_types;
    import graphicsmagick_c.magick.magick;
    import graphicsmagick_c.magick.memory;
    import graphicsmagick_c.magick.module_;
    import graphicsmagick_c.magick.monitor;
    import graphicsmagick_c.magick.montage;
    import graphicsmagick_c.magick.operator;
    import graphicsmagick_c.magick.paint;
    import graphicsmagick_c.magick.pixel_cache;
    import graphicsmagick_c.magick.pixel_iterator;
    import graphicsmagick_c.magick.profile;
    import graphicsmagick_c.magick.quantize;
    import graphicsmagick_c.magick.registry;
    import graphicsmagick_c.magick.render;
    import graphicsmagick_c.magick.resize;
    import graphicsmagick_c.magick.resource;
    import graphicsmagick_c.magick.shear;
    import graphicsmagick_c.magick.signature;
    import graphicsmagick_c.magick.timer;
    import graphicsmagick_c.magick.transform;
    import graphicsmagick_c.magick.utility;
    import graphicsmagick_c.magick.version_;
}
