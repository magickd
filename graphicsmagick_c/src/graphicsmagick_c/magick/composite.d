/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Image Composite Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.composite;

import core.stdc.config : c_long, c_ulong;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.error : ExceptionInfo;
import graphicsmagick_c.magick.image : CompositeOperator, Image;

/*
    Special options required by some composite operators.
*/
struct _CompositeOptions_t
{
    /* ModulateComposite */
    double percent_brightness;

    /* ThresholdComposite */
    double amount;
    double threshold;
}
alias CompositeOptions_t = _CompositeOptions_t;

version (GMagick_Static)
{
    @system @nogc nothrow extern(C):

    MagickPassFail CompositeImage(Image* canvas_image, const scope CompositeOperator compose,
                                  const scope Image* update_image,
                                  const scope c_long x_offset, const scope c_long y_offset);
    MagickPassFail CompositeImageRegion(const scope CompositeOperator compose,
                                        const scope CompositeOptions_t* options,
                                        const scope c_ulong columns,
                                        const scope c_ulong rows,
                                        const scope Image* update_image,
                                        const scope c_long update_x,
                                        const scope c_long update_y,
                                        Image* canvas_image,
                                        const scope c_long canvas_x,
                                        const scope c_long canvas_y,
                                        ExceptionInfo* exception);
}
else
{
    @system @nogc nothrow extern(C)
    {
        alias mCompositeImage = MagickPassFail function(Image* canvas_image, const scope CompositeOperator compose,
                                                        const scope Image* update_image,
                                                        const scope c_long x_offset, const scope c_long y_offset);
        alias mCompositeImageRegion = MagickPassFail function(const scope CompositeOperator compose,
                                                              const scope CompositeOptions_t* options,
                                                              const scope c_ulong columns,
                                                              const scope c_ulong rows,
                                                              const scope Image* update_image,
                                                              const scope c_long update_x,
                                                              const scope c_long update_y,
                                                              Image* canvas_image,
                                                              const scope c_long canvas_x,
                                                              const scope c_long canvas_y,
                                                              ExceptionInfo* exception);
    }

    __gshared
    {
        mCompositeImage CompositeImage;
        mCompositeImageRegion CompositeImageRegion;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadCompositeH(void* dlib)
        {
            char* errmsg;
            bool success = true;

            CompositeImage = cast(mCompositeImage)dlsym(dlib, "CompositeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.composite:\n  %s\n", errmsg);
                }
                success = false;
            }
            CompositeImageRegion = cast(mCompositeImageRegion)dlsym(dlib, "CompositeImageRegion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.magick.composite:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
