/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2002 ImageMagick Studio
  Copyright 1991-1999 E. I. du Pont de Nemours and Company

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Colorspace Methods.
*/

// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.magick.colorspace;

import graphicsmagick_c.magick.api : MagickPassFail;
import graphicsmagick_c.magick.forward;
import graphicsmagick_c.magick.image : PixelPacket, Quantum;

version(Q8)       enum QuantumDepth = 8;
else version(Q16) enum QuantumDepth = 16;
else version(Q32) enum QuantumDepth = 32;
else              enum QuantumDepth = -1;

extern(C):
nothrow:
@nogc:

static if(QuantumDepth == 8 || QuantumDepth == 16)
{
	/*
	 * intensity=0.299*red+0.587*green+0.114*blue.
	 * Premultiply by 1024 to obtain integral values, and then divide
	 * rersult by 1024 by shifting to the right by 10 bits.
	 */
	pragma(inline, true)
	extern(D) uint PixelIntensityRec601(const scope PixelPacket* pixel) pure @trusted
	{
		return cast(uint)((cast(uint)pixel.red*306U +
						   cast(uint)pixel.green*601U +
						   cast(uint)pixel.blue*117U) >> 10U);
	}
}
else static if(QuantumDepth == 32)
{
	/*
	 * intensity=0.299*red+0.587*green+0.114*blue
	 */
	pragma(inline, true)
	extern(D) uint PixelIntensityRec601(const scope PixelPacket* pixel) pure @trusted
	{
		return cast(uint)((cast(double)306.0*pixel.red +
						   cast(double)601.0*pixel.green +
						   cast(double)117.0*pixel.blue) / 1024.0);
	}
}

/*
 * intensity=0.2126*red+0.7152*green+0.0722*blue
 */
extern(D) uint PixelIntensityRec701(const scope PixelPacket* pixel) pure @trusted
{
	return cast(uint)(0.2126*pixel.red +
					  0.7152*pixel.green +
					  0.0722*pixel.blue);
}

alias PixelIntensity = PixelIntensityRec601;

pragma(inline, true)
extern(D) double PixelIntensityToDouble(const scope PixelPacket* pixel) pure @trusted
{
	return cast(double)PixelIntensity(pixel);
}

pragma(inline, true)
extern(D) Quantum PixelIntensityToQuantum(const scope PixelPacket* pixel) pure @trusted
{
	return cast(Quantum)PixelIntensity(pixel);
}

pragma(inline, true)
extern(D) bool IsCMYKColorspace(ColorspaceType colorspace) @safe
{
	return colorspace == CMYKColorspace;
}

pragma(inline, true)
extern(D) bool IsGrayColorspace(ColorspaceType colorspace) @safe
{
	return (colorspace == GRAYColorspace ||
			colorspace == Rec601LumaColorspace ||
			colorspace == Rec709LumaColorspace);
}

pragma(inline, true)
extern(D) bool IsRGBColorspace(ColorspaceType colorspace) @safe
{
	return (IsGrayColorspace(colorspace) ||
			colorspace == RGBColorspace  ||
			colorspace == TransparentColorspace);

}

pragma(inline, true)
extern(D) bool IsYCbCrColorspace(ColorspaceType colorspace) @safe
{
	return (colorspace == YCbCrColorspace ||
			colorspace == Rec601YCbCrColorspace ||
			colorspace == Rec709YCbCrColorspace);
}

alias YCbCrColorspace = Rec601YCbCrColorspace;

alias ColorspaceType = uint;
enum : ColorspaceType
{
	UndefinedColorspace,
	RGBColorspace,         /** Plain old RGB colorspace */
	GRAYColorspace,        /** Plain old full-range grayscale */
	TransparentColorspace, /** RGB but preserves matte channel during quantize */
	OHTAColorspace,
	XYZColorspace,         /** CIE XYZ */
	YCCColorspace,         /** Kodak PhotoCD PhotoYCC */
	YIQColorspace,
	YPbPrColorspace,
	YUVColorspace,
	CMYKColorspace,        /** Cyan, magenta, yellow, black, alpha */
	sRGBColorspace,        /** Kodak PhotoCD sRGB */
	HSLColorspace,         /** Hue, saturation, luminosity */
	HWBColorspace,         /** Hue, whiteness, blackness */
	LABColorspace,         /** LAB colorspace not supported yet other than via lcms */
	CineonLogRGBColorspace,/** RGB data with Cineon Log scaling, 2.048 density range */
	Rec601LumaColorspace,  /** Luma (Y) according to ITU-R 601 */
	Rec601YCbCrColorspace, /** YCbCr according to ITU-R 601 */
	Rec709LumaColorspace,  /** Luma (Y) according to ITU-R 709 */
	Rec709YCbCrColorspace  /** YCbCr according to ITU-R 709 */
}

version (GMagick_Static)
{
	@system:

	extern MagickPassFail RGBTransformImage(ImagePtr, const scope ColorspaceType);
	extern MagickPassFail TransformColorspace(ImagePtr, const scope ColorspaceType);
	extern MagickPassFail TransformRGBImage(ImagePtr, const scope ColorspaceType);
}
else
{
	@system
	{
		alias mRGBTransformImage = MagickPassFail function(ImagePtr, const scope ColorspaceType);
		alias mTransformColorspace = MagickPassFail function(ImagePtr, const scope ColorspaceType);
		alias mTransformRGBImage = MagickPassFail function(ImagePtr, const scope ColorspaceType);
	}

	__gshared extern (D)
	{
		mRGBTransformImage RGBTransformImage;
		mTransformColorspace TransformColorspace;
		mTransformRGBImage TransformRGBImage;
	}

	package (graphicsmagick_c) extern (D)
	{
		import core.sys.posix.dlfcn : dlerror, dlsym;
		import core.stdc.stdio : fprintf, stderr;

		bool _loadColorspaceH(void* dlib)
		{
			const(char)* errmsg;
			bool success = true;

			RGBTransformImage = cast(mRGBTransformImage) dlsym(dlib, "RGBTransformImage");
      if (null !is (errmsg = dlerror())) {
          debug(graphicsmagick_c)
          {
              fprintf(stderr, "ERROR graphicsmagick_c.magick.colorspace:\n  %s\n", errmsg);
          }
          success = false;
      }

			TransformColorspace = cast(mTransformColorspace) dlsym(dlib, "TransformColorspace");
      if (null !is (errmsg = dlerror())) {
          debug(graphicsmagick_c)
          {
              fprintf(stderr, "ERROR graphicsmagick_c.magick.colorspace:\n  %s\n", errmsg);
          }
          success = false;
      }

			TransformRGBImage = cast(mTransformRGBImage) dlsym(dlib, "TransformRGBImage");
      if (null !is (errmsg = dlerror())) {
          debug(graphicsmagick_c)
          {
              fprintf(stderr, "ERROR graphicsmagick_c.magick.colorspace:\n  %s\n", errmsg);
          }
          success = false;
      }

			return success;
		}
	}
}
