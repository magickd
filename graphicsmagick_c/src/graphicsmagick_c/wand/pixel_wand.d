/*
  Copyright (C) 2003 GraphicsMagick Group
  Copyright (C) 2003 ImageMagick Studio

  This program is covered by multiple licenses, which are described in
  Copyright.txt. You should have received a copy of Copyright.txt with this
  package; otherwise see http://www.graphicsmagick.org/www/Copyright.html.

  GraphicsMagick Pixel Wand Methods.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.wand.pixel_wand;

import core.stdc.config : c_ulong;

import graphicsmagick_c.magick.image : PixelPacket, Quantum;

extern struct PixelWand;

version(GMagick_Static)
{
    @system @nogc nothrow extern (C):

    char* PixelGetColorAsString(const scope PixelWand*);

    double PixelGetBlack(const scope PixelWand*);
    double PixelGetBlue(const scope PixelWand*);
    double PixelGetCyan(const scope PixelWand*);
    double PixelGetGreen(const scope PixelWand*);
    double PixelGetMagenta(const scope PixelWand*);
    double PixelGetOpacity(const scope PixelWand*);
    double PixelGetRed(const scope PixelWand*);
    double PixelGetYellow(const scope PixelWand*);

    PixelWand* NewPixelWand();
    PixelWand** NewPixelWands(const scope c_ulong);

    Quantum PixelGetBlackQuantum(const scope PixelWand*);
    Quantum PixelGetBlueQuantum(const scope PixelWand*);
    Quantum PixelGetCyanQuantum(const scope PixelWand*);
    Quantum PixelGetGreenQuantum(const scope PixelWand*);
    Quantum PixelGetMagentaQuantum(const scope PixelWand*);
    Quantum PixelGetOpacityQuantum(const scope PixelWand*);
    Quantum PixelGetRedQuantum(const scope PixelWand*);
    Quantum PixelGetYellowQuantum(const scope PixelWand*);

    uint PixelSetColor(PixelWand*, const scope char*);

    c_ulong PixelGetColorCount(const scope PixelWand*);

    void DestroyPixelWand(PixelWand*);
    void PixelGetQuantumColor(const scope PixelWand*, PixelPacket*);
    void PixelSetBlack(PixelWand*, const scope double);
    void PixelSetBlackQuantum(PixelWand*, const scope Quantum);
    void PixelSetBlue(PixelWand*, const scope double);
    void PixelSetBlueQuantum(PixelWand*, const scope Quantum);
    void PixelSetColorCount(PixelWand*, const scope c_ulong);
    void PixelSetCyan(PixelWand*, const scope double);
    void PixelSetCyanQuantum(PixelWand*, const scope Quantum);
    void PixelSetGreen(PixelWand*, const scope double);
    void PixelSetGreenQuantum(PixelWand*, const scope Quantum);
    void PixelSetMagenta(PixelWand*, const scope double);
    void PixelSetMagentaQuantum(PixelWand*, const scope Quantum);
    void PixelSetOpacity(PixelWand*, const scope double);
    void PixelSetOpacityQuantum(PixelWand*, const scope Quantum);
    void PixelSetQuantumColor(PixelWand*, PixelPacket *);
    void PixelSetRed(PixelWand*, const scope double);
    void PixelSetRedQuantum(PixelWand*, const scope Quantum);
    void PixelSetYellow(PixelWand*, const scope double);
    void PixelSetYellowQuantum(PixelWand*, const scope Quantum);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mPixelGetColorAsString = char* function(const scope PixelWand*);

        alias mPixelGetBlack = double function(const scope PixelWand*);
        alias mPixelGetBlue = double function(const scope PixelWand*);
        alias mPixelGetCyan = double function(const scope PixelWand*);
        alias mPixelGetGreen = double function(const scope PixelWand*);
        alias mPixelGetMagenta = double function(const scope PixelWand*);
        alias mPixelGetOpacity = double function(const scope PixelWand*);
        alias mPixelGetRed = double function(const scope PixelWand*);
        alias mPixelGetYellow = double function(const scope PixelWand*);

        alias mNewPixelWand = PixelWand* function();
        alias mNewPixelWands = PixelWand** function(const scope c_ulong);

        alias mPixelGetBlackQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetBlueQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetCyanQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetGreenQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetMagentaQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetOpacityQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetRedQuantum = Quantum function(const scope PixelWand*);
        alias mPixelGetYellowQuantum = Quantum function(const scope PixelWand*);

        alias mPixelSetColor = uint function(PixelWand*, const scope char*);

        alias mPixelGetColorCount = c_ulong function(const scope PixelWand*);

        alias mDestroyPixelWand = void function(PixelWand*);
        alias mPixelGetQuantumColor = void function(const scope PixelWand*, PixelPacket*);
        alias mPixelSetBlack = void function(PixelWand*, const scope double);
        alias mPixelSetBlackQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetBlue = void function(PixelWand*, const scope double);
        alias mPixelSetBlueQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetColorCount = void function(PixelWand*, const scope c_ulong);
        alias mPixelSetCyan = void function(PixelWand*, const scope double);
        alias mPixelSetCyanQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetGreen = void function(PixelWand*, const scope double);
        alias mPixelSetGreenQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetMagenta = void function(PixelWand*, const scope double);
        alias mPixelSetMagentaQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetOpacity = void function(PixelWand*, const scope double);
        alias mPixelSetOpacityQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetQuantumColor = void function(PixelWand*, PixelPacket *);
        alias mPixelSetRed = void function(PixelWand*, const scope double);
        alias mPixelSetRedQuantum = void function(PixelWand*, const scope Quantum);
        alias mPixelSetYellow = void function(PixelWand*, const scope double);
        alias mPixelSetYellowQuantum = void function(PixelWand*, const scope Quantum);
    }

    __gshared
    {
        mPixelGetColorAsString PixelGetColorAsString;

        mPixelGetBlack PixelGetBlack;
        mPixelGetBlue PixelGetBlue;
        mPixelGetCyan PixelGetCyan;
        mPixelGetGreen PixelGetGreen;
        mPixelGetMagenta PixelGetMagenta;
        mPixelGetOpacity PixelGetOpacity;
        mPixelGetRed PixelGetRed;
        mPixelGetYellow PixelGetYellow;

        mNewPixelWand NewPixelWand;
        mNewPixelWands NewPixelWands;

        mPixelGetBlackQuantum PixelGetBlackQuantum;
        mPixelGetBlueQuantum PixelGetBlueQuantum;
        mPixelGetCyanQuantum PixelGetCyanQuantum;
        mPixelGetGreenQuantum PixelGetGreenQuantum;
        mPixelGetMagentaQuantum PixelGetMagentaQuantum;
        mPixelGetOpacityQuantum PixelGetOpacityQuantum;
        mPixelGetRedQuantum PixelGetRedQuantum;
        mPixelGetYellowQuantum PixelGetYellowQuantum;

        mPixelSetColor PixelSetColor;

        mPixelGetColorCount PixelGetColorCount;

        mDestroyPixelWand DestroyPixelWand;
        mPixelGetQuantumColor PixelGetQuantumColor;
        mPixelSetBlack PixelSetBlack;
        mPixelSetBlackQuantum PixelSetBlackQuantum;
        mPixelSetBlue PixelSetBlue;
        mPixelSetBlueQuantum PixelSetBlueQuantum;
        mPixelSetColorCount PixelSetColorCount;
        mPixelSetCyan PixelSetCyan;
        mPixelSetCyanQuantum PixelSetCyanQuantum;
        mPixelSetGreen PixelSetGreen;
        mPixelSetGreenQuantum PixelSetGreenQuantum;
        mPixelSetMagenta PixelSetMagenta;
        mPixelSetMagentaQuantum PixelSetMagentaQuantum;
        mPixelSetOpacity PixelSetOpacity;
        mPixelSetOpacityQuantum PixelSetOpacityQuantum;
        mPixelSetQuantumColor PixelSetQuantumColor;
        mPixelSetRed PixelSetRed;
        mPixelSetRedQuantum PixelSetRedQuantum;
        mPixelSetYellow PixelSetYellow;
        mPixelSetYellowQuantum PixelSetYellowQuantum;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadPixelWandH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            PixelGetColorAsString = cast(mPixelGetColorAsString)dlsym(dlib, "PixelGetColorAsString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            PixelGetBlack = cast(mPixelGetBlack)dlsym(dlib, "PixelGetBlack");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetBlue = cast(mPixelGetBlue)dlsym(dlib, "PixelGetBlue");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetCyan = cast(mPixelGetCyan)dlsym(dlib, "PixelGetCyan");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetGreen = cast(mPixelGetGreen)dlsym(dlib, "PixelGetGreen");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetMagenta = cast(mPixelGetMagenta)dlsym(dlib, "PixelGetMagenta");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetOpacity = cast(mPixelGetOpacity)dlsym(dlib, "PixelGetOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetRed = cast(mPixelGetRed)dlsym(dlib, "PixelGetRed");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetYellow = cast(mPixelGetYellow)dlsym(dlib, "PixelGetYellow");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            NewPixelWand = cast(mNewPixelWand)dlsym(dlib, "NewPixelWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            NewPixelWands = cast(mNewPixelWands)dlsym(dlib, "NewPixelWands");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            PixelGetBlackQuantum = cast(mPixelGetBlackQuantum)dlsym(dlib, "PixelGetBlackQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetBlueQuantum = cast(mPixelGetBlueQuantum)dlsym(dlib, "PixelGetBlueQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetCyanQuantum = cast(mPixelGetCyanQuantum)dlsym(dlib, "PixelGetCyanQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetGreenQuantum = cast(mPixelGetGreenQuantum)dlsym(dlib, "PixelGetGreenQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetMagentaQuantum = cast(mPixelGetMagentaQuantum)dlsym(dlib, "PixelGetMagentaQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetOpacityQuantum = cast(mPixelGetOpacityQuantum)dlsym(dlib, "PixelGetOpacityQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetRedQuantum = cast(mPixelGetRedQuantum)dlsym(dlib, "PixelGetRedQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetYellowQuantum = cast(mPixelGetYellowQuantum)dlsym(dlib, "PixelGetYellowQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            PixelSetColor = cast(mPixelSetColor)dlsym(dlib, "PixelSetColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            PixelGetColorCount = cast(mPixelGetColorCount)dlsym(dlib, "PixelGetColorCount");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyPixelWand = cast(mDestroyPixelWand)dlsym(dlib, "DestroyPixelWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelGetQuantumColor = cast(mPixelGetQuantumColor)dlsym(dlib, "PixelGetQuantumColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetBlack = cast(mPixelSetBlack)dlsym(dlib, "PixelSetBlack");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetBlackQuantum = cast(mPixelSetBlackQuantum)dlsym(dlib, "PixelSetBlackQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetBlue = cast(mPixelSetBlue)dlsym(dlib, "PixelSetBlue");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetBlueQuantum = cast(mPixelSetBlueQuantum)dlsym(dlib, "PixelSetBlueQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetColorCount = cast(mPixelSetColorCount)dlsym(dlib, "PixelSetColorCount");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetCyan = cast(mPixelSetCyan)dlsym(dlib, "PixelSetCyan");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetCyanQuantum = cast(mPixelSetCyanQuantum)dlsym(dlib, "PixelSetCyanQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetGreen = cast(mPixelSetGreen)dlsym(dlib, "PixelSetGreen");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetGreenQuantum = cast(mPixelSetGreenQuantum)dlsym(dlib, "PixelSetGreenQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetMagenta = cast(mPixelSetMagenta)dlsym(dlib, "PixelSetMagenta");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetMagentaQuantum = cast(mPixelSetMagentaQuantum)dlsym(dlib, "PixelSetMagentaQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetOpacity = cast(mPixelSetOpacity)dlsym(dlib, "PixelSetOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetOpacityQuantum = cast(mPixelSetOpacityQuantum)dlsym(dlib, "PixelSetOpacityQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetQuantumColor = cast(mPixelSetQuantumColor)dlsym(dlib, "PixelSetQuantumColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetRed = cast(mPixelSetRed)dlsym(dlib, "PixelSetRed");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetRedQuantum = cast(mPixelSetRedQuantum)dlsym(dlib, "PixelSetRedQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetYellow = cast(mPixelSetYellow)dlsym(dlib, "PixelSetYellow");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            PixelSetYellowQuantum = cast(mPixelSetYellowQuantum)dlsym(dlib, "PixelSetYellowQuantum");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.pixel_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
