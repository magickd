/*
  ImageMagick Drawing Wand API.
*/




// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.wand.drawing_wand;

import core.stdc.config : c_ulong;

import graphicsmagick_c.magick.image : AffineMatrix, CompositeOperator, GravityType, Image;
import graphicsmagick_c.magick.render;

import graphicsmagick_c.wand.pixel_wand : PixelWand;

extern struct DrawingWand;

/*
 * NOTE: drawing_wand.h includes a few #defines at the start which
 *       (as I understand it) prevent collision of symbol names.
 *       For example, in the header file:
 *
 *     #undef DrawGetClipPath
 *     #define DrawGetClipPath MagickDrawGetClipPath
 *     #undef DrawGetFont
 *     #define DrawGetFont MagickDrawGetFont
 *
 *       So the actual symbols names the we need to use are actually
 *       actually the Magick* ones.  Since all the documentation uses
 *       the versions without 'Magick', I've replicated all of the
 *       aliases here.
 */
alias DestroyDrawingWand = MagickDestroyDrawingWand;
alias DrawAffine = MagickDrawAffine;
alias DrawAllocateWand = MagickDrawAllocateWand;
alias DrawAnnotation = MagickDrawAnnotation;
alias DrawArc = MagickDrawArc;
alias DrawBezier = MagickDrawBezier;
alias DrawCircle = MagickDrawCircle;
alias DrawColor = MagickDrawColor;
alias DrawComment = MagickDrawComment;
alias DrawComposite = MagickDrawComposite;
alias DrawEllipse = MagickDrawEllipse;
alias DrawGetClipPath = MagickDrawGetClipPath;
alias DrawGetClipRule = MagickDrawGetClipRule;
alias DrawGetClipUnits = MagickDrawGetClipUnits;
alias DrawGetFillColor = MagickDrawGetFillColor;
alias DrawGetFillOpacity = MagickDrawGetFillOpacity;
alias DrawGetFillRule = MagickDrawGetFillRule;
alias DrawGetFont = MagickDrawGetFont;
alias DrawGetFontFamily = MagickDrawGetFontFamily;
alias DrawGetFontSize = MagickDrawGetFontSize;
alias DrawGetFontStretch = MagickDrawGetFontStretch;
alias DrawGetFontStyle = MagickDrawGetFontStyle;
alias DrawGetFontWeight = MagickDrawGetFontWeight;
alias DrawGetGravity = MagickDrawGetGravity;
alias DrawGetStrokeAntialias = MagickDrawGetStrokeAntialias;
alias DrawGetStrokeColor = MagickDrawGetStrokeColor;
alias DrawGetStrokeDashArray = MagickDrawGetStrokeDashArray;
alias DrawGetStrokeDashOffset = MagickDrawGetStrokeDashOffset;
alias DrawGetStrokeLineCap = MagickDrawGetStrokeLineCap;
alias DrawGetStrokeLineJoin = MagickDrawGetStrokeLineJoin;
alias DrawGetStrokeMiterLimit = MagickDrawGetStrokeMiterLimit;
alias DrawGetStrokeOpacity = MagickDrawGetStrokeOpacity;
alias DrawGetStrokeWidth = MagickDrawGetStrokeWidth;
alias DrawGetTextAntialias = MagickDrawGetTextAntialias;
alias DrawGetTextDecoration = MagickDrawGetTextDecoration;
alias DrawGetTextEncoding = MagickDrawGetTextEncoding;
alias DrawGetTextUnderColor = MagickDrawGetTextUnderColor;
alias DrawLine = MagickDrawLine;
alias DrawMatte = MagickDrawMatte;
alias DrawPathClose = MagickDrawPathClose;
alias DrawPathCurveToAbsolute = MagickDrawPathCurveToAbsolute;
alias DrawPathCurveToQuadraticBezierAbsolute = MagickDrawPathCurveToQuadraticBezierAbsolute;
alias DrawPathCurveToQuadraticBezierRelative = MagickDrawPathCurveToQuadraticBezierRelative;
alias DrawPathCurveToQuadraticBezierSmoothAbsolute = MagickDrawPathCurveToQuadraticBezierSmoothAbsolute;
alias DrawPathCurveToQuadraticBezierSmoothRelative = MagickDrawPathCurveToQuadraticBezierSmoothRelative;
alias DrawPathCurveToRelative = MagickDrawPathCurveToRelative;
alias DrawPathCurveToSmoothAbsolute = MagickDrawPathCurveToSmoothAbsolute;
alias DrawPathCurveToSmoothRelative = MagickDrawPathCurveToSmoothRelative;
alias DrawPathEllipticArcAbsolute = MagickDrawPathEllipticArcAbsolute;
alias DrawPathEllipticArcRelative = MagickDrawPathEllipticArcRelative;
alias DrawPathFinish = MagickDrawPathFinish;
alias DrawPathLineToAbsolute = MagickDrawPathLineToAbsolute;
alias DrawPathLineToHorizontalAbsolute = MagickDrawPathLineToHorizontalAbsolute;
alias DrawPathLineToHorizontalRelative = MagickDrawPathLineToHorizontalRelative;
alias DrawPathLineToRelative = MagickDrawPathLineToRelative;
alias DrawPathLineToVerticalAbsolute = MagickDrawPathLineToVerticalAbsolute;
alias DrawPathLineToVerticalRelative = MagickDrawPathLineToVerticalRelative;
alias DrawPathMoveToAbsolute = MagickDrawPathMoveToAbsolute;
alias DrawPathMoveToRelative = MagickDrawPathMoveToRelative;
alias DrawPathStart = MagickDrawPathStart;
alias DrawPeekGraphicContext = MagickDrawPeekGraphicContext;
alias DrawPoint = MagickDrawPoint;
alias DrawPolygon = MagickDrawPolygon;
alias DrawPolyline = MagickDrawPolyline;
alias DrawPopClipPath = MagickDrawPopClipPath;
alias DrawPopDefs = MagickDrawPopDefs;
alias DrawPopGraphicContext = MagickDrawPopGraphicContext;
alias DrawPopPattern = MagickDrawPopPattern;
alias DrawPushClipPath = MagickDrawPushClipPath;
alias DrawPushDefs = MagickDrawPushDefs;
alias DrawPushGraphicContext = MagickDrawPushGraphicContext;
alias DrawPushPattern = MagickDrawPushPattern;
alias DrawRectangle = MagickDrawRectangle;
alias DrawRender = MagickDrawRender;
alias DrawRotate = MagickDrawRotate;
alias DrawRoundRectangle = MagickDrawRoundRectangle;
alias DrawScale = MagickDrawScale;
alias DrawSetClipPath = MagickDrawSetClipPath;
alias DrawSetClipRule = MagickDrawSetClipRule;
alias DrawSetClipUnits = MagickDrawSetClipUnits;
alias DrawSetFillColor = MagickDrawSetFillColor;
alias DrawSetFillOpacity = MagickDrawSetFillOpacity;
alias DrawSetFillPatternURL = MagickDrawSetFillPatternURL;
alias DrawSetFillRule = MagickDrawSetFillRule;
alias DrawSetFont = MagickDrawSetFont;
alias DrawSetFontFamily = MagickDrawSetFontFamily;
alias DrawSetFontSize = MagickDrawSetFontSize;
alias DrawSetFontStretch = MagickDrawSetFontStretch;
alias DrawSetFontStyle = MagickDrawSetFontStyle;
alias DrawSetFontWeight = MagickDrawSetFontWeight;
alias DrawSetGravity = MagickDrawSetGravity;
alias DrawSetStrokeAntialias = MagickDrawSetStrokeAntialias;
alias DrawSetStrokeColor = MagickDrawSetStrokeColor;
alias DrawSetStrokeDashArray = MagickDrawSetStrokeDashArray;
alias DrawSetStrokeDashOffset = MagickDrawSetStrokeDashOffset;
alias DrawSetStrokeLineCap = MagickDrawSetStrokeLineCap;
alias DrawSetStrokeLineJoin = MagickDrawSetStrokeLineJoin;
alias DrawSetStrokeMiterLimit = MagickDrawSetStrokeMiterLimit;
alias DrawSetStrokeOpacity = MagickDrawSetStrokeOpacity;
alias DrawSetStrokePatternURL = MagickDrawSetStrokePatternURL;
alias DrawSetStrokeWidth = MagickDrawSetStrokeWidth;
alias DrawSetTextAntialias = MagickDrawSetTextAntialias;
alias DrawSetTextDecoration = MagickDrawSetTextDecoration;
alias DrawSetTextEncoding = MagickDrawSetTextEncoding;
alias DrawSetTextUnderColor = MagickDrawSetTextUnderColor;
alias DrawSetViewbox = MagickDrawSetViewbox;
alias DrawSkewX = MagickDrawSkewX;
alias DrawSkewY = MagickDrawSkewY;
alias DrawTranslate = MagickDrawTranslate;
alias NewDrawingWand = MagickNewDrawingWand;


/*-----------------------------------------*
 *             End of aliases              *
 *-----------------------------------------*/

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):


    char* MagickDrawGetClipPath(const scope DrawingWand*);
    char* MagickDrawGetFont(const scope DrawingWand*);
    char* MagickDrawGetFontFamily(const scope DrawingWand*);
    char* MagickDrawGetTextEncoding(const scope DrawingWand*);

    ClipPathUnits MagickDrawGetClipUnits(const scope DrawingWand*);

    DecorationType MagickDrawGetTextDecoration(const scope DrawingWand*);

    double MagickDrawGetFillOpacity(const scope DrawingWand*);
    double MagickDrawGetFontSize(const scope DrawingWand*);
    double* MagickDrawGetStrokeDashArray(const scope DrawingWand*, c_ulong*);
    double MagickDrawGetStrokeDashOffset(const scope DrawingWand*);
    double MagickDrawGetStrokeOpacity(const scope DrawingWand*);
    double MagickDrawGetStrokeWidth(const scope DrawingWand*);

    DrawInfo* MagickDrawPeekGraphicContext(const scope DrawingWand*);

    DrawingWand* MagickDrawAllocateWand(const scope DrawInfo*, Image*);
    DrawingWand* MagickNewDrawingWand();

    FillRule MagickDrawGetClipRule(const scope DrawingWand*);
    FillRule MagickDrawGetFillRule(const scope DrawingWand*);

    GravityType MagickDrawGetGravity(const scope DrawingWand*);

    LineCap MagickDrawGetStrokeLineCap(const scope DrawingWand*);

    LineJoin MagickDrawGetStrokeLineJoin(const scope DrawingWand*);

    StretchType MagickDrawGetFontStretch(const scope DrawingWand*);

    StyleType MagickDrawGetFontStyle(const scope DrawingWand*);

    uint MagickDrawGetStrokeAntialias(const scope DrawingWand*);
    uint MagickDrawGetTextAntialias(const scope DrawingWand*);
    uint MagickDrawRender(const scope DrawingWand*);

    c_ulong MagickDrawGetFontWeight(const scope DrawingWand*);
    c_ulong MagickDrawGetStrokeMiterLimit(const scope DrawingWand*);

    void MagickDrawAffine(DrawingWand*, const scope AffineMatrix*);
    void MagickDrawAnnotation(DrawingWand*, const scope double, const scope double, const scope ubyte*);
    void MagickDrawArc(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawBezier(DrawingWand*, const scope c_ulong, const scope PointInfo*);
    void MagickDrawCircle(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawColor(DrawingWand*, const scope double, const scope double, const scope PaintMethod);
    void MagickDrawComment(DrawingWand*, const scope char*);
    void MagickDestroyDrawingWand(DrawingWand*);
    void MagickDrawEllipse(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawComposite(DrawingWand*, const scope CompositeOperator, const scope double, const scope double, const scope double, const scope double, const scope Image*);
    void MagickDrawGetFillColor(const scope DrawingWand*, PixelWand*);
    void MagickDrawGetStrokeColor(const scope DrawingWand*, PixelWand*);
    void MagickDrawGetTextUnderColor(const scope DrawingWand*, PixelWand*);
    void MagickDrawLine(DrawingWand*, const scope double, const scope double,const scope double, const scope double);
    void MagickDrawMatte(DrawingWand*, const scope double, const scope double, const scope PaintMethod);
    void MagickDrawPathClose(DrawingWand*);
    void MagickDrawPathCurveToAbsolute(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawPathCurveToRelative(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawPathCurveToQuadraticBezierAbsolute(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawPathCurveToQuadraticBezierRelative(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawPathCurveToQuadraticBezierSmoothAbsolute(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathCurveToQuadraticBezierSmoothRelative(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathCurveToSmoothAbsolute(DrawingWand*, const scope double,const scope double, const scope double, const scope double);
    void MagickDrawPathCurveToSmoothRelative(DrawingWand*, const scope double,const scope double, const scope double, const scope double);
    void MagickDrawPathEllipticArcAbsolute(DrawingWand*, const scope double, const scope double, const scope double, uint, uint, const scope double, const scope double);
    void MagickDrawPathEllipticArcRelative(DrawingWand*, const scope double, const scope double, const scope double, uint, uint, const scope double, const scope double);
    void MagickDrawPathFinish(DrawingWand*);
    void MagickDrawPathLineToAbsolute(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathLineToRelative(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathLineToHorizontalAbsolute(DrawingWand*, const scope double);
    void MagickDrawPathLineToHorizontalRelative(DrawingWand*, const scope double);
    void MagickDrawPathLineToVerticalAbsolute(DrawingWand*, const scope double);
    void MagickDrawPathLineToVerticalRelative(DrawingWand*, const scope double);
    void MagickDrawPathMoveToAbsolute(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathMoveToRelative(DrawingWand*, const scope double, const scope double);
    void MagickDrawPathStart(DrawingWand*);
    void MagickDrawPoint(DrawingWand*, const scope double, const scope double);
    void MagickDrawPolygon(DrawingWand*, const scope c_ulong, const scope PointInfo*);
    void MagickDrawPolyline(DrawingWand*, const scope c_ulong, const scope PointInfo*);
    void MagickDrawPopClipPath(DrawingWand*);
    void MagickDrawPopDefs(DrawingWand*);
    void MagickDrawPopGraphicContext(DrawingWand*);
    void MagickDrawPopPattern(DrawingWand*);
    void MagickDrawPushClipPath(DrawingWand*, const scope char*);
    void MagickDrawPushDefs(DrawingWand*);
    void MagickDrawPushGraphicContext(DrawingWand*);
    void MagickDrawPushPattern(DrawingWand*, const scope char*, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawRectangle(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
    void MagickDrawRotate(DrawingWand*, const scope double);
    void MagickDrawRoundRectangle(DrawingWand*, double, double, double, double, double, double);
    void MagickDrawScale(DrawingWand*, const scope double, const scope double);
    void MagickDrawSetClipPath(DrawingWand*, const scope char*);
    void MagickDrawSetClipRule(DrawingWand*, const scope FillRule);
    void MagickDrawSetClipUnits(DrawingWand*, const scope ClipPathUnits);
    void MagickDrawSetFillColor(DrawingWand*, const scope PixelWand*);
    void MagickDrawSetFillOpacity(DrawingWand*, const scope double);
    void MagickDrawSetFillRule(DrawingWand*, const scope FillRule);
    void MagickDrawSetFillPatternURL(DrawingWand*, const scope char*);
    void MagickDrawSetFont(DrawingWand*, const scope char*);
    void MagickDrawSetFontFamily(DrawingWand*, const scope char*);
    void MagickDrawSetFontSize(DrawingWand*, const scope double);
    void MagickDrawSetFontStretch(DrawingWand*, const scope StretchType);
    void MagickDrawSetFontStyle(DrawingWand*, const scope StyleType);
    void MagickDrawSetFontWeight(DrawingWand*, const scope c_ulong);
    void MagickDrawSetGravity(DrawingWand*, const scope GravityType);
    void MagickDrawSkewX(DrawingWand*, const scope double);
    void MagickDrawSkewY(DrawingWand*, const scope double);
    void MagickDrawSetStrokeAntialias(DrawingWand*, const scope uint);
    void MagickDrawSetStrokeColor(DrawingWand*, const scope PixelWand*);
    void MagickDrawSetStrokeDashArray(DrawingWand*, const scope c_ulong, const scope double*);
    void MagickDrawSetStrokeDashOffset(DrawingWand*, const scope double dashoffset);
    void MagickDrawSetStrokeLineCap(DrawingWand*, const scope LineCap);
    void MagickDrawSetStrokeLineJoin(DrawingWand*, const scope LineJoin);
    void MagickDrawSetStrokeMiterLimit(DrawingWand*, const scope c_ulong);
    void MagickDrawSetStrokeOpacity(DrawingWand*, const scope double);
    void MagickDrawSetStrokePatternURL(DrawingWand*, const scope char*);
    void MagickDrawSetStrokeWidth(DrawingWand*, const scope double);
    void MagickDrawSetTextAntialias(DrawingWand*, const scope uint);
    void MagickDrawSetTextDecoration(DrawingWand*, const scope DecorationType);
    void MagickDrawSetTextEncoding(DrawingWand*, const scope char*);
    void MagickDrawSetTextUnderColor(DrawingWand*, const scope PixelWand*);
    void MagickDrawSetViewbox(DrawingWand*, c_ulong, c_ulong, c_ulong, c_ulong);
    void MagickDrawTranslate(DrawingWand*, const scope double, const scope double);
} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mDrawGetClipPath = char* function(const scope DrawingWand*);
        alias mDrawGetFont = char* function(const scope DrawingWand*);
        alias mDrawGetFontFamily = char* function(const scope DrawingWand*);
        alias mDrawGetTextEncoding = char* function(const scope DrawingWand*);

        alias mDrawGetClipUnits = ClipPathUnits function(const scope DrawingWand*);

        alias mDrawGetTextDecoration = DecorationType function(const scope DrawingWand*);

        alias mDrawGetFillOpacity = double function(const scope DrawingWand*);
        alias mDrawGetFontSize = double function(const scope DrawingWand*);
        alias mDrawGetStrokeDashArray = double* function(const scope DrawingWand*, c_ulong*);
        alias mDrawGetStrokeDashOffset = double function(const scope DrawingWand*);
        alias mDrawGetStrokeOpacity = double function(const scope DrawingWand*);
        alias mDrawGetStrokeWidth = double function(const scope DrawingWand*);

        alias mDrawPeekGraphicContext = DrawInfo* function(const scope DrawingWand*);

        alias mDrawAllocateWand = DrawingWand* function(const scope DrawInfo*, Image*);
        alias mNewDrawingWand = DrawingWand* function();

        alias mDrawGetClipRule = FillRule function(const scope DrawingWand*);
        alias mDrawGetFillRule = FillRule function(const scope DrawingWand*);

        alias mDrawGetGravity = GravityType function(const scope DrawingWand*);

        alias mDrawGetStrokeLineCap = LineCap function(const scope DrawingWand*);

        alias mDrawGetStrokeLineJoin = LineJoin function(const scope DrawingWand*);

        alias mDrawGetFontStretch = StretchType function(const scope DrawingWand*);

        alias mDrawGetFontStyle = StyleType function(const scope DrawingWand*);

        alias mDrawGetStrokeAntialias = uint function(const scope DrawingWand*);
        alias mDrawGetTextAntialias = uint function(const scope DrawingWand*);
        alias mDrawRender = uint function(const scope DrawingWand*);

        alias mDrawGetFontWeight = c_ulong function(const scope DrawingWand*);
        alias mDrawGetStrokeMiterLimit = c_ulong function(const scope DrawingWand*);

        alias mDrawAffine = void function(DrawingWand*, const scope AffineMatrix*);
        alias mDrawAnnotation = void function(DrawingWand*, const scope double, const scope double, const scope ubyte*);
        alias mDrawArc = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double, const scope double);
        alias mDrawBezier = void function(DrawingWand*, const scope c_ulong, const scope PointInfo*);
        alias mDrawCircle = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
        alias mDrawColor = void function(DrawingWand*, const scope double, const scope double, const scope PaintMethod);
        alias mDrawComment = void function(DrawingWand*, const scope char*);
        alias mDestroyDrawingWand = void function(DrawingWand*);
        alias mDrawEllipse = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double, const scope double,
            const scope double);
        alias mDrawComposite = void function(DrawingWand*, const scope CompositeOperator, const scope double, const scope double, const scope double,
            const scope double, const scope Image*);
        alias mDrawGetFillColor = void function(const scope DrawingWand*, PixelWand*);
        alias mDrawGetStrokeColor = void function(const scope DrawingWand*, PixelWand*);
        alias mDrawGetTextUnderColor = void function(const scope DrawingWand*, PixelWand*);
        alias mDrawLine = void function(DrawingWand*, const scope double, const scope double,const scope double, const scope double);
        alias mDrawMatte = void function(DrawingWand*, const scope double, const scope double, const scope PaintMethod);
        alias mDrawPathClose = void function(DrawingWand*);
        alias mDrawPathCurveToAbsolute = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double,
            const scope double, const scope double);
        alias mDrawPathCurveToRelative = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double,
            const scope double, const scope double);
        alias mDrawPathCurveToQuadraticBezierAbsolute = void function(DrawingWand*, const scope double, const scope double, const scope double,
            const scope double);
        alias mDrawPathCurveToQuadraticBezierRelative = void function(DrawingWand*, const scope double, const scope double, const scope double,
            const scope double);
        alias mDrawPathCurveToQuadraticBezierSmoothAbsolute = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathCurveToQuadraticBezierSmoothRelative = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathCurveToSmoothAbsolute = void function(DrawingWand*, const scope double,const scope double, const scope double, const scope double);
        alias mDrawPathCurveToSmoothRelative = void function(DrawingWand*, const scope double,const scope double, const scope double, const scope double);
        alias mDrawPathEllipticArcAbsolute = void function(DrawingWand*, const scope double, const scope double, const scope double, uint, uint,
            const scope double, const scope double);
        alias mDrawPathEllipticArcRelative = void function(DrawingWand*, const scope double, const scope double, const scope double, uint, uint,
            const scope double, const scope double);
        alias mDrawPathFinish = void function(DrawingWand*);
        alias mDrawPathLineToAbsolute = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathLineToRelative = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathLineToHorizontalAbsolute = void function(DrawingWand*, const scope double);
        alias mDrawPathLineToHorizontalRelative = void function(DrawingWand*, const scope double);
        alias mDrawPathLineToVerticalAbsolute = void function(DrawingWand*, const scope double);
        alias mDrawPathLineToVerticalRelative = void function(DrawingWand*, const scope double);
        alias mDrawPathMoveToAbsolute = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathMoveToRelative = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPathStart = void function(DrawingWand*);
        alias mDrawPoint = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawPolygon = void function(DrawingWand*, const scope c_ulong, const scope PointInfo*);
        alias mDrawPolyline = void function(DrawingWand*, const scope c_ulong, const scope PointInfo*);
        alias mDrawPopClipPath = void function(DrawingWand*);
        alias mDrawPopDefs = void function(DrawingWand*);
        alias mDrawPopGraphicContext = void function(DrawingWand*);
        alias mDrawPopPattern = void function(DrawingWand*);
        alias mDrawPushClipPath = void function(DrawingWand*, const scope char*);
        alias mDrawPushDefs = void function(DrawingWand*);
        alias mDrawPushGraphicContext = void function(DrawingWand*);
        alias mDrawPushPattern = void function(DrawingWand*, const scope char*, const scope double, const scope double, const scope double, const scope double);
        alias mDrawRectangle = void function(DrawingWand*, const scope double, const scope double, const scope double, const scope double);
        alias mDrawRotate = void function(DrawingWand*, const scope double);
        alias mDrawRoundRectangle = void function(DrawingWand*, double, double, double, double, double, double);
        alias mDrawScale = void function(DrawingWand*, const scope double, const scope double);
        alias mDrawSetClipPath = void function(DrawingWand*, const scope char*);
        alias mDrawSetClipRule = void function(DrawingWand*, const scope FillRule);
        alias mDrawSetClipUnits = void function(DrawingWand*, const scope ClipPathUnits);
        alias mDrawSetFillColor = void function(DrawingWand*, const scope PixelWand*);
        alias mDrawSetFillOpacity = void function(DrawingWand*, const scope double);
        alias mDrawSetFillRule = void function(DrawingWand*, const scope FillRule);
        alias mDrawSetFillPatternURL = void function(DrawingWand*, const scope char*);
        alias mDrawSetFont = void function(DrawingWand*, const scope char*);
        alias mDrawSetFontFamily = void function(DrawingWand*, const scope char*);
        alias mDrawSetFontSize = void function(DrawingWand*, const scope double);
        alias mDrawSetFontStretch = void function(DrawingWand*, const scope StretchType);
        alias mDrawSetFontStyle = void function(DrawingWand*, const scope StyleType);
        alias mDrawSetFontWeight = void function(DrawingWand*, const scope c_ulong);
        alias mDrawSetGravity = void function(DrawingWand*, const scope GravityType);
        alias mDrawSkewX = void function(DrawingWand*, const scope double);
        alias mDrawSkewY = void function(DrawingWand*, const scope double);
        alias mDrawSetStrokeAntialias = void function(DrawingWand*, const scope uint);
        alias mDrawSetStrokeColor = void function(DrawingWand*, const scope PixelWand*);
        alias mDrawSetStrokeDashArray = void function(DrawingWand*, const scope c_ulong, const scope double*);
        alias mDrawSetStrokeDashOffset = void function(DrawingWand*, const scope double dashoffset);
        alias mDrawSetStrokeLineCap = void function(DrawingWand*, const scope LineCap);
        alias mDrawSetStrokeLineJoin = void function(DrawingWand*, const scope LineJoin);
        alias mDrawSetStrokeMiterLimit = void function(DrawingWand*, const scope c_ulong);
        alias mDrawSetStrokeOpacity = void function(DrawingWand*, const scope double);
        alias mDrawSetStrokePatternURL = void function(DrawingWand*, const scope char*);
        alias mDrawSetStrokeWidth = void function(DrawingWand*, const scope double);
        alias mDrawSetTextAntialias = void function(DrawingWand*, const scope uint);
        alias mDrawSetTextDecoration = void function(DrawingWand*, const scope DecorationType);
        alias mDrawSetTextEncoding = void function(DrawingWand*, const scope char*);
        alias mDrawSetTextUnderColor = void function(DrawingWand*, const scope PixelWand*);
        alias mDrawSetViewbox = void function(DrawingWand*, c_ulong, c_ulong, c_ulong, c_ulong);
        alias mDrawTranslate = void function(DrawingWand*, const scope double, const scope double);
    }

    __gshared
    {
        mDrawGetClipPath MagickDrawGetClipPath;
        mDrawGetFont MagickDrawGetFont;
        mDrawGetFontFamily MagickDrawGetFontFamily;
        mDrawGetTextEncoding MagickDrawGetTextEncoding;

        mDrawGetClipUnits MagickDrawGetClipUnits;

        mDrawGetTextDecoration MagickDrawGetTextDecoration;

        mDrawGetFillOpacity MagickDrawGetFillOpacity;
        mDrawGetFontSize MagickDrawGetFontSize;
        mDrawGetStrokeDashArray MagickDrawGetStrokeDashArray;
        mDrawGetStrokeDashOffset MagickDrawGetStrokeDashOffset;
        mDrawGetStrokeOpacity MagickDrawGetStrokeOpacity;
        mDrawGetStrokeWidth MagickDrawGetStrokeWidth;

        mDrawPeekGraphicContext MagickDrawPeekGraphicContext;

        mDrawAllocateWand MagickDrawAllocateWand;
        mNewDrawingWand MagickNewDrawingWand;

        mDrawGetClipRule MagickDrawGetClipRule;
        mDrawGetFillRule MagickDrawGetFillRule;

        mDrawGetGravity MagickDrawGetGravity;

        mDrawGetStrokeLineCap MagickDrawGetStrokeLineCap;

        mDrawGetStrokeLineJoin MagickDrawGetStrokeLineJoin;

        mDrawGetFontStretch MagickDrawGetFontStretch;

        mDrawGetFontStyle MagickDrawGetFontStyle;

        mDrawGetStrokeAntialias MagickDrawGetStrokeAntialias;
        mDrawGetTextAntialias MagickDrawGetTextAntialias;
        mDrawRender MagickDrawRender;

        mDrawGetFontWeight MagickDrawGetFontWeight;
        mDrawGetStrokeMiterLimit MagickDrawGetStrokeMiterLimit;

        mDrawAffine MagickDrawAffine;
        mDrawAnnotation MagickDrawAnnotation;
        mDrawArc MagickDrawArc;
        mDrawBezier MagickDrawBezier;
        mDrawCircle MagickDrawCircle;
        mDrawColor MagickDrawColor;
        mDrawComment MagickDrawComment;
        mDestroyDrawingWand MagickDestroyDrawingWand;
        mDrawEllipse MagickDrawEllipse;
        mDrawComposite MagickDrawComposite;
        mDrawGetFillColor MagickDrawGetFillColor;
        mDrawGetStrokeColor MagickDrawGetStrokeColor;
        mDrawGetTextUnderColor MagickDrawGetTextUnderColor;
        mDrawLine MagickDrawLine;
        mDrawMatte MagickDrawMatte;
        mDrawPathClose MagickDrawPathClose;
        mDrawPathCurveToAbsolute MagickDrawPathCurveToAbsolute;
        mDrawPathCurveToRelative MagickDrawPathCurveToRelative;
        mDrawPathCurveToQuadraticBezierAbsolute MagickDrawPathCurveToQuadraticBezierAbsolute;
        mDrawPathCurveToQuadraticBezierRelative MagickDrawPathCurveToQuadraticBezierRelative;
        mDrawPathCurveToQuadraticBezierSmoothAbsolute MagickDrawPathCurveToQuadraticBezierSmoothAbsolute;
        mDrawPathCurveToQuadraticBezierSmoothRelative MagickDrawPathCurveToQuadraticBezierSmoothRelative;
        mDrawPathCurveToSmoothAbsolute MagickDrawPathCurveToSmoothAbsolute;
        mDrawPathCurveToSmoothRelative MagickDrawPathCurveToSmoothRelative;
        mDrawPathEllipticArcAbsolute MagickDrawPathEllipticArcAbsolute;
        mDrawPathEllipticArcRelative MagickDrawPathEllipticArcRelative;
        mDrawPathFinish MagickDrawPathFinish;
        mDrawPathLineToAbsolute MagickDrawPathLineToAbsolute;
        mDrawPathLineToRelative MagickDrawPathLineToRelative;
        mDrawPathLineToHorizontalAbsolute MagickDrawPathLineToHorizontalAbsolute;
        mDrawPathLineToHorizontalRelative MagickDrawPathLineToHorizontalRelative;
        mDrawPathLineToVerticalAbsolute MagickDrawPathLineToVerticalAbsolute;
        mDrawPathLineToVerticalRelative MagickDrawPathLineToVerticalRelative;
        mDrawPathMoveToAbsolute MagickDrawPathMoveToAbsolute;
        mDrawPathMoveToRelative MagickDrawPathMoveToRelative;
        mDrawPathStart MagickDrawPathStart;
        mDrawPoint MagickDrawPoint;
        mDrawPolygon MagickDrawPolygon;
        mDrawPolyline MagickDrawPolyline;
        mDrawPopClipPath MagickDrawPopClipPath;
        mDrawPopDefs MagickDrawPopDefs;
        mDrawPopGraphicContext MagickDrawPopGraphicContext;
        mDrawPopPattern MagickDrawPopPattern;
        mDrawPushClipPath MagickDrawPushClipPath;
        mDrawPushDefs MagickDrawPushDefs;
        mDrawPushGraphicContext MagickDrawPushGraphicContext;
        mDrawPushPattern MagickDrawPushPattern;
        mDrawRectangle MagickDrawRectangle;
        mDrawRotate MagickDrawRotate;
        mDrawRoundRectangle MagickDrawRoundRectangle;
        mDrawScale MagickDrawScale;
        mDrawSetClipPath MagickDrawSetClipPath;
        mDrawSetClipRule MagickDrawSetClipRule;
        mDrawSetClipUnits MagickDrawSetClipUnits;
        mDrawSetFillColor MagickDrawSetFillColor;
        mDrawSetFillOpacity MagickDrawSetFillOpacity;
        mDrawSetFillRule MagickDrawSetFillRule;
        mDrawSetFillPatternURL MagickDrawSetFillPatternURL;
        mDrawSetFont MagickDrawSetFont;
        mDrawSetFontFamily MagickDrawSetFontFamily;
        mDrawSetFontSize MagickDrawSetFontSize;
        mDrawSetFontStretch MagickDrawSetFontStretch;
        mDrawSetFontStyle MagickDrawSetFontStyle;
        mDrawSetFontWeight MagickDrawSetFontWeight;
        mDrawSetGravity MagickDrawSetGravity;
        mDrawSkewX MagickDrawSkewX;
        mDrawSkewY MagickDrawSkewY;
        mDrawSetStrokeAntialias MagickDrawSetStrokeAntialias;
        mDrawSetStrokeColor MagickDrawSetStrokeColor;
        mDrawSetStrokeDashArray MagickDrawSetStrokeDashArray;
        mDrawSetStrokeDashOffset MagickDrawSetStrokeDashOffset;
        mDrawSetStrokeLineCap MagickDrawSetStrokeLineCap;
        mDrawSetStrokeLineJoin MagickDrawSetStrokeLineJoin;
        mDrawSetStrokeMiterLimit MagickDrawSetStrokeMiterLimit;
        mDrawSetStrokeOpacity MagickDrawSetStrokeOpacity;
        mDrawSetStrokePatternURL MagickDrawSetStrokePatternURL;
        mDrawSetStrokeWidth MagickDrawSetStrokeWidth;
        mDrawSetTextAntialias MagickDrawSetTextAntialias;
        mDrawSetTextDecoration MagickDrawSetTextDecoration;
        mDrawSetTextEncoding MagickDrawSetTextEncoding;
        mDrawSetTextUnderColor MagickDrawSetTextUnderColor;
        mDrawSetViewbox MagickDrawSetViewbox;
        mDrawTranslate MagickDrawTranslate;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadDrawingWandH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            MagickDrawGetClipPath = cast(mDrawGetClipPath)dlsym(dlib, "MagickDrawGetClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetFont = cast(mDrawGetFont)dlsym(dlib, "MagickDrawGetFont");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetFontFamily = cast(mDrawGetFontFamily)dlsym(dlib, "MagickDrawGetFontFamily");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetTextEncoding = cast(mDrawGetTextEncoding)dlsym(dlib, "MagickDrawGetTextEncoding");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetClipUnits = cast(mDrawGetClipUnits)dlsym(dlib, "MagickDrawGetClipUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetTextDecoration = cast(mDrawGetTextDecoration)dlsym(dlib, "MagickDrawGetTextDecoration");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetFillOpacity = cast(mDrawGetFillOpacity)dlsym(dlib, "MagickDrawGetFillOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetFontSize = cast(mDrawGetFontSize)dlsym(dlib, "MagickDrawGetFontSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeDashArray = cast(mDrawGetStrokeDashArray)dlsym(dlib, "MagickDrawGetStrokeDashArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeDashOffset = cast(mDrawGetStrokeDashOffset)dlsym(dlib, "MagickDrawGetStrokeDashOffset");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeOpacity = cast(mDrawGetStrokeOpacity)dlsym(dlib, "MagickDrawGetStrokeOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeWidth = cast(mDrawGetStrokeWidth)dlsym(dlib, "MagickDrawGetStrokeWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawPeekGraphicContext = cast(mDrawPeekGraphicContext)dlsym(dlib, "MagickDrawPeekGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawAllocateWand = cast(mDrawAllocateWand)dlsym(dlib, "MagickDrawAllocateWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickNewDrawingWand = cast(mNewDrawingWand)dlsym(dlib, "MagickNewDrawingWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetClipRule = cast(mDrawGetClipRule)dlsym(dlib, "MagickDrawGetClipRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetFillRule = cast(mDrawGetFillRule)dlsym(dlib, "MagickDrawGetFillRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetGravity = cast(mDrawGetGravity)dlsym(dlib, "MagickDrawGetGravity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetStrokeLineCap = cast(mDrawGetStrokeLineCap)dlsym(dlib, "MagickDrawGetStrokeLineCap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetStrokeLineJoin = cast(mDrawGetStrokeLineJoin)dlsym(dlib, "MagickDrawGetStrokeLineJoin");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetFontStretch = cast(mDrawGetFontStretch)dlsym(dlib, "MagickDrawGetFontStretch");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetFontStyle = cast(mDrawGetFontStyle)dlsym(dlib, "MagickDrawGetFontStyle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetStrokeAntialias = cast(mDrawGetStrokeAntialias)dlsym(dlib, "MagickDrawGetStrokeAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetTextAntialias = cast(mDrawGetTextAntialias)dlsym(dlib, "MagickDrawGetTextAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawRender = cast(mDrawRender)dlsym(dlib, "MagickDrawRender");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawGetFontWeight = cast(mDrawGetFontWeight)dlsym(dlib, "MagickDrawGetFontWeight");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeMiterLimit = cast(mDrawGetStrokeMiterLimit)dlsym(dlib, "MagickDrawGetStrokeMiterLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDrawAffine = cast(mDrawAffine)dlsym(dlib, "MagickDrawAffine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawAnnotation = cast(mDrawAnnotation)dlsym(dlib, "MagickDrawAnnotation");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawArc = cast(mDrawArc)dlsym(dlib, "MagickDrawArc");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawBezier = cast(mDrawBezier)dlsym(dlib, "MagickDrawBezier");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawCircle = cast(mDrawCircle)dlsym(dlib, "MagickDrawCircle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawColor = cast(mDrawColor)dlsym(dlib, "MagickDrawColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawComment = cast(mDrawComment)dlsym(dlib, "MagickDrawComment");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDestroyDrawingWand = cast(mDestroyDrawingWand)dlsym(dlib, "MagickDestroyDrawingWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawEllipse = cast(mDrawEllipse)dlsym(dlib, "MagickDrawEllipse");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawComposite = cast(mDrawComposite)dlsym(dlib, "MagickDrawComposite");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetFillColor = cast(mDrawGetFillColor)dlsym(dlib, "MagickDrawGetFillColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetStrokeColor = cast(mDrawGetStrokeColor)dlsym(dlib, "MagickDrawGetStrokeColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawGetTextUnderColor = cast(mDrawGetTextUnderColor)dlsym(dlib, "MagickDrawGetTextUnderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawLine = cast(mDrawLine)dlsym(dlib, "MagickDrawLine");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawMatte = cast(mDrawMatte)dlsym(dlib, "MagickDrawMatte");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathClose = cast(mDrawPathClose)dlsym(dlib, "MagickDrawPathClose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathCurveToAbsolute = cast(mDrawPathCurveToAbsolute)dlsym(dlib, "MagickDrawPathCurveToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathCurveToRelative = cast(mDrawPathCurveToRelative)dlsym(dlib, "MagickDrawPathCurveToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierAbsolute =
                cast(mDrawPathCurveToQuadraticBezierAbsolute)dlsym(dlib, "MagickDrawPathCurveToQuadraticBezierAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierRelative =
                cast(mDrawPathCurveToQuadraticBezierRelative)dlsym(dlib,"MagickDrawPathCurveToQuadraticBezierRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierSmoothAbsolute =
                cast(mDrawPathCurveToQuadraticBezierSmoothAbsolute)dlsym(dlib, "MagickDrawPathCurveToQuadraticBezierSmoothAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToQuadraticBezierSmoothRelative =
                cast(mDrawPathCurveToQuadraticBezierSmoothRelative)dlsym(dlib, "MagickDrawPathCurveToQuadraticBezierSmoothRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToSmoothAbsolute =
                cast(mDrawPathCurveToSmoothAbsolute)dlsym(dlib, "MagickDrawPathCurveToSmoothAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathCurveToSmoothRelative =
                cast(mDrawPathCurveToSmoothRelative)dlsym(dlib, "MagickDrawPathCurveToSmoothRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathEllipticArcAbsolute =
                cast(mDrawPathEllipticArcAbsolute)dlsym(dlib, "MagickDrawPathEllipticArcAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathEllipticArcRelative =
                cast(mDrawPathEllipticArcRelative)dlsym(dlib, "MagickDrawPathEllipticArcRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathFinish = cast(mDrawPathFinish)dlsym(dlib, "MagickDrawPathFinish");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathLineToAbsolute = cast(mDrawPathLineToAbsolute)dlsym(dlib, "MagickDrawPathLineToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathLineToRelative = cast(mDrawPathLineToRelative)dlsym(dlib, "MagickDrawPathLineToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToHorizontalAbsolute =
                cast(mDrawPathLineToHorizontalAbsolute)dlsym(dlib, "MagickDrawPathLineToHorizontalAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToHorizontalRelative =
                cast(mDrawPathLineToHorizontalRelative)dlsym(dlib, "MagickDrawPathLineToHorizontalRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToVerticalAbsolute =
                cast(mDrawPathLineToVerticalAbsolute)dlsym(dlib, "MagickDrawPathLineToVerticalAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            DrawPathLineToVerticalRelative =
                cast(mDrawPathLineToVerticalRelative)dlsym(dlib, "MagickDrawPathLineToVerticalRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathMoveToAbsolute = cast(mDrawPathMoveToAbsolute)dlsym(dlib, "MagickDrawPathMoveToAbsolute");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathMoveToRelative = cast(mDrawPathMoveToRelative)dlsym(dlib, "MagickDrawPathMoveToRelative");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPathStart = cast(mDrawPathStart)dlsym(dlib, "MagickDrawPathStart");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPoint = cast(mDrawPoint)dlsym(dlib, "MagickDrawPoint");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPolygon = cast(mDrawPolygon)dlsym(dlib, "MagickDrawPolygon");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPolyline = cast(mDrawPolyline)dlsym(dlib, "MagickDrawPolyline");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPopClipPath = cast(mDrawPopClipPath)dlsym(dlib, "MagickDrawPopClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPopDefs = cast(mDrawPopDefs)dlsym(dlib, "MagickDrawPopDefs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPopGraphicContext = cast(mDrawPopGraphicContext)dlsym(dlib, "MagickDrawPopGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPopPattern = cast(mDrawPopPattern)dlsym(dlib, "MagickDrawPopPattern");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPushClipPath = cast(mDrawPushClipPath)dlsym(dlib, "MagickDrawPushClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPushDefs = cast(mDrawPushDefs)dlsym(dlib, "MagickDrawPushDefs");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPushGraphicContext = cast(mDrawPushGraphicContext)dlsym(dlib, "MagickDrawPushGraphicContext");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawPushPattern = cast(mDrawPushPattern)dlsym(dlib, "MagickDrawPushPattern");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawRectangle = cast(mDrawRectangle)dlsym(dlib, "MagickDrawRectangle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawRotate = cast(mDrawRotate)dlsym(dlib, "MagickDrawRotate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawRoundRectangle = cast(mDrawRoundRectangle)dlsym(dlib, "MagickDrawRoundRectangle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawScale = cast(mDrawScale)dlsym(dlib, "MagickDrawScale");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetClipPath = cast(mDrawSetClipPath)dlsym(dlib, "MagickDrawSetClipPath");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetClipRule = cast(mDrawSetClipRule)dlsym(dlib, "MagickDrawSetClipRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetClipUnits = cast(mDrawSetClipUnits)dlsym(dlib, "MagickDrawSetClipUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFillColor = cast(mDrawSetFillColor)dlsym(dlib, "MagickDrawSetFillColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFillOpacity = cast(mDrawSetFillOpacity)dlsym(dlib, "MagickDrawSetFillOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFillRule = cast(mDrawSetFillRule)dlsym(dlib, "MagickDrawSetFillRule");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFillPatternURL = cast(mDrawSetFillPatternURL)dlsym(dlib, "MagickDrawSetFillPatternURL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFont = cast(mDrawSetFont)dlsym(dlib, "MagickDrawSetFont");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFontFamily = cast(mDrawSetFontFamily)dlsym(dlib, "MagickDrawSetFontFamily");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFontSize = cast(mDrawSetFontSize)dlsym(dlib, "MagickDrawSetFontSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFontStretch = cast(mDrawSetFontStretch)dlsym(dlib, "MagickDrawSetFontStretch");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFontStyle = cast(mDrawSetFontStyle)dlsym(dlib, "MagickDrawSetFontStyle");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetFontWeight = cast(mDrawSetFontWeight)dlsym(dlib, "MagickDrawSetFontWeight");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetGravity = cast(mDrawSetGravity)dlsym(dlib, "MagickDrawSetGravity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSkewX = cast(mDrawSkewX)dlsym(dlib, "MagickDrawSkewX");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSkewY = cast(mDrawSkewY)dlsym(dlib, "MagickDrawSkewY");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeAntialias = cast(mDrawSetStrokeAntialias)dlsym(dlib, "MagickDrawSetStrokeAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeColor = cast(mDrawSetStrokeColor)dlsym(dlib, "MagickDrawSetStrokeColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeDashArray = cast(mDrawSetStrokeDashArray)dlsym(dlib, "MagickDrawSetStrokeDashArray");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeDashOffset = cast(mDrawSetStrokeDashOffset)dlsym(dlib, "MagickDrawSetStrokeDashOffset");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeLineCap = cast(mDrawSetStrokeLineCap)dlsym(dlib, "MagickDrawSetStrokeLineCap");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeLineJoin = cast(mDrawSetStrokeLineJoin)dlsym(dlib, "MagickDrawSetStrokeLineJoin");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeMiterLimit = cast(mDrawSetStrokeMiterLimit)dlsym(dlib, "MagickDrawSetStrokeMiterLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeOpacity = cast(mDrawSetStrokeOpacity)dlsym(dlib, "MagickDrawSetStrokeOpacity");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokePatternURL = cast(mDrawSetStrokePatternURL)dlsym(dlib, "MagickDrawSetStrokePatternURL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetStrokeWidth = cast(mDrawSetStrokeWidth)dlsym(dlib, "MagickDrawSetStrokeWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetTextAntialias = cast(mDrawSetTextAntialias)dlsym(dlib, "MagickDrawSetTextAntialias");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetTextDecoration = cast(mDrawSetTextDecoration)dlsym(dlib, "MagickDrawSetTextDecoration");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetTextEncoding = cast(mDrawSetTextEncoding)dlsym(dlib, "MagickDrawSetTextEncoding");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetTextUnderColor = cast(mDrawSetTextUnderColor)dlsym(dlib, "MagickDrawSetTextUnderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawSetViewbox = cast(mDrawSetViewbox)dlsym(dlib, "MagickDrawSetViewbox");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawTranslate = cast(mDrawTranslate)dlsym(dlib, "MagickDrawTranslate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.drawing_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
