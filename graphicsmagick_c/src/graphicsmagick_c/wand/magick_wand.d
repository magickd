/*
  ImageMagick MagickWand interface.
*/
// Complete graphicsmagick_c wrapper for version 1.3.
module graphicsmagick_c.wand.magick_wand;

import core.stdc.config : c_long, c_ulong;
import core.stdc.stdarg;
import core.stdc.stdio : FILE;

import graphicsmagick_c.magick;

import graphicsmagick_c.wand.drawing_wand : DrawingWand;
import graphicsmagick_c.wand.pixel_wand : PixelWand;

alias MagickSizeType = magick_int64_t;
alias ReplaceCompositeOp = CopyCompositeOp;
alias IndexChannel = BlackChannel;
alias AreaResource = UndefinedResource; /* not supported */

extern struct MagickWand;

version (GMagick_Static)
{
    @system @nogc nothrow extern (C):

    int FormatMagickString(char*, const scope size_t, const scope char*, ...);
    size_t CopyMagickString(char*, const scope char*, const scope size_t);

    char* MagickDescribeImage(MagickWand*);
    char* MagickGetConfigureInfo(MagickWand*, const scope char*);
    char* MagickGetException(const scope MagickWand*, ExceptionType*);
    char* MagickGetFilename(const scope MagickWand*);
    char* MagickGetImageFilename(const scope MagickWand*);
    char* MagickGetImageFormat(const scope MagickWand*);
    char* MagickGetImageSignature(const scope MagickWand*);
    char** MagickQueryFonts(const scope char*, c_ulong*);
    char** MagickQueryFormats(const scope char*, c_ulong*);

    CompositeOperator MagickGetImageCompose(MagickWand*);

    ColorspaceType MagickGetImageColorspace(MagickWand*);

    CompressionType MagickGetImageCompression(MagickWand*);

    const(char)* MagickGetCopyright();
    const(char)* MagickGetHomeURL();
    const(char)* MagickGetPackageName();
    const(char)* MagickGetQuantumDepth(c_ulong*);
    const(char)* MagickGetReleaseDate();
    const(char)* MagickGetVersion(c_ulong*);

    DisposeType MagickGetImageDispose(MagickWand*);

    double MagickGetImageGamma(MagickWand*);
    double* MagickGetSamplingFactors(MagickWand*, c_ulong*);
    double* MagickQueryFontMetrics(MagickWand*, const scope DrawingWand*, const scope char*);

    ImageType MagickGetImageType(MagickWand*);

    InterlaceType MagickGetImageInterlaceScheme(MagickWand*);

    c_long MagickGetImageIndex(MagickWand*);

    MagickSizeType MagickGetImageSize(MagickWand*);

    MagickWand* CloneMagickWand(const scope MagickWand*);
    MagickWand* MagickAppendImages(MagickWand*, const scope uint);
    MagickWand* MagickAverageImages(MagickWand*);
    MagickWand* MagickCoalesceImages(MagickWand*);
    MagickWand* MagickCompareImageChannels(MagickWand*, const scope MagickWand*, const scope ChannelType,
        const scope MetricType, double*);
    MagickWand* MagickCompareImages(MagickWand*, const scope MagickWand*, const scope MetricType,
        double*);
    MagickWand* MagickDeconstructImages(MagickWand*);
    MagickWand* MagickFlattenImages(MagickWand*);
    MagickWand* MagickFxImage(MagickWand*, const scope char*);
    MagickWand* MagickFxImageChannel(MagickWand*, const scope ChannelType, const scope char*);
    MagickWand* MagickGetImage(MagickWand*);
    MagickWand* MagickMorphImages(MagickWand*, const scope c_ulong);
    MagickWand* MagickMosaicImages(MagickWand*);
    MagickWand* MagickMontageImage(MagickWand*, const scope DrawingWand*, const scope char*,
        const scope char*, const scope MontageMode, const scope char*);
    MagickWand* MagickPreviewImages(MagickWand*, const scope PreviewType);
    MagickWand* MagickSteganoImage(MagickWand*, const scope MagickWand*, const scope c_long);
    MagickWand* MagickStereoImage(MagickWand*, const scope MagickWand*);
    MagickWand* MagickTextureImage(MagickWand*, const scope MagickWand*);
    MagickWand* MagickTransformImage(MagickWand*, const scope char*, const scope char*);
    MagickWand* NewMagickWand();

    PixelWand** MagickGetImageHistogram(MagickWand*, c_ulong*);

    RenderingIntent MagickGetImageRenderingIntent(MagickWand*);

    ResolutionType MagickGetImageUnits(MagickWand*);

    uint DestroyMagickWand(MagickWand*);
    uint MagickAdaptiveThresholdImage(MagickWand*, const scope c_ulong, const scope c_ulong,
        const scope c_long);
    uint MagickAddImage(MagickWand*, const scope MagickWand*);
    uint MagickAddNoiseImage(MagickWand*, const scope NoiseType);
    uint MagickAffineTransformImage(MagickWand*, const scope DrawingWand*);
    uint MagickAnnotateImage(MagickWand*, const scope DrawingWand*, const scope double,
        const scope double, const scope double, const scope char*);
    uint MagickAnimateImages(MagickWand*, const scope char*);
    uint MagickBlackThresholdImage(MagickWand*, const scope PixelWand*);
    uint MagickBlurImage(MagickWand*, const scope double, const scope double);
    uint MagickBorderImage(MagickWand*, const scope PixelWand*, const scope c_ulong, const scope c_ulong);
    uint MagickCharcoalImage(MagickWand*, const scope double, const scope double);
    uint MagickChopImage(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long,
        const scope c_long);
    uint MagickClipImage(MagickWand*);
    uint MagickClipPathImage(MagickWand*, const scope char*, const scope uint);
    uint MagickColorFloodfillImage(MagickWand*, const scope PixelWand*, const scope double,
        const scope PixelWand*, const scope c_long, const scope c_long);
    uint MagickColorizeImage(MagickWand*, const scope PixelWand*, const scope PixelWand*);
    uint MagickCommentImage(MagickWand*, const scope char*);
    uint MagickCompositeImage(MagickWand*, const scope MagickWand*, const scope CompositeOperator,
        const scope c_long, const scope c_long);
    uint MagickContrastImage(MagickWand*, const scope uint);
    uint MagickConvolveImage(MagickWand*, const scope c_ulong, const scope double*);
    uint MagickCropImage(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long,
        const scope c_long);
    uint MagickCycleColormapImage(MagickWand*, const scope c_long);
    uint MagickDespeckleImage(MagickWand*, const scope c_long);
    uint MagickDisplayImage(MagickWand*, const scope char*);
    uint MagickDisplayImages(MagickWand*, const scope char*);
    uint MagickDrawImage(MagickWand*, const scope DrawingWand*);
    uint MagickEdgeImage(MagickWand*, const scope double);
    uint MagickEmbossImage(MagickWand*, const scope double, const scope double);
    uint MagickEnhanceImage(MagickWand*);
    uint MagickEqualizeImage(MagickWand*);
    uint MagickFlipImage(MagickWand*);
    uint MagickFlopImage(MagickWand*);
    uint MagickFrameImage(MagickWand*, const scope PixelWand*, const scope c_ulong, const scope c_ulong,
        const scope c_long, const scope c_long);
    uint MagickGammaImage(MagickWand*, const scope double);
    uint MagickGammaImageChannel(MagickWand*, const scope ChannelType, const scope double);
    uint MagickGetImageBackgroundColor(MagickWand*, PixelWand*);
    uint MagickGetImageBluePrimary(MagickWand*, double*, double*);
    uint MagickGetImageBorderColor(MagickWand*, PixelWand*);
    uint MagickGetImageChannelExtrema(MagickWand*, const scope ChannelType, c_ulong*,
        c_ulong*);
    uint MagickGetImageChannelMean(MagickWand*, const scope ChannelType, double*, double*);
    uint MagickGetImageColormapColor(MagickWand*, const scope c_ulong, PixelWand*);
    uint MagickGetImageExtrema(MagickWand*, c_ulong*, c_ulong*);
    uint MagickGetImageGreenPrimary(MagickWand*, double*, double*);
    uint MagickGetImageMatteColor(MagickWand*, PixelWand*);
    uint MagickGetImagePixels(MagickWand*, const scope c_long, const scope c_long, const scope c_ulong,
        const scope c_ulong, const scope char*, const scope StorageType, ubyte*);
    uint MagickGetImageRedPrimary(MagickWand*, double*, double*);
    uint MagickGetImageResolution(MagickWand*, double*, double*);
    uint MagickGetImageWhitePoint(MagickWand*, double*, double*);
    uint MagickGetSize(const scope MagickWand*, c_ulong*, c_ulong*);
    uint MagickHasNextImage(MagickWand*);
    uint MagickHasPreviousImage(MagickWand*);
    uint MagickImplodeImage(MagickWand*, const scope double);
    uint MagickLabelImage(MagickWand*, const scope char*);
    uint MagickLevelImage(MagickWand*, const scope double, const scope double, const scope double);
    uint MagickLevelImageChannel(MagickWand*, const scope ChannelType, const scope double, const scope double, const scope double);
    uint MagickMagnifyImage(MagickWand*);
    uint MagickMapImage(MagickWand*, const scope MagickWand*, const scope uint);
    uint MagickMatteFloodfillImage(MagickWand*, const scope Quantum, const scope double, const scope PixelWand*, const scope c_long, const scope c_long);
    uint MagickMedianFilterImage(MagickWand*, const scope double);
    uint MagickMinifyImage(MagickWand*);
    uint MagickModulateImage(MagickWand*, const scope double, const scope double, const scope double);
    uint MagickMotionBlurImage(MagickWand*, const scope double, const scope double, const scope double);
    uint MagickNegateImage(MagickWand*, const scope uint);
    uint MagickNegateImageChannel(MagickWand*, const scope ChannelType, const scope uint);
    uint MagickNextImage(MagickWand*);
    uint MagickNormalizeImage(MagickWand*);
    uint MagickOilPaintImage(MagickWand*, const scope double);
    uint MagickOpaqueImage(MagickWand*, const scope PixelWand*, const scope PixelWand*, const scope double);
    uint MagickPingImage(MagickWand*, const scope char*);
    uint MagickPreviousImage(MagickWand*);
    uint MagickProfileImage(MagickWand*, const scope char*, const scope ubyte*, const scope c_ulong);
    uint MagickQuantizeImage(MagickWand*, const scope c_ulong, const scope ColorspaceType, const scope c_ulong, const scope uint, const scope uint);
    uint MagickQuantizeImages(MagickWand*, const scope c_ulong, const scope ColorspaceType, const scope c_ulong, const scope uint, const scope uint);
    uint MagickRadialBlurImage(MagickWand*, const scope double);
    uint MagickRaiseImage(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long, const scope c_long, const scope uint);
    uint MagickReadImage(MagickWand*, const scope char*);
    uint MagickReadImageBlob(MagickWand*, const scope ubyte*, const scope size_t length);
    uint MagickReadImageFile(MagickWand*, FILE*);
    uint MagickReduceNoiseImage(MagickWand*, const scope double);
    uint MagickRelinquishMemory(void*);
    uint MagickRemoveImage(MagickWand*);
    uint MagickResampleImage(MagickWand*, const scope double, const scope double, const scope FilterTypes, const scope double);
    uint MagickResizeImage(MagickWand*, const scope c_ulong, const scope c_ulong, const scope FilterTypes, const scope double);
    uint MagickRollImage(MagickWand*, const scope c_long, const scope c_long);
    uint MagickRotateImage(MagickWand*, const scope PixelWand*, const scope double);
    uint MagickSampleImage(MagickWand*, const scope c_ulong, const scope c_ulong);
    uint MagickScaleImage(MagickWand*, const scope c_ulong, const scope c_ulong);
    uint MagickSeparateImageChannel(MagickWand*, const scope ChannelType);
    uint MagickSetImage(MagickWand*, const scope MagickWand*);
    uint MagickSetFilename(MagickWand*, const scope char*);
    uint MagickSetImageBackgroundColor(MagickWand*, const scope PixelWand*);
    uint MagickSetImageBluePrimary(MagickWand*, const scope double, const scope double);
    uint MagickSetImageBorderColor(MagickWand*, const scope PixelWand*);
    uint MagickSetImageChannelDepth(MagickWand*, const scope ChannelType, const scope c_ulong);
    uint MagickSetImageColormapColor(MagickWand*, const scope c_ulong, const scope PixelWand*);
    uint MagickSetImageCompose(MagickWand*, const scope CompositeOperator);
    uint MagickSetImageCompression(MagickWand*, const scope CompressionType);
    uint MagickSetImageDelay(MagickWand*, const scope c_ulong);
    uint MagickSetImageDepth(MagickWand*, const scope c_ulong);
    uint MagickSetImageDispose(MagickWand*, const scope DisposeType);
    uint MagickSetImageColorspace(MagickWand*, const scope ColorspaceType);
    uint MagickSetImageGreenPrimary(MagickWand*, const scope double, const scope double);
    uint MagickSetImageGamma(MagickWand*, const scope double);
    uint MagickSetImageFilename(MagickWand*, const scope char*);
    uint MagickSetImageFormat(MagickWand*wand, const scope char* format);
    uint MagickSetImageIndex(MagickWand*, const scope c_long);
    uint MagickSetImageInterlaceScheme(MagickWand*, const scope InterlaceType);
    uint MagickSetImageIterations(MagickWand*, const scope c_ulong);
    uint MagickSetImageMatteColor(MagickWand*, const scope PixelWand*);
    uint MagickSetImageOption(MagickWand*, const scope char*, const scope char*, const scope char*);
    uint MagickSetImagePixels(MagickWand*, const scope c_long, const scope c_long, const scope c_ulong, const scope c_ulong, const scope char*, const scope StorageType,
        ubyte*);
    uint MagickSetImageRedPrimary(MagickWand*, const scope double, const scope double);
    uint MagickSetImageRenderingIntent(MagickWand*, const scope RenderingIntent);
    uint MagickSetImageResolution(MagickWand*, const scope double, const scope double);
    uint MagickSetImageScene(MagickWand*, const scope c_ulong);
    uint MagickSetImageType(MagickWand*, const scope ImageType);
    uint MagickSetImageUnits(MagickWand*, const scope ResolutionType);
    uint MagickSetImageVirtualPixelMethod(MagickWand*, const scope VirtualPixelMethod);
    uint MagickSetPassphrase(MagickWand*, const scope char*);
    uint MagickSetImageProfile(MagickWand*, const scope char*, const scope ubyte*,  const scope c_ulong);
    uint MagickSetResourceLimit(const scope ResourceType type, const scope c_ulong limit);
    uint MagickSetSamplingFactors(MagickWand*, const scope c_ulong, const scope double*);
    uint MagickSetSize(MagickWand*, const scope c_ulong, const scope c_ulong);
    uint MagickSetImageWhitePoint(MagickWand*, const scope double, const scope double);
    uint MagickSetInterlaceScheme(MagickWand*, const scope InterlaceType);
    uint MagickSharpenImage(MagickWand*, const scope double, const scope double);
    uint MagickShaveImage(MagickWand*, const scope c_ulong, const scope c_ulong);
    uint MagickShearImage(MagickWand*, const scope PixelWand*, const scope double, const scope double);
    uint MagickSolarizeImage(MagickWand*, const scope double);
    uint MagickSpreadImage(MagickWand*, const scope double);
    uint MagickStripImage(MagickWand*);
    uint MagickSwirlImage(MagickWand*, const scope double);
    uint MagickTintImage(MagickWand*, const scope PixelWand*, const scope PixelWand*);
    uint MagickThresholdImage(MagickWand*, const scope double);
    uint MagickThresholdImageChannel(MagickWand*, const scope ChannelType, const scope double);
    uint MagickTransparentImage(MagickWand*, const scope PixelWand*, const scope Quantum, const scope double);
    uint MagickTrimImage(MagickWand*, const scope double);
    uint MagickUnsharpMaskImage(MagickWand*, const scope double, const scope double, const scope double, const scope double);
    uint MagickWaveImage(MagickWand*, const scope double, const scope double);
    uint MagickWhiteThresholdImage(MagickWand*, const scope PixelWand*);
    uint MagickWriteImage(MagickWand*, const scope char*);
    uint MagickWriteImageFile(MagickWand*, FILE*);
    uint MagickWriteImages(MagickWand*, const scope char*, const scope uint);

    c_ulong MagickGetImageColors(MagickWand*);
    c_ulong MagickGetImageDelay(MagickWand*);
    c_ulong MagickGetImageChannelDepth(MagickWand*, const scope ChannelType);
    c_ulong MagickGetImageDepth(MagickWand*);
    c_ulong MagickGetImageHeight(MagickWand*);
    c_ulong MagickGetImageIterations(MagickWand*);
    c_ulong MagickGetImageScene(MagickWand*);
    c_ulong MagickGetImageWidth(MagickWand*);
    c_ulong MagickGetNumberImages(MagickWand*);
    c_ulong MagickGetResourceLimit(const scope ResourceType);

    VirtualPixelMethod MagickGetImageVirtualPixelMethod(MagickWand*);

    ubyte* MagickGetImageProfile(MagickWand*, const scope char*, c_ulong*);
    ubyte* MagickRemoveImageProfile(MagickWand*, const scope char*, c_ulong*);
    ubyte* MagickWriteImageBlob(MagickWand*, size_t*);

    void MagickResetIterator(MagickWand*);

} // version (GMagick_Static)
else
{
    @system @nogc nothrow extern (C)
    {
        alias mFormatMagickString = int function(char*, const scope size_t, const scope char*, ...);
        alias mCopyMagickString = size_t function(char*, const scope char*, const scope size_t);

        alias mMagickDescribeImage = char* function(MagickWand*);
        alias mMagickGetConfigureInfo = char* function(MagickWand*, const scope char*);
        alias mMagickGetException = char* function(const scope MagickWand*, ExceptionType*);
        alias mMagickGetFilename = char* function(const scope MagickWand*);
        alias mMagickGetImageFilename = char* function(const scope MagickWand*);
        alias mMagickGetImageFormat = char* function(const scope MagickWand*);
        alias mMagickGetImageSignature = char* function(const scope MagickWand*);
        alias mMagickQueryFonts = char** function(const scope char*, c_ulong*);
        alias mMagickQueryFormats = char** function(const scope char*, c_ulong*);

        alias mMagickGetImageCompose = CompositeOperator function(MagickWand*);

        alias mMagickGetImageColorspace = ColorspaceType function(MagickWand*);

        alias mMagickGetImageCompression = CompressionType function(MagickWand*);

        alias mMagickGetCopyright = const(char)* function();
        alias mMagickGetHomeURL = const(char)* function();
        alias mMagickGetPackageName = const(char)* function();
        alias mMagickGetQuantumDepth = const(char)* function(c_ulong*);
        alias mMagickGetReleaseDate = const(char)* function();
        alias mMagickGetVersion = const(char)* function(c_ulong*);

        alias mMagickGetImageDispose = DisposeType function(MagickWand*);

        alias mMagickGetImageGamma = double function(MagickWand*);
        alias mMagickGetSamplingFactors = double* function(MagickWand*, c_ulong*);
        alias mMagickQueryFontMetrics = double* function(MagickWand*, const scope DrawingWand*, const scope char*);

        alias mMagickGetImageType = ImageType function(MagickWand*);

        alias mMagickGetImageInterlaceScheme = InterlaceType function(MagickWand*);

        alias mMagickGetImageIndex = c_long function(MagickWand*);

        alias mMagickGetImageSize = MagickSizeType function(MagickWand*);

        alias mCloneMagickWand = MagickWand* function(const scope MagickWand*);
        alias mMagickAppendImages = MagickWand* function(MagickWand*, const scope uint);
        alias mMagickAverageImages = MagickWand* function(MagickWand*);
        alias mMagickCoalesceImages = MagickWand* function(MagickWand*);
        alias mMagickCompareImageChannels = MagickWand* function(MagickWand*, const scope MagickWand*, const scope ChannelType,
            const scope MetricType, double*);
        alias mMagickCompareImages = MagickWand* function(MagickWand*, const scope MagickWand*, const scope MetricType,
            double*);
        alias mMagickDeconstructImages = MagickWand* function(MagickWand*);
        alias mMagickFlattenImages = MagickWand* function(MagickWand*);
        alias mMagickFxImage = MagickWand* function(MagickWand*, const scope char*);
        alias mMagickFxImageChannel = MagickWand* function(MagickWand*, const scope ChannelType, const scope char*);
        alias mMagickGetImage = MagickWand* function(MagickWand*);
        alias mMagickMorphImages = MagickWand* function(MagickWand*, const scope c_ulong);
        alias mMagickMosaicImages = MagickWand* function(MagickWand*);
        alias mMagickMontageImage = MagickWand* function(MagickWand*, const scope DrawingWand*, const scope char*,
            const scope char*, const scope MontageMode, const scope char*);
        alias mMagickPreviewImages = MagickWand* function(MagickWand*, const scope PreviewType);
        alias mMagickSteganoImage = MagickWand* function(MagickWand*, const scope MagickWand*, const scope c_long);
        alias mMagickStereoImage = MagickWand* function(MagickWand*, const scope MagickWand*);
        alias mMagickTextureImage = MagickWand* function(MagickWand*, const scope MagickWand*);
        alias mMagickTransformImage = MagickWand* function(MagickWand*, const scope char*, const scope char*);
        alias mNewMagickWand = MagickWand* function();

        alias mMagickGetImageHistogram = PixelWand** function(MagickWand*, c_ulong*);

        alias mMagickGetImageRenderingIntent = RenderingIntent function(MagickWand*);

        alias mMagickGetImageUnits = ResolutionType function(MagickWand*);

        alias mDestroyMagickWand = uint function(MagickWand*);
        alias mMagickAdaptiveThresholdImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong,
            const scope c_long);
        alias mMagickAddImage = uint function(MagickWand*, const scope MagickWand*);
        alias mMagickAddNoiseImage = uint function(MagickWand*, const scope NoiseType);
        alias mMagickAffineTransformImage = uint function(MagickWand*, const scope DrawingWand*);
        alias mMagickAnnotateImage = uint function(MagickWand*, const scope DrawingWand*, const scope double,
            const scope double, const scope double, const scope char*);
        alias mMagickAnimateImages = uint function(MagickWand*, const scope char*);
        alias mMagickBlackThresholdImage = uint function(MagickWand*, const scope PixelWand*);
        alias mMagickBlurImage = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickBorderImage = uint function(MagickWand*, const scope PixelWand*, const scope c_ulong, const scope c_ulong);
        alias mMagickCharcoalImage = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickChopImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long,
            const scope c_long);
        alias mMagickClipImage = uint function(MagickWand*);
        alias mMagickClipPathImage = uint function(MagickWand*, const scope char*, const scope uint);
        alias mMagickColorFloodfillImage = uint function(MagickWand*, const scope PixelWand*, const scope double,
            const scope PixelWand*, const scope c_long, const scope c_long);
        alias mMagickColorizeImage = uint function(MagickWand*, const scope PixelWand*, const scope PixelWand*);
        alias mMagickCommentImage = uint function(MagickWand*, const scope char*);
        alias mMagickCompositeImage = uint function(MagickWand*, const scope MagickWand*, const scope CompositeOperator,
            const scope c_long, const scope c_long);
        alias mMagickContrastImage = uint function(MagickWand*, const scope uint);
        alias mMagickConvolveImage = uint function(MagickWand*, const scope c_ulong, const scope double*);
        alias mMagickCropImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long,
            const scope c_long);
        alias mMagickCycleColormapImage = uint function(MagickWand*, const scope c_long);
        alias mMagickDespeckleImage = uint function(MagickWand*, const scope c_long);
        alias mMagickDisplayImage = uint function(MagickWand*, const scope char*);
        alias mMagickDisplayImages = uint function(MagickWand*, const scope char*);
        alias mMagickDrawImage = uint function(MagickWand*, const scope DrawingWand*);
        alias mMagickEdgeImage = uint function(MagickWand*, const scope double);
        alias mMagickEmbossImage = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickEnhanceImage = uint function(MagickWand*);
        alias mMagickEqualizeImage = uint function(MagickWand*);
        alias mMagickFlipImage = uint function(MagickWand*);
        alias mMagickFlopImage = uint function(MagickWand*);
        alias mMagickFrameImage = uint function(MagickWand*, const scope PixelWand*, const scope c_ulong, const scope c_ulong,
            const scope c_long, const scope c_long);
        alias mMagickGammaImage = uint function(MagickWand*, const scope double);
        alias mMagickGammaImageChannel = uint function(MagickWand*, const scope ChannelType, const scope double);
        alias mMagickGetImageBackgroundColor = uint function(MagickWand*, PixelWand*);
        alias mMagickGetImageBluePrimary = uint function(MagickWand*, double*, double*);
        alias mMagickGetImageBorderColor = uint function(MagickWand*, PixelWand*);
        alias mMagickGetImageChannelExtrema = uint function(MagickWand*, const scope ChannelType, c_ulong*,
            c_ulong*);
        alias mMagickGetImageChannelMean = uint function(MagickWand*, const scope ChannelType, double*, double*);
        alias mMagickGetImageColormapColor = uint function(MagickWand*, const scope c_ulong, PixelWand*);
        alias mMagickGetImageExtrema = uint function(MagickWand*, c_ulong*, c_ulong*);
        alias mMagickGetImageGreenPrimary = uint function(MagickWand*, double*, double*);
        alias mMagickGetImageMatteColor = uint function(MagickWand*, PixelWand*);
        alias mMagickGetImagePixels = uint function(MagickWand*, const scope c_long, const scope c_long, const scope c_ulong,
            const scope c_ulong, const scope char*, const scope StorageType, ubyte*);
        alias mMagickGetImageRedPrimary = uint function(MagickWand*, double*, double*);
        alias mMagickGetImageResolution = uint function(MagickWand*, double*, double*);
        alias mMagickGetImageWhitePoint = uint function(MagickWand*, double*, double*);
        alias mMagickGetSize = uint function(const scope MagickWand*, c_ulong*, c_ulong*);
        alias mMagickHasNextImage = uint function(MagickWand*);
        alias mMagickHasPreviousImage = uint function(MagickWand*);
        alias mMagickImplodeImage = uint function(MagickWand*, const scope double);
        alias mMagickLabelImage = uint function(MagickWand*, const scope char*);
        alias mMagickLevelImage = uint function(MagickWand*, const scope double, const scope double, const scope double);
        alias mMagickLevelImageChannel = uint function(MagickWand*, const scope ChannelType, const scope double, const scope double, const scope double);
        alias mMagickMagnifyImage = uint function(MagickWand*);
        alias mMagickMapImage = uint function(MagickWand*, const scope MagickWand*, const scope uint);
        alias mMagickMatteFloodfillImage = uint function(MagickWand*, const scope Quantum, const scope double, const scope PixelWand*, const scope c_long,
            const scope c_long);
        alias mMagickMedianFilterImage = uint function(MagickWand*, const scope double);
        alias mMagickMinifyImage = uint function(MagickWand*);
        alias mMagickModulateImage = uint function(MagickWand*, const scope double, const scope double, const scope double);
        alias mMagickMotionBlurImage = uint function(MagickWand*, const scope double, const scope double, const scope double);
        alias mMagickNegateImage = uint function(MagickWand*, const scope uint);
        alias mMagickNegateImageChannel = uint function(MagickWand*, const scope ChannelType, const scope uint);
        alias mMagickNextImage = uint function(MagickWand*);
        alias mMagickNormalizeImage = uint function(MagickWand*);
        alias mMagickOilPaintImage = uint function(MagickWand*, const scope double);
        alias mMagickOpaqueImage = uint function(MagickWand*, const scope PixelWand*, const scope PixelWand*, const scope double);
        alias mMagickPingImage = uint function(MagickWand*, const scope char*);
        alias mMagickPreviousImage = uint function(MagickWand*);
        alias mMagickProfileImage = uint function(MagickWand*, const scope char*, const scope ubyte*, const scope c_ulong);
        alias mMagickQuantizeImage = uint function(MagickWand*, const scope c_ulong, const scope ColorspaceType, const scope c_ulong, const scope uint,
            const scope uint);
        alias mMagickQuantizeImages = uint function(MagickWand*, const scope c_ulong, const scope ColorspaceType, const scope c_ulong, const scope uint,
            const scope uint);
        alias mMagickRadialBlurImage = uint function(MagickWand*, const scope double);
        alias mMagickRaiseImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong, const scope c_long, const scope c_long, const scope uint);
        alias mMagickReadImage = uint function(MagickWand*, const scope char*);
        alias mMagickReadImageBlob = uint function(MagickWand*, const scope ubyte*, const scope size_t length);
        alias mMagickReadImageFile = uint function(MagickWand*, FILE*);
        alias mMagickReduceNoiseImage = uint function(MagickWand*, const scope double);
        alias mMagickRelinquishMemory = uint function(void*);
        alias mMagickRemoveImage = uint function(MagickWand*);
        alias mMagickResampleImage = uint function(MagickWand*, const scope double, const scope double, const scope FilterTypes, const scope double);
        alias mMagickResizeImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong, const scope FilterTypes, const scope double);
        alias mMagickRollImage = uint function(MagickWand*, const scope c_long, const scope c_long);
        alias mMagickRotateImage = uint function(MagickWand*, const scope PixelWand*, const scope double);
        alias mMagickSampleImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong);
        alias mMagickScaleImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong);
        alias mMagickSeparateImageChannel = uint function(MagickWand*, const scope ChannelType);
        alias mMagickSetImage = uint function(MagickWand*, const scope MagickWand*);
        alias mMagickSetFilename = uint function(MagickWand*, const scope char*);
        alias mMagickSetImageBackgroundColor = uint function(MagickWand*, const scope PixelWand*);
        alias mMagickSetImageBluePrimary = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickSetImageBorderColor = uint function(MagickWand*, const scope PixelWand*);
        alias mMagickSetImageChannelDepth = uint function(MagickWand*, const scope ChannelType, const scope c_ulong);
        alias mMagickSetImageColormapColor = uint function(MagickWand*, const scope c_ulong, const scope PixelWand*);
        alias mMagickSetImageCompose = uint function(MagickWand*, const scope CompositeOperator);
        alias mMagickSetImageCompression = uint function(MagickWand*, const scope CompressionType);
        alias mMagickSetImageDelay = uint function(MagickWand*, const scope c_ulong);
        alias mMagickSetImageDepth = uint function(MagickWand*, const scope c_ulong);
        alias mMagickSetImageDispose = uint function(MagickWand*, const scope DisposeType);
        alias mMagickSetImageColorspace = uint function(MagickWand*, const scope ColorspaceType);
        alias mMagickSetImageGreenPrimary = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickSetImageGamma = uint function(MagickWand*, const scope double);
        alias mMagickSetImageFilename = uint function(MagickWand*, const scope char*);
        alias mMagickSetImageFormat = uint function(MagickWand*wand, const scope char* format);
        alias mMagickSetImageIndex = uint function(MagickWand*, const scope c_long);
        alias mMagickSetImageInterlaceScheme = uint function(MagickWand*, const scope InterlaceType);
        alias mMagickSetImageIterations = uint function(MagickWand*, const scope c_ulong);
        alias mMagickSetImageMatteColor = uint function(MagickWand*, const scope PixelWand*);
        alias mMagickSetImageOption = uint function(MagickWand*, const scope char*, const scope char*, const scope char*);
        alias mMagickSetImagePixels = uint function(MagickWand*, const scope c_long, const scope c_long, const scope c_ulong, const scope c_ulong, const scope char*,
            const scope StorageType, ubyte*);
        alias mMagickSetImageRedPrimary = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickSetImageRenderingIntent = uint function(MagickWand*, const scope RenderingIntent);
        alias mMagickSetImageResolution = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickSetImageScene = uint function(MagickWand*, const scope c_ulong);
        alias mMagickSetImageType = uint function(MagickWand*, const scope ImageType);
        alias mMagickSetImageUnits = uint function(MagickWand*, const scope ResolutionType);
        alias mMagickSetImageVirtualPixelMethod = uint function(MagickWand*, const scope VirtualPixelMethod);
        alias mMagickSetPassphrase = uint function(MagickWand*, const scope char*);
        alias mMagickSetImageProfile = uint function(MagickWand*, const scope char*, const scope ubyte*,  const scope c_ulong);
        alias mMagickSetResourceLimit = uint function(const scope ResourceType type, const scope c_ulong limit);
        alias mMagickSetSamplingFactors = uint function(MagickWand*, const scope c_ulong, const scope double*);
        alias mMagickSetSize = uint function(MagickWand*, const scope c_ulong, const scope c_ulong);
        alias mMagickSetImageWhitePoint = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickSetInterlaceScheme = uint function(MagickWand*, const scope InterlaceType);
        alias mMagickSharpenImage = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickShaveImage = uint function(MagickWand*, const scope c_ulong, const scope c_ulong);
        alias mMagickShearImage = uint function(MagickWand*, const scope PixelWand*, const scope double, const scope double);
        alias mMagickSolarizeImage = uint function(MagickWand*, const scope double);
        alias mMagickSpreadImage = uint function(MagickWand*, const scope double);
        alias mMagickStripImage = uint function(MagickWand*);
        alias mMagickSwirlImage = uint function(MagickWand*, const scope double);
        alias mMagickTintImage = uint function(MagickWand*, const scope PixelWand*, const scope PixelWand*);
        alias mMagickThresholdImage = uint function(MagickWand*, const scope double);
        alias mMagickThresholdImageChannel = uint function(MagickWand*, const scope ChannelType, const scope double);
        alias mMagickTransparentImage = uint function(MagickWand*, const scope PixelWand*, const scope Quantum, const scope double);
        alias mMagickTrimImage = uint function(MagickWand*, const scope double);
        alias mMagickUnsharpMaskImage = uint function(MagickWand*, const scope double, const scope double, const scope double, const scope double);
        alias mMagickWaveImage = uint function(MagickWand*, const scope double, const scope double);
        alias mMagickWhiteThresholdImage = uint function(MagickWand*, const scope PixelWand*);
        alias mMagickWriteImage = uint function(MagickWand*, const scope char*);
        alias mMagickWriteImageFile = uint function(MagickWand*, FILE*);
        alias mMagickWriteImages = uint function(MagickWand*, const scope char*, const scope uint);

        alias mMagickGetImageColors = c_ulong function(MagickWand*);
        alias mMagickGetImageDelay = c_ulong function(MagickWand*);
        alias mMagickGetImageChannelDepth = c_ulong function(MagickWand*, const scope ChannelType);
        alias mMagickGetImageDepth = c_ulong function(MagickWand*);
        alias mMagickGetImageHeight = c_ulong function(MagickWand*);
        alias mMagickGetImageIterations = c_ulong function(MagickWand*);
        alias mMagickGetImageScene = c_ulong function(MagickWand*);
        alias mMagickGetImageWidth = c_ulong function(MagickWand*);
        alias mMagickGetNumberImages = c_ulong function(MagickWand*);
        alias mMagickGetResourceLimit = c_ulong function(const scope ResourceType);

        alias mMagickGetImageVirtualPixelMethod = VirtualPixelMethod function(MagickWand*);

        alias mMagickGetImageProfile = ubyte* function(MagickWand*, const scope char*, c_ulong*);
        alias mMagickRemoveImageProfile = ubyte* function(MagickWand*, const scope char*, c_ulong*);
        alias mMagickWriteImageBlob = ubyte* function(MagickWand*, size_t*);

        alias mMagickResetIterator = void function(MagickWand*);
    }

    __gshared
    {
        mFormatMagickString FormatMagickString;
        mCopyMagickString CopyMagickString;

        mMagickDescribeImage MagickDescribeImage;
        mMagickGetConfigureInfo MagickGetConfigureInfo;
        mMagickGetException MagickGetException;
        mMagickGetFilename MagickGetFilename;
        mMagickGetImageFilename MagickGetImageFilename;
        mMagickGetImageFormat MagickGetImageFormat;
        mMagickGetImageSignature MagickGetImageSignature;
        mMagickQueryFonts MagickQueryFonts;
        mMagickQueryFormats MagickQueryFormats;

        mMagickGetImageCompose MagickGetImageCompose;

        mMagickGetImageColorspace MagickGetImageColorspace;

        mMagickGetImageCompression MagickGetImageCompression;

        mMagickGetCopyright MagickGetCopyright;
        mMagickGetHomeURL MagickGetHomeURL;
        mMagickGetPackageName MagickGetPackageName;
        mMagickGetQuantumDepth MagickGetQuantumDepth;
        mMagickGetReleaseDate MagickGetReleaseDate;
        mMagickGetVersion MagickGetVersion;

        mMagickGetImageDispose MagickGetImageDispose;

        mMagickGetImageGamma MagickGetImageGamma;
        mMagickGetSamplingFactors MagickGetSamplingFactors;
        mMagickQueryFontMetrics MagickQueryFontMetrics;

        mMagickGetImageType MagickGetImageType;

        mMagickGetImageInterlaceScheme MagickGetImageInterlaceScheme;

        mMagickGetImageIndex MagickGetImageIndex;

        mMagickGetImageSize MagickGetImageSize;

        mCloneMagickWand CloneMagickWand;
        mMagickAppendImages MagickAppendImages;
        mMagickAverageImages MagickAverageImages;
        mMagickCoalesceImages MagickCoalesceImages;
        mMagickCompareImageChannels MagickCompareImageChannels;
        mMagickCompareImages MagickCompareImages;
        mMagickDeconstructImages MagickDeconstructImages;
        mMagickFlattenImages MagickFlattenImages;
        mMagickFxImage MagickFxImage;
        mMagickFxImageChannel MagickFxImageChannel;
        mMagickGetImage MagickGetImage;
        mMagickMorphImages MagickMorphImages;
        mMagickMosaicImages MagickMosaicImages;
        mMagickMontageImage MagickMontageImage;
        mMagickPreviewImages MagickPreviewImages;
        mMagickSteganoImage MagickSteganoImage;
        mMagickStereoImage MagickStereoImage;
        mMagickTextureImage MagickTextureImage;
        mMagickTransformImage MagickTransformImage;
        mNewMagickWand NewMagickWand;

        mMagickGetImageHistogram MagickGetImageHistogram;

        mMagickGetImageRenderingIntent MagickGetImageRenderingIntent;

        mMagickGetImageUnits MagickGetImageUnits;

        mDestroyMagickWand DestroyMagickWand;
        mMagickAdaptiveThresholdImage MagickAdaptiveThresholdImage;
        mMagickAddImage MagickAddImage;
        mMagickAddNoiseImage MagickAddNoiseImage;
        mMagickAffineTransformImage MagickAffineTransformImage;
        mMagickAnnotateImage MagickAnnotateImage;
        mMagickAnimateImages MagickAnimateImages;
        mMagickBlackThresholdImage MagickBlackThresholdImage;
        mMagickBlurImage MagickBlurImage;
        mMagickBorderImage MagickBorderImage;
        mMagickCharcoalImage MagickCharcoalImage;
        mMagickChopImage MagickChopImage;
        mMagickClipImage MagickClipImage;
        mMagickClipPathImage MagickClipPathImage;
        mMagickColorFloodfillImage MagickColorFloodfillImage;
        mMagickColorizeImage MagickColorizeImage;
        mMagickCommentImage MagickCommentImage;
        mMagickCompositeImage MagickCompositeImage;
        mMagickContrastImage MagickContrastImage;
        mMagickConvolveImage MagickConvolveImage;
        mMagickCropImage MagickCropImage;
        mMagickCycleColormapImage MagickCycleColormapImage;
        mMagickDespeckleImage MagickDespeckleImage;
        mMagickDisplayImage MagickDisplayImage;
        mMagickDisplayImages MagickDisplayImages;
        mMagickDrawImage MagickDrawImage;
        mMagickEdgeImage MagickEdgeImage;
        mMagickEmbossImage MagickEmbossImage;
        mMagickEnhanceImage MagickEnhanceImage;
        mMagickEqualizeImage MagickEqualizeImage;
        mMagickFlipImage MagickFlipImage;
        mMagickFlopImage MagickFlopImage;
        mMagickFrameImage MagickFrameImage;
        mMagickGammaImage MagickGammaImage;
        mMagickGammaImageChannel MagickGammaImageChannel;
        mMagickGetImageBackgroundColor MagickGetImageBackgroundColor;
        mMagickGetImageBluePrimary MagickGetImageBluePrimary;
        mMagickGetImageBorderColor MagickGetImageBorderColor;
        mMagickGetImageChannelExtrema MagickGetImageChannelExtrema;
        mMagickGetImageChannelMean MagickGetImageChannelMean;
        mMagickGetImageColormapColor MagickGetImageColormapColor;
        mMagickGetImageExtrema MagickGetImageExtrema;
        mMagickGetImageGreenPrimary MagickGetImageGreenPrimary;
        mMagickGetImageMatteColor MagickGetImageMatteColor;
        mMagickGetImagePixels MagickGetImagePixels;
        mMagickGetImageRedPrimary MagickGetImageRedPrimary;
        mMagickGetImageResolution MagickGetImageResolution;
        mMagickGetImageWhitePoint MagickGetImageWhitePoint;
        mMagickGetSize MagickGetSize;
        mMagickHasNextImage MagickHasNextImage;
        mMagickHasPreviousImage MagickHasPreviousImage;
        mMagickImplodeImage MagickImplodeImage;
        mMagickLabelImage MagickLabelImage;
        mMagickLevelImage MagickLevelImage;
        mMagickLevelImageChannel MagickLevelImageChannel;
        mMagickMagnifyImage MagickMagnifyImage;
        mMagickMapImage MagickMapImage;
        mMagickMatteFloodfillImage MagickMatteFloodfillImage;
        mMagickMedianFilterImage MagickMedianFilterImage;
        mMagickMinifyImage MagickMinifyImage;
        mMagickModulateImage MagickModulateImage;
        mMagickMotionBlurImage MagickMotionBlurImage;
        mMagickNegateImage MagickNegateImage;
        mMagickNegateImageChannel MagickNegateImageChannel;
        mMagickNextImage MagickNextImage;
        mMagickNormalizeImage MagickNormalizeImage;
        mMagickOilPaintImage MagickOilPaintImage;
        mMagickOpaqueImage MagickOpaqueImage;
        mMagickPingImage MagickPingImage;
        mMagickPreviousImage MagickPreviousImage;
        mMagickProfileImage MagickProfileImage;
        mMagickQuantizeImage MagickQuantizeImage;
        mMagickQuantizeImages MagickQuantizeImages;
        mMagickRadialBlurImage MagickRadialBlurImage;
        mMagickRaiseImage MagickRaiseImage;
        mMagickReadImage MagickReadImage;
        mMagickReadImageBlob MagickReadImageBlob;
        mMagickReadImageFile MagickReadImageFile;
        mMagickReduceNoiseImage MagickReduceNoiseImage;
        mMagickRelinquishMemory MagickRelinquishMemory;
        mMagickRemoveImage MagickRemoveImage;
        mMagickResampleImage MagickResampleImage;
        mMagickResizeImage MagickResizeImage;
        mMagickRollImage MagickRollImage;
        mMagickRotateImage MagickRotateImage;
        mMagickSampleImage MagickSampleImage;
        mMagickScaleImage MagickScaleImage;
        mMagickSeparateImageChannel MagickSeparateImageChannel;
        mMagickSetImage MagickSetImage;
        mMagickSetFilename MagickSetFilename;
        mMagickSetImageBackgroundColor MagickSetImageBackgroundColor;
        mMagickSetImageBluePrimary MagickSetImageBluePrimary;
        mMagickSetImageBorderColor MagickSetImageBorderColor;
        mMagickSetImageChannelDepth MagickSetImageChannelDepth;
        mMagickSetImageColormapColor MagickSetImageColormapColor;
        mMagickSetImageCompose MagickSetImageCompose;
        mMagickSetImageCompression MagickSetImageCompression;
        mMagickSetImageDelay MagickSetImageDelay;
        mMagickSetImageDepth MagickSetImageDepth;
        mMagickSetImageDispose MagickSetImageDispose;
        mMagickSetImageColorspace MagickSetImageColorspace;
        mMagickSetImageGreenPrimary MagickSetImageGreenPrimary;
        mMagickSetImageGamma MagickSetImageGamma;
        mMagickSetImageFilename MagickSetImageFilename;
        mMagickSetImageFormat MagickSetImageFormat;
        mMagickSetImageIndex MagickSetImageIndex;
        mMagickSetImageInterlaceScheme MagickSetImageInterlaceScheme;
        mMagickSetImageIterations MagickSetImageIterations;
        mMagickSetImageMatteColor MagickSetImageMatteColor;
        mMagickSetImageOption MagickSetImageOption;
        mMagickSetImagePixels MagickSetImagePixels;
        mMagickSetImageRedPrimary MagickSetImageRedPrimary;
        mMagickSetImageRenderingIntent MagickSetImageRenderingIntent;
        mMagickSetImageResolution MagickSetImageResolution;
        mMagickSetImageScene MagickSetImageScene;
        mMagickSetImageType MagickSetImageType;
        mMagickSetImageUnits MagickSetImageUnits;
        mMagickSetImageVirtualPixelMethod MagickSetImageVirtualPixelMethod;
        mMagickSetPassphrase MagickSetPassphrase;
        mMagickSetImageProfile MagickSetImageProfile;
        mMagickSetResourceLimit MagickSetResourceLimit;
        mMagickSetSamplingFactors MagickSetSamplingFactors;
        mMagickSetSize MagickSetSize;
        mMagickSetImageWhitePoint MagickSetImageWhitePoint;
        mMagickSetInterlaceScheme MagickSetInterlaceScheme;
        mMagickSharpenImage MagickSharpenImage;
        mMagickShaveImage MagickShaveImage;
        mMagickShearImage MagickShearImage;
        mMagickSolarizeImage MagickSolarizeImage;
        mMagickSpreadImage MagickSpreadImage;
        mMagickStripImage MagickStripImage;
        mMagickSwirlImage MagickSwirlImage;
        mMagickTintImage MagickTintImage;
        mMagickThresholdImage MagickThresholdImage;
        mMagickThresholdImageChannel MagickThresholdImageChannel;
        mMagickTransparentImage MagickTransparentImage;
        mMagickTrimImage MagickTrimImage;
        mMagickUnsharpMaskImage MagickUnsharpMaskImage;
        mMagickWaveImage MagickWaveImage;
        mMagickWhiteThresholdImage MagickWhiteThresholdImage;
        mMagickWriteImage MagickWriteImage;
        mMagickWriteImageFile MagickWriteImageFile;
        mMagickWriteImages MagickWriteImages;

        mMagickGetImageColors MagickGetImageColors;
        mMagickGetImageDelay MagickGetImageDelay;
        mMagickGetImageChannelDepth MagickGetImageChannelDepth;
        mMagickGetImageDepth MagickGetImageDepth;
        mMagickGetImageHeight MagickGetImageHeight;
        mMagickGetImageIterations MagickGetImageIterations;
        mMagickGetImageScene MagickGetImageScene;
        mMagickGetImageWidth MagickGetImageWidth;
        mMagickGetNumberImages MagickGetNumberImages;
        mMagickGetResourceLimit MagickGetResourceLimit;

        mMagickGetImageVirtualPixelMethod MagickGetImageVirtualPixelMethod;

        mMagickGetImageProfile MagickGetImageProfile;
        mMagickRemoveImageProfile MagickRemoveImageProfile;
        mMagickWriteImageBlob MagickWriteImageBlob;

        mMagickResetIterator MagickResetIterator;
    }

    package(graphicsmagick_c)
    {
        import core.sys.posix.dlfcn : dlerror, dlsym;
        import core.stdc.stdio : fprintf, stderr;

        bool _loadMagickWandH(void* dlib)
        {
            const(char)* errmsg;
            bool success = true;

            FormatMagickString = cast(mFormatMagickString)dlsym(dlib, "FormatMagickString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            CopyMagickString = cast(mCopyMagickString)dlsym(dlib, "CopyMagickString");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickDescribeImage = cast(mMagickDescribeImage)dlsym(dlib, "MagickDescribeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetConfigureInfo = cast(mMagickGetConfigureInfo)dlsym(dlib, "MagickGetConfigureInfo");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetException = cast(mMagickGetException)dlsym(dlib, "MagickGetException");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetFilename = cast(mMagickGetFilename)dlsym(dlib, "MagickGetFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageFilename = cast(mMagickGetImageFilename)dlsym(dlib, "MagickGetImageFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageFormat = cast(mMagickGetImageFormat)dlsym(dlib, "MagickGetImageFormat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageSignature = cast(mMagickGetImageSignature)dlsym(dlib, "MagickGetImageSignature");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickQueryFonts = cast(mMagickQueryFonts)dlsym(dlib, "MagickQueryFonts");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickQueryFormats = cast(mMagickQueryFormats)dlsym(dlib, "MagickQueryFormats");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageCompose = cast(mMagickGetImageCompose)dlsym(dlib, "MagickGetImageCompose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageColorspace = cast(mMagickGetImageColorspace)dlsym(dlib, "MagickGetImageColorspace");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageCompression = cast(mMagickGetImageCompression)dlsym(dlib, "MagickGetImageCompression");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetCopyright = cast(mMagickGetCopyright)dlsym(dlib, "MagickGetCopyright");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetHomeURL = cast(mMagickGetHomeURL)dlsym(dlib, "MagickGetHomeURL");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetPackageName = cast(mMagickGetPackageName)dlsym(dlib, "MagickGetPackageName");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetQuantumDepth = cast(mMagickGetQuantumDepth)dlsym(dlib, "MagickGetQuantumDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetReleaseDate = cast(mMagickGetReleaseDate)dlsym(dlib, "MagickGetReleaseDate");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetVersion = cast(mMagickGetVersion)dlsym(dlib, "MagickGetVersion");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageDispose = cast(mMagickGetImageDispose)dlsym(dlib, "MagickGetImageDispose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageGamma = cast(mMagickGetImageGamma)dlsym(dlib, "MagickGetImageGamma");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetSamplingFactors = cast(mMagickGetSamplingFactors)dlsym(dlib, "MagickGetSamplingFactors");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickQueryFontMetrics = cast(mMagickQueryFontMetrics)dlsym(dlib, "MagickQueryFontMetrics");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageType = cast(mMagickGetImageType)dlsym(dlib, "MagickGetImageType");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageInterlaceScheme = cast(mMagickGetImageInterlaceScheme)dlsym(dlib, "MagickGetImageInterlaceScheme");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageIndex = cast(mMagickGetImageIndex)dlsym(dlib, "MagickGetImageIndex");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageSize = cast(mMagickGetImageSize)dlsym(dlib, "MagickGetImageSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            CloneMagickWand = cast(mCloneMagickWand)dlsym(dlib, "CloneMagickWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAppendImages = cast(mMagickAppendImages)dlsym(dlib, "MagickAppendImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAverageImages = cast(mMagickAverageImages)dlsym(dlib, "MagickAverageImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCoalesceImages = cast(mMagickCoalesceImages)dlsym(dlib, "MagickCoalesceImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCompareImageChannels = cast(mMagickCompareImageChannels)dlsym(dlib, "MagickCompareImageChannels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCompareImages = cast(mMagickCompareImages)dlsym(dlib, "MagickCompareImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDeconstructImages = cast(mMagickDeconstructImages)dlsym(dlib, "MagickDeconstructImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFlattenImages = cast(mMagickFlattenImages)dlsym(dlib, "MagickFlattenImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFxImage = cast(mMagickFxImage)dlsym(dlib, "MagickFxImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFxImageChannel = cast(mMagickFxImageChannel)dlsym(dlib, "MagickFxImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImage = cast(mMagickGetImage)dlsym(dlib, "MagickGetImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMorphImages = cast(mMagickMorphImages)dlsym(dlib, "MagickMorphImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMosaicImages = cast(mMagickMosaicImages)dlsym(dlib, "MagickMosaicImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMontageImage = cast(mMagickMontageImage)dlsym(dlib, "MagickMontageImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickPreviewImages = cast(mMagickPreviewImages)dlsym(dlib, "MagickPreviewImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSteganoImage = cast(mMagickSteganoImage)dlsym(dlib, "MagickSteganoImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickStereoImage = cast(mMagickStereoImage)dlsym(dlib, "MagickStereoImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickTextureImage = cast(mMagickTextureImage)dlsym(dlib, "MagickTextureImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickTransformImage = cast(mMagickTransformImage)dlsym(dlib, "MagickTransformImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            NewMagickWand = cast(mNewMagickWand)dlsym(dlib, "NewMagickWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageHistogram = cast(mMagickGetImageHistogram)dlsym(dlib, "MagickGetImageHistogram");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageRenderingIntent = cast(mMagickGetImageRenderingIntent)dlsym(dlib, "MagickGetImageRenderingIntent");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageUnits = cast(mMagickGetImageUnits)dlsym(dlib, "MagickGetImageUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            DestroyMagickWand = cast(mDestroyMagickWand)dlsym(dlib, "DestroyMagickWand");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAdaptiveThresholdImage = cast(mMagickAdaptiveThresholdImage)dlsym(dlib, "MagickAdaptiveThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAddImage = cast(mMagickAddImage)dlsym(dlib, "MagickAddImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAddNoiseImage = cast(mMagickAddNoiseImage)dlsym(dlib, "MagickAddNoiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAffineTransformImage = cast(mMagickAffineTransformImage)dlsym(dlib, "MagickAffineTransformImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAnnotateImage = cast(mMagickAnnotateImage)dlsym(dlib, "MagickAnnotateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickAnimateImages = cast(mMagickAnimateImages)dlsym(dlib, "MagickAnimateImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickBlackThresholdImage = cast(mMagickBlackThresholdImage)dlsym(dlib, "MagickBlackThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickBlurImage = cast(mMagickBlurImage)dlsym(dlib, "MagickBlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickBorderImage = cast(mMagickBorderImage)dlsym(dlib, "MagickBorderImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCharcoalImage = cast(mMagickCharcoalImage)dlsym(dlib, "MagickCharcoalImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickChopImage = cast(mMagickChopImage)dlsym(dlib, "MagickChopImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickClipImage = cast(mMagickClipImage)dlsym(dlib, "MagickClipImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickClipPathImage = cast(mMagickClipPathImage)dlsym(dlib, "MagickClipPathImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickColorFloodfillImage = cast(mMagickColorFloodfillImage)dlsym(dlib, "MagickColorFloodfillImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickColorizeImage = cast(mMagickColorizeImage)dlsym(dlib, "MagickColorizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCommentImage = cast(mMagickCommentImage)dlsym(dlib, "MagickCommentImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCompositeImage = cast(mMagickCompositeImage)dlsym(dlib, "MagickCompositeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickContrastImage = cast(mMagickContrastImage)dlsym(dlib, "MagickContrastImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickConvolveImage = cast(mMagickConvolveImage)dlsym(dlib, "MagickConvolveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCropImage = cast(mMagickCropImage)dlsym(dlib, "MagickCropImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickCycleColormapImage = cast(mMagickCycleColormapImage)dlsym(dlib, "MagickCycleColormapImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDespeckleImage = cast(mMagickDespeckleImage)dlsym(dlib, "MagickDespeckleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDisplayImage = cast(mMagickDisplayImage)dlsym(dlib, "MagickDisplayImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDisplayImages = cast(mMagickDisplayImages)dlsym(dlib, "MagickDisplayImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickDrawImage = cast(mMagickDrawImage)dlsym(dlib, "MagickDrawImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickEdgeImage = cast(mMagickEdgeImage)dlsym(dlib, "MagickEdgeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickEmbossImage = cast(mMagickEmbossImage)dlsym(dlib, "MagickEmbossImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickEnhanceImage = cast(mMagickEnhanceImage)dlsym(dlib, "MagickEnhanceImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickEqualizeImage = cast(mMagickEqualizeImage)dlsym(dlib, "MagickEqualizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFlipImage = cast(mMagickFlipImage)dlsym(dlib, "MagickFlipImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFlopImage = cast(mMagickFlopImage)dlsym(dlib, "MagickFlopImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickFrameImage = cast(mMagickFrameImage)dlsym(dlib, "MagickFrameImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGammaImage = cast(mMagickGammaImage)dlsym(dlib, "MagickGammaImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGammaImageChannel = cast(mMagickGammaImageChannel)dlsym(dlib, "MagickGammaImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageBackgroundColor = cast(mMagickGetImageBackgroundColor)dlsym(dlib, "MagickGetImageBackgroundColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageBluePrimary = cast(mMagickGetImageBluePrimary)dlsym(dlib, "MagickGetImageBluePrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageBorderColor = cast(mMagickGetImageBorderColor)dlsym(dlib, "MagickGetImageBorderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageChannelExtrema = cast(mMagickGetImageChannelExtrema)dlsym(dlib, "MagickGetImageChannelExtrema");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageChannelMean = cast(mMagickGetImageChannelMean)dlsym(dlib, "MagickGetImageChannelMean");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageColormapColor = cast(mMagickGetImageColormapColor)dlsym(dlib, "MagickGetImageColormapColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageExtrema = cast(mMagickGetImageExtrema)dlsym(dlib, "MagickGetImageExtrema");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageGreenPrimary = cast(mMagickGetImageGreenPrimary)dlsym(dlib, "MagickGetImageGreenPrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageMatteColor = cast(mMagickGetImageMatteColor)dlsym(dlib, "MagickGetImageMatteColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImagePixels = cast(mMagickGetImagePixels)dlsym(dlib, "MagickGetImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageRedPrimary = cast(mMagickGetImageRedPrimary)dlsym(dlib, "MagickGetImageRedPrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageResolution = cast(mMagickGetImageResolution)dlsym(dlib, "MagickGetImageResolution");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageWhitePoint = cast(mMagickGetImageWhitePoint)dlsym(dlib, "MagickGetImageWhitePoint");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetSize = cast(mMagickGetSize)dlsym(dlib, "MagickGetSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickHasNextImage = cast(mMagickHasNextImage)dlsym(dlib, "MagickHasNextImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickHasPreviousImage = cast(mMagickHasPreviousImage)dlsym(dlib, "MagickHasPreviousImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickImplodeImage = cast(mMagickImplodeImage)dlsym(dlib, "MagickImplodeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickLabelImage = cast(mMagickLabelImage)dlsym(dlib, "MagickLabelImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickLevelImage = cast(mMagickLevelImage)dlsym(dlib, "MagickLevelImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickLevelImageChannel = cast(mMagickLevelImageChannel)dlsym(dlib, "MagickLevelImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMagnifyImage = cast(mMagickMagnifyImage)dlsym(dlib, "MagickMagnifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMapImage = cast(mMagickMapImage)dlsym(dlib, "MagickMapImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMatteFloodfillImage = cast(mMagickMatteFloodfillImage)dlsym(dlib, "MagickMatteFloodfillImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMedianFilterImage = cast(mMagickMedianFilterImage)dlsym(dlib, "MagickMedianFilterImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMinifyImage = cast(mMagickMinifyImage)dlsym(dlib, "MagickMinifyImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickModulateImage = cast(mMagickModulateImage)dlsym(dlib, "MagickModulateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickMotionBlurImage = cast(mMagickMotionBlurImage)dlsym(dlib, "MagickMotionBlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickNegateImage = cast(mMagickNegateImage)dlsym(dlib, "MagickNegateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickNegateImageChannel = cast(mMagickNegateImageChannel)dlsym(dlib, "MagickNegateImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickNextImage = cast(mMagickNextImage)dlsym(dlib, "MagickNextImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickNormalizeImage = cast(mMagickNormalizeImage)dlsym(dlib, "MagickNormalizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickOilPaintImage = cast(mMagickOilPaintImage)dlsym(dlib, "MagickOilPaintImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickOpaqueImage = cast(mMagickOpaqueImage)dlsym(dlib, "MagickOpaqueImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickPingImage = cast(mMagickPingImage)dlsym(dlib, "MagickPingImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickPreviousImage = cast(mMagickPreviousImage)dlsym(dlib, "MagickPreviousImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickProfileImage = cast(mMagickProfileImage)dlsym(dlib, "MagickProfileImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickQuantizeImage = cast(mMagickQuantizeImage)dlsym(dlib, "MagickQuantizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickQuantizeImages = cast(mMagickQuantizeImages)dlsym(dlib, "MagickQuantizeImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRadialBlurImage = cast(mMagickRadialBlurImage)dlsym(dlib, "MagickRadialBlurImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRaiseImage = cast(mMagickRaiseImage)dlsym(dlib, "MagickRaiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickReadImage = cast(mMagickReadImage)dlsym(dlib, "MagickReadImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickReadImageBlob = cast(mMagickReadImageBlob)dlsym(dlib, "MagickReadImageBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickReadImageFile = cast(mMagickReadImageFile)dlsym(dlib, "MagickReadImageFile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickReduceNoiseImage = cast(mMagickReduceNoiseImage)dlsym(dlib, "MagickReduceNoiseImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRelinquishMemory = cast(mMagickRelinquishMemory)dlsym(dlib, "MagickRelinquishMemory");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRemoveImage = cast(mMagickRemoveImage)dlsym(dlib, "MagickRemoveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickResampleImage = cast(mMagickResampleImage)dlsym(dlib, "MagickResampleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickResizeImage = cast(mMagickResizeImage)dlsym(dlib, "MagickResizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRollImage = cast(mMagickRollImage)dlsym(dlib, "MagickRollImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRotateImage = cast(mMagickRotateImage)dlsym(dlib, "MagickRotateImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSampleImage = cast(mMagickSampleImage)dlsym(dlib, "MagickSampleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickScaleImage = cast(mMagickScaleImage)dlsym(dlib, "MagickScaleImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSeparateImageChannel = cast(mMagickSeparateImageChannel)dlsym(dlib, "MagickSeparateImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImage = cast(mMagickSetImage)dlsym(dlib, "MagickSetImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetFilename = cast(mMagickSetFilename)dlsym(dlib, "MagickSetFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageBackgroundColor =
                cast(mMagickSetImageBackgroundColor)dlsym(dlib, "MagickSetImageBackgroundColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageBluePrimary = cast(mMagickSetImageBluePrimary)dlsym(dlib, "MagickSetImageBluePrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageBorderColor = cast(mMagickSetImageBorderColor)dlsym(dlib, "MagickSetImageBorderColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageChannelDepth = cast(mMagickSetImageChannelDepth)dlsym(dlib, "MagickSetImageChannelDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageColormapColor = cast(mMagickSetImageColormapColor)dlsym(dlib, "MagickSetImageColormapColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageCompose = cast(mMagickSetImageCompose)dlsym(dlib, "MagickSetImageCompose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageCompression = cast(mMagickSetImageCompression)dlsym(dlib, "MagickSetImageCompression");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageDelay = cast(mMagickSetImageDelay)dlsym(dlib, "MagickSetImageDelay");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageDepth = cast(mMagickSetImageDepth)dlsym(dlib, "MagickSetImageDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageDispose = cast(mMagickSetImageDispose)dlsym(dlib, "MagickSetImageDispose");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageColorspace = cast(mMagickSetImageColorspace)dlsym(dlib, "MagickSetImageColorspace");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageGreenPrimary = cast(mMagickSetImageGreenPrimary)dlsym(dlib, "MagickSetImageGreenPrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageGamma = cast(mMagickSetImageGamma)dlsym(dlib, "MagickSetImageGamma");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageFilename = cast(mMagickSetImageFilename)dlsym(dlib, "MagickSetImageFilename");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageFormat = cast(mMagickSetImageFormat)dlsym(dlib, "MagickSetImageFormat");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageIndex = cast(mMagickSetImageIndex)dlsym(dlib, "MagickSetImageIndex");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageInterlaceScheme =
                cast(mMagickSetImageInterlaceScheme)dlsym(dlib, "MagickSetImageInterlaceScheme");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageIterations = cast(mMagickSetImageIterations)dlsym(dlib, "MagickSetImageIterations");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageMatteColor = cast(mMagickSetImageMatteColor)dlsym(dlib, "MagickSetImageMatteColor");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageOption = cast(mMagickSetImageOption)dlsym(dlib, "MagickSetImageOption");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImagePixels = cast(mMagickSetImagePixels)dlsym(dlib, "MagickSetImagePixels");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageRedPrimary = cast(mMagickSetImageRedPrimary)dlsym(dlib, "MagickSetImageRedPrimary");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageRenderingIntent =
                cast(mMagickSetImageRenderingIntent)dlsym(dlib, "MagickSetImageRenderingIntent");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageResolution = cast(mMagickSetImageResolution)dlsym(dlib, "MagickSetImageResolution");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageScene = cast(mMagickSetImageScene)dlsym(dlib, "MagickSetImageScene");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageType = cast(mMagickSetImageType)dlsym(dlib, "MagickSetImageType");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageUnits = cast(mMagickSetImageUnits)dlsym(dlib, "MagickSetImageUnits");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageVirtualPixelMethod =
                cast(mMagickSetImageVirtualPixelMethod)dlsym(dlib, "MagickSetImageVirtualPixelMethod");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetPassphrase = cast(mMagickSetPassphrase)dlsym(dlib, "MagickSetPassphrase");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageProfile = cast(mMagickSetImageProfile)dlsym(dlib, "MagickSetImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetResourceLimit = cast(mMagickSetResourceLimit)dlsym(dlib, "MagickSetResourceLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetSamplingFactors = cast(mMagickSetSamplingFactors)dlsym(dlib, "MagickSetSamplingFactors");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetSize = cast(mMagickSetSize)dlsym(dlib, "MagickSetSize");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetImageWhitePoint = cast(mMagickSetImageWhitePoint)dlsym(dlib, "MagickSetImageWhitePoint");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSetInterlaceScheme = cast(mMagickSetInterlaceScheme)dlsym(dlib, "MagickSetInterlaceScheme");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSharpenImage = cast(mMagickSharpenImage)dlsym(dlib, "MagickSharpenImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickShaveImage = cast(mMagickShaveImage)dlsym(dlib, "MagickShaveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickShearImage = cast(mMagickShearImage)dlsym(dlib, "MagickShearImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSolarizeImage = cast(mMagickSolarizeImage)dlsym(dlib, "MagickSolarizeImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSpreadImage = cast(mMagickSpreadImage)dlsym(dlib, "MagickSpreadImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickStripImage = cast(mMagickStripImage)dlsym(dlib, "MagickStripImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickSwirlImage = cast(mMagickSwirlImage)dlsym(dlib, "MagickSwirlImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickTintImage = cast(mMagickTintImage)dlsym(dlib, "MagickTintImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickThresholdImage = cast(mMagickThresholdImage)dlsym(dlib, "MagickThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickThresholdImageChannel = cast(mMagickThresholdImageChannel)dlsym(dlib, "MagickThresholdImageChannel");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickTransparentImage = cast(mMagickTransparentImage)dlsym(dlib, "MagickTransparentImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickTrimImage = cast(mMagickTrimImage)dlsym(dlib, "MagickTrimImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickUnsharpMaskImage = cast(mMagickUnsharpMaskImage)dlsym(dlib, "MagickUnsharpMaskImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWaveImage = cast(mMagickWaveImage)dlsym(dlib, "MagickWaveImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWhiteThresholdImage = cast(mMagickWhiteThresholdImage)dlsym(dlib, "MagickWhiteThresholdImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWriteImage = cast(mMagickWriteImage)dlsym(dlib, "MagickWriteImage");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWriteImageFile = cast(mMagickWriteImageFile)dlsym(dlib, "MagickWriteImageFile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWriteImages = cast(mMagickWriteImages)dlsym(dlib, "MagickWriteImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageColors = cast(mMagickGetImageColors)dlsym(dlib, "MagickGetImageColors");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageDelay = cast(mMagickGetImageDelay)dlsym(dlib, "MagickGetImageDelay");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageChannelDepth = cast(mMagickGetImageChannelDepth)dlsym(dlib, "MagickGetImageChannelDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageDepth = cast(mMagickGetImageDepth)dlsym(dlib, "MagickGetImageDepth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageHeight = cast(mMagickGetImageHeight)dlsym(dlib, "MagickGetImageHeight");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageIterations = cast(mMagickGetImageIterations)dlsym(dlib, "MagickGetImageIterations");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageScene = cast(mMagickGetImageScene)dlsym(dlib, "MagickGetImageScene");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetImageWidth = cast(mMagickGetImageWidth)dlsym(dlib, "MagickGetImageWidth");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetNumberImages = cast(mMagickGetNumberImages)dlsym(dlib, "MagickGetNumberImages");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickGetResourceLimit = cast(mMagickGetResourceLimit)dlsym(dlib, "MagickGetResourceLimit");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageVirtualPixelMethod =
                cast(mMagickGetImageVirtualPixelMethod)dlsym(dlib, "MagickGetImageVirtualPixelMethod");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickGetImageProfile = cast(mMagickGetImageProfile)dlsym(dlib, "MagickGetImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickRemoveImageProfile = cast(mMagickRemoveImageProfile)dlsym(dlib, "MagickRemoveImageProfile");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }
            MagickWriteImageBlob = cast(mMagickWriteImageBlob)dlsym(dlib, "MagickWriteImageBlob");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            MagickResetIterator = cast(mMagickResetIterator)dlsym(dlib, "MagickResetIterator");
            if (null !is (errmsg = dlerror())) {
                debug(graphicsmagick_c)
                {
                    fprintf(stderr, "ERROR graphicsmagick_c.wand.magick_wand:\n  %s\n", errmsg);
                }
                success = false;
            }

            return success;
        }
    }
}
