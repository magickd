/*
  File:           graphicsmagick_c/config.d
  Contains:       Configurations for compiling the C wrapper.
  Copyright:      (C) 2021 - 2023 kaerou <stigma@disroot.org>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
*/
module graphicsmagick_c.config;

version (GMagick_Static) {}
else
{
    version (Posix)
    {
        import core.sys.posix.dlfcn;
    }

    /**
     * Dynamically load the GraphicsMagick library.
     *
     * Params:
     *   dlib = The dynamic library handle as returned by dlopen().  You can use this to close the
     *          library when not needed, or to load your own functions from GraphicsMagick.
     *
     * Returns:
     *   This function returns `true` if it executed without any errors occurring, otherwise, it returns
     *   `false`.
     */
    bool loadGraphicsMagick(out void* dlib)
    {
        import graphicsmagick_c.magick;

        bool success = true;

        enum MaxLibNames = 3;
        immutable(char)[][MaxLibNames] libNames = [
            "libGraphicsMagick.so",
            "libGraphicsMagick.dylib",   // macOS
            ""
        ];

        version (Q8)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagick-Q8.so";
        }
        else version (Q16)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagick-Q16.so";
        }
        else version (Q32)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagick-Q32.so";
        }

        dlib = null;

        foreach (name; libNames)
        {
            debug(graphicsmagick_c)
            {
                import core.stdc.stdio : fprintf, stderr;
                fprintf(stderr, "INFO: Attempting to load %s\n", name.ptr);
            }
            dlib = dlopen (name.ptr, RTLD_LAZY);
            if (null !is dlib)
            {
                break;
            }
        }

        if (null is dlib)
        {
            debug(graphicsmagick_c)
            {
                import core.stdc.stdio : fprintf, stderr;
                fprintf(stderr, "CRIT: graphicsmagick_c.config.loadGraphicsMagick:\n  %s\n", dlerror());
            }
            return false;
        }

        /* Clear errors */
        dlerror();

        /* <magick/version.h> */
        success &= _loadVersionH (dlib);

        /* <magick/colorspace.h> */
        success &= _loadColorspaceH (dlib);

        /* <magick/error.h> */
        success &= _loadErrorH (dlib);

        /* <magick/image.h> */
        success &= _loadImageH (dlib);

        /* <magick/magick.h> */
        success &= _loadMagickH (dlib);

        /* <magick/list.h> */
        success &= _loadListH (dlib);

        /* <magick/constitute.h> */
        success &= _loadConstituteH (dlib);

        /* <magick/resize.h> */
        success &= _loadResizeH (dlib);

        /* <magick/timer.h> */
        success &= _loadTimerH (dlib);

        /* <magick/magic.h> */
        success &= _loadMagicH (dlib);

        /* <magick/log.h> */
        success &= _loadLogH (dlib);

        /* <magick/resource.h> */
        success &= _loadResourceH (dlib);

        /* <magick/channel.h> */
        success &= _loadChannelH (dlib);

        /* <magick/render.h> */
        success &= _loadRenderH (dlib);

        /* <magick/paint.h> */
        success &= _loadPaintH (dlib);

        /* <magick/profile.h> */
        success &= _loadProfileH (dlib);

        /* <magick/draw.h> */
        success &= _loadDrawH (dlib);

        /* <magick/gem.h> */
        success &= _loadGemH (dlib);

        /* <magick/quantize.h> */
        success &= _loadQuantizeH (dlib);

        /* <magick/compress.h> */
        success &= _loadCompressH (dlib);

        /* <magick/attribute.h> */
        success &= _loadAttributeH (dlib);

        /* <magick/command.h> */
        success &= _loadCommandH (dlib);

        /* <magick/utility.h> */
        success &= _loadUtilityH (dlib);

        /* <magick/signature.h> */
        success &= _loadSignatureH (dlib,);

        /* <magick/blob.h> */
        success &= _loadBlobH (dlib);

        /* <magick/color.h> */
        success &= _loadColorH (dlib);

        /* <magick/decorate.h> */
        success &= _loadDecorateH (dlib);

        /* <magick/enhance.h> */
        success &= _loadEnhanceH (dlib);

        /* <magick/fx.h> */
        success &= _loadFxH (dlib);

        /* <magick/memory.h> */
        success &= _loadMemoryH (dlib);

        /* <magick/montage.h> */
        success &= _loadMontageH (dlib);

        /* <magick/effect.h> */
        success &= _loadEffectH (dlib);

        /* <magick/shear.h> */
        success &= _loadShearH (dlib);

        /* <magick/transform.h> */
        success &= _loadTransformH (dlib);

        /* <magick/composite.h> */
        success &= _loadCompositeH (dlib);

        /* <magick/registry.h> */
        success &= _loadRegistryH (dlib);

        /* <magick/delegate.h> */
        success &= _loadDelegateH (dlib);

        /* <magick/module.h> */
        success &= _loadModuleH (dlib);

        /* <magick/monitor.h> */
        success &= _loadMonitorH (dlib);

        /* <magick/operator.h> */
        success &= _loadOperatorH (dlib);

        /* <magick/pixel_cache.h> */
        success &= _loadPixelCacheH (dlib);

        /* <magick/pixel_iterator.h> */
        success &= _loadPixelIteratorH (dlib);

        /* <magick/deprecate.h> */
        success &= _loadDeprecateH (dlib);

        return success;
    }

    /**
     * Dynamically load the GraphicsMagickWand library.
     *
     * Params:
     *   dlib = The dynamic library handle as returned by dlopen().  You can use this to close the
     *          library when not needed, or to load your own functions from GraphicsMagickWand.
     *
     * Returns:
     *   This will return `true` if the library loaded without warnings/errors.
     */
    bool loadGraphicsMagickWand(out void* dlib)
    {
        import graphicsmagick_c.wand.wand_api;

        bool success = true;

        enum MaxLibNames = 3;
        immutable(char)[][MaxLibNames] libNames = [
            "libGraphicsMagickWand.so",
            "libGraphicsMagickWand.dylib", // macOS
            ""
        ];

        version (Q8)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagickWand-Q8.so";
        }
        else version (Q16)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagickWand-Q16.so";
        }
        else version (Q32)
        {
            libNames[MaxLibNames - 1] = "libGraphicsMagickWand-Q32.so";
        }

        dlib = null;

        foreach (name; libNames)
        {
            debug(graphicsmagick_c)
            {
                import core.stdc.stdio : fprintf, stderr;
                fprintf(stderr, "INFO: Attempting to load %s\n", name.ptr);
            }
            dlib = dlopen (name.ptr, RTLD_LAZY);
            if (null !is dlib)
            {
                break;
            }
        }

        if (null is dlib)
        {
            debug(graphicsmagick_c)
            {
                import core.stdc.stdio : fprintf, stderr;
                fprintf(stderr, "CRIT: graphicsmagick_c.config.loadGraphicsMagickWand:\n  %s\n", dlerror());
            }
            return false;
        }

        /* Clear errors */
        dlerror();

        /* <wand/drawing_wand.h> */
        success &= _loadDrawingWandH(dlib);

        /* <wand/pixel_wand.h> */
        success &= _loadPixelWandH(dlib);

        /* <wand/magick_wand.h> */
        success &= _loadMagickWandH(dlib);

        return success;
    }
}
